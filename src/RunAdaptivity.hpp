/** \file RunAdaptivity.hpp
 * \ingroup solid_shell_prism_element
 * \brief Implementation of solid shell prism element
 *
 */

/*
 * This file is part of MoFEM.
 * MoFEM is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * MoFEM is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with MoFEM. If not, see <http://www.gnu.org/licenses/>. */

#ifndef __RUNADAPTIVITY_HPP__
#define __RUNADAPTIVITY_HPP__

namespace SolidShellModule {

struct RunAdaptivity {

  MoFEM::Interface &mField;
  DM dM;
  SolidShellPrismElement::CommonData &commonData;

  FEMethod &shellLhs;
  FEMethod &shellError;
  FEMethod &dirichletBc;
  FEMethod &fixEnts;

  PostProcFatPrismOnRefinedMesh postProcFatPrism;

  RunAdaptivity(
    MoFEM::Interface &m_field,
    DM dm,
    SolidShellPrismElement::CommonData &common_data,
    FEMethod &shell_lhs,
    FEMethod &shell_error,
    FEMethod &dirichlet_bc,
    FEMethod &fix_ents
  ):
  mField(m_field),
  dM(dm),
  commonData(common_data),
  shellLhs(shell_lhs),
  shellError(shell_error),
  dirichletBc(dirichlet_bc),
  fixEnts(fix_ents),
  postProcFatPrism(mField) {
  }

  
  
  Tag thOrder;

  vector<IS> isLevels;
  vector<IS> mgLevels;
  vector<Vec> solutionAtLevel;

  KSP sOlver;
  PC pC;

  /**
   * Multi grid pre-conditioner
   */
  struct ShellPCMGSetUpViaApproxOrdersCtx: public PCMGSetUpViaApproxOrdersCtx {
    vector<IS> &mgLevels;
    AO aO;
    ShellPCMGSetUpViaApproxOrdersCtx(DM dm,Mat a,vector<IS> &mg_levels):
    PCMGSetUpViaApproxOrdersCtx(dm,a,false),
    mgLevels(mg_levels) {
    }

    PetscErrorCode createIsAtLevel(int kk,IS *is);

    PetscErrorCode destroyIsAtLevel(int kk,IS *is);

    PetscErrorCode getOptions();

    PetscErrorCode buildProlongationOperator(PC pc,int verb = 0);

  };

  /**
   * \brief Set order on entities
   */
  struct SetOrderToEntities: public FEMethod {
    MoFEM::Interface &mField;
    Tag thOrder;

    SetOrderToEntities(
      MoFEM::Interface &m_field,Tag th_order
    ):
    mField(m_field),
    thOrder(th_order) {
    }

    
    

    PetscErrorCode preProcess();

    PetscErrorCode postProcess();

    /**
     * Set order on element tags
     * @return error code
     */
    PetscErrorCode operator()();

  };

  /**
   * \brief Set order of finite element
   */
  struct SetOrderToElement: public FEMethod {
    MoFEM::Interface &mField;
    Tag thOrder;
    int sEt,uP;
    double eRror;
    int maxOrder;
    SetOrderToElement(
      MoFEM::Interface &m_field,
      Tag th_order,int set,int up,double error,int max_order
    ):
    mField(m_field),
    thOrder(th_order),
    sEt(set),
    uP(up),
    eRror(error),
    maxOrder(max_order) {
    }

    Tag thError;
    
    


    PetscErrorCode preProcess();

    PetscErrorCode postProcess();

    /**
     * Set order on element tags
     * @return error code
     */
    PetscErrorCode operator()();

  };

  /**
   * \brief Set approximation order to entities
   * @param  set   set value, if 0 not set
   * @param  up    increase  order by value
   * @param  error increase order base on error estimator
   * @param  max_order     maximal allowed order
   * @return       error code
   */
  PetscErrorCode setOrder(int set,int up,double error,int max_order,int verb = 0);

  vector<int> vecOrderDofs;
  PetscErrorCode createIS(IS *is);

  PetscErrorCode runKsp(DM dm,Mat Aij,Vec F,Vec D);

  PetscErrorCode runKspAtLevel(DM dm,Mat Aij,Vec F,Vec D,bool add_level);

  double procentErrorMax; ///< max value at given precent

  PetscErrorCode sortErrorVector(const double procent = 0.33);

  /**
   * \brief Solve linear problem with p-adaptivity
   * @param  Aij           Matrix
   * @param  F             The right hand side vector
   * @param  D             Unknown vector
   * @param  nb_ref_cycles Nb. of refinement cycles. Default 2*(order-2)
   * @param  max_order     maximal allowed order
   * @param  procent      set precent of refined elements, def 33%
   * @param  verbose level     maximal allowed order
   * @return               [description]
   */
  PetscErrorCode solveProblem(DM dm,Mat Aij,Vec F,Vec D,int nb_ref_cycles,int max_order,double procent = 0.33,int verb = 0);

  /**
   * \brief Set up operators for post-processing shell elements
   * @return error code
   */
  PetscErrorCode setUpOperators();

  /**
   * \brief Save post-processed data
   * @param  name file name
   * @return      error code
   */
  PetscErrorCode postProcFatPrims(const string &name);

  /**
   * \brief Print displacements at selected points
   *
   * List of points for printing is in RESULTS block set
   *
   * @return error code
   */
  PetscErrorCode printDisplacements();

};

}

#endif // __RUNADAPTIVITY_HPP__
