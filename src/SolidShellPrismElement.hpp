/** \file SolidShellPrismElement.hpp
 * \ingroup solid_shell_prism_element
 * \brief Implementation of solid shell prism element
 *
 */

/*
 * This file is part of MoFEM.
 * MoFEM is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * MoFEM is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with MoFEM. If not, see <http://www.gnu.org/licenses/>. */

#ifndef __SOLIDSHELLPRISMELEMENT_HPP__
#define __SOLIDSHELLPRISMELEMENT_HPP__

namespace SolidShellModule {

/** \brief Main class for solid shell element
  \ingroup solid_shell_prism_element
*/
struct SolidShellPrismElement {

  /** \brief Common data for solid shell element
  \ingroup solid_shell_prism_element
  */
  struct CommonData {

    string meshNodalPosition;     ///< Name of field of positions in reference configuration
    string displacementFieldName; ///< Name of displacement field
    string zetaFieldName;         ///< Name of zeta field
    string ksiEtaFieldName;       ///< In plane HO approximation
    string directorFieldName;
    string coVectorFieldName;

    vector<ublas::matrix<double,ublas::column_major> > jacobianInvAtGaussPts;  ///< For element local to global
    MatrixDouble positionsAtGaussPts;       ///< Reference gauss points coordinates
    MatrixDouble directorVectorAtGaussPts;          ///< Approximation of director vector at integration Pts
    vector<MatrixDouble> diffDirectorVectorAtGaussPts;      ///< Director vector derivatives at integration pts
    MatrixDouble tangent0AtGaussPts;          ///< Approximation of tangent 0 vector at integration Pts
    MatrixDouble tangent1AtGaussPts;          ///< Approximation of tangent 1 vector at integration Pts
    MatrixDouble localDisplacementsAtGaussPts; ///< Local displacement at integration Pts
    MatrixDouble globalDisplacentAtGaussPts; ///< Global displacement at integration ptrs
    VectorDouble jacDeterminantAtGaussPts;  ///< Determinant of jacobian
    map<EntityType,map<int,MatrixDouble> > diffGlobN;

    MatrixDouble sTrain;  ///< Strain in Voigt (https://en.wikipedia.org/wiki/Voigt_notation) notation (User coord. sys.)
    MatrixDouble sTress;  ///< Stress in Voigt (https://en.wikipedia.org/wiki/Voigt_notation) notation (User coors. sys.)
    MatrixDouble materialStiffness;   ///< Material stiffness matrix in Voigt notation

    double tHickness;  ///< Shell thickness
    double sHift;      ///< Shift elements in normal direction
    VectorDouble surfaceParametrisation;    ///< User direction of surface parametrization
    // vector<MatrixDouble> rOtationUser;      ///< Rotate from local user coord. sys. to global
    vector<ublas::symmetric_matrix<double,ublas::lower> > localStress;   ///< Stress in local user coordinate system (Tensorial Notation)

    vector<MatrixDouble> mGrad1,mGrad2;  ///< Mid-surface gradient of deformation

    //Note: "_" before index indicate lower index, if no "_" it is upper index
    vector<MatrixDouble3by3> mE_I_A,mEAI,mGAB,mG_A_B;
    vector<MatrixDouble3by3> globalPiolaStressIi_I;
    vector<MatrixDouble3by3> localPiolaStressIA_a;
    vector<MatrixDouble3by3> globalPiolaStressIiBTangent;
    vector<MatrixDouble3by3> localPiolaStressIaBTangent;

    vector<MatrixDouble> tangentOfPiolaStressGlobalGlobal;
    vector<MatrixDouble> tangentOfPiolaStressLocalLocal;
    vector<MatrixDouble> tangentOfPiolaStressLocalGlobal;
    vector<MatrixDouble> tangentOfPiolaStressGlobalLocal;

    //CoVectors
    map<EntityHandle,int> shellElementsMap;
    map<int,map<UId,double> > coVector0DofsMap;
    map<int,map<UId,double> > coVector1DofsMap;

    // This is to evaluate error
    Vec vecErrorOnElements;
    bool saveOnTag;

    #ifdef ADOLC_ADOLC_H

    template<typename TYPE>
    struct ADouble {
      TYPE dEt,tR;
      //Acrive Variables
      ublas::matrix<TYPE,ublas::row_major,ublas::bounded_array<TYPE,9> > mGrad1;
      ublas::matrix<TYPE,ublas::row_major,ublas::bounded_array<TYPE,9> > mGrad2;

      ublas::matrix<TYPE,ublas::row_major,ublas::bounded_array<TYPE,9> > mMF,mF;
      ublas::matrix<TYPE,ublas::row_major,ublas::bounded_array<TYPE,9> > gMab,gM_a_b;
      ublas::matrix<TYPE,ublas::row_major,ublas::bounded_array<TYPE,9> > eM_i_a,eMai;

      ublas::matrix<TYPE,ublas::row_major,ublas::bounded_array<TYPE,9> > mCA_B;
      ublas::matrix<TYPE,ublas::row_major,ublas::bounded_array<TYPE,9> > mEA_B;
      ublas::matrix<TYPE,ublas::row_major,ublas::bounded_array<TYPE,9> > mCAB;
      ublas::matrix<TYPE,ublas::row_major,ublas::bounded_array<TYPE,9> > mEAB;

      ublas::matrix<TYPE,ublas::row_major,ublas::bounded_array<TYPE,9> > piolaStressIIA_B;
      ublas::matrix<TYPE,ublas::row_major,ublas::bounded_array<TYPE,9> > globalPiolaStressIi_I;
      ublas::matrix<TYPE,ublas::row_major,ublas::bounded_array<TYPE,9> > localPiolaStressIA_a;
      ublas::matrix<TYPE,ublas::row_major,ublas::bounded_array<TYPE,9> > localPiolaStressIa_A;

      TYPE t0,t1,t2,t3,t4;
    };

    ADouble<adouble> aDouble;
    ADouble<double> dDouble;

    #endif //ADOLC_ADOLC_H


    #ifdef __ARC_LENGTH_TOOLS_HPP__
    vector<VectorDouble3> incrementsOfDisplacements;
    // vector<MatrixDouble3> incrementsOfGradOfDisplacements;

    #endif

    CommonData() {
    }

  };

  struct SolidShell: public MoFEM::FatPrismElementForcesAndSourcesCore {

    CommonData &commonData;
    SolidShell(MoFEM::Interface &m_field,CommonData &common_data):
    MoFEM::FatPrismElementForcesAndSourcesCore(m_field),
    commonData(common_data) {
    }
    int getRuleTrianglesOnly(int order) {
      Tag th;
      
      rval = mField.get_moab().tag_get_handle("ADAPT_ORDER",th);
      if(rval == MB_SUCCESS) {
        order = 2;
        int last_ent = 0;
        for(
          _IT_FENUMEREDDOF_BY_NAME_ROW_FOR_LOOP_(numeredEntFiniteElementPtr,"DISPLACEMENT",dof)
        ) {
          EntityHandle ent = dof->get()->getEnt();
          if(last_ent == ent) continue;
          last_ent = ent;
          int ent_order;
          rval = mField.get_moab().tag_get_data(th,&ent,1,&ent_order); MOAB_THROW(rval);
          order = (order>ent_order) ? order : ent_order;
        }
      }
      return 2*(order+1)+1;
    };
    int getRuleThroughThickness(int order) {
      int rule = 2*order+1;
      return (rule <= 1) ? 2 : rule;
    }
    PetscErrorCode preProcess();
    PetscErrorCode postProcess();

  };

  struct SolidShellError: public SolidShell {
    SolidShellError(MoFEM::Interface &m_field,CommonData &common_data):
    SolidShell(m_field,common_data) {
    }

    PetscErrorCode preProcess();
    PetscErrorCode postProcess();

  };

  /** \brief Calculate strain element operators
  \ingroup solid_shell_prism_element
  */
  struct MakeB {
    /** \brief Calculate strain operator for displacements DoFs

    Note: Displacements DoFs are in global coordinate system

    */
    PetscErrorCode makeBDisp(
      const MatrixAdaptor &diffN,const MatrixDouble &T,MatrixDouble &B
    );

    /** \brief Calculate strain operator for displacement thought shell thickness

    Note: Zeta displacements are in local coordinate system

    */
    PetscErrorCode makeBZeta(
      const MatrixAdaptor &diffN,MatrixDouble &B
    );

    /** \brief Calculate strain operator for non-plannar displacements
    through shell thickness

    Note: Ksi and Eta displacements are in local coordinate system

    */
    PetscErrorCode makeBKsiEta(
      const MatrixAdaptor &diffN,MatrixDouble &B
    );

    /** \brief Gradient of deformation

    Values of tensor are arranged in columns (row order)

    */
    PetscErrorCode makeFDisp(
       const MatrixAdaptor &diffN,MatrixDouble &F
    );

    PetscErrorCode makeFZeta(
       const MatrixAdaptor &diffN,MatrixDouble &F
    );

    PetscErrorCode makeFKsiEta(
       const MatrixAdaptor &diffN,MatrixDouble &F
    );

    template<typename TYPE>
    PetscErrorCode dEterminatnt(
      ublas::matrix<TYPE,ublas::row_major,ublas::bounded_array<TYPE,9> >& a,
      TYPE &det
    ) {
      PetscFunctionBegin;
      // a11a22a33
      //+a21a32a13
      //+a31a12a23
      //-a11a32a23
      //-a31a22a13
      //-a21a12a33
      //http://www.cg.info.hiroshima-cu.ac.jp/~miyazaki/knowledge/teche23.html
      //http://mathworld.wolfram.com/MatrixInverse.html
      det = a(0,0)*a(1,1)*a(2,2)
        +a(1,0)*a(2,1)*a(0,2)
        +a(2,0)*a(0,1)*a(1,2)
        -a(0,0)*a(2,1)*a(1,2)
        -a(2,0)*a(1,1)*a(0,2)
        -a(1,0)*a(0,1)*a(2,2);
      PetscFunctionReturn(0);
    }

    /** \brief Calculate inverse of 3x3 matrix
      */
    template<typename TYPE>
    PetscErrorCode iNvert(
      TYPE det,
      ublas::matrix<TYPE,ublas::row_major,ublas::bounded_array<TYPE,9> >& a,
      ublas::matrix<TYPE,ublas::row_major,ublas::bounded_array<TYPE,9> >& inv_a
    ) {
      PetscFunctionBegin;
      //
      inv_a.resize(3,3,false);
      //http://www.cg.info.hiroshima-cu.ac.jp/~miyazaki/knowledge/teche23.html
      //http://mathworld.wolfram.com/MatrixInverse.html
      inv_a(0,0) = a(1,1)*a(2,2)-a(1,2)*a(2,1);
      inv_a(0,1) = a(0,2)*a(2,1)-a(0,1)*a(2,2);
      inv_a(0,2) = a(0,1)*a(1,2)-a(0,2)*a(1,1);
      inv_a(1,0) = a(1,2)*a(2,0)-a(1,0)*a(2,2);
      inv_a(1,1) = a(0,0)*a(2,2)-a(0,2)*a(2,0);
      inv_a(1,2) = a(0,2)*a(1,0)-a(0,0)*a(1,2);
      inv_a(2,0) = a(1,0)*a(2,1)-a(1,1)*a(2,0);
      inv_a(2,1) = a(0,1)*a(2,0)-a(0,0)*a(2,1);
      inv_a(2,2) = a(0,0)*a(1,1)-a(0,1)*a(1,0);
      inv_a /= det;
      PetscFunctionReturn(0);
    }

  };

  /** \brief Calculate displacements at integration points
  \ingroup solid_shell_prism_element
  */
  struct OpGetDisplacements: public MoFEM::FatPrismElementForcesAndSourcesCore::UserDataOperator {
    CommonData &commonData;
    OpGetDisplacements(CommonData &common_data):
    MoFEM::FatPrismElementForcesAndSourcesCore::UserDataOperator(
      common_data.displacementFieldName,UserDataOperator::OPROW
    ),
    commonData(common_data) {}
    PetscErrorCode doWork(
      int side,EntityType type,DataForcesAndSourcesCore::EntData &data
    );
  };

  struct OpGetDirectorVector: public MoFEM::FatPrismElementForcesAndSourcesCore::UserDataOperator {
    CommonData &commonData;
    OpGetDirectorVector(CommonData &common_data):
    MoFEM::FatPrismElementForcesAndSourcesCore::UserDataOperator(
      common_data.directorFieldName,UserDataOperator::OPROW
    ),
    commonData(common_data) {}
    PetscErrorCode doWork(
      int side,EntityType type,DataForcesAndSourcesCore::EntData &data
    );
  };

  struct OpGetCoVectors: public MoFEM::FatPrismElementForcesAndSourcesCore::UserDataOperator {
    CommonData &commonData;
    VectorDouble coVector0Data;
    VectorDouble coVector1Data;
    OpGetCoVectors(CommonData &common_data):
    MoFEM::FatPrismElementForcesAndSourcesCore::UserDataOperator(
      common_data.coVectorFieldName,UserDataOperator::OPROW
    ),
    commonData(common_data) {}
    PetscErrorCode doWork(
      int side,EntityType type,DataForcesAndSourcesCore::EntData &data
    );
  };

  /** \brief Calculate rotation matrices to local (user) element coordinate system
  \ingroup solid_shell_prism_element

  Material physical equation is defined and evaluated in local element
  coordinate system.

  */
  struct OpGetReferenceBase: public MoFEM::FatPrismElementForcesAndSourcesCore::UserDataOperator,MakeB {
    CommonData &commonData;
    OpGetReferenceBase(CommonData &common_data):
    MoFEM::FatPrismElementForcesAndSourcesCore::UserDataOperator(
      common_data.meshNodalPosition,UserDataOperator::OPROW
    ),
    commonData(common_data) {}
    PetscErrorCode doWork(
      int side,EntityType type,DataForcesAndSourcesCore::EntData &data
    );
  };

  /** \brief Calculate jacobian
  \ingroup solid_shell_prism_element
  */
  struct OpJacobioan: public MoFEM::FatPrismElementForcesAndSourcesCore::UserDataOperator {
    CommonData &commonData;
    OpJacobioan(CommonData &common_data):
    MoFEM::FatPrismElementForcesAndSourcesCore::UserDataOperator(
      common_data.meshNodalPosition,UserDataOperator::OPROW
    ),
    commonData(common_data) {}
    PetscErrorCode doWork(
      int side,EntityType type,DataForcesAndSourcesCore::EntData &data
    );
  };

  /** \brief Calculate inverse of Jacobian
  \ingroup solid_shell_prism_element

  In addition all derivatives of shape functions are transformed to
  local element coordinate system

  */
  struct OpInvJacobian: public MoFEM::FatPrismElementForcesAndSourcesCore::UserDataOperator {
    CommonData &commonData;
    OpInvJacobian(CommonData &common_data):
    MoFEM::FatPrismElementForcesAndSourcesCore::UserDataOperator(
      common_data.displacementFieldName,UserDataOperator::OPROW
    ),
    commonData(common_data) {}
    PetscErrorCode doWork(
      int side,EntityType type,DataForcesAndSourcesCore::EntData &data
    );
  };

  /** \brief Get strain form displacements at integration points
  \ingroup solid_shell_prism_element
  */
  struct OpGetStrainDisp: public MoFEM::FatPrismElementForcesAndSourcesCore::UserDataOperator,MakeB {
    CommonData &commonData;
    MatrixDouble mB;
    VectorDouble sTrain;
    OpGetStrainDisp(CommonData &common_data):
    MoFEM::FatPrismElementForcesAndSourcesCore::UserDataOperator(
      common_data.displacementFieldName,UserDataOperator::OPROW
    ),
    commonData(common_data) {

    }
    PetscErrorCode doWork(
      int side,EntityType type,DataForcesAndSourcesCore::EntData &data
    );
  };

  /** \brief Get strain form local through thickness displacements at integration points
  \ingroup solid_shell_prism_element
  */
  struct OpGetStrainLocal: public MoFEM::FatPrismElementForcesAndSourcesCore::UserDataOperator,MakeB {
    CommonData &commonData;
    MatrixDouble mB;
    VectorDouble sTrain;
    OpGetStrainLocal(CommonData &common_data,const string &field):
    MoFEM::FatPrismElementForcesAndSourcesCore::UserDataOperator(
      field,UserDataOperator::OPROW
    ),
    commonData(common_data) {}
    PetscErrorCode doWork(
      int side,EntityType type,DataForcesAndSourcesCore::EntData &data
    );
  };

  /** \brief Evaluate stress at integration points
  \ingroup solid_shell_prism_element

  Stress is expressed in local coordinate system
  */
  struct OpGetStress: public MoFEM::FatPrismElementForcesAndSourcesCore::UserDataOperator {
    CommonData &commonData;
    OpGetStress(CommonData &common_data):
    MoFEM::FatPrismElementForcesAndSourcesCore::UserDataOperator(
      common_data.displacementFieldName,UserDataOperator::OPROW
    ),
    commonData(common_data) {}
    PetscErrorCode doWork(
      int side,EntityType type,DataForcesAndSourcesCore::EntData &data
    );
  };

  /** \brief Assemble internal forces for displacement DoFs
  \ingroup solid_shell_prism_element
  */
  struct OpAssembleRhsDisp: public MoFEM::FatPrismElementForcesAndSourcesCore::UserDataOperator,MakeB {
    CommonData &commonData;
    VectorDouble nF;
    MatrixDouble B;
    
    OpAssembleRhsDisp(
      CommonData &common_data
    ):
    MoFEM::FatPrismElementForcesAndSourcesCore::UserDataOperator(
      common_data.displacementFieldName,UserDataOperator::OPROW
    ),
    commonData(common_data) {
    }
    PetscErrorCode doWork(
      int side,EntityType type,DataForcesAndSourcesCore::EntData &data
    );
  };

  /** \brief Assemble internal forces for local thorough thickness DoFs
  \ingroup solid_shell_prism_element
  */
  struct OpAssembleRhsLocal: public MoFEM::FatPrismElementForcesAndSourcesCore::UserDataOperator,MakeB {
    CommonData &commonData;
    VectorDouble nF;
    MatrixDouble mB;
    
    OpAssembleRhsLocal(
      CommonData &common_data,const string &field
    ):
    MoFEM::FatPrismElementForcesAndSourcesCore::UserDataOperator(
      field,UserDataOperator::OPROW
    ),
    commonData(common_data) {
    }
    PetscErrorCode doWork(
      int side,EntityType type,DataForcesAndSourcesCore::EntData &data
    );
  };

  /** \brief Assemble stiffness matrix
  */
  struct OpAssembleLhsMix: public MoFEM::FatPrismElementForcesAndSourcesCore::UserDataOperator,MakeB {
    CommonData &commonData;
    MatrixDouble mK,mTransK;
    MatrixDouble mRowB,mColB;
    MatrixDouble mCB;
    
    OpAssembleLhsMix(
      CommonData &common_data,
      const string &row_field_name,
      const string &col_field_name,
      const bool symm = false
    ):
    MoFEM::FatPrismElementForcesAndSourcesCore::UserDataOperator(
      row_field_name,col_field_name,UserDataOperator::OPROWCOL
    ),
    commonData(common_data) {
      sYmm = symm;
    }
    PetscErrorCode doWork(
      int row_side,int col_side,
      EntityType row_type,EntityType col_type,
      DataForcesAndSourcesCore::EntData &row_data,
      DataForcesAndSourcesCore::EntData &col_data
    );
  };

  /** \brief Post process strain and stresses on post-processing mesh
  \ingroup solid_shell_prism_element
  */
  struct OpPostProcStressAndStrain: public MoFEM::FatPrismElementForcesAndSourcesCore::UserDataOperator {
    moab::Interface &postProcMesh;
    map<EntityHandle,EntityHandle> &elementsMap;
    vector<EntityHandle> &mapGaussPts;
    CommonData &commonData;
    OpPostProcStressAndStrain(
      moab::Interface &post_proc_mesh,
      map<EntityHandle,EntityHandle> &elements_map,
      vector<EntityHandle> &map_gauss_pts,
      CommonData &common_data
    ):
    MoFEM::FatPrismElementForcesAndSourcesCore::UserDataOperator(
      common_data.displacementFieldName,UserDataOperator::OPROW
    ),
    postProcMesh(post_proc_mesh),
    elementsMap(elements_map),
    mapGaussPts(map_gauss_pts),
    commonData(common_data) {}
    PetscErrorCode doWork(
      int side,EntityType type,DataForcesAndSourcesCore::EntData &data
    );
  };

  /** \brief Post process displacements on post-processing mesh
  \ingroup solid_shell_prism_element
  */
  struct OpPostProcMeshNodeDispalcements: public MoFEM::FatPrismElementForcesAndSourcesCore::UserDataOperator {
    moab::Interface &postProcMesh;
    vector<EntityHandle> &mapGaussPts;
    CommonData &commonData;
    OpPostProcMeshNodeDispalcements(
      moab::Interface &post_proc_mesh,
      vector<EntityHandle> &map_gauss_pts,
      CommonData &common_data
    ):
    MoFEM::FatPrismElementForcesAndSourcesCore::UserDataOperator(
      common_data.displacementFieldName,UserDataOperator::OPROW
    ),
    postProcMesh(post_proc_mesh),
    mapGaussPts(map_gauss_pts),
    commonData(common_data) {}
    MatrixDouble meshNodePositions;
    PetscErrorCode doWork(
      int side,EntityType type,DataForcesAndSourcesCore::EntData &data
    );
  };

  /** \brief Post process geometry (mesh nodal positions) on post-processing mesh
  \ingroup solid_shell_prism_element
  */
  struct OpPostProcMeshNodePositions: public MoFEM::FatPrismElementForcesAndSourcesCore::UserDataOperator {
    moab::Interface &postProcMesh;
    vector<EntityHandle> &mapGaussPts;
    CommonData &commonData;
    OpPostProcMeshNodePositions(
      moab::Interface &post_proc_mesh,
      vector<EntityHandle> &map_gauss_pts,
      CommonData &common_data
    ):
    MoFEM::FatPrismElementForcesAndSourcesCore::UserDataOperator(
      common_data.meshNodalPosition,UserDataOperator::OPROW
    ),
    postProcMesh(post_proc_mesh),
    mapGaussPts(map_gauss_pts),
    commonData(common_data) {}
    MatrixDouble meshNodePositions;
    PetscErrorCode doWork(
      int side,EntityType type,DataForcesAndSourcesCore::EntData &data
    );
  };

  #ifndef __POSTPROC_ON_REF_MESH_HPP
    #error "Include file PostProcOnRefMesh.hpp before this header"
  #endif //__POSTPROC_ON_REF_MESH_HPP

  /** \brief Generate post-processing triangular mesh for solid-shell element
  \ingroup solid_shell_prism_element
  */
  struct PostProcFatPrismOnTriangleOnRefinedMesh: public PostProcFatPrismOnRefinedMesh {
    PostProcFatPrismOnTriangleOnRefinedMesh(
      MoFEM::Interface &m_field,
      bool ten_nodes_post_proc_tets = true
    ):
    PostProcFatPrismOnRefinedMesh(m_field,ten_nodes_post_proc_tets) {
    }
    int getRuleTrianglesOnly(int order) { return -1; };
    int getRuleThroughThickness(int order) { return -1; };
    PetscErrorCode setGaussPtsTrianglesOnly(int order_triangles_only);
    PetscErrorCode setGaussPtsThroughThickness(int order_thickness);
    PetscErrorCode generateReferenceElementMesh();
    PetscErrorCode postProcess();
  };

  /** \brief Post process axial and shear forces
  \ingroup solid_shell_prism_element
  */
  struct OpGetAxialForces: public MoFEM::FatPrismElementForcesAndSourcesCore::UserDataOperator {
    moab::Interface &postProcMesh;
    vector<EntityHandle> &mapGaussPts;
    CommonData &commonData;
    OpGetAxialForces(
      moab::Interface &post_proc_mesh,
      vector<EntityHandle> &map_gauss_pts,
      CommonData &common_data
    ):
    MoFEM::FatPrismElementForcesAndSourcesCore::UserDataOperator(
      common_data.displacementFieldName,UserDataOperator::OPROW
    ),
    postProcMesh(post_proc_mesh),
    mapGaussPts(map_gauss_pts),
    commonData(common_data) {}
    enum AxialForces { NX = 0,NY,NXY };
    enum ShearForces { TZ = 0,TXZ,TYZ };
    MatrixDouble aXial;
    MatrixDouble sHear;
    PetscErrorCode doWork(
      int side,EntityType type,DataForcesAndSourcesCore::EntData &data
    );
  };

  /** \brief Post process moments forces
  \ingroup solid_shell_prism_element
  */
  struct OpGetMoment: public MoFEM::FatPrismElementForcesAndSourcesCore::UserDataOperator {
    moab::Interface &postProcMesh;
    vector<EntityHandle> &mapGaussPts;
    CommonData &commonData;
    OpGetMoment(
      moab::Interface &post_proc_mesh,
      vector<EntityHandle> &map_gauss_pts,
      CommonData &common_data
    ):
    MoFEM::FatPrismElementForcesAndSourcesCore::UserDataOperator(
      common_data.displacementFieldName,UserDataOperator::OPROW
    ),
    postProcMesh(post_proc_mesh),
    mapGaussPts(map_gauss_pts),
    commonData(common_data) {}
    enum Moments { MX = 0,MY,MXY };
    MatrixDouble mOment;
    PetscErrorCode doWork(
      int side,EntityType type,DataForcesAndSourcesCore::EntData &data
    );
  };

  struct OpGetFCartesian: public MoFEM::FatPrismElementForcesAndSourcesCore::UserDataOperator,MakeB {
    CommonData &commonData;
    MatrixDouble mF;
    OpGetFCartesian(
      CommonData &common_data
    ):
    MoFEM::FatPrismElementForcesAndSourcesCore::UserDataOperator(
      common_data.displacementFieldName,UserDataOperator::OPROW
    ),
    commonData(common_data) {}
    PetscErrorCode doWork(
      int side,EntityType type,DataForcesAndSourcesCore::EntData &data
    );
  };

  struct OpGetFLocal: public MoFEM::FatPrismElementForcesAndSourcesCore::UserDataOperator,MakeB {
    CommonData &commonData;
    MatrixDouble mF;
    bool zEro;
    OpGetFLocal(
      CommonData &common_data,const string &field_name,bool zero
    ):
    MoFEM::FatPrismElementForcesAndSourcesCore::UserDataOperator(
      field_name,UserDataOperator::OPROW
    ),
    commonData(common_data),
    zEro(zero) {}
    PetscErrorCode doWork(
      int side,EntityType type,DataForcesAndSourcesCore::EntData &data
    );
  };


  #ifdef ADOLC_ADOLC_H

  /** \brief Evaluate physical equation and tangent using ADOL-C
  */
  struct EvaluateForward: public MakeB {
    
    CommonData &commonData;
    double lAmbda,mU;
    bool convectiveLike; //< current base of shell CS is fixed
    EvaluateForward(
      CommonData &common_data,
      double lambda,
      double mu
    ):
    commonData(common_data),
    lAmbda(lambda),
    mU(mu),
    convectiveLike(true) {
    }

    template<typename TYPE>
    PetscErrorCode initializeData(CommonData::ADouble<TYPE> &data) {
      PetscFunctionBegin;
      if(data.mGrad1.size1()!=3) {
        // get current base
        data.mGrad1.resize(3,3,false);
        data.mGrad2.resize(3,3,false);
        data.mMF.resize(3,3,false);
        data.mF.resize(3,3,false);
        data.eM_i_a.resize(3,3,false);
        data.eMai.resize(3,3,false);
        data.gMab.resize(3,3,false);
        data.gM_a_b.resize(3,3,false);
        data.mCA_B.resize(3,3,false);
        data.mEA_B.resize(3,3,false);
        data.mCAB.resize(3,3,false);
        data.mEAB.resize(3,3,false);
        data.piolaStressIIA_B.resize(3,3,false);
        data.localPiolaStressIA_a.resize(3,3,false);
        data.localPiolaStressIa_A.resize(3,3,false);
        data.globalPiolaStressIi_I.resize(3,3,false);
      }
      PetscFunctionReturn(0);
    }

    /** \brief Calculate Piola-Kirchhoff II Stress

    \[f
    S^A_B = \lambda E^A_A + 2\mu E^A_B
    \f]

    */
    template<typename TYPE>
    PetscErrorCode physicalEquation(
      CommonData::ADouble<TYPE> &data,int ggf,int ggt,int gg,bool global_row,bool global_col
    ) {
      PetscFunctionBegin;
      try {

        // Calculate material strain tensor
        noalias(data.mEA_B) = data.mCA_B;
        for(int AA = 0;AA!=3;AA++) {
          int BB = AA;
          data.mEA_B(AA,BB) -= 1;
        }
        data.mEA_B *= 0.5;

        // Calculate stress (physical equation here)
        data.piolaStressIIA_B = 2*mU*data.mEA_B;
        data.tR = 0;
        for(int AA = 0;AA!=3;AA++) {
          int BB = AA;
          data.tR += data.mEA_B(AA,BB);
        }
        for(int AA = 0;AA!=3;AA++) {
          int BB = AA;
          data.piolaStressIIA_B(AA,BB) += data.tR*lAmbda;
        }

        /* Piola Local Coords
         P^c_C = F^c_B S^B_C \\
         P^A_a = g_{ac} G^{AC} F^c_B S^B_C = g_{ac} G^{AC}  P^c_C \\
         P^A_a = g_{ac} G^{AC} F^c_B S^B_C
        */
        data.localPiolaStressIa_A.clear();
        for(int cc = 0;cc!=3;cc++) {
          for(int CC = 0;CC!=3;CC++) {
            TYPE &localPiolaStressIa_A = data.localPiolaStressIa_A(CC,cc);
            for(int BB = 0;BB!=3;BB++) {
              localPiolaStressIa_A +=
              data.mF(cc,BB)*
              data.piolaStressIIA_B(BB,CC);
            }
          }
        }
        data.localPiolaStressIA_a.clear();
        for(int aa = 0;aa!=3;aa++) {
          for(int AA = 0;AA!=3;AA++) {
            TYPE &localPiolaStressIA_a = data.localPiolaStressIA_a(aa,AA);
            for(int cc = 0;cc!=3;cc++) {
              TYPE &gM_a_b = data.gM_a_b(aa,cc);
              for(int CC = 0;CC!=3;CC++) {
                data.t0 = gM_a_b*commonData.mGAB[ggf](AA,CC);
                data.t1 = data.t0*data.localPiolaStressIa_A(CC,cc);
                localPiolaStressIA_a += data.t1;
              }
            }
          }
        }

      } catch (const std::exception& ex) {
        ostringstream ss;
        ss << "throw in method: " << ex.what() << endl;
        SETERRQ(PETSC_COMM_SELF,1,ss.str().c_str());
      }
      PetscFunctionReturn(0);
    }

    template<typename TYPE>
    PetscErrorCode evaluateForward(
      CommonData::ADouble<TYPE> &data,int ggf,int ggt,int gg,bool global_row,bool global_col
    ) {
      PetscFunctionBegin;

      //FIXME: This is not optimized,

      try {

        if(convectiveLike) {

          noalias(data.mMF) = data.mGrad1;
          for(int II = 0;II!=3;II++) {
            int ii = II;
            data.mMF(ii,II) += 1;
          }

          data.eMai.clear();
          for(int aa = 0;aa!=3;aa++) {
            int AA = aa;
            for(int ii = 0;ii!=3;ii++) {
              TYPE &eMai = data.eMai(aa,ii);
              for(int II = 0;II!=3;II++) {
                eMai +=
                data.mMF(ii,II)*
                commonData.mEAI[ggf](AA,II);
              }
            }
          }

          ierr = dEterminatnt(data.eMai,data.dEt); CHKERRQ(ierr);
          ierr = iNvert(data.dEt,data.eMai,data.eM_i_a); CHKERRQ(ierr);

          data.gMab.clear();
          for(int aa = 0;aa!=3;aa++) {
            for(int bb = 0;bb!=3;bb++) {
              TYPE &gMab = data.gM_a_b(aa,bb);
              for(int ii = 0;ii!=3;ii++) {
                data.t0 = data.eMai(aa,ii)*data.eMai(bb,ii);
                gMab += data.t0;
              }
            }
          }
          data.gM_a_b.clear();
          for(int aa = 0;aa!=3;aa++) {
            for(int bb = 0;bb!=3;bb++) {
              TYPE &gM_a_b = data.gM_a_b(aa,bb);
              for(int ii = 0;ii!=3;ii++) {
                data.t0 = data.eM_i_a(ii,aa)*data.eM_i_a(ii,bb);
                gM_a_b += data.t0;
              }
            }
          }

        } else {

          noalias(data.eMai) = commonData.mEAI[ggf];
          noalias(data.eM_i_a) = commonData.mE_I_A[ggf];
          noalias(data.gMab) = commonData.mGAB[ggf];
          noalias(data.gM_a_b) = commonData.mG_A_B[ggf];

        }

        // Gradient of defamation (curvilinear system)
        // Local current coordinate system is convected by
        // global DoFs
        data.mF.clear();
        for(int aa = 0;aa!=3;aa++) {
          for(int AA = 0;AA!=3;AA++) {
            TYPE &mF = data.mF(aa,AA);
            for(int ii = 0;ii!=3;ii++) {
              int II = ii;
              mF +=
              data.eMai(aa,ii)*
              commonData.mE_I_A[ggf](II,AA);
            }
          }
        }
        if(global_col) {
          for(int aa = 0;aa!=3;aa++) {
            for(int AA = 0;AA!=3;AA++) {
              TYPE &mF = data.mF(aa,AA);
              for(int ii = 0;ii!=3;ii++) {
                for(int II = 0;II!=3;II++) {
                  data.t0 = data.eMai(aa,ii)*commonData.mE_I_A[ggf](II,AA);
                  mF += data.t0*data.mGrad1(ii,II);
                }
              }
            }
          }
          noalias(data.mF) += commonData.mGrad2[gg];
        } else {
          for(int aa = 0;aa!=3;aa++) {
            for(int AA = 0;AA!=3;AA++) {
              TYPE &mF = data.mF(aa,AA);
              for(int ii = 0;ii!=3;ii++) {
                for(int II = 0;II!=3;II++) {
                  data.t0 = data.eMai(aa,ii)*commonData.mE_I_A[ggf](II,AA);
                  mF += data.t0*commonData.mGrad1[gg](ii,II);
                }
              }
            }
          }
          noalias(data.mF) += data.mGrad2;
        }

        // Calculate material deformation tensor
        // C^A_B = g_{ab}G^{AC} F^b_C F^a_B
        data.mCA_B.clear();
        TYPE &mGAB_AA_CCmF_bb_CC = data.t0;
        TYPE &mGAB_AA_CCmF_bb_CCgM_aa_ba = data.t1;
        for(int AA = 0;AA!=3;AA++) {
          for(int CC = 0;CC!=3;CC++) {
            double mGAB_AA_CC = commonData.mGAB[ggf](AA,CC);
            for(int bb = 0;bb!=3;bb++) {
              mGAB_AA_CCmF_bb_CC = mGAB_AA_CC*data.mF(bb,CC);
              for(int aa = 0;aa!=3;aa++) {
                 mGAB_AA_CCmF_bb_CCgM_aa_ba = mGAB_AA_CCmF_bb_CC*data.gM_a_b(aa,bb);
                for(int BB = 0;BB!=3;BB++) {
                  data.t3 = mGAB_AA_CCmF_bb_CCgM_aa_ba*data.mF(aa,BB);
                  data.mCA_B(AA,BB)+= data.t3;
                  // data.gM_a_b(aa,bb)*
                  // commonData.mGAB[ggf](AA,CC)*
                  // data.mF(bb,CC)*
                  // data.mF(aa,BB);
                }
              }
            }
          }
        }

        ierr = physicalEquation<TYPE>(data,ggf,ggt,gg,global_row,global_col); CHKERRQ(ierr);

        if(global_row) {
          // Piola Global Coords
          // P^I_i = E_A^I e^a_i P^A_a
          data.globalPiolaStressIi_I.clear();
          for(int ii = 0;ii!=3;ii++) {
            for(int II = 0;II!=3;II++) {
              TYPE &globalPiolaStressIi_I = data.globalPiolaStressIi_I(ii,II);
              for(int aa = 0;aa!=3;aa++) {
                for(int AA  = 0;AA!=3;AA++) {
                  data.t0 = data.eMai(aa,ii)*commonData.mE_I_A[ggf](II,AA);
                  data.t1 = data.t0*data.localPiolaStressIA_a(aa,AA);
                  globalPiolaStressIi_I += data.t1;
                }
              }
            }
          }
        }

      } catch (const std::exception& ex) {
        ostringstream ss;
        ss << "throw in method: " << ex.what() << endl;
        SETERRQ(PETSC_COMM_SELF,1,ss.str().c_str());
      }

      PetscFunctionReturn(0);
    }
  };

  struct OpEvaluatePiolaStress: public
  MoFEM::FatPrismElementForcesAndSourcesCore::UserDataOperator,
  EvaluateForward {
    int orderThickness;
    bool getTangent;
    int tAg;
    OpEvaluatePiolaStress(
      CommonData &common_data,
      double lambda,
      double mu,
      int tag,
      int order_thickness,
      bool get_tangent
    ):
    MoFEM::FatPrismElementForcesAndSourcesCore::UserDataOperator(
      common_data.displacementFieldName,UserDataOperator::OPROW
    ),
    EvaluateForward(common_data,lambda,mu),
    orderThickness(order_thickness),
    getTangent(get_tangent),
    tAg(tag) {
    }
    PetscErrorCode doWork(
      int side,EntityType type,DataForcesAndSourcesCore::EntData &data
    );
  };

  struct OpAssembleLhsPiolaStress: public
    MoFEM::FatPrismElementForcesAndSourcesCore::UserDataOperator,
    MakeB {
    CommonData &commonData;
    MatrixDouble mK,mTransK;
    MatrixDouble mRowF,mColF;
    MatrixDouble dP;
    bool assembleTranspose;
    OpAssembleLhsPiolaStress(
      CommonData &common_data,
      const string &row_field_name,
      const string &col_field_name,
      const bool symm = false,
      const bool assemble_transpose = true
    ):
    MoFEM::FatPrismElementForcesAndSourcesCore::UserDataOperator(
      row_field_name,col_field_name,UserDataOperator::OPROWCOL
    ),
    commonData(common_data),
    assembleTranspose(assemble_transpose) {
      sYmm = symm;
    }
    PetscErrorCode doWork(
      int row_side,int col_side,
      EntityType row_type,EntityType col_type,
      DataForcesAndSourcesCore::EntData &row_data,
      DataForcesAndSourcesCore::EntData &col_data
    );
  };


  struct OpAssembleRhsPiolaStress: public
    MoFEM::FatPrismElementForcesAndSourcesCore::UserDataOperator,
    MakeB {
    CommonData &commonData;
    MatrixDouble mF;
    VectorDouble vF;
    OpAssembleRhsPiolaStress(
      CommonData &common_data,
      const string &field_name
    ):
    MoFEM::FatPrismElementForcesAndSourcesCore::UserDataOperator(
      field_name,UserDataOperator::OPROW
    ),
    commonData(common_data) {
    }
    PetscErrorCode doWork(
      int side,EntityType type,DataForcesAndSourcesCore::EntData &data
    );
  };


  #endif // ADOLC_ADOLC_H


  /** \brief Evaluate normals and tangents on shell surface
  \ingroup solid_shell_prism_element

  It is used to create prism geometry for geometry of triangles and
  making prism by moving nodes of triangles in normal direction (extruding)

  */
  struct NormalApprox {
    moab::Interface& mOab;
    double aNgle;
    vector<VectorDouble> rEsult;
    VectorDouble surfaceParametrisation;
    NormalApprox(moab::Interface& moab):
    mOab(moab),
    aNgle(0) {}
    NormalApprox(moab::Interface& moab,const double angle):
    mOab(moab),
    aNgle(angle) {
    }

    MatrixDouble rotMatrix;
    PetscErrorCode getRotMatrix(VectorDouble &u,double phi);

    vector<VectorDouble>& operator()(
      EntityHandle ent,
      double x,
      double y,
      double z,
      VectorDouble normal,
      VectorDouble tangent1,
      VectorDouble tangent2
    );
  };

  #ifdef __ARC_LENGTH_TOOLS_HPP__

  struct OpGetIncrementOfDisplacements: public MoFEM::FatPrismElementForcesAndSourcesCore::UserDataOperator {
    CommonData &commonData;
    OpGetIncrementOfDisplacements(CommonData &common_data):
    MoFEM::FatPrismElementForcesAndSourcesCore::UserDataOperator(
      common_data.displacementFieldName,UserDataOperator::OPROW
    ),
    commonData(common_data) {
    }
    PetscErrorCode doWork(
      int side,EntityType type,DataForcesAndSourcesCore::EntData &data
    );
  };

  struct OpNumericalViscousDampingRhs: public MoFEM::FatPrismElementForcesAndSourcesCore::UserDataOperator {
    CommonData &commonData;
    double &aLpha;
    VectorDouble vC;
    OpNumericalViscousDampingRhs(CommonData &common_data,double &alpha):
    MoFEM::FatPrismElementForcesAndSourcesCore::UserDataOperator(
      common_data.displacementFieldName,UserDataOperator::OPROW
    ),
    commonData(common_data),
    aLpha(alpha) {
    }
    PetscErrorCode doWork(
      int side,EntityType type,DataForcesAndSourcesCore::EntData &data
    );
  };

  struct OpNumericalViscousDampingLhs: public MoFEM::FatPrismElementForcesAndSourcesCore::UserDataOperator {
    CommonData &commonData;
    double &aLpha;
    MatrixDouble mC,mCTrans;
    OpNumericalViscousDampingLhs(CommonData &common_data,double &alpha):
    MoFEM::FatPrismElementForcesAndSourcesCore::UserDataOperator(
      common_data.displacementFieldName,UserDataOperator::OPROWCOL
    ),
    commonData(common_data),
    aLpha(alpha) {
      sYmm = true;
    }
    PetscErrorCode doWork(
      int row_side,int col_side,
      EntityType row_type,EntityType col_type,
      DataForcesAndSourcesCore::EntData &row_data,
      DataForcesAndSourcesCore::EntData &col_data
    );
  };

  #endif //__ARC_LENGTH_TOOLS_HPP__

  /**
   * \brief Calculate semi norm
   */
  struct OpEnergyNormH1: public MoFEM::FatPrismElementForcesAndSourcesCore::UserDataOperator,MakeB {
    CommonData &commonData;
    Tag thError;
    OpEnergyNormH1(
      CommonData &common_data
    ):
    MoFEM::FatPrismElementForcesAndSourcesCore::UserDataOperator("DISPLACEMENT",UserDataOperator::OPROW),
    commonData(common_data) {
      commonData.saveOnTag = true;
    }
    PetscErrorCode doWork(
      int side,EntityType type,DataForcesAndSourcesCore::EntData &data
    );
  };

};

}

#endif //__SOLIDSHELLPRISMELEMENT_HPP__

 /***************************************************************************//**
  * \defgroup solid_shell_prism_element Solid Shell Prism Element
  * \ingroup user_modules
  ******************************************************************************/

 /***************************************************************************//**
  * \defgroup user_modules User modules
  ******************************************************************************/
