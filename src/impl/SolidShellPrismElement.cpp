/** \file SolidShellPrismElement.cpp
 * \ingroup solid_shell_prism_element
 * \brief Implementation of solid shell prism element
 *
 */

/*
 * This file is part of MoFEM.
 * MoFEM is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * MoFEM is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with MoFEM. If not, see <http://www.gnu.org/licenses/>. */

#define BOOST_UBLAS_NDEBUG

#include <MoFEM.hpp>
#include <PrismsFromSurfaceInterface.hpp>
#include <boost/numeric/ublas/symmetric.hpp>

using namespace MoFEM;
#include <PostProcOnRefMesh.hpp>

#ifdef WITH_ADOL_C
// #define ADOLC_TAPELESS
// #include <adolc/adtl.h>
#include <adolc/adolc.h>
#endif

#include <ArcLengthTools.hpp>
#include <SolidShellPrismElement.hpp>

// #ifdef __cplusplus
// extern "C" {
// #endif
//   #include <gm_rule.h>
// #ifdef __cplusplus
// }
// #endif

namespace SolidShellModule {

// PetscErrorCode SolidShellPrismElement::SolidShell::setGaussPtsThroughThickness(int order_thickness) {
//   PetscFunctionBegin;
//   int rule = 2*(order_thickness-1);
//   rule = (rule == 0) ? 1 : rule;
//   int nb_gauss_pts_through_thickness = gm_rule_size(rule,1);
//   gaussPtsThroughThickness.resize(2,nb_gauss_pts_through_thickness,false);
//   ierr = Grundmann_Moeller_integration_points_1D_EDGE(
//     rule,
//     &gaussPtsThroughThickness(0,0),
//     &gaussPtsThroughThickness(1,0)
//   ); CHKERRQ(ierr);
//   PetscFunctionReturn(0);
// }

PetscErrorCode SolidShellPrismElement::SolidShell::preProcess() {
  PetscFunctionBegin;

  ierr = FatPrismElementForcesAndSourcesCore::preProcess(); CHKERRQ(ierr);
  switch (ts_ctx) {
    case CTX_TSSETIFUNCTION: {
      snes_ctx = CTX_SNESSETFUNCTION;
      snes_x = ts_u;
      snes_f = ts_F;
      break;
    }
    case CTX_TSSETIJACOBIAN: {
      snes_ctx = CTX_SNESSETJACOBIAN;
      snes_x = ts_u;
      snes_B = ts_B;
      break;
    }
    default:
    break;
  }
  PetscFunctionReturn(0);
}

PetscErrorCode SolidShellPrismElement::SolidShell::postProcess() {
  PetscFunctionBegin;

  ierr = FatPrismElementForcesAndSourcesCore::postProcess(); CHKERRQ(ierr);
  PetscFunctionReturn(0);
}

PetscErrorCode SolidShellPrismElement::SolidShellError::preProcess() {
  PetscFunctionBegin;

  ierr = SolidShell::preProcess(); CHKERRQ(ierr);
  PetscFunctionReturn(0);
}

PetscErrorCode SolidShellPrismElement::SolidShellError::postProcess() {
  PetscFunctionBegin;

  ierr = SolidShell::postProcess(); CHKERRQ(ierr);
  ierr = VecAssemblyBegin(commonData.vecErrorOnElements); CHKERRQ(ierr);
  ierr = VecAssemblyEnd(commonData.vecErrorOnElements); CHKERRQ(ierr);
  PetscFunctionReturn(0);
}

PetscErrorCode SolidShellPrismElement::OpGetDisplacements::doWork(
  int side,EntityType type,DataForcesAndSourcesCore::EntData &data
) {
  PetscFunctionBegin;
  if(data.getFieldData().size() == 0) PetscFunctionReturn(0);
  int nb_gauss_pts = data.getN().size1();
  int nb_gauss_pts_through_thickness = getGaussPtsThroughThickness().size2();
  int nb_gauss_pts_on_faces = getGaussPtsTrianglesOnly().size2();
  if(type == MBVERTEX) {
    commonData.localDisplacementsAtGaussPts.resize(nb_gauss_pts,3,false);
    commonData.localDisplacementsAtGaussPts.clear();
    commonData.globalDisplacentAtGaussPts.resize(nb_gauss_pts,3,false);
    commonData.globalDisplacentAtGaussPts.clear();
  }

  int nb_shapes = data.getN().size2();
  double *shape_ptr = &data.getN()(0,0);
  int nb_dofs = data.getFieldData().size()/3;
  double *data_ptr = &data.getFieldData()[0];
  VectorDouble local_disp(3);
  VectorDouble global_disp(3);
  local_disp.clear();
  global_disp.clear();

  int gg = 0;
  for(int ggf = 0;ggf<nb_gauss_pts_on_faces;ggf++) {
    for(int ggt = 0;ggt<nb_gauss_pts_through_thickness;ggt++,gg++) {
      VectorAdaptor shape_fun(
        nb_shapes,ublas::shallow_array_adaptor<double>(3,shape_ptr[gg*nb_shapes])
      );
      for(int dd = 0;dd<3;dd++) {
        global_disp[dd] = cblas_ddot(nb_dofs,&shape_ptr[gg*nb_shapes],1,&data_ptr[dd],3);
        commonData.globalDisplacentAtGaussPts(gg,dd) += global_disp[dd];
      }
      noalias(local_disp) = prod(commonData.mEAI[ggf],global_disp);
      for(int dd = 0;dd<3;dd++) {
        commonData.localDisplacementsAtGaussPts(gg,dd) += local_disp[dd];
      }
    }
  }

  PetscFunctionReturn(0);
}

PetscErrorCode SolidShellPrismElement::OpGetDirectorVector::doWork(
  int side,EntityType type,DataForcesAndSourcesCore::EntData &data
) {
  PetscFunctionBegin;
  if(data.getFieldData().size() == 0) PetscFunctionReturn(0);
  int nb_gauss_pts_on_faces = getGaussPtsTrianglesOnly().size2();
  if(type == MBVERTEX) {
    commonData.directorVectorAtGaussPts.resize(nb_gauss_pts_on_faces,3);
    commonData.directorVectorAtGaussPts.clear();
    commonData.diffDirectorVectorAtGaussPts.resize(nb_gauss_pts_on_faces);
    for(int ggf = 0;ggf<nb_gauss_pts_on_faces;ggf++) {
      commonData.diffDirectorVectorAtGaussPts[ggf].resize(3,2,false);
      commonData.diffDirectorVectorAtGaussPts[ggf].clear();
    }
  }
  int nb_dofs = data.getFieldData().size();
  if(type == MBVERTEX) nb_dofs /= 2;
  for(int ggf = 0;ggf<nb_gauss_pts_on_faces;ggf++) {
    const double *shape_ptr = &getTrianglesOnlyDataStructure().dataOnEntities[type][side].getN(ggf)[0];
    const double *diff_shape_ptr = &getTrianglesOnlyDataStructure().dataOnEntities[type][side].getDiffN(ggf)(0,0);
    const double *data_ptr = &data.getFieldData()[0];
    for(int dd = 0;dd<3;dd++) {
      commonData.directorVectorAtGaussPts(ggf,dd) += cblas_ddot(nb_dofs/3,shape_ptr,1,&data_ptr[dd],3);
      commonData.diffDirectorVectorAtGaussPts[ggf](dd,0) +=
      cblas_ddot(nb_dofs/3,&diff_shape_ptr[0],2,&data_ptr[dd],3);
      commonData.diffDirectorVectorAtGaussPts[ggf](dd,1) +=
      cblas_ddot(nb_dofs/3,&diff_shape_ptr[1],2,&data_ptr[dd],3);
    }
  }
  PetscFunctionReturn(0);
}

PetscErrorCode SolidShellPrismElement::OpGetCoVectors::doWork(
  int side,EntityType type,DataForcesAndSourcesCore::EntData &data
) {
  PetscFunctionBegin;
  if(data.getFieldData().size() == 0) PetscFunctionReturn(0);
  int nb_gauss_pts_on_faces = getGaussPtsTrianglesOnly().size2();
  if(type == MBVERTEX) {
    commonData.tangent0AtGaussPts.resize(nb_gauss_pts_on_faces,3);
    commonData.tangent0AtGaussPts.clear();
    commonData.tangent1AtGaussPts.resize(nb_gauss_pts_on_faces,3);
    commonData.tangent1AtGaussPts.clear();
  }
  EntityHandle f3;
  SideNumber_multiIndex& side_table = const_cast<SideNumber_multiIndex&>(
    getNumeredEntFiniteElementPtr()->getSideNumberTable()
  );
  f3 = side_table.get<1>().find(boost::make_tuple(MBTRI,3))->get()->ent;
  EntityHandle shell_id = commonData.shellElementsMap[f3];
  int nb_dofs = data.getFieldData().size();
  if(type == MBVERTEX) nb_dofs /= 2;
  coVector0Data.resize(nb_dofs,false);
  coVector1Data.resize(nb_dofs,false);
  for(int dd = 0;dd<nb_dofs;dd++) {
    if(data.getFieldDofs()[dd]!=NULL) {
      UId uid = data.getFieldDofs()[dd]->getGlobalUniqueId();
      coVector0Data[dd] = commonData.coVector0DofsMap[shell_id][uid];
      coVector1Data[dd] = commonData.coVector1DofsMap[shell_id][uid];
    } else {
      // cerr << data.getFieldData() << endl;
      SETERRQ3(PETSC_COMM_SELF,MOFEM_DATA_INCONSISTENCY,
        "on ent %d and side %d dd %d",type,side,dd
      );
    }
  }
  // cerr << data.getFieldData() << endl;
  // cerr << coVector0Data << endl;
  // cerr << coVector1Data << endl;
  // cerr << endl;
  for(int ggf = 0;ggf<nb_gauss_pts_on_faces;ggf++) {
    const double *shape_ptr = &getTrianglesOnlyDataStructure().dataOnEntities[type][side].getN(ggf)[0];
    const double *data_ptr0 = &coVector0Data[0];
    const double *data_ptr1 = &coVector1Data[0];
    for(int dd = 0;dd<3;dd++) {
      commonData.tangent0AtGaussPts(ggf,dd) += cblas_ddot(nb_dofs/3,shape_ptr,1,&data_ptr0[dd],3);
      commonData.tangent1AtGaussPts(ggf,dd) += cblas_ddot(nb_dofs/3,shape_ptr,1,&data_ptr1[dd],3);
    }
  }
  PetscFunctionReturn(0);
}

PetscErrorCode SolidShellPrismElement::OpGetReferenceBase::doWork(
  int side,EntityType type,DataForcesAndSourcesCore::EntData &data
) {
  PetscFunctionBegin;


  try {

    if(data.getFieldData().size() == 0) PetscFunctionReturn(0);

    int nb_gauss_pts_on_faces = getGaussPtsTrianglesOnly().size2();

    if(type == MBVERTEX) {

      VectorDouble& surface_parametrisation = commonData.surfaceParametrisation;
      surface_parametrisation.resize(3,false);
      {
        Tag th_surface_parametrisation;

        rval = getPrismFE()->mField.get_moab().tag_get_handle(
          "SURFACE_PARAMETRISATION",th_surface_parametrisation
        ); CHKERRQ_MOAB(rval);
        EntityHandle f3;
        SideNumber_multiIndex& side_table = const_cast<SideNumber_multiIndex&>(
          getNumeredEntFiniteElementPtr()->getSideNumberTable()
        );
        f3 = side_table.get<1>().find(boost::make_tuple(MBTRI,3))->get()->ent;
        rval = getPrismFE()->mField.get_moab().tag_get_data(
          th_surface_parametrisation,&f3,1,&surface_parametrisation[0]
        ); CHKERRQ_MOAB(rval);
        double nrm2 = cblas_dnrm2(3,&surface_parametrisation[0],1);
        cblas_dscal(3,1./nrm2,&surface_parametrisation[0],1);
      }

      // commonData.rOtationUser.resize(nb_gauss_pts_on_faces);
      commonData.mEAI.resize(nb_gauss_pts_on_faces);

      for(int ggf = 0;ggf<nb_gauss_pts_on_faces;ggf++) {

        double nrm2 = 1;
        double *t0_ptr = &commonData.tangent0AtGaussPts(ggf,0);
        // nrm2 = cblas_dnrm2(3,t0_ptr,1);
        // cerr << nrm2 << endl;
        // Local element coordinate system
        double t0x = t0_ptr[0]/nrm2;
        double t0y = t0_ptr[1]/nrm2;
        double t0z = t0_ptr[2]/nrm2;
        double *t1_ptr = &commonData.tangent1AtGaussPts(ggf,0);
        // nrm2 = cblas_dnrm2(3,t1_ptr,1);
        // cerr << nrm2 << endl;
        // Local element coordinate system
        double t1x = t1_ptr[0]/nrm2;
        double t1y = t1_ptr[1]/nrm2;
        double t1z = t1_ptr[2]/nrm2;
        double nx = -t0z*t1y+t0y*t1z;
        double ny = +t0z*t1x-t0x*t1z;
        double nz = -t0y*t1x+t0x*t1y;

        commonData.mEAI[ggf].resize(3,3,false);
        //row 0
        commonData.mEAI[ggf](0,0) = t0x;
        commonData.mEAI[ggf](0,1) = t0y;
        commonData.mEAI[ggf](0,2) = t0z;
        //row 1
        commonData.mEAI[ggf](1,0) = t1x;
        commonData.mEAI[ggf](1,1) = t1y;
        commonData.mEAI[ggf](1,2) = t1z;
        //row 2
        commonData.mEAI[ggf](2,0) = nx;
        commonData.mEAI[ggf](2,1) = ny;
        commonData.mEAI[ggf](2,2) = nz;

        // commonData.mEAI[ggf].clear();
        // for(int ii = 0;ii<3;ii++) {
        //   commonData.mEAI[ggf](ii,ii) = 1;
        // }

      }

      commonData.mE_I_A.resize(nb_gauss_pts_on_faces);
      commonData.mGAB.resize(nb_gauss_pts_on_faces);
      commonData.mG_A_B.resize(nb_gauss_pts_on_faces);

      for(int ggf = 0;ggf<nb_gauss_pts_on_faces;ggf++) {
        commonData.mE_I_A[ggf].resize(3,3,false);
        //commonData.mE_I_A[ggf].clear();
        double det;
        ierr = dEterminatnt(commonData.mEAI[ggf],det); CHKERRQ(ierr);
        ierr = iNvert(
          det,commonData.mEAI[ggf],commonData.mE_I_A[ggf]
        ); CHKERRQ(ierr);
        // cerr << setprecision(3) << prod(commonData.mEAI[ggf],commonData.mE_I_A[ggf]) << endl;
        commonData.mGAB[ggf].resize(3,3,false);
        commonData.mGAB[ggf].clear();
        commonData.mG_A_B[ggf].resize(3,3,false);
        commonData.mG_A_B[ggf].clear();
        for(int AA = 0;AA<3;AA++) {
          for(int BB = 0;BB<3;BB++) {
            for(int II = 0;II<3;II++) {
              commonData.mGAB[ggf](AA,BB) +=
              commonData.mEAI[ggf](AA,II)*commonData.mEAI[ggf](BB,II);
              commonData.mG_A_B[ggf](AA,BB) +=
              commonData.mE_I_A[ggf](II,AA)*commonData.mE_I_A[ggf](II,BB);
            }
          }
        }
      }
    } // MBVERTEX

  } catch (exception& ex) {
    ostringstream ss;
    ss << "thorw in method: " << ex.what() << " at line " << __LINE__ << " in file " << __FILE__;
    SETERRQ(PETSC_COMM_SELF,MOFEM_STD_EXCEPTION_THROW,ss.str().c_str());
  }

  PetscFunctionReturn(0);
}

PetscErrorCode SolidShellPrismElement::OpJacobioan::doWork(
  int side,EntityType type,DataForcesAndSourcesCore::EntData &data
) {
  PetscFunctionBegin;

  try {

    if(data.getFieldData().size() == 0) PetscFunctionReturn(0);

    int nb_gauss_pts = data.getN().size1();
    int nb_gauss_pts_through_thickness = getGaussPtsThroughThickness().size2();
    int nb_gauss_pts_on_faces = getGaussPtsTrianglesOnly().size2();

    if(type == MBVERTEX) {

      VectorDouble& surface_parametrisation = commonData.surfaceParametrisation;
      surface_parametrisation.resize(3,false);
      {
        Tag th_surface_parametrisation;

        rval = getPrismFE()->mField.get_moab().tag_get_handle(
          "SURFACE_PARAMETRISATION",th_surface_parametrisation
        ); CHKERRQ_MOAB(rval);
        EntityHandle f3;
        SideNumber_multiIndex& side_table = const_cast<SideNumber_multiIndex&>(
          getNumeredEntFiniteElementPtr()->getSideNumberTable()
        );
        f3 = side_table.get<1>().find(boost::make_tuple(MBTRI,3))->get()->ent;
        rval = getPrismFE()->mField.get_moab().tag_get_data(
          th_surface_parametrisation,&f3,1,&surface_parametrisation[0]
        ); CHKERRQ_MOAB(rval);
        double nrm2 = cblas_dnrm2(3,&surface_parametrisation[0],1);
        // cerr << surface_arametrisation << endl;
        cblas_dscal(3,1./nrm2,&surface_parametrisation[0],1);
      }
      int element_layer = 0;
      {

        Tag th_layer_number;
        rval = getPrismFE()->mField.get_moab().tag_get_handle("LAYER_NUMBER",th_layer_number); CHKERRQ_MOAB(rval);
        EntityHandle ent = getNumeredEntFiniteElementPtr()->getEnt();
        rval = getPrismFE()->mField.get_moab().tag_get_data( th_layer_number,&ent,1,&element_layer); CHKERRQ_MOAB(rval);
      }

      commonData.positionsAtGaussPts.resize(nb_gauss_pts,3,false);
      commonData.jacobianInvAtGaussPts.resize(nb_gauss_pts);
      commonData.positionsAtGaussPts.clear();
      commonData.jacobianInvAtGaussPts.clear();

      int gg = 0;
      for(int ggf = 0;ggf<nb_gauss_pts_on_faces;ggf++) {
        double h = commonData.tHickness;
        //double nrm2 = cblas_dnrm2(3,&getNormalsAtGaussPtF3()(ggf,0),1);
        double n[] = {
          h*commonData.directorVectorAtGaussPts(ggf,0),
          h*commonData.directorVectorAtGaussPts(ggf,1),
          h*commonData.directorVectorAtGaussPts(ggf,2)
        };
        double s = commonData.sHift*element_layer;
        double shift[] = {
          s*commonData.directorVectorAtGaussPts(ggf,0),
          s*commonData.directorVectorAtGaussPts(ggf,1),
          s*commonData.directorVectorAtGaussPts(ggf,2)
        };
        double diff_n_ksi[] = {
          h*commonData.diffDirectorVectorAtGaussPts[ggf](0,0),
          h*commonData.diffDirectorVectorAtGaussPts[ggf](1,0),
          h*commonData.diffDirectorVectorAtGaussPts[ggf](2,0)
        };
        double diff_n_eta[] = {
          h*commonData.diffDirectorVectorAtGaussPts[ggf](0,1),
          h*commonData.diffDirectorVectorAtGaussPts[ggf](1,1),
          h*commonData.diffDirectorVectorAtGaussPts[ggf](2,1)
        };
        // cerr << n[0] << " " << n[1] << " " << n[2] << endl;
        for(int ggt = 0;ggt<nb_gauss_pts_through_thickness;ggt++,gg++) {
          commonData.jacobianInvAtGaussPts[gg].resize(3,3,false);
          commonData.jacobianInvAtGaussPts[gg].clear();
          double zeta = getGaussPtsThroughThickness()(0,ggt)-0.5;
          for(int dd = 0;dd<3;dd++) {
            commonData.jacobianInvAtGaussPts[gg](dd,0) = diff_n_ksi[dd]*zeta;
            commonData.jacobianInvAtGaussPts[gg](dd,1) = diff_n_eta[dd]*zeta;
            commonData.jacobianInvAtGaussPts[gg](dd,2) = n[dd];
            commonData.positionsAtGaussPts(gg,dd) = n[dd]*zeta+shift[dd];
          }
        }
      }

    } // MBVERTEX

    int nb_shapes = data.getN().size2();
    double *shape_ptr = &data.getN()(0,0);
    double *diff_shape_ptr = &data.getDiffN()(0,0);
    int nb_dofs = data.getFieldData().size()/3;
    double *data_ptr = &*data.getFieldData().data().begin();

    // get positions
    for(int gg = 0;gg<nb_gauss_pts;gg++) {
      VectorAdaptor shape_fun(
        nb_shapes,ublas::shallow_array_adaptor<double>(3,shape_ptr[gg*nb_shapes])
      );
      for(int dd = 0;dd!=3;dd++) {
        commonData.positionsAtGaussPts(gg,dd) += cblas_ddot(
          nb_dofs,&shape_ptr[gg*nb_shapes],1,&data_ptr[dd],3
        );
      }
    }

    //get jacobian
    for(int gg = 0;gg<nb_gauss_pts;gg++) {
      ublas::matrix<double,ublas::column_major> &jac
      = commonData.jacobianInvAtGaussPts[gg];
      for(int dd1 = 0;dd1!=3;dd1++) {
        for(int dd2 = 0;dd2!=3;dd2++) {
          jac(dd2,dd1) += cblas_ddot(
            nb_dofs,&diff_shape_ptr[gg*3*nb_shapes+dd1],3,&data_ptr[dd2],3
          );
        }
      }
    }

  } catch (exception& ex) {
    ostringstream ss;
    ss << "thorw in method: " << ex.what() << " at line " << __LINE__ << " in file " << __FILE__;
    SETERRQ(PETSC_COMM_SELF,MOFEM_STD_EXCEPTION_THROW,ss.str().c_str());
  }

  PetscFunctionReturn(0);
}

PetscErrorCode SolidShellPrismElement::OpInvJacobian::doWork(
  int side,EntityType type,DataForcesAndSourcesCore::EntData &data
) {
  PetscFunctionBegin;

  try {

    if(data.getN().size2() == 0) PetscFunctionReturn(0);

    int nb_gauss_pts = data.getN().size1();

    __CLPK_integer ipiv[4];
    __CLPK_doublereal work[3];
    __CLPK_integer lwork = 3;
    __CLPK_integer info;

    // invert jacobian and calculate determinant
    if(type == MBVERTEX) {
      commonData.jacDeterminantAtGaussPts.resize(nb_gauss_pts,false);
      commonData.jacDeterminantAtGaussPts.clear();

      for(int gg = 0;gg<nb_gauss_pts;gg++) {
        // cerr << "Type " << type << " " << side << endl;
        // cerr << "Diff: " << data.getDiffN() << endl;
        // cerr << "Nodal positions: " << data.getFieldData() << endl;
        // cerr << endl;
        // std::cerr << commonData.jacobianInvAtGaussPts[gg] << endl;
        double *jac = &commonData.jacobianInvAtGaussPts[gg](0,0);
        info = lapack_dgetrf(3,3,jac,3,ipiv);
        if(info != 0) {
          cerr << "Nodal positions: " << data.getFieldData() << endl;
          cerr << "Positions: " << commonData.positionsAtGaussPts << endl;
          cerr << "Thickness: " << commonData.tHickness << endl;
          cerr << "Normal: " << getNormalsAtGaussPtF3() << endl;
          cerr << "Jacobian: " << commonData.jacobianInvAtGaussPts[gg] << endl;
          cerr << "Diff: " << data.getDiffN() << endl;
          SETERRQ1(PETSC_COMM_SELF,1,"info = %d",info);
        }
        int i = 0,j = 0;
        double det_jac = 1.;
        for(; i<3; i++) {
          det_jac *= jac[3*i+i];
          if( ipiv[i] != i+1 ) j++;
        }
        if ( (j - ( j/2 )*2) != 0 ) {
          det_jac = - det_jac;
        }
        det_jac *= ::copysign(1,commonData.tHickness);
        commonData.jacDeterminantAtGaussPts[gg] = det_jac;
        info = lapack_dgetri(3,jac,3,ipiv,work,lwork);
        if(info != 0) SETERRQ1(PETSC_COMM_SELF,1,"info = %d",info);
      }
    }

    double inv_shape[3] = {0,0,0};
    unsigned int nb_shapes = data.getN().size2();
    if(nb_shapes!=data.getDiffN().size2()/3) {
      SETERRQ(PETSC_COMM_SELF,1,"data inconsistency");
    }

    int nb_gauss_pts_on_faces = getGaussPtsTrianglesOnly().size2();
    int nb_gauss_pts_through_thickness = getGaussPtsThroughThickness().size2();

    {
      // apply to shape functions
      commonData.diffGlobN[type][side].resize(data.getDiffN().size1(),data.getDiffN().size2(),false);
      int gg = 0;
      for(int ggf = 0;ggf<nb_gauss_pts_on_faces;ggf++) {
        double *e_ptr = &commonData.mE_I_A[ggf](0,0);
        for(int ggt = 0;ggt<nb_gauss_pts_through_thickness;ggt++,gg++) {
          double *inv_jac = &commonData.jacobianInvAtGaussPts[gg](0,0);
          double *diff_n = &data.getDiffN()(gg,0);
          double *glob_diff_n = &commonData.diffGlobN[type][side](gg,0);
          for(int ii = 0;ii<nb_shapes;ii++) {
            cblas_dgemv(
              CblasColMajor,CblasTrans,3,3,1.,inv_jac,3,&diff_n[ii*3],1,0,inv_shape,1
            );
            cblas_dcopy(3,inv_shape,1,&glob_diff_n[ii*3],1);
            // from global to local element
            cblas_dgemv(
              CblasColMajor,CblasNoTrans,3,3,1,e_ptr,3,inv_shape,1,0,&diff_n[ii*3],1
            );
            //cblas_dcopy(3,inv_shape,1,&diff_n[ii*3],1);
          }
        }
      }
      // cerr << type << " " << side <<  commonData.diffGlobN[type][side] << endl;
    }

    // if(type == MBQUAD) {
    //   cerr << data.getN() << endl;
    //   cerr << data.getDiffN() << endl;
    // }

  } catch (exception& ex) {
    ostringstream ss;
    ss << "thorw in method: " << ex.what() << " at line " << __LINE__ << " in file " << __FILE__;
    SETERRQ(PETSC_COMM_SELF,MOFEM_STD_EXCEPTION_THROW,ss.str().c_str());
  }

  PetscFunctionReturn(0);
}

PetscErrorCode SolidShellPrismElement::MakeB::makeBDisp(
  const MatrixAdaptor &diffN,const MatrixDouble &T,MatrixDouble &B
) {
  PetscFunctionBegin;
  unsigned int nb_dofs = diffN.size1();
  if(B.size2()!=3*nb_dofs) {
    B.resize(6,3*nb_dofs,false);
    B.clear();
  }
  for(int dd = 0;dd<nb_dofs;dd++) {
    const double diff[] = {
      diffN(dd,0),diffN(dd,1),diffN(dd,2)
    };
    // eps_kk
    const int dd3 = 3*dd;
    //eps xx
    B(0,dd3+0) = T(0,0)*diff[0];
    B(0,dd3+1) = T(0,1)*diff[0];
    B(0,dd3+2) = T(0,2)*diff[0];
    //eps yy
    B(1,dd3+0) = T(1,0)*diff[1];
    B(1,dd3+1) = T(1,1)*diff[1];
    B(1,dd3+2) = T(1,2)*diff[1];
    //eps zz
    B(2,dd3+0) = T(2,0)*diff[2];
    B(2,dd3+1) = T(2,1)*diff[2];
    B(2,dd3+2) = T(2,2)*diff[2];
    // gamma_xy
    B(3,dd3+0) = T(0,0)*diff[1]+T(1,0)*diff[0];
    B(3,dd3+1) = T(0,1)*diff[1]+T(1,1)*diff[0];
    B(3,dd3+2) = T(0,2)*diff[1]+T(1,2)*diff[0];
    // gamma_yz
    B(4,dd3+0) = T(1,0)*diff[2]+T(2,0)*diff[1];
    B(4,dd3+1) = T(1,1)*diff[2]+T(2,1)*diff[1];
    B(4,dd3+2) = T(1,2)*diff[2]+T(2,2)*diff[1];
    // gamma_xz
    B(5,dd3+0) = T(0,0)*diff[2]+T(2,0)*diff[0];
    B(5,dd3+1) = T(0,1)*diff[2]+T(2,1)*diff[0];
    B(5,dd3+2) = T(0,2)*diff[2]+T(2,2)*diff[0];
  }
  PetscFunctionReturn(0);
}

PetscErrorCode SolidShellPrismElement::MakeB::makeBZeta(
  const MatrixAdaptor &diffN,MatrixDouble &B
) {
  PetscFunctionBegin;
  unsigned int nb_dofs = diffN.size1();
  if(B.size2()!=nb_dofs) {
    B.resize(6,nb_dofs,false);
  }
  B.clear();
  for(int dd = 0;dd<nb_dofs;dd++) {
    const double diff[] = {
      diffN(dd,0),diffN(dd,1),diffN(dd,2)
    };
    //eps zz
    B(2,dd) = diff[2];
    // gamma_yz
    B(4,dd) = diff[1];
    // gamma_xz
    B(5,dd) = diff[0];
  }
  PetscFunctionReturn(0);
}

PetscErrorCode SolidShellPrismElement::MakeB::makeBKsiEta(
  const MatrixAdaptor &diffN,MatrixDouble &B
) {
  PetscFunctionBegin;
  unsigned int nb_dofs = diffN.size1();
  if(B.size2()!=2*nb_dofs) {
    B.resize(6,2*nb_dofs,false);
  }
  B.clear();
  for(int dd = 0;dd<nb_dofs;dd++) {
    const double diff[] = {
      diffN(dd,0),diffN(dd,1),diffN(dd,2)
    };
    // eps_kk
    const int dd2 = 2*dd;
    //eps xx
    B(0,dd2+0) = diff[0];
    //eps yy
    B(1,dd2+1) = diff[1];
    // gamma_xy
    B(3,dd2+0) = diff[1];
    B(3,dd2+1) = diff[0];
    // gamma_yz
    B(4,dd2+1) = diff[2];
    // gamma_xz
    B(5,dd2+0) = diff[2];
  }
  PetscFunctionReturn(0);
}

PetscErrorCode SolidShellPrismElement::MakeB::makeFDisp(
  const MatrixAdaptor &diffN,MatrixDouble &F
) {
  PetscFunctionBegin;
  unsigned int nb_dofs = diffN.size1();
  if(F.size1()!=3*nb_dofs) {
    F.resize(3*nb_dofs,9,false);
    F.clear();
  }
  for(int dd = 0;dd<nb_dofs;dd++) {
    const double diff[] = {
      diffN(dd,0),diffN(dd,1),diffN(dd,2)
    };
    const int dd3 = 3*dd;
    int kk = 0;
    for(int ii = 0;ii!=3;ii++) {
      double row = dd3+ii;
      cblas_dcopy(3,diff,1,&F(row,kk),1);
      kk += 3;
      // for(int jj = 0;jj!=3;jj++,kk++) {
      //   F(row,kk) = diff[jj];
      // }
    }
  }
  PetscFunctionReturn(0);
}

PetscErrorCode SolidShellPrismElement::MakeB::makeFZeta(
  const MatrixAdaptor &diffN,MatrixDouble &F
) {
  PetscFunctionBegin;
  unsigned int nb_dofs = diffN.size1();
  if(F.size2()!=nb_dofs) {
    F.resize(nb_dofs,9,false);
    F.clear();
  }
  for(int dd = 0;dd<nb_dofs;dd++) {
    const double diff[] = {
      diffN(dd,0),diffN(dd,1),diffN(dd,2)
    };
    cblas_dcopy(3,diff,1,&F(dd,6),1);
    // for(int jj = 0;jj<3;jj++) {
    //   F(dd,6+jj) = diff[jj];
    // }
  }
  PetscFunctionReturn(0);
}

PetscErrorCode SolidShellPrismElement::MakeB::makeFKsiEta(
  const MatrixAdaptor &diffN,MatrixDouble &F
) {
  PetscFunctionBegin;
  unsigned int nb_dofs = diffN.size1();
  if(F.size2()!=2*nb_dofs) {
    F.resize(2*nb_dofs,9,false);
    F.clear();
  }
  for(int dd = 0;dd<nb_dofs;dd++) {
    const double diff[] = {
      diffN(dd,0),diffN(dd,1),diffN(dd,2)
    };
    const int dd2 = 2*dd;
    int kk = 0;
    for(int ii = 0;ii<2;ii++) {
      cblas_dcopy(3,diff,1,&F(dd2+ii,kk),1);
      kk+=3;
      // for(int jj = 0;jj<3;jj++,kk++) {
      //   F(dd2+ii,kk) = diff[jj];
      // }
    }
  }
  PetscFunctionReturn(0);
}

PetscErrorCode SolidShellPrismElement::OpGetStrainDisp::doWork(
  int side,EntityType type,DataForcesAndSourcesCore::EntData &data
) {
  PetscFunctionBegin;

  try {
    if(data.getFieldData().size() == 0) PetscFunctionReturn(0);
    int nb_gauss_pts_on_faces = getGaussPtsTrianglesOnly().size2();
    int nb_gauss_pts_through_thickness = getGaussPtsThroughThickness().size2();
    if(type == MBVERTEX) {
      int nb_gauss_pts = data.getN().size1();
      commonData.sTrain.resize(nb_gauss_pts,6,false);
      commonData.sTrain.clear();
    }
    // MatrixDouble one = ublas::identity_matrix<double>(3);
    sTrain.resize(6,false);
    int nb_dofs = data.getFieldData().size();
    if(type!=MBVERTEX) {
      int order = data.getFieldDofs()[0]->getMaxOrder();
      Tag   th;

      rval = getPrismFE()->mField.get_moab().tag_get_handle("ADAPT_ORDER",th);
      if(rval == MB_SUCCESS) {
        EntityHandle ent;
        ent = data.getFieldDofs()[0]->getEnt();
        getPrismFE()->mField.get_moab().tag_get_data(th,&ent,1,&order);
        nb_dofs = data.getIndicesUpToOrder(order).size();
        if(nb_dofs == 0) PetscFunctionReturn(0);
      }
    }
    int gg = 0;
    for(int ggf = 0;ggf<nb_gauss_pts_on_faces;ggf++) {
      MatrixDouble mEAI = commonData.mEAI[ggf];
      for(int ggt = 0;ggt<nb_gauss_pts_through_thickness;ggt++,gg++) {
        const MatrixAdaptor &diffN = data.getDiffN(gg,nb_dofs/3);
        ierr = makeBDisp(diffN,mEAI,mB); CHKERRQ(ierr);
        noalias(sTrain) = prod(mB,data.getFieldData());
        for(int dd = 0;dd<6;dd++) {
          commonData.sTrain(gg,dd) += sTrain[dd];
        }
      }
    }
  } catch (exception& ex) {
    ostringstream ss;
    ss << "thorw in method: " << ex.what() << " at line " << __LINE__ << " in file " << __FILE__;
    SETERRQ(PETSC_COMM_SELF,MOFEM_STD_EXCEPTION_THROW,ss.str().c_str());
  }
  PetscFunctionReturn(0);
}

PetscErrorCode SolidShellPrismElement::OpGetStrainLocal::doWork(
  int side,EntityType type,DataForcesAndSourcesCore::EntData &data
) {
  PetscFunctionBegin;

  try {
    if(data.getFieldData().size() == 0) PetscFunctionReturn(0);
    int nb_gauss_pts_on_faces = getGaussPtsTrianglesOnly().size2();
    int nb_gauss_pts_through_thickness = getGaussPtsThroughThickness().size2();
    int nb_dofs = data.getFieldData().size();
    int rank = data.getFieldDofs()[0]->getNbOfCoeffs();
    if(type == MBVERTEX) {
      int nb_gauss_pts = data.getN().size1();
      commonData.sTrain.resize(nb_gauss_pts,6,false);
      commonData.sTrain.clear();
    }
    sTrain.resize(6,false);
    int gg = 0;
    for(int ggf = 0;ggf<nb_gauss_pts_on_faces;ggf++) {
      for(int ggt = 0;ggt<nb_gauss_pts_through_thickness;ggt++,gg++) {
        const MatrixAdaptor &diffN = data.getDiffN(gg,nb_dofs/rank);
        if(rank == 1) {
          ierr = makeBZeta(diffN,mB); CHKERRQ(ierr);
        } else {
          ierr = makeBKsiEta(diffN,mB); CHKERRQ(ierr);
        }
        noalias(sTrain) = prod(mB,data.getFieldData());
        for(int dd = 0;dd<6;dd++) {
          commonData.sTrain(gg,dd) += sTrain[dd];
        }
      }
    }
  } catch (exception& ex) {
    ostringstream ss;
    ss << "thorw in method: " << ex.what() << " at line " << __LINE__ << " in file " << __FILE__;
    SETERRQ(PETSC_COMM_SELF,MOFEM_STD_EXCEPTION_THROW,ss.str().c_str());
  }
  PetscFunctionReturn(0);
}

PetscErrorCode SolidShellPrismElement::OpGetStress::doWork(
  int side,EntityType type,DataForcesAndSourcesCore::EntData &data
) {
  PetscFunctionBegin;
  try {
    if(type!=MBVERTEX) PetscFunctionReturn(0);
    int nb_gauss_pts = data.getN().size1();
    double *strain_ptr = &commonData.sTrain(0,0);
    commonData.sTress.resize(nb_gauss_pts,6,false);
    double *stress_ptr = &commonData.sTress(0,0);
    for(int gg = 0;gg<nb_gauss_pts;gg++) {
      VectorAdaptor strain = VectorAdaptor(
        6,ublas::shallow_array_adaptor<double>(6,&strain_ptr[6*gg])
      );
      VectorAdaptor stress = VectorAdaptor(
        6,ublas::shallow_array_adaptor<double>(6,&stress_ptr[6*gg])
      );
      noalias(stress) = prod(commonData.materialStiffness,strain);
      // cerr << stress << " " << strain << " " << inner_prod(strain,stress) << endl;
    }
  } catch (exception& ex) {
    ostringstream ss;
    ss << "thorw in method: " << ex.what() << " at line " << __LINE__ << " in file " << __FILE__;
    SETERRQ(PETSC_COMM_SELF,MOFEM_STD_EXCEPTION_THROW,ss.str().c_str());
  }
  PetscFunctionReturn(0);
}

PetscErrorCode SolidShellPrismElement::OpAssembleRhsDisp::doWork(
  int side,EntityType type,DataForcesAndSourcesCore::EntData &data
) {
  PetscFunctionBegin;
  try {
    int nb_dofs = data.getIndices().size();
    if(nb_dofs==0) PetscFunctionReturn(0);
    // int nb_gauss_pts = data.getN().size1();
    int nb_gauss_pts_on_faces = getGaussPtsTrianglesOnly().size2();
    int nb_gauss_pts_through_thickness = getGaussPtsThroughThickness().size2();
    Tag   th;

    rval = getPrismFE()->mField.get_moab().tag_get_handle("ADAPT_ORDER",th);
    if(type!=MBVERTEX) {
      int order = data.getFieldDofs()[0]->getMaxOrder();
      if(rval == MB_SUCCESS) {
        EntityHandle ent;
        ent = data.getFieldDofs()[0]->getEnt();
        getPrismFE()->mField.get_moab().tag_get_data(th,&ent,1,&order);
      }
      nb_dofs = data.getIndicesUpToOrder(order).size();
      if(nb_dofs == 0) PetscFunctionReturn(0);
    }
    nF.resize(nb_dofs,false);
    nF.clear();
    double *stress_ptr = &commonData.sTress(0,0);
    int gg = 0;
    for(int ggf = 0;ggf<nb_gauss_pts_on_faces;ggf++) {
      for(int ggt = 0;ggt<nb_gauss_pts_through_thickness;ggt++,gg++) {
        double val = 0.5*commonData.jacDeterminantAtGaussPts[gg]*getGaussPts()(3,gg);
        const MatrixAdaptor &diffN = data.getDiffN(gg,nb_dofs/3);
        // cerr << diffN << endl;
        ierr = makeBDisp(diffN,commonData.mEAI[ggf],B); CHKERRQ(ierr);
        VectorAdaptor stress = VectorAdaptor(
          6,ublas::shallow_array_adaptor<double>(6,&stress_ptr[6*gg])
        );
        noalias(nF) += val*prod(trans(B),stress);
      }
    }
    int *indices_ptr = &data.getIndices()[0];
    ierr = VecSetValues(
      getFEMethod()->snes_f,
      nb_dofs,
      indices_ptr,
      &nF[0],
      ADD_VALUES
    ); CHKERRQ(ierr);
  } catch (const std::exception& ex) {
    ostringstream ss;
    ss << "throw in method: " << ex.what() << endl;
    SETERRQ(PETSC_COMM_SELF,1,ss.str().c_str());
  }

  PetscFunctionReturn(0);
}

PetscErrorCode SolidShellPrismElement::OpAssembleRhsLocal::doWork(
  int side,EntityType type,DataForcesAndSourcesCore::EntData &data
) {
  PetscFunctionBegin;
  try {
    int nb_dofs = data.getIndices().size();
    if(nb_dofs==0) PetscFunctionReturn(0);
    int nb_gauss_pts_on_faces = getGaussPtsTrianglesOnly().size2();
    int nb_gauss_pts_through_thickness = getGaussPtsThroughThickness().size2();
    int rank = data.getFieldDofs()[0]->getNbOfCoeffs();
    nF.resize(nb_dofs,false);
    nF.clear();
    double *stress_ptr = &commonData.sTress(0,0);
    int gg = 0;
    for(int ggf = 0;ggf<nb_gauss_pts_on_faces;ggf++) {
      for(int ggt = 0;ggt<nb_gauss_pts_through_thickness;ggt++,gg++) {
        double val = 0.5*commonData.jacDeterminantAtGaussPts[gg]*getGaussPts()(3,gg);
        const MatrixAdaptor &diffN = data.getDiffN(gg,nb_dofs/rank);
        if(rank == 1) {
          ierr = makeBZeta(diffN,mB); CHKERRQ(ierr);
        } else {
          ierr = makeBKsiEta(diffN,mB); CHKERRQ(ierr);
        }
        VectorAdaptor stress = VectorAdaptor(
          6,ublas::shallow_array_adaptor<double>(6,&stress_ptr[6*gg])
        );
        noalias(nF) += val*prod(trans(mB),stress);
      }
    }
    int *indices_ptr = &data.getIndices()[0];
    ierr = VecSetValues(
      getFEMethod()->snes_f,
      nb_dofs,
      indices_ptr,
      &nF[0],
      ADD_VALUES
    ); CHKERRQ(ierr);
  } catch (const std::exception& ex) {
    ostringstream ss;
    ss << "throw in method: " << ex.what() << endl;
    SETERRQ(PETSC_COMM_SELF,1,ss.str().c_str());
  }
  PetscFunctionReturn(0);
}

PetscErrorCode SolidShellPrismElement::OpAssembleLhsMix::doWork(
  int row_side,int col_side,
  EntityType row_type,EntityType col_type,
  DataForcesAndSourcesCore::EntData &row_data,
  DataForcesAndSourcesCore::EntData &col_data
) {
  PetscFunctionBegin;
  try {

    int nb_dofs_row = row_data.getIndices().size();
    if(nb_dofs_row==0) PetscFunctionReturn(0);
    int nb_dofs_col = col_data.getIndices().size();
    if(nb_dofs_col==0) PetscFunctionReturn(0);

    int row_rank = row_data.getFieldDofs()[0]->getNbOfCoeffs();
    int col_rank = col_data.getFieldDofs()[0]->getNbOfCoeffs();

    mK.resize(nb_dofs_row,nb_dofs_col,false);
    mK.clear();
    mCB.resize(6,nb_dofs_col,false);
    mCB.clear();
    mRowB.clear();
    mColB.clear();
    // int nb_gauss_pts = row_data.getN().size1();
    int nb_gauss_pts_on_faces = getGaussPtsTrianglesOnly().size2();
    int nb_gauss_pts_through_thickness = getGaussPtsThroughThickness().size2();
    int *row_indices_ptr,*col_indices_ptr;
    row_indices_ptr = &row_data.getIndices()[0];
    col_indices_ptr = &col_data.getIndices()[0];
    int gg = 0;
    for(int ggf = 0;ggf<nb_gauss_pts_on_faces;ggf++) {
      for(int ggt = 0;ggt<nb_gauss_pts_through_thickness;ggt++,gg++) {
        double val = 0.5*commonData.jacDeterminantAtGaussPts[gg]*getGaussPts()(3,gg);
        const MatrixAdaptor &diffN_row = row_data.getDiffN(gg,nb_dofs_row/row_rank);
        const MatrixAdaptor &diffN_col = col_data.getDiffN(gg,nb_dofs_col/col_rank);
        if(row_rank == 3) {
          ierr = makeBDisp(diffN_row,commonData.mEAI[ggf],mRowB); CHKERRQ(ierr);
        } else if(row_rank == 2) {
          ierr = makeBKsiEta(diffN_row,mRowB); CHKERRQ(ierr);
        } else {
          ierr = makeBZeta(diffN_row,mRowB); CHKERRQ(ierr);
        }
        if(col_rank == 3) {
          ierr = makeBDisp(diffN_col,commonData.mEAI[ggf],mColB); CHKERRQ(ierr);
        } else if(col_rank == 2) {
          ierr = makeBKsiEta(diffN_col,mColB); CHKERRQ(ierr);
        } else {
          ierr = makeBZeta(diffN_col,mColB); CHKERRQ(ierr);
        }
        cblas_dsymm(
          CblasRowMajor,CblasLeft,CblasLower,
          6,nb_dofs_col,
          1,&*commonData.materialStiffness.data().begin(),6,
          &mColB(0,0),nb_dofs_col,
          0,&mCB(0,0),nb_dofs_col
        );
        cblas_dgemm(
          CblasRowMajor,CblasTrans,CblasNoTrans,
          nb_dofs_row,nb_dofs_col,6,
          val,&mRowB(0,0),nb_dofs_row,&mCB(0,0),nb_dofs_col,
          1,&mK(0,0),nb_dofs_col
        );
      }
      // cerr << endl;
    }
    ierr = MatSetValues(
      getFEMethod()->snes_B,
      nb_dofs_row,row_indices_ptr,
      nb_dofs_col,col_indices_ptr,
      &mK(0,0),ADD_VALUES
    ); CHKERRQ(ierr);
    if(row_side != col_side || row_type != col_type || row_rank != col_rank) {
      mTransK.resize(nb_dofs_col,nb_dofs_row,false);
      noalias(mTransK) = trans(mK);
      ierr = MatSetValues(
        getFEMethod()->snes_B,
        nb_dofs_col,col_indices_ptr,
        nb_dofs_row,row_indices_ptr,
        &mTransK(0,0),ADD_VALUES
      ); CHKERRQ(ierr);
    }
  } catch (const std::exception& ex) {
    ostringstream ss;
    ss << "throw in method: " << ex.what() << endl;
    SETERRQ(PETSC_COMM_SELF,1,ss.str().c_str());
  }
  PetscFunctionReturn(0);
}

PetscErrorCode SolidShellPrismElement::OpPostProcStressAndStrain::doWork(
  int side,EntityType type,DataForcesAndSourcesCore::EntData &data
) {
  PetscFunctionBegin;
  try {

    if(type == MBVERTEX) {
      double def_VAL[9];
      bzero(def_VAL,9*sizeof(double));
      Tag th_strain,th_stress;
      rval = postProcMesh.tag_get_handle(
        "STRAIN",9,MB_TYPE_DOUBLE,th_strain,MB_TAG_CREAT|MB_TAG_SPARSE,def_VAL
      ); CHKERRQ_MOAB(rval);
      rval = postProcMesh.tag_get_handle(
        "STRESS",9,MB_TYPE_DOUBLE,th_stress,MB_TAG_CREAT|MB_TAG_SPARSE,def_VAL
      ); CHKERRQ_MOAB(rval);
      EntityHandle ent = getNumeredEntFiniteElementPtr()->getEnt();
      EntityHandle post_proc_elem = elementsMap[ent];
      Tag th_error;
      rval = getVolumeFE()->mField.get_moab().tag_get_handle("ENERGY_OR_ENERGY_ERROR",th_error);
      if(rval==MB_SUCCESS) {
        double error;
        rval = getVolumeFE()->mField.get_moab().tag_get_data(th_error,&ent,1,&error); CHKERRQ_MOAB(rval);
        // cerr << error << endl;
        Tag th_post_proc_error;
        double def_double_val = 0;
        rval = postProcMesh.tag_get_handle(
          "ENERGY_OR_ENERGY_ERROR",1,MB_TYPE_DOUBLE,th_post_proc_error,MB_TAG_CREAT|MB_TAG_SPARSE,&def_double_val
        ); CHKERRQ_MOAB(rval);
        // cerr << post_proc_elem << endl;
        rval = postProcMesh.tag_set_data(
          th_post_proc_error,&post_proc_elem,1,&error
        ); CHKERRQ(rval);
      }
      Tag th_layer_number;
      rval = getVolumeFE()->mField.get_moab().tag_get_handle("LAYER_NUMBER",th_layer_number); CHKERRQ_MOAB(rval);
      if(rval==MB_SUCCESS) {
        double layer_number;
        rval = getVolumeFE()->mField.get_moab().tag_get_data(th_layer_number,&ent,1,&layer_number); CHKERRQ_MOAB(rval);
        // cerr << error << endl;
        Tag th_post_proc_layer_number;
        int def_layer_number = 0;
        rval = postProcMesh.tag_get_handle(
          "LAYER_NUMBER",1,MB_TYPE_INTEGER,th_post_proc_layer_number,MB_TAG_CREAT|MB_TAG_SPARSE,&def_layer_number
        ); CHKERRQ_MOAB(rval);
        // cerr << post_proc_elem << endl;
        rval = postProcMesh.tag_set_data(
          th_post_proc_layer_number,&post_proc_elem,1,&layer_number
        ); CHKERRQ(rval);
      }
      Tag th_order;
      rval = getVolumeFE()->mField.get_moab().tag_get_handle("ADAPT_ORDER",th_order);
      if(rval==MB_SUCCESS) {
        int order;
        rval = getVolumeFE()->mField.get_moab().tag_get_data(th_order,&ent,1,&order); CHKERRQ_MOAB(rval);
        Tag th_post_proc_order;
        double def_int_val = 0;
        rval = postProcMesh.tag_get_handle(
          "ORDER_ADAPT",1,MB_TYPE_INTEGER,th_post_proc_order,MB_TAG_CREAT|MB_TAG_SPARSE,&def_int_val
        ); CHKERRQ_MOAB(rval);
        rval = postProcMesh.tag_set_data(
          th_post_proc_order,&post_proc_elem,1,&order
        ); CHKERRQ(rval);
      }
      int nb_gauss_pts = data.getN().size1();
      if(mapGaussPts.size()!=(unsigned int)nb_gauss_pts) {
        SETERRQ2(
          PETSC_COMM_SELF,
          MOFEM_DATA_INCONSISTENCY,
          "data inconsistency %d!=%d",
          mapGaussPts.size(),nb_gauss_pts
        );
      }
      double stress_vals[9],strain_vals[9];
      for(int gg = 0;gg<nb_gauss_pts;gg++) {
        if(mapGaussPts[gg]==0) continue;
        //strain
        for(int ii = 0;ii<3;ii++) {
          strain_vals[3*ii+ii] = commonData.sTrain(gg,ii);
        }
        strain_vals[0*3+1] = strain_vals[1*3+0] = 0.5*commonData.sTrain(gg,3);
        strain_vals[1*3+2] = strain_vals[2*3+1] = 0.5*commonData.sTrain(gg,4);
        strain_vals[0*3+2] = strain_vals[2*3+0] = 0.5*commonData.sTrain(gg,5);
        rval = postProcMesh.tag_set_data(
          th_strain,&mapGaussPts[gg],1,strain_vals
        ); CHKERRQ_MOAB(rval);
        //stress
        for(int ii = 0;ii<3;ii++) {
          stress_vals[3*ii+ii] = commonData.sTress(gg,ii);
        }
        stress_vals[0*3+1] = stress_vals[1*3+0] = commonData.sTress(gg,3);
        stress_vals[1*3+2] = stress_vals[2*3+1] = commonData.sTress(gg,4);
        stress_vals[0*3+2] = stress_vals[2*3+0] = commonData.sTress(gg,5);
        rval = postProcMesh.tag_set_data(
          th_stress,&mapGaussPts[gg],1,stress_vals
        ); CHKERRQ_MOAB(rval);
      }
    }

  } catch (exception& ex) {
    ostringstream ss;
    ss << "thorw in method: " << ex.what() << " at line " << __LINE__ << " in file " << __FILE__;
    SETERRQ(PETSC_COMM_SELF,MOFEM_STD_EXCEPTION_THROW,ss.str().c_str());
  }
  PetscFunctionReturn(0);
}

PetscErrorCode SolidShellPrismElement::OpPostProcMeshNodeDispalcements::doWork(
  int side,EntityType type,DataForcesAndSourcesCore::EntData &data
) {
  PetscFunctionBegin;
  try {
    Tag th_local_disp,th_global_disp;

    if(type == MBVERTEX) {

      EntityHandle ent = getNumeredEntFiniteElementPtr()->getEnt();

      double def_VAL[3];
      bzero(def_VAL,3*sizeof(double));
      rval = postProcMesh.tag_get_handle(
        "LOCAL_DISP",3,MB_TYPE_DOUBLE,th_local_disp,MB_TAG_CREAT|MB_TAG_SPARSE,def_VAL
      ); CHKERRQ_MOAB(rval);
      rval = postProcMesh.tag_get_handle(
        "GLOBAL_DISP",3,MB_TYPE_DOUBLE,th_global_disp,MB_TAG_CREAT|MB_TAG_SPARSE,def_VAL
      ); CHKERRQ_MOAB(rval);

      Tag th_post_proc_layer_number;
      double layer_number;
      {
        Tag th_layer_number;
        rval = getVolumeFE()->mField.get_moab().tag_get_handle("LAYER_NUMBER",th_layer_number); CHKERRQ_MOAB(rval);
        if(rval==MB_SUCCESS) {
          rval = getVolumeFE()->mField.get_moab().tag_get_data(th_layer_number,&ent,1,&layer_number); CHKERRQ_MOAB(rval);
          // cerr << error << endl;
          int def_layer_number = 0;
          rval = postProcMesh.tag_get_handle(
            "LAYER_NUMBER",1,MB_TYPE_INTEGER,th_post_proc_layer_number,MB_TAG_CREAT|MB_TAG_SPARSE,&def_layer_number
          ); CHKERRQ_MOAB(rval);
          // cerr << post_proc_elem << endl;
        }
      }

      int nb_gauss_pts = data.getN().size1();
      if(mapGaussPts.size()!=(unsigned int)nb_gauss_pts) {
        SETERRQ2(
          PETSC_COMM_SELF,
          MOFEM_DATA_INCONSISTENCY,
          "data inconsistency %d!=%d",
          mapGaussPts.size(),nb_gauss_pts
        );
      }
      // cerr << commonData.localDisplacementsAtGaussPts << endl;
      // cerr << commonData.globalDisplacentAtGaussPts << endl;
      double *loc_disp_ptr = &commonData.localDisplacementsAtGaussPts(0,0);
      double *glob_disp_ptr = &commonData.globalDisplacentAtGaussPts(0,0);
      for(int gg = 0;gg<nb_gauss_pts;gg++) {
        if(mapGaussPts[gg]==0) continue;
        rval = postProcMesh.tag_set_data(
          th_local_disp,&mapGaussPts[gg],1,&loc_disp_ptr[3*gg]
        ); CHKERRQ_MOAB(rval);
        rval = postProcMesh.tag_set_data(
          th_global_disp,&mapGaussPts[gg],1,&glob_disp_ptr[3*gg]
        ); CHKERRQ_MOAB(rval);
        rval = postProcMesh.tag_set_data(
          th_post_proc_layer_number,&mapGaussPts[gg],1,&layer_number
        ); CHKERRQ(rval);
      }
    }
  } catch (exception& ex) {
    ostringstream ss;
    ss << "thorw in method: " << ex.what() << " at line " << __LINE__ << " in file " << __FILE__;
    SETERRQ(PETSC_COMM_SELF,MOFEM_STD_EXCEPTION_THROW,ss.str().c_str());
  }
  PetscFunctionReturn(0);
}

PetscErrorCode SolidShellPrismElement::OpPostProcMeshNodePositions::doWork(
  int side,EntityType type,DataForcesAndSourcesCore::EntData &data
) {
  PetscFunctionBegin;
  try {
    Tag th_mesh_node_positions;

    if(type == MBVERTEX) {
      double def_VAL[3];
      bzero(def_VAL,3*sizeof(double));
      rval = postProcMesh.tag_get_handle(
        "MESH_NODE_POSITIONS",3,MB_TYPE_DOUBLE,th_mesh_node_positions,MB_TAG_CREAT|MB_TAG_SPARSE,def_VAL
      ); CHKERRQ_MOAB(rval);
      int nb_gauss_pts = data.getN().size1();
      if(mapGaussPts.size()!=(unsigned int)nb_gauss_pts) {
        SETERRQ2(
          PETSC_COMM_SELF,
          MOFEM_DATA_INCONSISTENCY,
          "data inconsistency %d!=%d",
          mapGaussPts.size(),nb_gauss_pts
        );
      }
      double *positon_ptr = &commonData.positionsAtGaussPts(0,0);
      for(int gg = 0;gg<nb_gauss_pts;gg++) {
        if(mapGaussPts[gg]==0) continue;
        rval = postProcMesh.tag_set_data(
          th_mesh_node_positions,&mapGaussPts[gg],1,&positon_ptr[3*gg]
        ); CHKERRQ_MOAB(rval);
      }
    }
  } catch (exception& ex) {
    ostringstream ss;
    ss << "thorw in method: " << ex.what() << " at line " << __LINE__ << " in file " << __FILE__;
    SETERRQ(PETSC_COMM_SELF,MOFEM_STD_EXCEPTION_THROW,ss.str().c_str());
  }
  PetscFunctionReturn(0);
}

PetscErrorCode
SolidShellPrismElement::PostProcFatPrismOnTriangleOnRefinedMesh::generateReferenceElementMesh() {
  PetscFunctionBegin;
  {
    gaussPtsTrianglesOnly.resize(3,6,false);
    gaussPtsTrianglesOnly.clear();
    gaussPtsTrianglesOnly(0,0) = 0;
    gaussPtsTrianglesOnly(1,0) = 0;
    gaussPtsTrianglesOnly(0,1) = 1;
    gaussPtsTrianglesOnly(1,1) = 0;
    gaussPtsTrianglesOnly(0,2) = 0;
    gaussPtsTrianglesOnly(1,2) = 1;
    gaussPtsTrianglesOnly(0,3) = 0.5;
    gaussPtsTrianglesOnly(1,3) = 0.0;
    gaussPtsTrianglesOnly(0,4) = 0.5;
    gaussPtsTrianglesOnly(1,4) = 0.5;
    gaussPtsTrianglesOnly(0,5) = 0.0;
    gaussPtsTrianglesOnly(1,5) = 0.5;
  }
  PetscFunctionReturn(0);
}

PetscErrorCode
SolidShellPrismElement::PostProcFatPrismOnTriangleOnRefinedMesh::setGaussPtsTrianglesOnly(
  int order_triangles_only
) {
  PetscFunctionBegin;
  if(gaussPtsTrianglesOnly.size1()==0 || gaussPtsTrianglesOnly.size2()==0) {
    SETERRQ(PETSC_COMM_SELF,MOFEM_DATA_INCONSISTENCY,"post-process mesh not generated");
  }

  // FIXME: Use 3 points to integrate through thickness
  int nb_gauss_pts_through_thickness = 3;
  gaussPtsThroughThickness.resize(2,nb_gauss_pts_through_thickness,false);
  gaussPtsThroughThickness(0,0) = 0;
  gaussPtsThroughThickness(1,0) = 8./9.;
  gaussPtsThroughThickness(0,1) = +sqrt(3./5.);
  gaussPtsThroughThickness(1,1) = 5./9.;
  gaussPtsThroughThickness(0,2) = -sqrt(3./5.);
  gaussPtsThroughThickness(1,2) = 5./9.;
  for(int nn = 0;nn<3;nn++) {
    gaussPtsThroughThickness(0,nn) += 1;
    gaussPtsThroughThickness(0,nn) *= 0.5;
    gaussPtsThroughThickness(1,nn) *= 0.5;
  }


  const EntityHandle *conn;
  int num_nodes;
  EntityHandle tri;

  if(
    elementsMap.find(numeredEntFiniteElementPtr->getEnt())!=elementsMap.end()
  ) {
    tri = elementsMap[numeredEntFiniteElementPtr->getEnt()];
  } else  {

    ublas::vector<EntityHandle> tri_conn(6);
    MatrixDouble coords_tri(6,3);
    VectorDouble coords(3);
    rval = mField.get_moab().get_connectivity(numeredEntFiniteElementPtr->getEnt(),conn,num_nodes,true); CHKERRQ_MOAB(rval);
    rval = mField.get_moab().get_coords(conn,num_nodes,&coords_tri(0,0));
    for(int ggf = 0;ggf!=3;ggf++) {
      double ksi = gaussPtsTrianglesOnly(0,ggf);
      double eta = gaussPtsTrianglesOnly(1,ggf);
      double n0 = N_MBTRI0(ksi,eta);
      double n1 = N_MBTRI1(ksi,eta);
      double n2 = N_MBTRI2(ksi,eta);
      for(int dd = 0;dd<3;dd++) {
        coords[dd] = n0*coords_tri(0,dd)+n1*coords_tri(1,dd)+n2*coords_tri(2,dd);
      }
      rval = postProcMesh.create_vertex(&coords[0],tri_conn[ggf]); CHKERRQ_MOAB(rval);
    }
    rval = postProcMesh.create_element(MBTRI,&tri_conn[0],3,tri); CHKERRQ_MOAB(rval);

    elementsMap[numeredEntFiniteElementPtr->getEnt()] = tri;

    Range edges;
    rval = postProcMesh.get_adjacencies(&tri,1,1,true,edges); CHKERRQ_MOAB(rval);
    EntityHandle meshset;
    rval = postProcMesh.create_meshset(MESHSET_SET,meshset); CHKERRQ_MOAB(rval);
    rval = postProcMesh.add_entities(meshset,&tri,1); CHKERRQ_MOAB(rval);
    rval = postProcMesh.add_entities(meshset,edges); CHKERRQ_MOAB(rval);
    if(tenNodesPostProcTets) {
      rval = postProcMesh.convert_entities(meshset,true,false,false); CHKERRQ_MOAB(rval);
    }
    rval = postProcMesh.delete_entities(&meshset,1); CHKERRQ_MOAB(rval);
    rval = postProcMesh.delete_entities(edges); CHKERRQ_MOAB(rval);
  }
  {
    rval = postProcMesh.get_connectivity(tri,conn,num_nodes,false); CHKERRQ_MOAB(rval);
    mapGaussPts.resize(gaussPtsTrianglesOnly.size2()*gaussPtsThroughThickness.size2());
    for(unsigned int nn = 0;nn<mapGaussPts.size();nn++) {
      mapGaussPts[nn] = 0; // only nodes on triangle will be visible
    }
    for(int nn = 0;nn<num_nodes;nn++) {
      // set nodes on the triangle
      mapGaussPts[nn*nb_gauss_pts_through_thickness] = conn[nn];
    }
  }
  PetscFunctionReturn(0);
}

PetscErrorCode
SolidShellPrismElement::PostProcFatPrismOnTriangleOnRefinedMesh::setGaussPtsThroughThickness(
  int order_thickness
)  {
  PetscFunctionBegin;
  if(gaussPtsTrianglesOnly.size1()==0 || gaussPtsTrianglesOnly.size2()==0) {
    SETERRQ(PETSC_COMM_SELF,MOFEM_DATA_INCONSISTENCY,"post-process mesh not generated");
  }
  PetscFunctionReturn(0);
}

PetscErrorCode SolidShellPrismElement::PostProcFatPrismOnTriangleOnRefinedMesh::postProcess() {
  PetscFunctionBegin;
  ParallelComm* pcomm = ParallelComm::get_pcomm(&mField.get_moab(),MYPCOMM_INDEX);
  ParallelComm* pcomm_post_proc_mesh = ParallelComm::get_pcomm(&postProcMesh,MYPCOMM_INDEX);
  if(pcomm_post_proc_mesh == NULL) {
    pcomm_post_proc_mesh = new ParallelComm(&postProcMesh,mField.get_comm());
    Range tris;
    rval = postProcMesh.get_entities_by_type(0,MBTRI,tris,false);  CHKERRQ_MOAB(rval);
    //cerr << "total tris size " << tris.size() << endl;
    int rank = pcomm->rank();
    Range::iterator tit = tris.begin();
    for(;tit!=tris.end();tit++) {
      rval = postProcMesh.tag_set_data(
        pcomm_post_proc_mesh->part_tag(),&*tit,1,&rank
      ); CHKERRQ_MOAB(rval);
    }
    // rval = pcomm->resolve_shared_ents(0); CHKERRQ_MOAB(rval);
  }
  PetscFunctionReturn(0);
}

PetscErrorCode SolidShellPrismElement::OpGetAxialForces::doWork(
  int side,EntityType type,DataForcesAndSourcesCore::EntData &data
) {
  PetscFunctionBegin;
  try {

    if(type!=MBVERTEX) {
      PetscFunctionReturn(0);
    }


    double def_VAL[3];
    bzero(def_VAL,3*sizeof(double));
    Tag th_axial,th_shear,th_normal,th_loc0,th_loc1;
    rval = postProcMesh.tag_get_handle(
      "AXIAL_FORCES",3,MB_TYPE_DOUBLE,th_axial,MB_TAG_CREAT|MB_TAG_SPARSE,def_VAL
    ); CHKERRQ_MOAB(rval);
    rval = postProcMesh.tag_get_handle(
      "SHEAR_FORCES",3,MB_TYPE_DOUBLE,th_shear,MB_TAG_CREAT|MB_TAG_SPARSE,def_VAL
    ); CHKERRQ_MOAB(rval);
    rval = postProcMesh.tag_get_handle(
      "NORMAL",3,MB_TYPE_DOUBLE,th_normal,MB_TAG_CREAT|MB_TAG_SPARSE,def_VAL
    ); CHKERRQ_MOAB(rval);
    rval = postProcMesh.tag_get_handle(
      "LOC0",3,MB_TYPE_DOUBLE,th_loc0,MB_TAG_CREAT|MB_TAG_SPARSE,def_VAL
    ); CHKERRQ_MOAB(rval);
    rval = postProcMesh.tag_get_handle(
      "LOC1",3,MB_TYPE_DOUBLE,th_loc1,MB_TAG_CREAT|MB_TAG_SPARSE,def_VAL
    ); CHKERRQ_MOAB(rval);
    int nb_gauss_pts_on_faces = getGaussPtsTrianglesOnly().size2();
    int nb_gauss_pts_through_thickness = getGaussPtsThroughThickness().size2();
    aXial.resize(nb_gauss_pts_on_faces,3,false);
    aXial.clear();
    sHear.resize(nb_gauss_pts_on_faces,3,false);
    sHear.clear();
    int nb_gauss_pts = data.getN().size1();
    if(mapGaussPts.size()!=(unsigned int)nb_gauss_pts) {
      // cerr << side << " " << type << endl;
      SETERRQ2(
        PETSC_COMM_SELF,
        MOFEM_DATA_INCONSISTENCY,
        "data inconsistency %d!=%d",
        mapGaussPts.size(),nb_gauss_pts
      );
    }
    commonData.localStress.resize(nb_gauss_pts);

    {
      int gg = 0;
      for(int ggf = 0;ggf<nb_gauss_pts_on_faces;ggf++) {
        for(int ggt = 0;ggt<nb_gauss_pts_through_thickness;ggt++,gg++) {
          {
            commonData.localStress[gg].resize(3,3,false);
            for(int ii = 0;ii<3;ii++) {
              commonData.localStress[gg](ii,ii) = commonData.sTress(gg,ii);
            }
            commonData.localStress[gg](0,1) = commonData.localStress[gg](1,0) = commonData.sTress(gg,3);
            commonData.localStress[gg](1,2) = commonData.localStress[gg](2,1) = commonData.sTress(gg,4);
            commonData.localStress[gg](0,2) = commonData.localStress[gg](2,0) = commonData.sTress(gg,5);
          }
          aXial(ggf,NX) +=
          getGaussPtsThroughThickness()(1,ggt)*commonData.tHickness*commonData.localStress[gg](0,0);
          aXial(ggf,NY) +=
          getGaussPtsThroughThickness()(1,ggt)*commonData.tHickness*commonData.localStress[gg](1,1);
          aXial(ggf,NXY) +=
          getGaussPtsThroughThickness()(1,ggt)*commonData.tHickness*commonData.localStress[gg](0,1);
          sHear(ggf,TZ) +=
          getGaussPtsThroughThickness()(1,ggt)*commonData.tHickness*commonData.localStress[gg](2,2);
          sHear(ggf,TXZ) +=
          getGaussPtsThroughThickness()(1,ggt)*commonData.tHickness*commonData.localStress[gg](0,2);
          sHear(ggf,TYZ) +=
          getGaussPtsThroughThickness()(1,ggt)*commonData.tHickness*commonData.localStress[gg](1,2);
        }
      }
      // cerr << aXial << endl;
      // cerr << sHear << endl;
    }

    // Set values which map nodes with integration points on the prism
    {
      int gg = 0;
      for(int ggf = 0;ggf<nb_gauss_pts_on_faces;ggf++) {
        double normal[] = {
          // getNormalsAtGaussPtF3()(ggf,0), getNormalsAtGaussPtF3()(ggf,1), getNormalsAtGaussPtF3()(ggf,2)
          commonData.mEAI[ggf](2,0),commonData.mEAI[ggf](2,1),commonData.mEAI[ggf](2,2)
        };
        double nrm2 = cblas_dnrm2(3,normal,1);
        cblas_dscal(3,1./nrm2,normal,1);
        double loc0[] = {
          commonData.mEAI[ggf](0,0),
          commonData.mEAI[ggf](0,1),
          commonData.mEAI[ggf](0,2)
        };
        double loc1[] = {
          commonData.mEAI[ggf](1,0),
          commonData.mEAI[ggf](1,1),
          commonData.mEAI[ggf](1,2)
        };
        for(int ggt = 0;ggt<nb_gauss_pts_through_thickness;ggt++,gg++) {
          if(mapGaussPts[gg]==0) continue;
          rval = postProcMesh.tag_set_data(th_axial,&mapGaussPts[gg],1,&aXial(ggf,0)); CHKERRQ_MOAB(rval);
          rval = postProcMesh.tag_set_data(th_shear,&mapGaussPts[gg],1,&sHear(ggf,0)); CHKERRQ_MOAB(rval);
          rval = postProcMesh.tag_set_data(th_normal,&mapGaussPts[gg],1,&normal); CHKERRQ_MOAB(rval);
          rval = postProcMesh.tag_set_data(th_loc0,&mapGaussPts[gg],1,&loc0); CHKERRQ_MOAB(rval);
          rval = postProcMesh.tag_set_data(th_loc1,&mapGaussPts[gg],1,&loc1); CHKERRQ_MOAB(rval);
        }
      }
    }

  } catch (exception& ex) {
    ostringstream ss;
    ss << "thorw in method: " << ex.what() << " at line " << __LINE__ << " in file " << __FILE__;
    SETERRQ(PETSC_COMM_SELF,MOFEM_STD_EXCEPTION_THROW,ss.str().c_str());
  }
  PetscFunctionReturn(0);
}

PetscErrorCode SolidShellPrismElement::OpGetMoment::doWork(
  int side,EntityType type,DataForcesAndSourcesCore::EntData &data
) {
  PetscFunctionBegin;
  try {

    if(type!=MBVERTEX) {
      PetscFunctionReturn(0);
    }


    double def_VAL[3];
    bzero(def_VAL,3*sizeof(double));
    Tag th_moment;
    rval = postProcMesh.tag_get_handle(
      "MOMENT",3,MB_TYPE_DOUBLE,th_moment,MB_TAG_CREAT|MB_TAG_SPARSE,def_VAL
    ); CHKERRQ_MOAB(rval);

    int nb_gauss_pts_on_faces = getGaussPtsTrianglesOnly().size2();
    int nb_gauss_pts_through_thickness = getGaussPtsThroughThickness().size2();
    mOment.resize(nb_gauss_pts_on_faces,3,false);
    mOment.clear();

    int nb_gauss_pts = data.getN().size1();
    if(mapGaussPts.size()!=(unsigned int)nb_gauss_pts) {
      // cerr << side << " " << type << endl;
      SETERRQ2(
        PETSC_COMM_SELF,
        MOFEM_DATA_INCONSISTENCY,
        "data inconsistency %d!=%d",
        mapGaussPts.size(),nb_gauss_pts
      );
    }

    {
      int gg = 0;
      for(int ggf = 0;ggf<nb_gauss_pts_on_faces;ggf++) {
        for(int ggt = 0;ggt<nb_gauss_pts_through_thickness;ggt++,gg++) {
          double h = commonData.tHickness;
          double zeta = getGaussPtsThroughThickness()(0,ggt)*h;
          mOment(ggf,MX) +=
          getGaussPtsThroughThickness()(1,ggt)*h*commonData.localStress[gg](0,0)*zeta;
          mOment(ggf,MY) +=
          getGaussPtsThroughThickness()(1,ggt)*h*commonData.localStress[gg](1,1)*zeta;
          mOment(ggf,MXY) +=
          getGaussPtsThroughThickness()(1,ggt)*h*commonData.localStress[gg](0,1)*zeta;
        }
      }
    }

    {
      int gg = 0;
      for(int ggf = 0;ggf<nb_gauss_pts_on_faces;ggf++) {
        for(int ggt = 0;ggt<nb_gauss_pts_through_thickness;ggt++,gg++) {
          if(mapGaussPts[gg]==0) continue;
          rval = postProcMesh.tag_set_data(
            th_moment,&mapGaussPts[gg],1,&mOment(ggf,0)
          ); CHKERRQ_MOAB(rval);
        }
      }
    }

  } catch (exception& ex) {
    ostringstream ss;
    ss << "thorw in method: " << ex.what() << " at line " << __LINE__ << " in file " << __FILE__;
    SETERRQ(PETSC_COMM_SELF,MOFEM_STD_EXCEPTION_THROW,ss.str().c_str());
  }
  PetscFunctionReturn(0);
}

PetscErrorCode SolidShellPrismElement::OpGetFCartesian::doWork(
  int side,EntityType type,DataForcesAndSourcesCore::EntData &data
) {
  PetscFunctionBegin;

  try {
    if(data.getFieldData().size() == 0) PetscFunctionReturn(0);
    int nb_gauss_pts = data.getN().size1();
    if(type == MBVERTEX) {
      commonData.mGrad1.resize(nb_gauss_pts);
      // commonData.mGrad2.resize(nb_gauss_pts);
      for(int gg = 0;gg<nb_gauss_pts;gg++) {
        commonData.mGrad1[gg].resize(3,3,false);
        commonData.mGrad1[gg].clear();
      }
    }
    int nb_dofs = data.getFieldData().size();
    int rank = data.getFieldDofs()[0]->getNbOfCoeffs();
    double *data_ptr = &data.getFieldData()[0];
    for(int gg = 0;gg<nb_gauss_pts;gg++) {
      const MatrixAdaptor &diff_n = MatrixAdaptor(
        nb_dofs/rank,3,ublas::shallow_array_adaptor<double>(
          3*nb_dofs/rank,&commonData.diffGlobN[type][side](gg,0)
        )
      );
      ierr = makeFDisp(diff_n,mF); CHKERRQ(ierr);
      double *f_ptr = &commonData.mGrad1[gg](0,0);
      cblas_dgemv(
        CblasRowMajor,CblasTrans,nb_dofs,9,1,&mF(0,0),9,data_ptr,1,1,f_ptr,1
      );
    }
  } catch (const std::exception& ex) {
    ostringstream ss;
    ss << "throw in method: " << ex.what() << endl;
    SETERRQ(PETSC_COMM_SELF,1,ss.str().c_str());
  }
  PetscFunctionReturn(0);
}

PetscErrorCode SolidShellPrismElement::OpGetFLocal::doWork(
  int side,EntityType type,DataForcesAndSourcesCore::EntData &data
) {
  PetscFunctionBegin;

  try {
    int nb_gauss_pts = data.getN().size1();
    if(type == MBVERTEX && zEro) {
      commonData.mGrad2.resize(nb_gauss_pts);
      for(int gg = 0;gg<nb_gauss_pts;gg++) {
        commonData.mGrad2[gg].resize(3,3,false);
        commonData.mGrad2[gg].clear();
      }
    }
    if(data.getFieldData().size() == 0) PetscFunctionReturn(0);
    int nb_dofs = data.getFieldData().size();
    int rank = data.getFieldDofs()[0]->getNbOfCoeffs();
    mF.resize(nb_dofs,9,false);
    double *data_ptr = &data.getFieldData()[0];
    for(int gg = 0;gg<nb_gauss_pts;gg++) {
      const MatrixAdaptor &diff_n = data.getDiffN(gg,nb_dofs/rank);
      if(rank == 1) {
        ierr = makeFZeta(diff_n,mF); CHKERRQ(ierr);
      } else {
        ierr = makeFKsiEta(diff_n,mF); CHKERRQ(ierr);
      }
      double *f_ptr = &commonData.mGrad2[gg](0,0);
      cblas_dgemv(
        CblasRowMajor,CblasTrans,nb_dofs,9,1,&mF(0,0),9,data_ptr,1,1,f_ptr,1
      );
    }
  } catch (const std::exception& ex) {
    ostringstream ss;
    ss << "throw in method: " << ex.what() << endl;
    SETERRQ(PETSC_COMM_SELF,1,ss.str().c_str());
  }
  PetscFunctionReturn(0);
}

#ifdef WITH_ADOL_C

PetscErrorCode SolidShellPrismElement::OpEvaluatePiolaStress::doWork(
  int side,EntityType type,DataForcesAndSourcesCore::EntData &data
) {
  PetscFunctionBegin;
  try {
    if(type == MBVERTEX) {
      int nb_gauss_pts = data.getN().size1();
      int nb_gauss_pts_on_faces = getGaussPtsTrianglesOnly().size2();
      int nb_gauss_pts_through_thickness = getGaussPtsThroughThickness().size2();
      commonData.globalPiolaStressIi_I.resize(nb_gauss_pts);
      commonData.localPiolaStressIA_a.resize(nb_gauss_pts);
      commonData.tangentOfPiolaStressGlobalGlobal.resize(nb_gauss_pts);
      commonData.tangentOfPiolaStressLocalLocal.resize(nb_gauss_pts);
      commonData.tangentOfPiolaStressLocalGlobal.resize(nb_gauss_pts);
      commonData.tangentOfPiolaStressGlobalLocal.resize(nb_gauss_pts);
      VectorDouble weight_vector(9);
      int gg = 0;
      for(int ggf = 0;ggf<nb_gauss_pts_on_faces;ggf++) {
        for(int ggt = 0;ggt<nb_gauss_pts_through_thickness;ggt++,gg++) {
          const bool case_array[4][2] = {
            {true,true}, {false,true}, {true,false}, {false,false}
          };
          MatrixDouble *mat_ref_array[4] = {
            &(commonData.tangentOfPiolaStressGlobalGlobal[gg]),
            &(commonData.tangentOfPiolaStressLocalGlobal[gg]),
            &(commonData.tangentOfPiolaStressGlobalLocal[gg]),
            &(commonData.tangentOfPiolaStressLocalLocal[gg]),
          };
          int ii_end = (orderThickness > 1 ) ? 4 : 1;
          if(!getTangent) {
            ii_end = (ii_end > 1) ? 2 : 1;
          }
          for(int ii = 0;ii<ii_end;ii++) {
            if(!getTangent) {
              ierr = initializeData<double>(commonData.dDouble);
              if(case_array[ii][1]) {
                noalias(commonData.dDouble.mGrad1) = commonData.mGrad1[gg];
              } else {
                noalias(commonData.dDouble.mGrad2) = commonData.mGrad2[gg];
              }
              ierr = evaluateForward<double>(
                commonData.dDouble,ggf,ggt,gg,case_array[ii][0],case_array[ii][1]
              ); CHKERRQ(ierr);
              if(case_array[ii][0]) {
                commonData.globalPiolaStressIi_I[gg].resize(3,3,false);
                noalias(commonData.globalPiolaStressIi_I[gg]) = commonData.dDouble.globalPiolaStressIi_I;
              } else {
                commonData.localPiolaStressIA_a[gg].resize(3,3,false);
                noalias(commonData.localPiolaStressIA_a[gg]) =  commonData.dDouble.localPiolaStressIA_a;
              }
            }
            if(getTangent) {
              ierr = initializeData<adouble>(commonData.aDouble);
              trace_on(tAg+ii,1);
              if(case_array[ii][1]) {
                for(int ii = 0;ii!=3;ii++) {
                  for(int II = 0;II!=3;II++) {
                    commonData.aDouble.mGrad1(ii,II) <<= commonData.mGrad1[gg](ii,II);
                  }
                }
              } else {
                for(int aa = 0;aa!=3;aa++) {
                  for(int AA = 0;AA!=3;AA++) {
                    commonData.aDouble.mGrad2(aa,AA) <<= commonData.mGrad2[gg](aa,AA);
                  }
                }
              }
              ierr = evaluateForward<adouble>(
                commonData.aDouble,ggf,ggt,gg,case_array[ii][0],case_array[ii][1]
              ); CHKERRQ(ierr);
              if(case_array[ii][0]) {
                commonData.globalPiolaStressIi_I[gg].resize(3,3,false);
                for(int ii = 0;ii!=3;ii++) {
                  for(int II = 0;II!=3;II++) {
                    commonData.aDouble.globalPiolaStressIi_I(ii,II) >>= commonData.globalPiolaStressIi_I[gg](ii,II);
                  }
                }
              } else {
                commonData.localPiolaStressIA_a[gg].resize(3,3,false);
                for(int aa = 0;aa!=3;aa++) {
                  for(int AA = 0;AA!=3;AA++) {
                    commonData.aDouble.localPiolaStressIA_a(aa,AA) >>= commonData.localPiolaStressIA_a[gg](aa,AA);
                  }
                }
              }
              trace_off();
              (*mat_ref_array[ii]).resize(9,9,false);
              for(int rr = 0;rr<9;rr++) {
                weight_vector.clear();
                weight_vector[rr] = 1;
                vec_jac(
                  tAg+ii,9,9,1,
                  &commonData.mGrad1[gg](0,0),
                  &weight_vector[0],
                  &(*mat_ref_array[ii])(rr,0)
                );
              }
            }
          }
        }
      }
    }
  } catch (const std::exception& ex) {
    ostringstream ss;
    ss << "throw in method: " << ex.what() << endl;
    SETERRQ(PETSC_COMM_SELF,MOFEM_STD_EXCEPTION_THROW,ss.str().c_str());
  }
  doVertices = true;
  doEdges = false;
  doQuads = false;
  doTris = false;
  doTets = false;
  doPrisms = false;
  PetscFunctionReturn(0);
}

PetscErrorCode SolidShellPrismElement::OpAssembleLhsPiolaStress::doWork(
  int row_side,int col_side,
  EntityType row_type,EntityType col_type,
  DataForcesAndSourcesCore::EntData &row_data,
  DataForcesAndSourcesCore::EntData &col_data
) {
  PetscFunctionBegin;

  try {
    int nb_dofs_row = row_data.getIndices().size();
    if(nb_dofs_row==0) PetscFunctionReturn(0);
    int nb_dofs_col = col_data.getIndices().size();
    if(nb_dofs_col==0) PetscFunctionReturn(0);
    mK.resize(nb_dofs_row,nb_dofs_col,false);
    mK.clear();
    dP.resize(nb_dofs_row,9,false);
    int row_rank = row_data.getFieldDofs()[0]->getNbOfCoeffs();
    int col_rank = col_data.getFieldDofs()[0]->getNbOfCoeffs();
    // int nb_gauss_pts = row_data.getN().size1();
    int nb_gauss_pts_on_faces = getGaussPtsTrianglesOnly().size2();
    int nb_gauss_pts_through_thickness = getGaussPtsThroughThickness().size2();
    int *row_indices_ptr,*col_indices_ptr;
    row_indices_ptr = &row_data.getIndices()[0];
    col_indices_ptr = &col_data.getIndices()[0];
    int gg = 0;
    for(int ggf = 0;ggf<nb_gauss_pts_on_faces;ggf++) {
      for(int ggt = 0;ggt<nb_gauss_pts_through_thickness;ggt++,gg++) {
        double val = 0.5*commonData.jacDeterminantAtGaussPts[gg]*getGaussPts()(3,gg);
        if(row_rank == 3) {
          const MatrixAdaptor &diffN_row = MatrixAdaptor(
            nb_dofs_row/row_rank,3,ublas::shallow_array_adaptor<double>(
              3*nb_dofs_row/row_rank,&commonData.diffGlobN[row_type][row_side](gg,0)
            )
          );
          ierr = makeFDisp(diffN_row,mRowF); CHKERRQ(ierr);
        } else if(row_rank == 2) {
          const MatrixAdaptor &diffN_row = row_data.getDiffN(gg,nb_dofs_row/row_rank);
          ierr = makeFKsiEta(diffN_row,mRowF); CHKERRQ(ierr);
        } else {
          const MatrixAdaptor &diffN_row = row_data.getDiffN(gg,nb_dofs_row/row_rank);
          ierr = makeFZeta(diffN_row,mRowF); CHKERRQ(ierr);
        }
        if(col_rank == 3) {
          const MatrixAdaptor &diffN_col = MatrixAdaptor(
            nb_dofs_col/col_rank,3,ublas::shallow_array_adaptor<double>(
              3*nb_dofs_col/col_rank,&commonData.diffGlobN[col_type][col_side](gg,0)
            )
          );
          ierr = makeFDisp(diffN_col,mColF); CHKERRQ(ierr);
        } else if(col_rank == 2) {
          const MatrixAdaptor &diffN_col = col_data.getDiffN(gg,nb_dofs_col/col_rank);
          ierr = makeFKsiEta(diffN_col,mColF); CHKERRQ(ierr);
        } else {
          const MatrixAdaptor &diffN_col = col_data.getDiffN(gg,nb_dofs_col/col_rank);
          ierr = makeFZeta(diffN_col,mColF); CHKERRQ(ierr);
        }
        MatrixDouble *tangent_piola_stress;
        if(row_rank == 3 && col_rank == 3)
        tangent_piola_stress = &commonData.tangentOfPiolaStressGlobalGlobal[gg];
        else if(row_rank == 3 && col_rank != 3)
        tangent_piola_stress = &commonData.tangentOfPiolaStressGlobalLocal[gg];
        else if(row_rank != 3 && col_rank == 3)
        tangent_piola_stress = &commonData.tangentOfPiolaStressLocalGlobal[gg];
        else
        tangent_piola_stress = &commonData.tangentOfPiolaStressLocalLocal[gg];
        dP.clear();
        double *m_row_f_ptr = &mRowF(0,0);
        for(int rr = 0;rr!=9;rr++) {
          double *tangent_ptr = &(*tangent_piola_stress)(rr,0);
          int row_idx = 0;
          for(int dd = 0;dd!=nb_dofs_row;dd++) {
            double alpha = m_row_f_ptr[row_idx+rr];
            row_idx += 9;
            if(alpha==0) continue;
            double *piola_ptr = &dP(dd,0);
            cblas_daxpy(
              9,alpha,tangent_ptr,1,piola_ptr,1
            );
          }
        }
        cblas_dgemm(
          CblasRowMajor,CblasNoTrans,CblasTrans,
          nb_dofs_row,nb_dofs_col,9,
          val,&dP(0,0),9,&mColF(0,0),9,
          1,&mK(0,0),nb_dofs_col
        );
        // noalias(mK) += val*prod(mRowF,trans(dP));
        // if(gg == 0) {
        //   cerr << "mK" << endl;
        //   cerr << dP << endl;
        //   cerr << mK << endl;
        //   cerr << endl << endl << endl;
        // }
      }
    }
    // cerr << "mK" << endl;
    // cerr << mK << endl;
    // cerr << endl << endl << endl;
    ierr = MatSetValues(
      getFEMethod()->snes_B,
      nb_dofs_row,row_indices_ptr,
      nb_dofs_col,col_indices_ptr,
      &mK(0,0),ADD_VALUES
    ); CHKERRQ(ierr);
    if(assembleTranspose) {
      if(row_side != col_side || row_type != col_type || row_rank != col_rank) {
        mTransK.resize(nb_dofs_col,nb_dofs_row,false);
        noalias(mTransK) = trans(mK);
        ierr = MatSetValues(
          getFEMethod()->snes_B,
          nb_dofs_col,col_indices_ptr,
          nb_dofs_row,row_indices_ptr,
          &mTransK(0,0),ADD_VALUES
        ); CHKERRQ(ierr);
      }
    }
  } catch (const std::exception& ex) {
    ostringstream ss;
    ss << "throw in method: " << ex.what() << endl;
    SETERRQ(PETSC_COMM_SELF,1,ss.str().c_str());
  }
  PetscFunctionReturn(0);
}

PetscErrorCode SolidShellPrismElement::OpAssembleRhsPiolaStress::doWork(
  int side,EntityType type,DataForcesAndSourcesCore::EntData &data
) {
  PetscFunctionBegin;
  try {

    int nb_dofs = data.getIndices().size();
    if(nb_dofs==0) PetscFunctionReturn(0);
    vF.resize(nb_dofs,false);
    vF.clear();
    int rank = data.getFieldDofs()[0]->getNbOfCoeffs();
    int nb_gauss_pts = data.getN().size1();
    int nb_gauss_pts_on_faces = getGaussPtsTrianglesOnly().size2();
    int nb_gauss_pts_through_thickness = getGaussPtsThroughThickness().size2();
    commonData.globalPiolaStressIi_I.resize(nb_gauss_pts);
    commonData.localPiolaStressIA_a.resize(nb_gauss_pts);
    int gg = 0;
    for(int ggf = 0;ggf<nb_gauss_pts_on_faces;ggf++) {
      for(int ggt = 0;ggt<nb_gauss_pts_through_thickness;ggt++,gg++) {
        double val = 0.5*commonData.jacDeterminantAtGaussPts[gg]*getGaussPts()(3,gg);
        if(rank == 3) {
          const MatrixAdaptor &diffN = MatrixAdaptor(
            nb_dofs/rank,3,ublas::shallow_array_adaptor<double>(
              3*nb_dofs/rank,&commonData.diffGlobN[type][side](gg,0)
            )
          );
          ierr = makeFDisp(diffN,mF); CHKERRQ(ierr);
        } else if(rank == 2) {
          const MatrixAdaptor &diffN = data.getDiffN(gg,nb_dofs/rank);
          ierr = makeFKsiEta(diffN,mF); CHKERRQ(ierr);
        } else {
          const MatrixAdaptor &diffN = data.getDiffN(gg,nb_dofs/rank);
          ierr = makeFZeta(diffN,mF); CHKERRQ(ierr);
        }
        if(rank == 3) {
          VectorAdaptor piola_stress_vector = VectorAdaptor(
            9,ublas::shallow_array_adaptor<double>(
              9,&commonData.globalPiolaStressIi_I[gg](0,0)
            )
          );
          for(int dd = 0;dd<3;dd++) {
            if(piola_stress_vector[dd]!=piola_stress_vector[dd]) {
              SETERRQ(
                PETSC_COMM_SELF,
                MOFEM_DATA_INCONSISTENCY,
                "not a number in residual"
              );
            }
          }
          noalias(vF) += val*prod(mF,piola_stress_vector);
        } else {
          VectorAdaptor piola_stress_vector = VectorAdaptor(
            9,ublas::shallow_array_adaptor<double>(
              9,&commonData.localPiolaStressIA_a[gg](0,0)
            )
          );
          for(int dd = 0;dd<3;dd++) {
            if(piola_stress_vector[dd]!=piola_stress_vector[dd]) {
              SETERRQ(
                PETSC_COMM_SELF,
                MOFEM_DATA_INCONSISTENCY,
                "not a number in residual"
              );
            }
          }
          noalias(vF) += val*prod(mF,piola_stress_vector);
        }
      }
    }
    int *indices_ptr;
    indices_ptr = &data.getIndices()[0];
    ierr = VecSetValues(
      getFEMethod()->snes_f,
      nb_dofs,
      indices_ptr,
      &vF[0],
      ADD_VALUES
    ); CHKERRQ(ierr);
  } catch (const std::exception& ex) {
    ostringstream ss;
    ss << "throw in method: " << ex.what() << endl;
    SETERRQ(PETSC_COMM_SELF,MOFEM_STD_EXCEPTION_THROW,ss.str().c_str());
  }
  PetscFunctionReturn(0);
}

#endif //WITH_ADOL_C

PetscErrorCode SolidShellPrismElement::NormalApprox::getRotMatrix(VectorDouble &u,double phi) {
  PetscFunctionBegin;

  double phi_rad = phi*M_PI/180;
  double c = cos(phi_rad);
  double s = sin(phi_rad);
  double t = 1-c;

  double ux = u[0];
  double uy = u[1];
  double uz = u[2];

  rotMatrix.resize(3,3,false);

  rotMatrix(0,0) = t*ux*ux+c;
  rotMatrix(0,1) = t*ux*uy-s*uz;
  rotMatrix(0,2) = t*ux*uz+s*uy;

  rotMatrix(1,0) = t*ux*uy+s*uz;
  rotMatrix(1,1) = t*uy*uy+c;
  rotMatrix(1,2) = t*uy*uz-s*ux;

  rotMatrix(2,0) = t*ux*uz-s*uy;
  rotMatrix(2,1) = t*uy*uz+s*ux;
  rotMatrix(2,2) = t*uz*uz+c;

  PetscFunctionReturn(0);
}


vector<VectorDouble>& SolidShellPrismElement::NormalApprox::operator()(
  EntityHandle ent,
  double x,
  double y,
  double z,
  VectorDouble normal,
  VectorDouble tangent1,
  VectorDouble tangent2
) {
  Tag th_surface_parametrisation;

  mOab.tag_get_handle(
    "SURFACE_PARAMETRISATION",th_surface_parametrisation
  );
  surfaceParametrisation.resize(3,false);
  mOab.tag_get_data(
    th_surface_parametrisation,&ent,1,&surfaceParametrisation[0]
  );
  surfaceParametrisation /= norm_2(surfaceParametrisation);

  // cerr << "Aaaaa " << endl;
  // cerr << surfaceParametrisation << endl;

  normal /= norm_2(normal);
  tangent1 /= norm_2(tangent1);
  //tangent2 /= norm_2(tangent2);

  double nx = normal[0];
  double ny = normal[1];
  double nz = normal[2];
  // Local element coordinate system
  double t0x = tangent1[0];
  double t0y = tangent1[1];
  double t0z = tangent1[2];
  double t1x = -nz*t0y+ny*t0z;
  double t1y = +nz*t0x-nx*t0z;
  double t1z = -ny*t0x+nx*t0y;
  // Align local system with user direction of surface parametrization
  double loc1[] = { t0x, t0y, t0z };
  double loc2[] = { t1x, t1y, t1z };
  // double loc3[] = { nx, ny, nz };
  double s1 = cblas_ddot(3,&surfaceParametrisation[0],1,loc1,1);
  double s2 = cblas_ddot(3,&surfaceParametrisation[0],1,loc2,1);
  // double s3 = cblas_ddot(3,&surfaceParametrisation[0],1,loc3,1);
  t0x = tangent1[0] = loc1[0]*s1 + loc2[0]*s2;
  t0y = tangent1[1] = loc1[1]*s1 + loc2[1]*s2;
  t0z = tangent1[2] = loc1[2]*s1 + loc2[2]*s2;
  tangent2[0] = -nz*t0y+ny*t0z;
  tangent2[1] = +nz*t0x-nx*t0z;
  tangent2[2] = -ny*t0x+nx*t0y;
  rEsult.resize(9);
  for(int vv = 0;vv<9;vv++) {
    rEsult[vv].resize(1,false);
  }
  // cerr << tangent1 << " : " << tangent2 << endl;

  normal /= norm_2(normal);
  tangent1 /= norm_2(tangent1);
  tangent2 /= norm_2(tangent2);

  if(aNgle) {
    getRotMatrix(normal,aNgle);
    tangent1 = prod(rotMatrix,tangent1);
    tangent2 = prod(rotMatrix,tangent2);
  }

  for(int dd = 0;dd<3;dd++) {
    rEsult[0+dd][0] = normal[dd];
    rEsult[3+dd][0] = tangent1[dd];
    rEsult[6+dd][0] = tangent2[dd];
  }

  return rEsult;
}

#ifdef __ARC_LENGTH_TOOLS_HPP__

PetscErrorCode SolidShellPrismElement::OpGetIncrementOfDisplacements::doWork(
  int side,EntityType type,DataForcesAndSourcesCore::EntData &data
) {
  PetscFunctionBegin;
  try {

    int nb_dofs = data.getIndices().size();
    if(nb_dofs==0) PetscFunctionReturn(0);
    int nb_gauss_pts = data.getN().size1();
    if(type == MBVERTEX) {
      commonData.incrementsOfDisplacements.resize(nb_gauss_pts);
      // commonData.incrementsOfGradOfDisplacements.resize(nb_gauss_pts);
      for(int gg = 0;gg<nb_gauss_pts;gg++) {
        commonData.incrementsOfDisplacements[gg].resize(3,false);
        commonData.incrementsOfDisplacements[gg].clear();
        // commonData.incrementsOfGradOfDisplacements[gg].resize(3,3,false);
        // commonData.incrementsOfGradOfDisplacements[gg].clear();
      }
    }
    double *a_dx;
    ierr = VecGetArray(getFEMethod()->ts_u_t,&a_dx); CHKERRQ(ierr);
    for(int gg = 0;gg<nb_gauss_pts;gg++) {
      const VectorAdaptor &sf = data.getN(gg,nb_dofs/3);
      // const MatrixAdaptor &grad_sf = data.getDiffN(gg,nb_dofs/3);
      for(int dd = 0;dd<nb_dofs/3;dd++) {
        double n = sf[dd];
        for(int rr = 0;rr<3;rr++) {
          int idx = data.getLocalIndices()[3*dd+rr];
          double val = a_dx[idx];
          commonData.incrementsOfDisplacements[gg][rr] += n*val;
          // for(int dd = 0;dd<3;dd++) {
          //   commonData.incrementsOfGradOfDisplacements[gg](rr,dd) += grad_sf(rr,dd)*val;
          // }
        }
      }
    }
    ierr = VecRestoreArray(getFEMethod()->ts_u_t,&a_dx); CHKERRQ(ierr);
  } catch (exception& ex) {
    ostringstream ss;
    ss << "thorw in method: "
    << ex.what() << " at line "
    << __LINE__
    << " in file " << __FILE__;
    SETERRQ(PETSC_COMM_SELF,MOFEM_STD_EXCEPTION_THROW,ss.str().c_str());
  }
  // doVerticesRow = true;
  // doEdgesRow = false;
  // doQuadsRow = false;
  // doTrisRow = false;
  // doPrismsRow = false;
  PetscFunctionReturn(0);
}

PetscErrorCode SolidShellPrismElement::OpNumericalViscousDampingRhs::doWork(
  int side,EntityType type,DataForcesAndSourcesCore::EntData &data
) {
  PetscFunctionBegin;
  try {

    int nb_dofs = data.getIndices().size();
    if(nb_dofs == 0) PetscFunctionReturn(0);
    vC.resize(nb_dofs,false);
    vC.clear();
    int nb_gauss_pts = data.getN().size1();
    for(int gg = 0;gg<nb_gauss_pts;gg++) {
      double val = 0.5*commonData.jacDeterminantAtGaussPts[gg]*getGaussPts()(3,gg);
      const VectorAdaptor &N = data.getN(gg,nb_dofs/3);
      double a[3];
      val *= aLpha;
      for(int rr = 0;rr<3;rr++) {
        a[rr] = val*commonData.incrementsOfDisplacements[gg][rr];
      }
      for(int dd = 0;dd!=nb_dofs/3;dd++) {
        double vn = N[dd];
        for(int rr = 0;rr!=3;rr++) {
          vC[3*dd+rr] += vn*a[rr];
        }
      }
    }
    int *indices_ptr = &data.getIndices()[0];
    ierr = VecSetValues(
      getFEMethod()->ts_F,
      nb_dofs,
      indices_ptr,
      &vC[0],
      ADD_VALUES
    ); CHKERRQ(ierr);
  } catch (exception& ex) {
    ostringstream ss;
    ss << "thorw in method: "
    << ex.what()
    << " at line "
    << __LINE__
    << " in file " << __FILE__;
    SETERRQ(PETSC_COMM_SELF,MOFEM_STD_EXCEPTION_THROW,ss.str().c_str());
  }
  // doVerticesRow = true;
  // doEdgesRow = false;
  // doQuadsRow = false;
  // doTrisRow = false;
  // doPrismsRow = false;
  PetscFunctionReturn(0);
}

PetscErrorCode SolidShellPrismElement::OpNumericalViscousDampingLhs::doWork(
  int row_side,int col_side,
  EntityType row_type,EntityType col_type,
  DataForcesAndSourcesCore::EntData &row_data,
  DataForcesAndSourcesCore::EntData &col_data
) {
  PetscFunctionBegin;
  try {

    int nb_dofs_row = row_data.getIndices().size();
    if(nb_dofs_row==0) PetscFunctionReturn(0);
    int nb_dofs_col = col_data.getIndices().size();
    if(nb_dofs_col==0) PetscFunctionReturn(0);
    mC.resize(nb_dofs_row,nb_dofs_col,false);
    mC.clear();
    int nb_gauss_pts = row_data.getN().size1();
    for(int gg = 0;gg<nb_gauss_pts;gg++) {
      double val = 0.5*commonData.jacDeterminantAtGaussPts[gg]*getGaussPts()(3,gg);
      const VectorAdaptor &N_row = row_data.getN(gg,nb_dofs_row/3);
      const VectorAdaptor &N_col = col_data.getN(gg,nb_dofs_col/3);
      val *= aLpha;
      double a[3];
      for(int rr = 0;rr<3;rr++) {
        a[rr] = val;
      }
      for(int rdd = 0;rdd!=nb_dofs_row/3;rdd++) {
        double nr = N_row[rdd];
        for(int cdd = 0;cdd!=nb_dofs_col/3;cdd++) {
          double nrnc = nr*N_col[cdd];
          for(int rr = 0;rr!=3;rr++) {
            mC(3*rdd+rr,3*cdd+rr) += nrnc*a[rr];
          }
        }
      }
    }
    int *row_indices_ptr,*col_indices_ptr;
    row_indices_ptr = &row_data.getIndices()[0];
    col_indices_ptr = &col_data.getIndices()[0];
    // cerr << mC << endl;
    // cerr << row_data.getIndices() << endl;
    // cerr << col_data.getIndices() << endl;
    mC *= getFEMethod()->ts_a;
    ierr = MatSetValues (
      getFEMethod()->ts_B,
      nb_dofs_row,row_indices_ptr,
      nb_dofs_col,col_indices_ptr,
      &mC(0,0),ADD_VALUES
    ); CHKERRQ(ierr);
    if(row_side != col_side || row_type != col_type) {
      mCTrans.resize(nb_dofs_col,nb_dofs_row,false);
      noalias(mCTrans) = trans(mC);
      ierr = MatSetValues(
        getFEMethod()->ts_B,
        nb_dofs_col,col_indices_ptr,
        nb_dofs_row,row_indices_ptr,
        &mCTrans(0,0),ADD_VALUES
      ); CHKERRQ(ierr);
    }
  } catch (exception& ex) {
    ostringstream ss;
    ss << "thorw in method: "
    << ex.what()
    << " at line "
    << __LINE__
    << " in file "
    << __FILE__;
    SETERRQ(PETSC_COMM_SELF,MOFEM_STD_EXCEPTION_THROW,ss.str().c_str());
  }
  // doVerticesRow = true;
  // doEdgesRow = false;
  // doQuadsRow = false;
  // doTrisRow = false;
  // doPrismsRow = false;
  // doVerticesCol = true;
  // doEdgesCol = false;
  // doQuadsCol = false;
  // doTrisCol = false;
  // doPrismsCol = false;
  PetscFunctionReturn(0);
}

#endif // __ARC_LENGTH_TOOLS_HPP__

PetscErrorCode SolidShellPrismElement::OpEnergyNormH1::doWork(
  int side,EntityType type,DataForcesAndSourcesCore::EntData &data
) {
  PetscFunctionBegin;
  if(type != MBVERTEX) PetscFunctionReturn(0);

  // int nb_gauss_pts = data.getN().size1();
  int nb_gauss_pts_on_faces = getGaussPtsTrianglesOnly().size2();
  int nb_gauss_pts_through_thickness = getGaussPtsThroughThickness().size2();
  double *stress_ptr = &commonData.sTress(0,0);
  double *strain_ptr = &commonData.sTrain(0,0);
  double energy = 0;
  int gg = 0;
  for(int ggf = 0;ggf<nb_gauss_pts_on_faces;ggf++) {
    for(int ggt = 0;ggt<nb_gauss_pts_through_thickness;ggt++,gg++) {
      double val = 0.5*commonData.jacDeterminantAtGaussPts[gg]*getGaussPts()(3,gg);
      VectorAdaptor stress = VectorAdaptor(
        6,ublas::shallow_array_adaptor<double>(6,&stress_ptr[6*gg])
      );
      VectorAdaptor strain = VectorAdaptor(
        6,ublas::shallow_array_adaptor<double>(6,&strain_ptr[6*gg])
      );
      energy += 0.5*val*inner_prod(stress,strain);
      // cerr << energy << " "<<  commonData.jacDeterminantAtGaussPts[gg] << " " << val << endl;
    }
  }
  {

    Tag th_global_id;
    rval = getVolumeFE()->mField.get_moab().tag_get_handle(
      GLOBAL_ID_TAG_NAME,th_global_id
    ); CHKERRQ_MOAB(rval);
    EntityHandle ent = getNumeredEntFiniteElementPtr()->getEnt();
    int id;
    rval = getVolumeFE()->mField.get_moab().tag_get_data(th_global_id,&ent,1,&id); CHKERRQ_MOAB(rval);
    ierr = VecSetValue(commonData.vecErrorOnElements,id,energy,INSERT_VALUES); CHKERRQ(ierr);
  }
  if(commonData.saveOnTag) {

    EntityHandle ent = getNumeredEntFiniteElementPtr()->getEnt();
    double def_val = 0;
    rval = getVolumeFE()->mField.get_moab().tag_get_handle(
      "ENERGY_OR_ENERGY_ERROR",1,MB_TYPE_DOUBLE,thError,MB_TAG_CREAT|MB_TAG_SPARSE,&def_val
    ); CHKERRQ_MOAB(rval);
    // Tag th_global_id;
    // rval = getVolumeFE()->mField.get_moab().tag_get_handle(
    //   GLOBAL_ID_TAG_NAME,th_global_id
    // ); CHKERRQ_MOAB(rval);
    // int id;
    // rval = getVolumeFE()->mField.get_moab().tag_get_data(th_global_id,&ent,1,&id); CHKERRQ_MOAB(rval);
    // energy = id;
    rval = getVolumeFE()->mField.get_moab().tag_set_data(thError,&ent,1,&energy); CHKERRQ_MOAB(rval);
  }
  PetscFunctionReturn(0);
}

} // SolidShellModule
