#include <MoFEM.hpp>

#if PETSC_VERSION_GE(3,6,0)
#include <petsc/private/kspimpl.h> /*I "petscksp.h" I*/
#include <petsc/private/dmimpl.h> /*I  "petscdm.h"   I*/
#include <petsc/private/pcmgimpl.h>
#else
#error "PETSc version should be 3.6.0 or higher"
#endif

namespace SolidShellModule {

// Reset only last component which is different
// FIXME: this is dirty hack, would be good if such functionality will be part of petsc

static PetscErrorCode ierr;

#undef __FUNCT__
#define __FUNCT__ "only_last_pc_reset_mg"
PetscErrorCode only_last_pc_reset_mg(PC pc) {
  PC_MG          *mg        = (PC_MG*)pc->data;
  PC_MG_Levels   **mglevels = mg->levels;

  PetscInt       i,n;

  PetscFunctionBegin;
  if (mglevels) {
    n = mglevels[0]->levels;

    for (i=n-2; i<n-1; i++) {
      if(i<0) continue;
      ierr = MatDestroy(&mglevels[i+1]->restrct);CHKERRQ(ierr);
      ierr = MatDestroy(&mglevels[i+1]->interpolate);CHKERRQ(ierr);
      ierr = VecDestroy(&mglevels[i+1]->rscale);CHKERRQ(ierr);
      mglevels[i+1]->restrct = PETSC_NULL;
      mglevels[i+1]->interpolate = PETSC_NULL;
      mglevels[i+1]->rscale = PETSC_NULL;
    }

    for (i=n-2; i<n-1; i++) {
      if(i<0) continue;
      ierr = VecDestroy(&mglevels[i+1]->r);CHKERRQ(ierr);
      ierr = VecDestroy(&mglevels[i]->b);CHKERRQ(ierr);
      ierr = VecDestroy(&mglevels[i]->x);CHKERRQ(ierr);
    }

    for (i=0; i<n; i++) {
      ierr = MatDestroy(&mglevels[i]->A);CHKERRQ(ierr);
    }

    for (i=0; i<n; i++) {
      if(i<=0) continue;
      // ierr = MatDestroy(&mglevels[i]->A);CHKERRQ(ierr);
      if (mglevels[i]->smoothd != mglevels[i]->smoothu) {
        ierr = KSPReset(mglevels[i]->smoothd);CHKERRQ(ierr);
      }
      ierr = KSPReset(mglevels[i]->smoothu);CHKERRQ(ierr);
    }

  }
  PetscFunctionReturn(0);
}

// Create all mg levels or reset last and add one.
// FIXME: this is dirty hack, would be good if such functionality will be part of petsc

#undef __FUNCT__
#define __FUNCT__ "pc_mg_set_last_level"
PetscErrorCode  pc_mg_set_last_level(PC pc,PetscInt levels,MPI_Comm *comms) {

  PC_MG          *mg        = (PC_MG*)pc->data;
  MPI_Comm       comm;
  PC_MG_Levels   **mglevels = mg->levels;
  PCMGType       mgtype     = mg->am;
  PetscInt       mgctype    = (PetscInt) PC_MG_CYCLE_V;
  PetscInt       i;
  PetscMPIInt    size;
  const char     *prefix;
  PC             ipc;
  PetscInt       n;

  PetscFunctionBegin;
  PetscValidHeaderSpecific(pc,PC_CLASSID,1);
  PetscValidLogicalCollectiveInt(pc,levels,2);
  ierr = PetscObjectGetComm((PetscObject)pc,&comm);CHKERRQ(ierr);
  vector<bool> set_ksp(levels,false);
  if(mglevels) {
    mgctype = mglevels[0]->cycles;
    /* free up the last level */
    ierr = only_last_pc_reset_mg(pc); CHKERRQ(ierr);
    n = mglevels[0]->levels;
    if(levels>2) {
      set_ksp[0] = true;
      for(i = 1;i<n-1;i++) {
        set_ksp[i] = true;
      }
    }
    for(i=0;i<n;i++) {
      if(set_ksp[i]) continue;
      if(mglevels[i]->smoothd != mglevels[i]->smoothu) {
        ierr = KSPDestroy(&mglevels[i]->smoothd);CHKERRQ(ierr);
      }
      ierr = KSPDestroy(&mglevels[i]->smoothu);CHKERRQ(ierr);
      ierr = PetscFree(mglevels[i]);CHKERRQ(ierr);
    }
  }

  mg->nlevels = levels;

  ierr = PetscMalloc1(levels,&mglevels);CHKERRQ(ierr);
  ierr = PetscLogObjectMemory((PetscObject)pc,levels*(sizeof(PC_MG*)));CHKERRQ(ierr);

  ierr = PCGetOptionsPrefix(pc,&prefix);CHKERRQ(ierr);

  mg->stageApply = 0;
  for (i=0; i<levels; i++) {
    ierr = PetscNewLog(pc,&mglevels[i]);CHKERRQ(ierr);

    mglevels[i]->levels              = levels;

    if(set_ksp[i]) {
      // Copy from old one to new one
      if(i!=mg->levels[i]->level) {
        SETERRQ(comm,MOFEM_DATA_INCONSISTENCY,"data inconsistency");
      }
      mglevels[i]->level               = mg->levels[i]->level;
      mglevels[i]->cycles              = mg->levels[i]->cycles;
      mglevels[i]->eventsmoothsetup    = mg->levels[i]->eventsmoothsetup;
      mglevels[i]->eventsmoothsolve    = mg->levels[i]->eventsmoothsolve;
      mglevels[i]->eventresidual       = mg->levels[i]->eventresidual;
      mglevels[i]->eventinterprestrict = mg->levels[i]->eventinterprestrict;
      mglevels[i]->restrct             = mg->levels[i]->restrct;
      mglevels[i]->interpolate         = mg->levels[i]->interpolate;
      mglevels[i]->rscale              = mg->levels[i]->rscale;
      mglevels[i]->r                   = mg->levels[i]->r;
      mglevels[i]->b                   = mg->levels[i]->b;
      mglevels[i]->x                   = mg->levels[i]->x;
    } else {
      mglevels[i]->level               = i;
      mglevels[i]->cycles              = mgctype;
      mg->default_smoothu              = 2;
      mg->default_smoothd              = 2;
      mglevels[i]->eventsmoothsetup    = 0;
      mglevels[i]->eventsmoothsolve    = 0;
      mglevels[i]->eventresidual       = 0;
      mglevels[i]->eventinterprestrict = 0;
    }

    if(set_ksp[i]) {

      mglevels[i]->smoothd = mg->levels[i]->smoothd;
      mglevels[i]->smoothu = mg->levels[i]->smoothu;

    } else {

      // Create all or create last two
      if(comms) comm = comms[i];
      ierr = KSPCreate(comm,&mglevels[i]->smoothd);CHKERRQ(ierr);
      ierr = KSPSetErrorIfNotConverged(mglevels[i]->smoothd,pc->erroriffailure);CHKERRQ(ierr);
      ierr = KSPSetType(mglevels[i]->smoothd,KSPCHEBYSHEV);CHKERRQ(ierr);
      ierr = KSPSetConvergenceTest(mglevels[i]->smoothd,KSPConvergedSkip,NULL,NULL);CHKERRQ(ierr);
      ierr = KSPSetNormType(mglevels[i]->smoothd,KSP_NORM_NONE);CHKERRQ(ierr);
      ierr = KSPGetPC(mglevels[i]->smoothd,&ipc);CHKERRQ(ierr);
      ierr = PCSetType(ipc,PCSOR);CHKERRQ(ierr);
      ierr = PetscObjectIncrementTabLevel((PetscObject)mglevels[i]->smoothd,(PetscObject)pc,levels-i);CHKERRQ(ierr);
      ierr = KSPSetTolerances(mglevels[i]->smoothd,PETSC_DEFAULT,PETSC_DEFAULT,PETSC_DEFAULT, i ? mg->default_smoothd : 1);CHKERRQ(ierr);
      ierr = KSPSetOptionsPrefix(mglevels[i]->smoothd,prefix);CHKERRQ(ierr);

      /* do special stuff for coarse grid */
      if(!i && levels > 1) {
        ierr = KSPAppendOptionsPrefix(mglevels[0]->smoothd,"mg_coarse_");CHKERRQ(ierr);

        /* coarse solve is (redundant) LU by default; set shifttype NONZERO to avoid annoying zero-pivot in LU preconditioner */
        ierr = KSPSetType(mglevels[0]->smoothd,KSPPREONLY);CHKERRQ(ierr);
        ierr = KSPGetPC(mglevels[0]->smoothd,&ipc);CHKERRQ(ierr);
        ierr = MPI_Comm_size(comm,&size);CHKERRQ(ierr);
        if (size > 1) {
          KSP innerksp;
          PC  innerpc;
          ierr = PCSetType(ipc,PCREDUNDANT);CHKERRQ(ierr);
          ierr = PCRedundantGetKSP(ipc,&innerksp);CHKERRQ(ierr);
          ierr = KSPGetPC(innerksp,&innerpc);CHKERRQ(ierr);
          ierr = PCFactorSetShiftType(innerpc,MAT_SHIFT_INBLOCKS);CHKERRQ(ierr);
        } else {
          ierr = PCSetType(ipc,PCLU);CHKERRQ(ierr);
          ierr = PCFactorSetShiftType(ipc,MAT_SHIFT_INBLOCKS);CHKERRQ(ierr);
        }

      } else {
        char tprefix[128];
        sprintf(tprefix,"mg_levels_%d_",(int)i);
        ierr = KSPAppendOptionsPrefix(mglevels[i]->smoothd,tprefix);CHKERRQ(ierr);
      }
      ierr = PetscLogObjectParent((PetscObject)pc,(PetscObject)mglevels[i]->smoothd);CHKERRQ(ierr);

      mglevels[i]->smoothu = mglevels[i]->smoothd;
      mg->rtol             = 0.0;
      mg->abstol           = 0.0;
      mg->dtol             = 0.0;
      mg->ttol             = 0.0;
      mg->cyclesperpcapply = 1;

    }

  }
  if(mg->levels) {
    ierr = PetscFree(mg->levels);CHKERRQ(ierr);
  }
  mg->levels = mglevels;
  ierr = PCMGSetType(pc,mgtype);CHKERRQ(ierr);
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "pc_setup_mg"
PetscErrorCode pc_setup_mg(PC pc) {
  PC_MG          *mg        = (PC_MG*)pc->data;
  PC_MG_Levels   **mglevels = mg->levels;

  PetscInt       i,n = mglevels[0]->levels;
  PC             cpc;
  PetscBool      preonly,lu,redundant,cholesky,svd,dump = PETSC_FALSE,opsset,use_amat,missinginterpolate = PETSC_FALSE;
  Vec            tvec;
  DM             *dms;
  PetscViewer    viewer = 0;

  PetscFunctionBegin;
  /* FIX: Move this to PCSetFromOptions_MG? */
  if (mg->usedmfornumberoflevels) {
    PetscInt levels;
    ierr = DMGetRefineLevel(pc->dm,&levels);CHKERRQ(ierr);
    levels++;
    if (levels > n) { /* the problem is now being solved on a finer grid */
      ierr     = PCMGSetLevels(pc,levels,NULL);CHKERRQ(ierr);
      n        = levels;
      ierr     = PCSetFromOptions(pc);CHKERRQ(ierr); /* it is bad to call this here, but otherwise will never be called for the new hierarchy */
      mglevels =  mg->levels;
    }
  }
  ierr = KSPGetPC(mglevels[0]->smoothd,&cpc);CHKERRQ(ierr);


  /* If user did not provide fine grid operators OR operator was not updated since last global KSPSetOperators() */
  /* so use those from global PC */
  /* Is this what we always want? What if user wants to keep old one? */
  ierr = KSPGetOperatorsSet(mglevels[n-1]->smoothd,NULL,&opsset);CHKERRQ(ierr);
  if (opsset) {
    Mat mmat;
    ierr = KSPGetOperators(mglevels[n-1]->smoothd,NULL,&mmat);CHKERRQ(ierr);
    if (mmat == pc->pmat) opsset = PETSC_FALSE;
  }

  if (!opsset) {
    ierr = PCGetUseAmat(pc,&use_amat);CHKERRQ(ierr);
    if(use_amat){
      ierr = PetscInfo(pc,"Using outer operators to define finest grid operator \n  because PCMGGetSmoother(pc,nlevels-1,&ksp);KSPSetOperators(ksp,...); was not called.\n");CHKERRQ(ierr);
      ierr = KSPSetOperators(mglevels[n-1]->smoothd,pc->mat,pc->pmat);CHKERRQ(ierr);
    }
    else {
      ierr = PetscInfo(pc,"Using matrix (pmat) operators to define finest grid operator \n  because PCMGGetSmoother(pc,nlevels-1,&ksp);KSPSetOperators(ksp,...); was not called.\n");CHKERRQ(ierr);
      ierr = KSPSetOperators(mglevels[n-1]->smoothd,pc->pmat,pc->pmat);CHKERRQ(ierr);
    }
  }

  for (i=n-1; i>0; i--) {
    if (!(mglevels[i]->interpolate || mglevels[i]->restrct)) {
      missinginterpolate = PETSC_TRUE;
      continue;
    }
  }

  vector<bool> set_dms_d(n,false);
  vector<bool> set_dms_u(n,false);
  for(int i = 0;i<n;i++) {
    if(mglevels[i]->smoothd->dm) set_dms_d[i] = true;
    if(mglevels[i]->smoothu->dm) set_dms_u[i] = true;
  }


  /*
   Skipping if user has provided all interpolation/restriction needed (since DM might not be able to produce them (when coming from SNES/TS)
   Skipping for galerkin==2 (externally managed hierarchy such as ML and GAMG). Cleaner logic here would be great. Wrap ML/GAMG as DMs?
  */
  if (missinginterpolate && pc->dm && mg->galerkin != 2 && !pc->setupcalled) {
    /* construct the interpolation from the DMs */
    Mat p;
    Vec rscale;
    ierr     = PetscMalloc1(n,&dms);CHKERRQ(ierr);
    dms[n-1] = pc->dm;
    /* Separately create them so we do not get DMKSP interference between levels */
    for (i=n-2; i>-1; i--) {
      if(!set_dms_d[i]) {
        ierr = DMCoarsen(dms[i+1],MPI_COMM_NULL,&dms[i]); CHKERRQ(ierr);
        ierr = KSPSetDM(mglevels[i]->smoothd,dms[i]);CHKERRQ(ierr);
        if (mg->galerkin) {ierr = KSPSetDMActive(mglevels[i]->smoothd,PETSC_FALSE);CHKERRQ(ierr);}
        DMKSP kdm;
        ierr = DMGetDMKSPWrite(dms[i],&kdm);CHKERRQ(ierr);
        /* Ugly hack so that the next KSPSetUp() will use the RHS that we set. A better fix is to change dmActive to take
         * a bitwise OR of computing the matrix, RHS, and initial iterate. */
        kdm->ops->computerhs = NULL;
        kdm->rhsctx          = NULL;
      } else {
        dms[i] = mglevels[i]->smoothd->dm;
        dms[i]->leveldown = dms[i+1]->leveldown + 1;
        ierr = PetscObjectReference((PetscObject)dms[i]); CHKERRQ(ierr);
      }
    }
    for (i=n-2; i>-1; i--) {
      if (!mglevels[i+1]->interpolate) {
        // cerr << "interpolate " << i+1 << endl;
        ierr = DMCreateInterpolation(dms[i],dms[i+1],&p,&rscale);CHKERRQ(ierr);
        ierr = PCMGSetInterpolation(pc,i+1,p);CHKERRQ(ierr);
        if (rscale) {ierr = PCMGSetRScale(pc,i+1,rscale);CHKERRQ(ierr);}
        ierr = VecDestroy(&rscale);CHKERRQ(ierr);
        ierr = MatDestroy(&p);CHKERRQ(ierr);
      } else {
        // cerr << "not interpolation " << i+1 << endl;
      }
    }
    for (i=n-2; i>-1; i--) {ierr = DMDestroy(&dms[i]);CHKERRQ(ierr);}
    ierr = PetscFree(dms);CHKERRQ(ierr);
  }

  if (pc->dm && !pc->setupcalled) {
    /* finest smoother also gets DM but it is not active, independent of whether galerkin==2 */
    ierr = KSPSetDM(mglevels[n-1]->smoothd,pc->dm);CHKERRQ(ierr);
    ierr = KSPSetDMActive(mglevels[n-1]->smoothd,PETSC_FALSE);CHKERRQ(ierr);
  }

  if (mg->galerkin == 1) {
    MPI_Comm comm;
    ierr = PetscObjectGetComm((PetscObject)pc,&comm);CHKERRQ(ierr);
    SETERRQ(comm,MOFEM_DATA_INCONSISTENCY,"should not happen");
  } // else if (!mg->galerkin && pc->dm && pc->dm->x) {
  //   /* need to restrict Jacobian location to coarser meshes for evaluation */
  //   for (i=n-2; i>-1; i--) {
  //     Mat R;
  //     Vec rscale;
  //     if (!mglevels[i]->smoothd->dm->x) {
  //       Vec *vecs;
  //       ierr = KSPCreateVecs(mglevels[i]->smoothd,1,&vecs,0,NULL);CHKERRQ(ierr);
  //
  //       mglevels[i]->smoothd->dm->x = vecs[0];
  //
  //       ierr = PetscFree(vecs);CHKERRQ(ierr);
  //     }
  //     ierr = PCMGGetRestriction(pc,i+1,&R);CHKERRQ(ierr);
  //     ierr = PCMGGetRScale(pc,i+1,&rscale);CHKERRQ(ierr);
  //     ierr = MatRestrict(R,mglevels[i+1]->smoothd->dm->x,mglevels[i]->smoothd->dm->x);CHKERRQ(ierr);
  //     ierr = VecPointwiseMult(mglevels[i]->smoothd->dm->x,mglevels[i]->smoothd->dm->x,rscale);CHKERRQ(ierr);
  //   }
  // }
  if (!mg->galerkin && pc->dm) {
    for (i=n-2; i>=0; i--) {
      if (!mglevels[i+1]->restrct) {
        // cerr << n << " " << i << endl;
        DM  dmfine,dmcoarse;
        Mat Restrict,Inject;
        Vec rscale;
        ierr   = KSPGetDM(mglevels[i+1]->smoothd,&dmfine);CHKERRQ(ierr);
        ierr   = KSPGetDM(mglevels[i]->smoothd,&dmcoarse);CHKERRQ(ierr);
        ierr   = PCMGGetRestriction(pc,i+1,&Restrict);CHKERRQ(ierr);
        ierr   = PCMGGetRScale(pc,i+1,&rscale);CHKERRQ(ierr);
        Inject = NULL;      /* Callback should create it if it needs Injection */
        ierr   = DMRestrict(dmfine,Restrict,rscale,Inject,dmcoarse);CHKERRQ(ierr);
      }
    }
  }

  if (!pc->setupcalled) {
    for (i=0; i<n; i++) {
      if(set_dms_d[i]) continue;
      ierr = KSPSetFromOptions(mglevels[i]->smoothd);CHKERRQ(ierr);
    }
    for (i=1; i<n; i++) {
      if(set_dms_u[i]) continue;
      if (mglevels[i]->smoothu && (mglevels[i]->smoothu != mglevels[i]->smoothd)) {
        ierr = KSPSetFromOptions(mglevels[i]->smoothu);CHKERRQ(ierr);
      }
    }
    for (i=1; i<n; i++) {
      ierr = PCMGGetInterpolation(pc,i,&mglevels[i]->interpolate);CHKERRQ(ierr);
      ierr = PCMGGetRestriction(pc,i,&mglevels[i]->restrct);CHKERRQ(ierr);
    }
    for (i=0; i<n-1; i++) {
      if (!mglevels[i]->b) {
        Vec *vec;
        ierr = KSPCreateVecs(mglevels[i]->smoothd,1,&vec,0,NULL);CHKERRQ(ierr);
        ierr = PCMGSetRhs(pc,i,*vec);CHKERRQ(ierr);
        ierr = VecDestroy(vec);CHKERRQ(ierr);
        ierr = PetscFree(vec);CHKERRQ(ierr);
      }
      if (!mglevels[i]->r && i) {
        ierr = VecDuplicate(mglevels[i]->b,&tvec);CHKERRQ(ierr);
        ierr = PCMGSetR(pc,i,tvec);CHKERRQ(ierr);
        ierr = VecDestroy(&tvec);CHKERRQ(ierr);
      }
      if (!mglevels[i]->x) {
        ierr = VecDuplicate(mglevels[i]->b,&tvec);CHKERRQ(ierr);
        ierr = PCMGSetX(pc,i,tvec);CHKERRQ(ierr);
        ierr = VecDestroy(&tvec);CHKERRQ(ierr);
      }
    }
    if (n != 1 && !mglevels[n-1]->r) {
      /* PCMGSetR() on the finest level if user did not supply it */
      Vec *vec;
      ierr = KSPCreateVecs(mglevels[n-1]->smoothd,1,&vec,0,NULL);CHKERRQ(ierr);
      ierr = PCMGSetR(pc,n-1,*vec);CHKERRQ(ierr);
      ierr = VecDestroy(vec);CHKERRQ(ierr);
      ierr = PetscFree(vec);CHKERRQ(ierr);
    }
  }

  if (pc->dm) {
    /* need to tell all the coarser levels to rebuild the matrix using the DM for that level */
    for (i=0; i<n-1; i++) {
      if (set_dms_d[i]) continue;
      if (mglevels[i]->smoothd->setupstage != KSP_SETUP_NEW) mglevels[i]->smoothd->setupstage = KSP_SETUP_NEWMATRIX;
    }
  }

  for (i=1; i<n; i++) {
    if (mglevels[i]->smoothu == mglevels[i]->smoothd || mg->am == PC_MG_FULL || mg->am == PC_MG_KASKADE || mg->cyclesperpcapply > 1){
      /* if doing only down then initial guess is zero */
      ierr = KSPSetInitialGuessNonzero(mglevels[i]->smoothd,PETSC_TRUE);CHKERRQ(ierr);
    }
    if (mglevels[i]->eventsmoothsetup) {ierr = PetscLogEventBegin(mglevels[i]->eventsmoothsetup,0,0,0,0);CHKERRQ(ierr);}
    ierr = KSPSetUp(mglevels[i]->smoothd);CHKERRQ(ierr);
    if (mglevels[i]->eventsmoothsetup) {ierr = PetscLogEventEnd(mglevels[i]->eventsmoothsetup,0,0,0,0);CHKERRQ(ierr);}
    if (!mglevels[i]->residual) {
      Mat mat;
      ierr = KSPGetOperators(mglevels[i]->smoothd,NULL,&mat);CHKERRQ(ierr);
      ierr = PCMGSetResidual(pc,i,PCMGResidualDefault,mat);CHKERRQ(ierr);
    }
  }
  for (i=1; i<n; i++) {
    if (mglevels[i]->smoothu && mglevels[i]->smoothu != mglevels[i]->smoothd) {
      Mat          downmat,downpmat;

      /* check if operators have been set for up, if not use down operators to set them */
      if(!set_dms_u[i]) {
        ierr = KSPGetOperatorsSet(mglevels[i]->smoothu,&opsset,NULL);CHKERRQ(ierr);
        if (!opsset) {
          ierr = KSPGetOperators(mglevels[i]->smoothd,&downmat,&downpmat);CHKERRQ(ierr);
          ierr = KSPSetOperators(mglevels[i]->smoothu,downmat,downpmat);CHKERRQ(ierr);
        }
        ierr = KSPSetInitialGuessNonzero(mglevels[i]->smoothu,PETSC_TRUE);CHKERRQ(ierr);
        if (mglevels[i]->eventsmoothsetup) {ierr = PetscLogEventBegin(mglevels[i]->eventsmoothsetup,0,0,0,0);CHKERRQ(ierr);}
        ierr = KSPSetUp(mglevels[i]->smoothu);CHKERRQ(ierr);
        if (mglevels[i]->eventsmoothsetup) {ierr = PetscLogEventEnd(mglevels[i]->eventsmoothsetup,0,0,0,0);CHKERRQ(ierr);}
      }
    }
  }

  /*
      If coarse solver is not direct method then DO NOT USE preonly
  */
  ierr = PetscObjectTypeCompare((PetscObject)mglevels[0]->smoothd,KSPPREONLY,&preonly);CHKERRQ(ierr);
  if (preonly) {
    ierr = PetscObjectTypeCompare((PetscObject)cpc,PCLU,&lu);CHKERRQ(ierr);
    ierr = PetscObjectTypeCompare((PetscObject)cpc,PCREDUNDANT,&redundant);CHKERRQ(ierr);
    ierr = PetscObjectTypeCompare((PetscObject)cpc,PCCHOLESKY,&cholesky);CHKERRQ(ierr);
    ierr = PetscObjectTypeCompare((PetscObject)cpc,PCSVD,&svd);CHKERRQ(ierr);
    if (!lu && !redundant && !cholesky && !svd) {
      ierr = KSPSetType(mglevels[0]->smoothd,KSPGMRES);CHKERRQ(ierr);
    }
  }

  if (mglevels[0]->eventsmoothsetup) {ierr = PetscLogEventBegin(mglevels[0]->eventsmoothsetup,0,0,0,0);CHKERRQ(ierr);}
  ierr = KSPSetUp(mglevels[0]->smoothd);CHKERRQ(ierr);
  if (mglevels[0]->eventsmoothsetup) {ierr = PetscLogEventEnd(mglevels[0]->eventsmoothsetup,0,0,0,0);CHKERRQ(ierr);}

  /*
     Dump the interpolation/restriction matrices plus the
   Jacobian/stiffness on each level. This allows MATLAB users to
   easily check if the Galerkin condition A_c = R A_f R^T is satisfied.

   Only support one or the other at the same time.
  */
// #if defined(PETSC_USE_SOCKET_VIEWER)
//   ierr = PetscOptionsGetBool(((PetscObject)pc)->prefix,"-pc_mg_dump_matlab",&dump,NULL);CHKERRQ(ierr);
//   if (dump) viewer = PETSC_VIEWER_SOCKET_(PetscObjectComm((PetscObject)pc));
//   dump = PETSC_FALSE;
// #endif
//   ierr = PetscOptionsGetBool(((PetscObject)pc)->prefix,"-pc_mg_dump_binary",&dump,NULL);CHKERRQ(ierr);
//   if (dump) viewer = PETSC_VIEWER_BINARY_(PetscObjectComm((PetscObject)pc));

  if (viewer) {
    for (i=1; i<n; i++) {
      ierr = MatView(mglevels[i]->restrct,viewer);CHKERRQ(ierr);
    }
    for (i=0; i<n; i++) {
      ierr = KSPGetPC(mglevels[i]->smoothd,&pc);CHKERRQ(ierr);
      ierr = MatView(pc->mat,viewer);CHKERRQ(ierr);
    }
  }
  PetscFunctionReturn(0);
}

}
