/** \file RunAdaptivity.cpp
 * \ingroup solid_shell_prism_element
 * \brief Implementation of solid shell prism element
 *
 */

/*
 * This file is part of MoFEM.
 * MoFEM is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * MoFEM is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with MoFEM. If not, see <http://www.gnu.org/licenses/>. */

#include <MoFEM.hpp>
#include <PrismsFromSurfaceInterface.hpp>
#include <boost/numeric/ublas/symmetric.hpp>

using namespace MoFEM;
#include <PostProcOnRefMesh.hpp>

#ifdef WITH_ADOL_C
// #define ADOLC_TAPELESS
// #include <adolc/adtl.h>
#include <adolc/adolc.h>
#endif

#include <MethodForForceScaling.hpp>
#include <DirichletBC.hpp>
#include <PCMGSetUpViaApproxOrders.hpp>

#include <ArcLengthTools.hpp>
#include <SolidShellPrismElement.hpp>
#include <RunAdaptivity.hpp>


#if PETSC_VERSION_GE(3,6,0)
#include <petsc/private/dmimpl.h> /*I  "petscdm.h"   I*/
#include <petsc/private/pcmgimpl.h>
#else
#include <petsc-private/dmimpl.h> /*I  "petscdm.h"   I*/
#include <petsc-private/pcmgimpl.h>
#endif

namespace SolidShellModule {

  PetscErrorCode pc_setup_mg(PC pc);
  PetscErrorCode only_last_pc_reset_mg(PC pc);
  PetscErrorCode pc_mg_set_last_level(PC pc,PetscInt levels,MPI_Comm *comms);
  PetscErrorCode pc_reset_mg(PC pc);


  PetscErrorCode RunAdaptivity::ShellPCMGSetUpViaApproxOrdersCtx::createIsAtLevel(int kk,IS *is) {
    PetscFunctionBegin;
    *is = mgLevels[kk];
    PetscFunctionReturn(0);
  }

  PetscErrorCode RunAdaptivity::ShellPCMGSetUpViaApproxOrdersCtx::destroyIsAtLevel(int kk,IS *is) {
    PetscFunctionBegin;
    PetscFunctionReturn(0);
  }

  PetscErrorCode RunAdaptivity::ShellPCMGSetUpViaApproxOrdersCtx::getOptions() {
    PetscFunctionBegin;
    ierr = PetscOptionsBegin(
      PETSC_COMM_WORLD,"",
      "MOFEM Shell Multi-Grid (Orders) pre-conditioner","none"
    ); CHKERRQ(ierr);

    ierr = PetscOptionsInt(
      "-mofem_mg_verbose",
      "nb levels of multi-grid solver","",
      0,&verboseLevel,PETSC_NULL
    ); CHKERRQ(ierr);

    ierr = PetscOptionsEnd(); CHKERRQ(ierr);

    PetscFunctionReturn(0);
  }


PetscErrorCode RunAdaptivity::ShellPCMGSetUpViaApproxOrdersCtx::buildProlongationOperator(PC pc,int verb) {
  PetscFunctionBegin;
  verb = verb > verboseLevel ? verb : verboseLevel;

  MPI_Comm comm;
  ierr = PetscObjectGetComm((PetscObject)dM,&comm); CHKERRQ(ierr);

  if(verb>0) {
    PetscPrintf(comm,"set MG levels %u\n",nbLevels);
  }

  vector<IS> is_vec(nbLevels+1);
  vector<int> is_glob_size(nbLevels+1),is_loc_size(nbLevels+1);

  for(int kk = 0;kk<nbLevels;kk++) {

    //get indices up to up to give approximation order
    ierr = createIsAtLevel(kk,&is_vec[kk]); CHKERRQ(ierr);
    ierr = ISGetSize(is_vec[kk],&is_glob_size[kk]); CHKERRQ(ierr);
    ierr = ISGetLocalSize(is_vec[kk],&is_loc_size[kk]); CHKERRQ(ierr);

    if(verb>0) {
      PetscSynchronizedPrintf(comm,
      "Nb. dofs at level [ %d ] global %u local %d\n",
      kk,is_glob_size[kk],is_loc_size[kk]);
    }

    //if no dofs on level kk finish here
    if(is_glob_size[kk]==0) {
      SETERRQ(PETSC_COMM_SELF,MOFEM_DATA_INCONSISTENCY,"no dofs at level");
    }

  }

#if PETSC_VERSION_GE(3, 8, 0)
  #warning "This is not working downgrade petsc-3.7.* or optimally petsc-3.6.*"
#else
  ierr = PCMGSetGalerkin(pc, PETSC_FALSE); 
  CHKERRQ(ierr);
#endif

  // ierr = PCMGSetLevels(pc,nbLevels,NULL);  CHKERRQ(ierr);
  ierr = pc_mg_set_last_level(pc,nbLevels,NULL);  CHKERRQ(ierr);

  ierr = DMMGViaApproxOrdersReplaceCoarseningIS(dM,&is_vec[0],nbLevels,A,1); CHKERRQ(ierr);

  for(unsigned int kk = 0;kk<is_vec.size();kk++) {
    ierr = destroyIsAtLevel(kk,&is_vec[kk]); CHKERRQ(ierr);
  }

  if(verb>0) {
    PetscSynchronizedFlush(comm,PETSC_STDOUT);
  }

  PetscFunctionReturn(0);
}

PetscErrorCode  RunAdaptivity::SetOrderToEntities::preProcess() {
    PetscFunctionBegin;
    PetscFunctionReturn(0);
  }

  PetscErrorCode RunAdaptivity::SetOrderToEntities::postProcess() {
    PetscFunctionBegin;
    PetscFunctionReturn(0);
  }

  PetscErrorCode RunAdaptivity::SetOrderToEntities::operator()() {
    PetscFunctionBegin;

    EntityHandle ent = 0;
    int order = 0;
    int max_order = 0;

    for(_IT_FENUMEREDDOF_BY_NAME_ROW_FOR_LOOP_(numeredEntFiniteElementPtr,"DISPLACEMENT",dof)) {
      if(dof->get()->getEntType() == MBVERTEX) continue;
      if(ent != dof->get()->getEnt()) {
        ent = dof->get()->getEnt();
        Range adj_prisms;
        rval = mField.get_moab().get_adjacencies(&ent,1,3,false,adj_prisms); CHKERRQ_MOAB(rval);
        vector<int> adj_orders(adj_prisms.size());
        rval = mField.get_moab().tag_get_data(thOrder,adj_prisms,&adj_orders[0]); CHKERRQ_MOAB(rval);
        vector<int>::iterator max_it = max_element(adj_orders.begin(),adj_orders.end());
        max_order = *max_it;
        if(order > max_order) {
          SETERRQ2(
            PETSC_COMM_SELF,
            MOFEM_DATA_INCONSISTENCY,
            "order of element can not be bigger than order of entity %d > %d",
            order,max_order
          );
        }
        rval = mField.get_moab().tag_set_data(thOrder,&ent,1,&order); CHKERRQ_MOAB(rval);
        if(order<max_order) {
          rval = mField.get_moab().tag_set_data(thOrder,&ent,1,&max_order); CHKERRQ_MOAB(rval);
        }
      }
    }

    PetscFunctionReturn(0);
  }

  PetscErrorCode RunAdaptivity::SetOrderToElement::preProcess() {
    PetscFunctionBegin;
    {
      double def_val = 0;
      rval = mField.get_moab().tag_get_handle(
        "ENERGY_OR_ENERGY_ERROR",1,MB_TYPE_DOUBLE,thError,MB_TAG_CREAT|MB_TAG_SPARSE,&def_val
      ); CHKERRQ_MOAB(rval);
    }
    PetscFunctionReturn(0);
  }

  PetscErrorCode RunAdaptivity::SetOrderToElement::postProcess() {
    PetscFunctionBegin;
    PetscFunctionReturn(0);
  }

  PetscErrorCode RunAdaptivity::SetOrderToElement::operator()() {
    PetscFunctionBegin;
    EntityHandle fe_ent = numeredEntFiniteElementPtr->getEnt();
    int order;
    rval = mField.get_moab().tag_get_data(thOrder,&fe_ent,1,&order); CHKERRQ_MOAB(rval);

    if(order == maxOrder) PetscFunctionReturn(0);

    if(eRror>0) {
      double elem_error;
      rval = mField.get_moab().tag_get_data(thError,&fe_ent,1,&elem_error); CHKERRQ_MOAB(rval);
      if(elem_error < eRror && order > 2) {
        order -= 1;
        rval = mField.get_moab().tag_set_data(thOrder,&fe_ent,1,&order); CHKERRQ_MOAB(rval);
      }
    }

    if(sEt > 0) {
      order = sEt;
    } else {
      if(order < maxOrder) {
        order += uP;
      }
    }

    rval = mField.get_moab().tag_set_data(thOrder,&fe_ent,1,&order); CHKERRQ_MOAB(rval);

    PetscFunctionReturn(0);
  }

  PetscErrorCode RunAdaptivity::setOrder(int set,int up,double error,int max_order,int verb) {
  PetscFunctionBegin;
  int def_val = 1;
  rval = mField.get_moab().tag_get_handle(
    "ADAPT_ORDER",1,MB_TYPE_INTEGER,thOrder,MB_TAG_CREAT|MB_TAG_SPARSE,&def_val
  ); CHKERRQ_MOAB(rval);
  SetOrderToElement fe_order_elements(mField,thOrder,set,up,error,max_order);
  ierr = DMoFEMLoopFiniteElements(dM,"SHELL",&fe_order_elements); CHKERRQ(ierr);
  EntityHandle meshset = mField.get_finite_element_meshset("SHELL");
  Range prisms;

  rval = mField.get_moab().get_entities_by_handle(meshset,prisms); CHKERRQ_MOAB(rval);
  Tag th;
  rval = mField.get_moab().tag_get_handle("ADAPT_ORDER",th); CHKERRQ_MOAB(rval);
  ParallelComm* pcomm = ParallelComm::get_pcomm(&mField.get_moab(),MYPCOMM_INDEX);
  // rval = pcomm->reduce_tags(th,MPI_SUM,prisms);
  rval = pcomm->exchange_tags(th,prisms);
  if(verb>1) {
    vector<Tag> tags;
    tags.push_back(th);
    tags.push_back(pcomm->part_tag());
    Tag th_global_id;
    rval = mField.get_moab().tag_get_handle(GLOBAL_ID_TAG_NAME,th_global_id); CHKERRQ_MOAB(rval);
    tags.push_back(th_global_id);
    for(int rr = 0;rr<mField.get_comm_size();rr++) {
      if(mField.get_comm_rank()==rr) {
        ostringstream o1;
        o1 << "test_" << rr << ".vtk";
        mField.get_moab().write_file(o1.str().c_str(),"VTK","",&meshset,1,&tags[0],tags.size());
      }
    }
  }
  // ierr = PetscBarrier(PETSC_NULL); CHKERRQ(ierr);
  SetOrderToEntities fe_order_entities(mField,thOrder);
  ierr = DMoFEMLoopFiniteElements(dM,"SHELL",&fe_order_entities); CHKERRQ(ierr);
  PetscFunctionReturn(0);
}


PetscErrorCode RunAdaptivity::createIS(IS *is) {
  PetscFunctionBegin;
  const MoFEM::Problem *problem_ptr;
  ierr = DMMoFEMGetProblemPtr(dM,&problem_ptr); CHKERRQ(ierr);
  EntityHandle ent = 0;
  int ent_order;
  vecOrderDofs.clear();
  for(_IT_NUMEREDDOF_ROW_BY_OWNPROC_FOR_LOOP_(problem_ptr,mField.get_comm_rank(),dof)) {
    if(dof->get()->getName()!="DISPLACEMENT") {
      vecOrderDofs.push_back(dof->get()->getPetscGlobalDofIdx());
    } else if(dof->get()->getEntType()==MBVERTEX) {
      vecOrderDofs.push_back(dof->get()->getPetscGlobalDofIdx());
    } else {
      if(ent != dof->get()->getEnt()) {
        ent = dof->get()->getEnt();
        rval = mField.get_moab().tag_get_data(thOrder,&ent,1,&ent_order); CHKERRQ_MOAB(rval);
      }
      if(dof->get()->getDofOrder()<=ent_order) {
        vecOrderDofs.push_back(dof->get()->getPetscGlobalDofIdx());
      }
    }
  }
  ierr = ISCreateGeneral(
    PETSC_COMM_WORLD,vecOrderDofs.size(),&*vecOrderDofs.begin(),PETSC_COPY_VALUES,is
  ); CHKERRQ(ierr);
  PetscFunctionReturn(0);
}

PetscErrorCode RunAdaptivity::runKsp(DM dm,Mat Aij,Vec F,Vec D) {
  PetscFunctionBegin;
  PetscPrintf(PETSC_COMM_WORLD,"SetUp sOlver ... ");
  ierr = KSPSetDM(sOlver,dm); CHKERRQ(ierr);
  ierr = KSPSetOperators(sOlver,Aij,Aij); CHKERRQ(ierr);
  ierr = KSPSetDMActive(sOlver,PETSC_FALSE); CHKERRQ(ierr);
  if(1) {
    //from PETSc example ex42.c
    PetscBool same = PETSC_FALSE;
    ierr = KSPGetPC(sOlver,&pC); CHKERRQ(ierr);
    PetscErrorCode (*org_pc_setup_mg)(PC pc) = pC->ops->setup;
    pC->ops->setup = pc_setup_mg;
    PetscObjectTypeCompare((PetscObject)pC,PCMG,&same);
    if(same) {
      PetscPrintf(PETSC_COMM_WORLD,"multigrid ..\n");
      int nb_levels = mgLevels.size();
      AO ao;
      ierr = AOCreateMappingIS(mgLevels.back(),PETSC_NULL,&ao); CHKERRQ(ierr);
      ierr = DMMGViaApproxOrdersSetAO(dm,ao); CHKERRQ(ierr);
      ShellPCMGSetUpViaApproxOrdersCtx pc_ctx(dm,Aij,mgLevels);
      pc_ctx.nbLevels = nb_levels;
      ierr = PCMGSetUpViaApproxOrders(pC,&pc_ctx); CHKERRQ(ierr);
      ierr = AODestroy(&ao); CHKERRQ(ierr);
    } else {
      SETERRQ(PETSC_COMM_WORLD,MOFEM_DATA_INCONSISTENCY,"Expected MG pre-conditioner");
    }
    ierr = KSPSetInitialGuessKnoll(sOlver,PETSC_FALSE); CHKERRQ(ierr);
    ierr = KSPSetInitialGuessNonzero(sOlver,PETSC_TRUE); CHKERRQ(ierr);
    PetscPrintf(PETSC_COMM_WORLD,"Solve problem ..\n");
    ierr = KSPSetUp(sOlver); CHKERRQ(ierr);
    ierr = KSPSolve(sOlver,F,D); CHKERRQ(ierr);
    PetscErrorCode (*org_reset)(PC pc) = pC->ops->reset;
    pC->ops->reset = only_last_pc_reset_mg;
    ierr = KSPReset(sOlver); CHKERRQ(ierr);
    pC->ops->reset = org_reset;
    pC->ops->setup = org_pc_setup_mg;
  }
  PetscFunctionReturn(0);
}

PetscErrorCode RunAdaptivity::runKspAtLevel(DM dm,Mat Aij,Vec F,Vec D,bool add_level) {
  PetscFunctionBegin;
  PetscPrintf(PETSC_COMM_WORLD,"Get level sub matrix ...\n");
  Mat sub_Aij;
  ierr = MatGetSubMatrix(
    Aij,mgLevels.back(),mgLevels.back(),MAT_INITIAL_MATRIX,&sub_Aij
  ); CHKERRQ(ierr);
  // {
  //   ierr = MatView(sub_Aij,PETSC_VIEWER_DRAW_WORLD); CHKERRQ(ierr);
  //   std::string wait;
  //   std::cin >> wait;
  // }
  Vec sub_F;
  ierr =  VecGetSubVector(F,mgLevels.back(),&sub_F); CHKERRQ(ierr);
  Vec sub_D;
  ierr = VecGetSubVector(D,mgLevels.back(),&sub_D); CHKERRQ(ierr);
  ierr = runKsp(dm,sub_Aij,sub_F,sub_D); CHKERRQ(ierr);
  ierr = VecRestoreSubVector(F,mgLevels.back(),&sub_F); CHKERRQ(ierr);
  ierr = VecRestoreSubVector(D,mgLevels.back(),&sub_D); CHKERRQ(ierr);
  ierr = VecGhostUpdateBegin(D,INSERT_VALUES,SCATTER_FORWARD); CHKERRQ(ierr);
  ierr = VecGhostUpdateEnd(D,INSERT_VALUES,SCATTER_FORWARD); CHKERRQ(ierr);
  ierr = MatDestroy(&sub_Aij); CHKERRQ(ierr);
  PetscFunctionReturn(0);
}

PetscErrorCode RunAdaptivity::sortErrorVector(const double procent) {
  PetscFunctionBegin;
  int size;
  ierr = VecGetSize(commonData.vecErrorOnElements,&size); CHKERRQ(ierr);
  int loc_size;
  ierr = VecGetLocalSize(commonData.vecErrorOnElements,&loc_size); CHKERRQ(ierr);
  double *a0;
  ierr = VecGetArray(commonData.vecErrorOnElements,&a0); CHKERRQ(ierr);
  for(int ii = 0;ii<loc_size;ii++) {
    a0[ii] = 1/a0[ii];
  }
  ierr = PetscSortReal(loc_size,a0); CHKERRQ(ierr);
  int procent_size = ceil(size*procent);
  procent_size = procent_size>loc_size ? loc_size : procent_size;
  Vec short_sorted;
  ierr = VecCreateMPIWithArray(
    mField.get_comm(),1,procent_size,PETSC_DETERMINE,a0,&short_sorted
  ); CHKERRQ(ierr);
  if(mField.get_comm_size()>1) {
    VecScatter ctx;
    Vec all_in_zero;
    ierr = VecScatterCreateToZero(short_sorted,&ctx,&all_in_zero); CHKERRQ(ierr);
    ierr = VecScatterBegin(ctx,short_sorted,all_in_zero,INSERT_VALUES,SCATTER_FORWARD); CHKERRQ(ierr);
    ierr = VecScatterEnd(ctx,short_sorted,all_in_zero,INSERT_VALUES,SCATTER_FORWARD); CHKERRQ(ierr);
    if(mField.get_comm_rank()==0) {
      double *a1;
      ierr = VecGetArray(all_in_zero,&a1); CHKERRQ(ierr);
      int all_in_zero_size;
      ierr = VecGetSize(all_in_zero,&all_in_zero_size); CHKERRQ(ierr);
      ierr = PetscSortReal(all_in_zero_size,a1); CHKERRQ(ierr);
      int pos = (unsigned int)ceil(size*procent);
      pos = pos>all_in_zero_size ? all_in_zero_size-1 : pos;
      procentErrorMax = a1[pos];
      procentErrorMax = 1./procentErrorMax;
      ierr = VecRestoreArray(all_in_zero,&a1); CHKERRQ(ierr);
    }
    ierr = VecDestroy(&all_in_zero); CHKERRQ(ierr);
    ierr = VecScatterDestroy(&ctx); CHKERRQ(ierr);
    ierr = VecDestroy(&short_sorted); CHKERRQ(ierr);
    Vec exchange_value;
    int one[] = {0};
    bool rank0 = (unsigned int)mField.get_comm_rank()==0;
    ierr = VecCreateGhostWithArray(
      mField.get_comm(),rank0?1:0,1,rank0?0:1,one,&procentErrorMax,&exchange_value
    ); CHKERRQ(ierr);
    ierr = VecGhostUpdateBegin(exchange_value,INSERT_VALUES,SCATTER_FORWARD); CHKERRQ(ierr);
    ierr = VecGhostUpdateEnd(exchange_value,INSERT_VALUES,SCATTER_FORWARD); CHKERRQ(ierr);
    ierr = VecDestroy(&exchange_value); CHKERRQ(ierr);
    for(int ii = 0;ii<loc_size;ii++) {
      a0[ii] = 1/a0[ii];
    }
  } else {
    procentErrorMax = a0[(unsigned int)ceil(size*procent)];
    procentErrorMax = 1./procentErrorMax;
  }
  ierr = VecDestroy(&short_sorted); CHKERRQ(ierr);
  ierr = VecRestoreArray(commonData.vecErrorOnElements,&a0); CHKERRQ(ierr);
  PetscFunctionReturn(0);
}

PetscErrorCode RunAdaptivity::solveProblem(DM dm,Mat Aij,Vec F,Vec D,int nb_ref_cycles,int max_order,double procent,int verb) {
  PetscFunctionBegin;

  const int max_levels = 40;
  isLevels.resize(max_levels,PETSC_NULL);
  solutionAtLevel.resize(max_levels,PETSC_NULL);

  ierr = KSPCreate(PETSC_COMM_WORLD,&sOlver); CHKERRQ(ierr);
  ierr = KSPSetFromOptions(sOlver); CHKERRQ(ierr);

  PetscLayout layout;
  ierr = DMMoFEMGetProblemFiniteElementLayout(dM,"SHELL",&layout); CHKERRQ(ierr);
  int size;
  ierr = PetscLayoutGetLocalSize(layout,&size); CHKERRQ(ierr);
  ierr = VecCreateMPI(PETSC_COMM_WORLD,size,PETSC_DETERMINE,&commonData.vecErrorOnElements); CHKERRQ(ierr);
  ierr = PetscLayoutDestroy(&layout); CHKERRQ(ierr);

  // Solve level 0
  ierr = setOrder(1,0,0,max_order); CHKERRQ(ierr);
  ierr = createIS(&isLevels[0]); CHKERRQ(ierr);
  mgLevels.push_back(isLevels[0]);

  // Solve level 1
  ierr = setOrder(0,1,0,max_order); CHKERRQ(ierr);
  ierr = createIS(&isLevels[1]); CHKERRQ(ierr);
  mgLevels.push_back(isLevels[1]);
  ierr = runKspAtLevel(dm,Aij,F,D,true); CHKERRQ(ierr);
  ierr = VecDuplicate(D,&solutionAtLevel[1]); CHKERRQ(ierr);
  ierr = VecCopy(D,solutionAtLevel[1]); CHKERRQ(ierr);

  commonData.saveOnTag = false;
  ierr = DMoFEMMeshToLocalVector(dM,D,INSERT_VALUES,SCATTER_REVERSE); CHKERRQ(ierr);
  ierr = DMoFEMLoopFiniteElements(dM,"SHELL",&shellError); CHKERRQ(ierr);
  double energy;
  ierr = VecSum(commonData.vecErrorOnElements,&energy); CHKERRQ(ierr);
  PetscPrintf(PETSC_COMM_WORLD,"Level %d Energy = %12.9e\n",0,energy);

  if(verb>0) {
    ierr = printDisplacements(); CHKERRQ(ierr);
    ierr = postProcFatPrims("solid_"+SSTR(0)+".h5m"); CHKERRQ(ierr);
  }

  Vec E;
  ierr = VecDuplicate(D,&E); CHKERRQ(ierr);

  int rr = 1;
  for(;;rr++) {

    double max_error = 0;
    if(rr%2 == 0) {
      double vnorm;
      int is_size;
      {
        ierr = VecWAXPY(E,-1,solutionAtLevel[rr-1],solutionAtLevel[rr]); CHKERRQ(ierr);
        ierr = VecGhostUpdateBegin(E,INSERT_VALUES,SCATTER_FORWARD); CHKERRQ(ierr);
        ierr = VecGhostUpdateEnd(E,INSERT_VALUES,SCATTER_FORWARD); CHKERRQ(ierr);
        ierr = VecNorm(E,NORM_2,&vnorm); CHKERRQ(ierr);
        ierr = DMoFEMMeshToLocalVector(dM,E,INSERT_VALUES,SCATTER_REVERSE); CHKERRQ(ierr);
        commonData.saveOnTag = true;
        ierr = DMoFEMLoopFiniteElements(dM,"SHELL",&shellError); CHKERRQ(ierr);
        ierr = ISGetSize(isLevels[rr],&is_size); CHKERRQ(ierr);
        ierr = VecMax(commonData.vecErrorOnElements,PETSC_NULL,&max_error); CHKERRQ(ierr);
        ierr = sortErrorVector(procent); CHKERRQ(ierr);
        PetscPrintf(
          PETSC_COMM_WORLD,
          "Refinement %d Size = %d vec norm = %8.6e error at precent (default 33) = %12.9e ( max error %12.9e )\n",
          rr,is_size,vnorm,procentErrorMax,max_error
        );
      }
      {
        ierr = DMoFEMMeshToLocalVector(dM,D,INSERT_VALUES,SCATTER_REVERSE); CHKERRQ(ierr);
        double vnorm;
        ierr = VecNorm(D,NORM_2,&vnorm); CHKERRQ(ierr);
        commonData.saveOnTag = false;
        ierr = DMoFEMLoopFiniteElements(dM,"SHELL",&shellError); CHKERRQ(ierr);
        double energy = 0;
        ierr = VecSum(commonData.vecErrorOnElements,&energy); CHKERRQ(ierr);
        PetscPrintf(
          PETSC_COMM_WORLD,
          "Level %d size = %d vec norm = %6.4e energy = %12.9e\n",
          rr+1,is_size,vnorm,energy
        );
      }
      if(verb>0) {
        ierr = printDisplacements(); CHKERRQ(ierr);
        ierr = postProcFatPrims("solid_"+SSTR(rr+1)+".h5m"); CHKERRQ(ierr);
      }
    }
    if(rr==nb_ref_cycles) {
      break;
    }

    if(rr%2 == 0) {
      ierr = setOrder(0,0,procentErrorMax,max_order); CHKERRQ(ierr);
    } else {
      ierr = setOrder(0,1,0,max_order); CHKERRQ(ierr);
    }

    ierr = createIS(&isLevels[rr+1]); CHKERRQ(ierr);
    mgLevels.push_back(isLevels[rr+1]);
    if(mgLevels.size()>1) {
      int s1,s2;
      ierr = ISGetSize(mgLevels.back(),&s1); CHKERRQ(ierr);
      ierr = ISGetSize(mgLevels[mgLevels.size()-2],&s2); CHKERRQ(ierr);
      PetscPrintf(PETSC_COMM_WORLD,"Last level size = %d, previous level size = %d\n",s1,s2);
      if(s1==s2) {
        IS is = mgLevels.back();
        ierr = ISDestroy(&mgLevels[mgLevels.size()-2]); CHKERRQ(ierr);
        mgLevels.pop_back();
        mgLevels.back() = is;
        isLevels[rr] = PETSC_NULL;
      }
    }
    {
      Vec sub_d,sub_b;
      VecGetSubVector(D,mgLevels[mgLevels.size()-2],&sub_d);
      VecGetSubVector(solutionAtLevel[rr],mgLevels[mgLevels.size()-2],&sub_b);
      ierr = VecCopy(sub_b,sub_d); CHKERRQ(ierr);
      VecRestoreSubVector(D,mgLevels[mgLevels.size()-2],&sub_d);
      VecRestoreSubVector(solutionAtLevel[rr],mgLevels[mgLevels.size()-2],&sub_b);
    }
    ierr = runKspAtLevel(dm,Aij,F,D,rr%2 == 0); CHKERRQ(ierr);
    ierr = VecDuplicate(D,&solutionAtLevel[rr+1]); CHKERRQ(ierr);
    {
      Vec sub_d,sub_b;
      VecGetSubVector(D,mgLevels.back(),&sub_d);
      VecGetSubVector(solutionAtLevel[rr+1],mgLevels.back(),&sub_b);
      ierr = VecCopy(sub_d,sub_b); CHKERRQ(ierr);
      VecRestoreSubVector(D,mgLevels.back(),&sub_d);
      VecRestoreSubVector(solutionAtLevel[rr+1],mgLevels.back(),&sub_b);
    }

    if(rr%2 != 0) {
      mgLevels.pop_back();
    }

  }

  for(int ll = 0;ll<max_levels;ll++) {
    // PetscPrintf(PETSC_COMM_WORLD,"Destroy %d of %d\n",ll,rr);
    if(isLevels[ll]) {
      // cerr << "destroy " << ll << endl;
      ierr = ISDestroy(&isLevels[ll]); CHKERRQ(ierr);
    }
    if(solutionAtLevel[ll]) {
      ierr = VecDestroy(&solutionAtLevel[ll]); CHKERRQ(ierr);
    }
  }

  ierr = VecDestroy(&commonData.vecErrorOnElements); CHKERRQ(ierr);
  ierr = VecDestroy(&E); CHKERRQ(ierr);

  ierr = KSPDestroy(&sOlver); CHKERRQ(ierr);

  PetscFunctionReturn(0);
}

PetscErrorCode RunAdaptivity::setUpOperators() {
  PetscFunctionBegin;
  ierr = postProcFatPrism.generateReferenceElementMesh(); CHKERRQ(ierr);
  postProcFatPrism.getOpPtrVector().push_back(
    new SolidShellPrismElement::OpGetDirectorVector(commonData)
  );
  postProcFatPrism.getOpPtrVector().push_back(
    new SolidShellPrismElement::OpGetCoVectors(commonData)
  );
  postProcFatPrism.getOpPtrVector().push_back(
    new SolidShellPrismElement::OpGetReferenceBase(commonData)
  );
  postProcFatPrism.getOpPtrVector().push_back(
    new SolidShellPrismElement::OpJacobioan(commonData)
  );
  postProcFatPrism.getOpPtrVector().push_back(
    new SolidShellPrismElement::OpInvJacobian(commonData)
  );
  postProcFatPrism.getOpPtrVector().push_back(
    new SolidShellPrismElement::OpGetStrainDisp(commonData)
  );
  postProcFatPrism.getOpPtrVector().push_back(
    new SolidShellPrismElement::OpGetStrainLocal(commonData,"ZETA")
  );
  postProcFatPrism.getOpPtrVector().push_back(
    new SolidShellPrismElement::OpGetStrainLocal(commonData,"KSIETA")
  );
  postProcFatPrism.getOpPtrVector().push_back(
    new SolidShellPrismElement::OpGetStress(commonData)
  );
  postProcFatPrism.getOpPtrVector().push_back(
    new SolidShellPrismElement::OpPostProcStressAndStrain(
      postProcFatPrism.postProcMesh,
      postProcFatPrism.elementsMap,
      postProcFatPrism.mapGaussPts,
      commonData
    )
  );
  postProcFatPrism.getOpPtrVector().push_back(
    new SolidShellPrismElement::OpGetDisplacements(commonData)
  );
  postProcFatPrism.getOpPtrVector().push_back(
    new SolidShellPrismElement::OpPostProcMeshNodeDispalcements(
      postProcFatPrism.postProcMesh,postProcFatPrism.mapGaussPts,commonData
    )
  );
  postProcFatPrism.getOpPtrVector().push_back(
    new SolidShellPrismElement::OpPostProcMeshNodePositions(
      postProcFatPrism.postProcMesh,postProcFatPrism.mapGaussPts,commonData
    )
  );
  ierr = postProcFatPrism.addFieldValuesGradientPostProc("DISPLACEMENT"); CHKERRQ(ierr);
  PetscFunctionReturn(0);
}

PetscErrorCode RunAdaptivity::postProcFatPrims(const string &name) {
  PetscFunctionBegin;
  ierr = DMoFEMLoopFiniteElements(dM,"SHELL",&postProcFatPrism); CHKERRQ(ierr);
  //Save data on mesh
  rval = postProcFatPrism.postProcMesh.write_file(
    name.c_str(),"MOAB","PARALLEL=WRITE_PART"
  ); CHKERRQ_MOAB(rval);
  PetscFunctionReturn(0);
}

PetscErrorCode RunAdaptivity::printDisplacements() {
  PetscFunctionBegin;
  const DofEntity_multiIndex *dofs_ptr;
  ierr = mField.get_dofs(&dofs_ptr); CHKERRQ(ierr);
  for(_IT_CUBITMESHSETS_BY_NAME_FOR_LOOP_(mField,"RESULTS",it)) {
    EntityHandle meshset = it->getMeshset();
    Range nodes;
    rval = mField.get_moab().get_entities_by_type(meshset,MBVERTEX,nodes,true); CHKERRQ_MOAB(rval);
    Range edges;
    rval = mField.get_moab().get_adjacencies(nodes,1,false,edges,moab::Interface::UNION); CHKERRQ_MOAB(rval);
    Range nodes_faces;
    rval = mField.get_moab().get_adjacencies(nodes,2,false,nodes_faces,moab::Interface::UNION); CHKERRQ_MOAB(rval);
    Range tris = nodes_faces.subset_by_type(MBTRI);
    Range tris_nodes;
    rval = mField.get_moab().get_connectivity(tris,tris_nodes,true); CHKERRQ_MOAB(rval);
    Range faces;
    rval = mField.get_moab().get_adjacencies(edges,2,false,faces,moab::Interface::UNION); CHKERRQ_MOAB(rval);
    Range quads;
    quads = faces.subset_by_type(MBQUAD);
    Range edges_nodes;
    rval = mField.get_moab().get_connectivity(edges,edges_nodes,true); CHKERRQ_MOAB(rval);
    Range quad_nodes;
    rval = mField.get_moab().get_connectivity(quads,quad_nodes,true); CHKERRQ_MOAB(rval);
    nodes.merge(subtract(intersect(edges_nodes,quad_nodes),tris_nodes));
    Range::iterator nit = nodes.begin();
    for(;nit!=nodes.end();nit++) {
      DofEntityByNameAndEnt::iterator dit,hi_dit;
      dit = dofs_ptr->get<Composite_Name_And_Ent_mi_tag>().lower_bound(boost::make_tuple("DISPLACEMENT",*nit));
      hi_dit = dofs_ptr->get<Composite_Name_And_Ent_mi_tag>().upper_bound(boost::make_tuple("DISPLACEMENT",*nit));
      for(;dit!=hi_dit;dit++) {
        EntityHandle ent = dit->get()->getEnt();
        double coords[3];
        rval = mField.get_moab().get_coords(&*nit,1,coords); CHKERRQ_MOAB(rval);
        PetscSynchronizedPrintf(
          PETSC_COMM_WORLD,"DISPLACEMENT [ %d ] %6.4e coord [ %3.4f %3.4f %3.4f ] EntityHandle %ld rank %d\n",
          dit->get()->getDofCoeffIdx(),dit->get()->getFieldData(),coords[0],coords[1],coords[2],ent,mField.get_comm_rank()
        );
      }
    }
  }
  PetscSynchronizedFlush(PETSC_COMM_WORLD,PETSC_STDOUT);
  PetscFunctionReturn(0);
}

}
