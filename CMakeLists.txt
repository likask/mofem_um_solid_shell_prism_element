# MoFEM is free software: you can redistribute it and/or modify it under
# the terms of the GNU Lesser General Public License as published by the
# Free Software Foundation, either version 3 of the License, or (at your
# option) any later version.
#
# MoFEM is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
# License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with MoFEM. If not, see <http://www.gnu.org/licenses/>

include_directories(${UM_SOURCE_DIR}/solid_shell_prism_element/src)

add_custom_target(
  make_link_to_mesh_directory_solid_shell
  ALL
  COMMAND ln -sf ${CMAKE_CURRENT_SOURCE_DIR}/meshes ${CMAKE_CURRENT_BINARY_DIR}
  WORKING_DIRECTORY ${CMAKE_CURRENT_BINARY_DIR}
  COMMENT "Make linking for meshes directory for solid shell module" VERBATIM
)

add_executable(solid_shell_partition_mesh
  ${UM_SOURCE_DIR}/solid_shell_prism_element/solid_shell_partition_mesh.cpp
)
target_link_libraries(solid_shell_partition_mesh
  users_modules
  mofem_finite_elements
  mofem_interfaces
  mofem_multi_indices
  mofem_petsc
  mofem_approx
  mofem_third_party
  mofem_cblas
  ${MoFEM_PROJECT_LIBS}
)

add_executable(solid_shell
  ${UM_SOURCE_DIR}/solid_shell_prism_element/solid_shell.cpp
  ${UM_SOURCE_DIR}/solid_shell_prism_element/src/impl/RunAdaptivity.cpp
  ${UM_SOURCE_DIR}/solid_shell_prism_element/src/impl/HackMG.cpp
  ${UM_SOURCE_DIR}/solid_shell_prism_element/src/impl/SolidShellPrismElement.cpp
)
target_link_libraries(solid_shell
  users_modules
  mofem_finite_elements
  mofem_interfaces
  mofem_multi_indices
  mofem_petsc
  mofem_approx
  mofem_third_party
  mofem_cblas
  ${MoFEM_PROJECT_LIBS}
)

add_executable(solid_shell_nonlinear
  ${UM_SOURCE_DIR}/solid_shell_prism_element/solid_shell_nonlinear.cpp
  ${UM_SOURCE_DIR}/solid_shell_prism_element/src/impl/SolidShellPrismElement.cpp
)
target_link_libraries(solid_shell_nonlinear
  users_modules
  mofem_finite_elements
  mofem_interfaces
  mofem_multi_indices
  mofem_petsc
  mofem_approx
  mofem_third_party
  mofem_cblas
  ${MoFEM_PROJECT_LIBS}
)

add_test(
  tets_solid_shell_partition_mesh
  ${MoFEM_MPI_RUN} ${MPI_RUN_FLAGS} -np 1 ${CMAKE_CURRENT_BINARY_DIR}/solid_shell_partition_mesh
  -my_file ${UM_SOURCE_DIR}/solid_shell_prism_element/meshes/Scordelis-LoRoof_7.h5m
  -my_out_file tmp.h5m -my_nb_parts 2
)

add_test(
  test_solid_shell_and_mg
  ${MoFEM_MPI_RUN} ${MPI_RUN_FLAGS} ${MPI_RUN_FLAGS} -np 2 ${CMAKE_CURRENT_BINARY_DIR}/solid_shell
  -my_file tmp.h5m  -my_is_partitioned 1
  -ksp_type gmres -pc_type lu -pc_factor_mat_solver_package mumps -ksp_atol 1e-5 -ksp_rtol 1e-5
  -ksp_monitor
  -my_order 5 -my_order_thickness 2 -my_order_3d 0
  -my_shell_thickness -0.25 -my_poisson_ratio 0.0 -my_young_modulus 4.32e8
  -my_run_adapt -my_nb_adapt_levels 10
  -pc_type mg
  -mg_coarse_ksp_type preonly -mg_coarse_pc_type lu -mg_coarse_pc_factor_mat_solver_package mumps
  -pc_mg_smoothup 2 -pc_mg_smoothdown 2
  -pc_mg_type multiplicative
  -log_summary
)

add_test(
  test_solid_shell_nonlinear
  ${MoFEM_MPI_RUN} ${MPI_RUN_FLAGS} ${MPI_RUN_FLAGS} -np 2 ${CMAKE_CURRENT_BINARY_DIR}/solid_shell_nonlinear
  -my_file ${UM_SOURCE_DIR}/solid_shell_prism_element/meshes/pinched_cylinder_6_long_compression.h5m
  -snes_monitor -snes_max_it 24 -snes_atol 1e-6 -snes_rtol 1e-5	-snes_stol 0
  -snes_linesearch_type l2 -snes_linesearch_monitor -snes_linesearch_minlambda 0.01
  -snes_converged_reason
  -ksp_type fgmres -pc_type lu -pc_factor_mat_solver_package mumps -ksp_monitor -ksp_atol 1e-12 -ksp_rtol 1e-12
  -ksp_converged_reason
  -my_step_size 1 -my_arc_beta 0
  -my_numerical_damping 0
  -my_order 2 -my_order_thickness 2 -my_order_3d 0
  -my_shell_thickness 1 -my_poisson_ratio 0.3 -my_young_modulus 30
  -ts_monitor -ts_dt 0.001 -ts_final_time  0.002
  -ts_max_snes_failures -1
  -log_summary
)

file(
  COPY pinched_cylinder_convetgence_test.sh.in
  DESTINATION ${CMAKE_CURRENT_BINARY_DIR}
  FILE_PERMISSIONS OWNER_READ OWNER_WRITE OWNER_EXECUTE GROUP_EXECUTE
)
configure_file(
  ${CMAKE_CURRENT_BINARY_DIR}/pinched_cylinder_convetgence_test.sh.in
  ${CMAKE_CURRENT_BINARY_DIR}/pinched_cylinder_convetgence_test.sh
  @ONLY
)

file(
  COPY Scordelis-LoRoof.sh.in
  DESTINATION ${CMAKE_CURRENT_BINARY_DIR}
  FILE_PERMISSIONS OWNER_READ OWNER_WRITE OWNER_EXECUTE GROUP_EXECUTE
)
configure_file(
  ${CMAKE_CURRENT_BINARY_DIR}/Scordelis-LoRoof.sh.in
  ${CMAKE_CURRENT_BINARY_DIR}/Scordelis-LoRoof.sh
  @ONLY
)

file(
  COPY example
  DESTINATION ${CMAKE_CURRENT_BINARY_DIR}
  FILE_PERMISSIONS OWNER_READ OWNER_WRITE
)
file(
  COPY example
  DESTINATION ${CMAKE_CURRENT_BINARY_DIR}
  FILE_PERMISSIONS OWNER_READ OWNER_WRITE OWNER_EXECUTE GROUP_EXECUTE
)
configure_file(
  ${CMAKE_CURRENT_BINARY_DIR}/example/laminate_plate.sh.in
  ${CMAKE_CURRENT_BINARY_DIR}/example/laminate_plate.sh
  @ONLY
)
file(
  COPY snes_config.in
  DESTINATION ${CMAKE_CURRENT_BINARY_DIR}
  FILE_PERMISSIONS OWNER_READ OWNER_WRITE
)

file(COPY Laminate_Analysis.sh.in
  DESTINATION ${CMAKE_CURRENT_BINARY_DIR}
  FILE_PERMISSIONS OWNER_READ OWNER_WRITE OWNER_EXECUTE GROUP_EXECUTE
)
configure_file(
  ${CMAKE_CURRENT_BINARY_DIR}/Laminate_Analysis.sh.in
  ${CMAKE_CURRENT_BINARY_DIR}/Laminate_Analysis.sh
  @ONLY
)


file(COPY cantilever_shear_test.sh.in
  DESTINATION ${CMAKE_CURRENT_BINARY_DIR}
  FILE_PERMISSIONS OWNER_READ OWNER_WRITE OWNER_EXECUTE GROUP_EXECUTE
)
configure_file(
  ${CMAKE_CURRENT_BINARY_DIR}/cantilever_shear_test.sh.in
  ${CMAKE_CURRENT_BINARY_DIR}/cantilever_shear_test.sh
  @ONLY
)

file(COPY anular_plate.sh.in
  DESTINATION ${CMAKE_CURRENT_BINARY_DIR}
  FILE_PERMISSIONS OWNER_READ OWNER_WRITE OWNER_EXECUTE GROUP_EXECUTE
)
configure_file(
  ${CMAKE_CURRENT_BINARY_DIR}/anular_plate.sh.in
  ${CMAKE_CURRENT_BINARY_DIR}/anular_plate.sh
  @ONLY
)
