/** \file solid_shell.cpp
 * \ingroup solid_shell_prism_element
 * \brief Solid shell prism element example
 *
 */

/* This file is part of MoFEM.
 * MoFEM is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * MoFEM is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with MoFEM. If not, see <http://www.gnu.org/licenses/>. */

#include <MoFEM.hpp>
using namespace MoFEM;

#include <boost/program_options.hpp>
using namespace std;
namespace po = boost::program_options;

#include <boost/numeric/ublas/vector_proxy.hpp>
#include <boost/numeric/ublas/matrix.hpp>
#include <boost/numeric/ublas/matrix_proxy.hpp>
#include <boost/numeric/ublas/vector.hpp>
#include <boost/numeric/ublas/symmetric.hpp>

#include <Projection10NodeCoordsOnField.hpp>
#include <PrismsFromSurfaceInterface.hpp>

#include <MethodForForceScaling.hpp>
#include <TimeForceScale.hpp>
#include <SurfacePressure.hpp>
#include <NodalForce.hpp>
#include <EdgeForce.hpp>
#include <DirichletBC.hpp>

#include <PostProcOnRefMesh.hpp>
#include <PCMGSetUpViaApproxOrders.hpp>

#ifdef WITH_ADOL_C
// #define ADOLC_TAPELESS
// #include <adolc/adtl.h>
#include <adolc/adolc.h>
#endif

#include <SolidShellPrismElement.hpp>
#include <algorithm>
#include <RunAdaptivity.hpp>
using namespace SolidShellModule;
#include <FieldApproximation.hpp>

#include <string>




static char help[] =
"...\n"
"\n";

static PetscErrorCode get_bounadry_conditions(MoFEM::Interface &m_field,BitRefLevel &bit_level,Range &quads_bc) {
  PetscFunctionBegin;
  for(_IT_CUBITMESHSETS_BY_BCDATA_TYPE_FOR_LOOP_(m_field,NODESET|DISPLACEMENTSET,it)) {
    DisplacementCubitBcData mydata;
    Range ents;
    ierr = it->getMeshsetIdEntitiesByDimension(m_field.get_moab(),1,ents,true); CHKERRQ(ierr);
    quads_bc.merge(ents);
    ierr = it->getBcDataStructure(mydata); CHKERRQ(ierr);
    if(mydata.data.flag4||mydata.data.flag5||mydata.data.flag6) {
      int nb_ents,new_nb_ents;
      ierr = m_field.get_moab().get_number_entities_by_handle(it->meshset,new_nb_ents,true); CHKERRQ_MOAB(rval);
      do {
        nb_ents = new_nb_ents;
        // Update bc meshset taking children of entities
        ierr = m_field.update_meshset_by_entities_children(
          it->getMeshset(),bit_level,it->getMeshset(),MBVERTEX,true
        ); CHKERRQ(ierr);
        ierr = m_field.update_meshset_by_entities_children(
          it->getMeshset(),bit_level,it->getMeshset(),MBEDGE,true
        ); CHKERRQ(ierr);
        ierr = m_field.update_meshset_by_entities_children(
          it->getMeshset(),bit_level,it->getMeshset(),MBTRI,true
        ); CHKERRQ(ierr);
        ierr = m_field.get_moab().get_number_entities_by_handle(it->meshset,new_nb_ents,true); CHKERRQ_MOAB(rval);
        // cerr << nb_ents << " ::: " << new_nb_ents << endl;
      } while(nb_ents != new_nb_ents);
      ierr = it->getMeshsetIdEntitiesByDimension(m_field.get_moab(),1,ents,true); CHKERRQ(ierr);
      Range faces;
      rval = m_field.get_moab().get_adjacencies(ents,2,false,faces,moab::Interface::UNION); CHKERRQ_MOAB(rval);
      Range quads;
      quads = faces.subset_by_type(MBQUAD);
      rval  = m_field.get_moab().add_entities(it->getMeshset(),quads);
      quads_bc.merge(quads);
      Range edges_quads_bc;
      rval = m_field.get_moab().get_adjacencies(
        quads,1,false,edges_quads_bc,moab::Interface::UNION
      ); CHKERRQ_MOAB(rval);
      quads_bc.merge(edges_quads_bc);
      // Now we have to take layers which are shared by
    }
    // if((!mydata.data.flag1)&&(!mydata.data.flag2)&&(!mydata.data.flag3)) {
    //   rval = m_field.get_moab().remove_entities(it->getMeshset(),ents); CHKERRQ_MOAB(rval);
    // }
  }
  PetscFunctionReturn(0);
}

int main(int argc, char *argv[]) {

  PetscInitialize(&argc,&argv,(char *)0,help);

  // Register various stages for profiling
  PetscLogStage stages[6];
  PetscLogStageRegister("Prelim setup",&stages[0]);
  PetscLogStageRegister("Calculate directors",&stages[1]);
  PetscLogStageRegister("Building DM",&stages[2]);
  PetscLogStageRegister("System assemble",&stages[3]);
  PetscLogStageRegister("Solution",&stages[4]);
  PetscLogStageRegister("Post-processing",&stages[5]);

  try {

    moab::Core mb_instance;
    moab::Interface& moab = mb_instance;
    int rank;
    MPI_Comm_rank(PETSC_COMM_WORLD,&rank);

    //Read parameters from line command
    PetscBool flg_file;
    char mesh_file_name[255];
    PetscInt order = 2,order_thickness = 2,order_3d = 0;
    PetscBool is_partitioned = PETSC_FALSE;
    PetscReal shell_thickness = 0.1;
    double young_modulus = 1;
    double poisson_ratio = 0.25;
    PetscBool run_adapt = PETSC_FALSE;
    int adapt_nb_levels;
    int verbose_run_adaptivity = 0;
    double adaptivity_percent = 0.33;
    int nb_plate_laminate_orientations = 255;
    vector<double> plate_laminate_orientations(nb_plate_laminate_orientations,0);
    PetscBool set_plate_laminate_orientation = PETSC_FALSE;
    char input_file_Dmat[255];
    PetscBool flg_input_file_Dmat = PETSC_FALSE;

    //read options
    {
      ierr = PetscOptionsBegin(
        PETSC_COMM_WORLD,"","Shell prism configure","none"
      ); CHKERRQ(ierr);
      ierr = PetscOptionsString(
        "-my_file",
        "mesh file name","", "mesh.h5m",mesh_file_name, 255, &flg_file
      ); CHKERRQ(ierr);
      ierr = PetscOptionsInt(
        "-my_order",
        "default approximation order",
        "",order,&order,PETSC_NULL
      ); CHKERRQ(ierr);
      ierr = PetscOptionsInt(
        "-my_order_thickness",
        "default approximation order for thickness",
        "",order_thickness,&order_thickness,PETSC_NULL
      ); CHKERRQ(ierr);
      ierr = PetscOptionsInt(
        "-my_order_3d",
        "default approximation order for non planar deformation of element "
        "(polish DEPLANACJA) (3d deformation effect)",
        "",order_3d,&order_3d,PETSC_NULL
      ); CHKERRQ(ierr);


      ierr = PetscOptionsReal(
        "-my_shell_thickness",
        "default shell thickness",
        "",shell_thickness,&shell_thickness,PETSC_NULL
      ); CHKERRQ(ierr);
      ierr = PetscOptionsBool(
        "-my_is_partitioned",
        "set if mesh is partitioned (this result that each process keeps only part of the mesh",
        "",PETSC_FALSE,&is_partitioned,PETSC_NULL
      ); CHKERRQ(ierr);
      // ierr = PetscOptionsString("-my_block_config",
      //   "elastic configure file name","",
      //   "block_conf.in",block_config_file,255,&flg_block_config); CHKERRQ(ierr);
      ierr = PetscOptionsReal(
        "-my_young_modulus","Young modulus","",young_modulus,&young_modulus,0
      ); CHKERRQ(ierr);
      ierr = PetscOptionsReal(
        "-my_poisson_ratio","Poisson ratio","",poisson_ratio,&poisson_ratio,0
      ); CHKERRQ(ierr);

      ierr = PetscOptionsBool(
        "-my_run_adapt",
        "run adaptation",
        "",PETSC_FALSE,&run_adapt,PETSC_NULL
      ); CHKERRQ(ierr);

      adapt_nb_levels = 2*order-1;
      ierr = PetscOptionsInt(
        "-my_nb_adapt_levels",
        "default approximation order",
        "",adapt_nb_levels,&adapt_nb_levels,PETSC_NULL
      ); CHKERRQ(ierr);

      ierr = PetscOptionsReal(
        "-my_adaprivity_percentage",
        "what percent of elements if refined in each step, default 0.33 i.e. 33 percent",
        "",adaptivity_percent,&adaptivity_percent,PETSC_NULL
      ); CHKERRQ(ierr);

      ierr = PetscOptionsInt(
        "-my_verbose_run_adaptivity",
        "verbose adaptivity",
        "",verbose_run_adaptivity,&verbose_run_adaptivity,PETSC_NULL
      ); CHKERRQ(ierr);

      ierr = PetscOptionsRealArray(
        "-my_plate_orientation",
        "array of plate orientations in degree angles",
        "",
        &plate_laminate_orientations[0],
        &nb_plate_laminate_orientations,
        &set_plate_laminate_orientation
      ); CHKERRQ(ierr);
      if(set_plate_laminate_orientation) {
        plate_laminate_orientations.resize(nb_plate_laminate_orientations);
      }

      ierr = PetscOptionsString(
        "-my_dmat_input_file",
        "input material stiffens file","", "material.dat",input_file_Dmat, 255, &flg_input_file_Dmat
      ); CHKERRQ(ierr);

      ierr = PetscOptionsEnd(); CHKERRQ(ierr);
      ierr = PetscPrintf(PETSC_COMM_WORLD,"order %d\n",order); CHKERRQ(ierr);
      ierr = PetscPrintf(PETSC_COMM_WORLD,"order thickness %d\n",order_thickness); CHKERRQ(ierr);
      ierr = PetscPrintf(PETSC_COMM_WORLD,"order 3d %d\n",order_3d); CHKERRQ(ierr);
      ierr = PetscPrintf(PETSC_COMM_WORLD,"shell thickness %4.3e\n",shell_thickness); CHKERRQ(ierr);
    }

    ParallelComm* pcomm = ParallelComm::get_pcomm(&moab,MYPCOMM_INDEX);
    if(pcomm == NULL) pcomm =  new ParallelComm(&moab,PETSC_COMM_WORLD);

    if(is_partitioned == PETSC_TRUE) {
      //Read mesh to MOAB
      const char *option;
      option = "PARALLEL=READ_PART;"
        "PARALLEL_RESOLVE_SHARED_ENTS;"
        "PARTITION=PARALLEL_PARTITION;";
      rval = moab.load_file(mesh_file_name, 0, option); CHKERRQ_MOAB(rval);

    } else {
      const char *option;
      option = "";
      rval = moab.load_file(mesh_file_name, 0, option); CHKERRQ_MOAB(rval);
    }

    Tag th_layer_number;
    int def_layer_number = 0;
    rval = moab.tag_get_handle(
      "LAYER_NUMBER",1,MB_TYPE_INTEGER,th_layer_number,MB_TAG_CREAT|MB_TAG_SPARSE,&def_layer_number
    ); CHKERRQ_MOAB(rval);

    // Stage 0: Preliminary Setup
    PetscLogStagePush(stages[0]);

    //Create MoFEM (Joseph) database
    MoFEM::Core core(moab);
    MoFEM::Interface& m_field = core;

    map<int,Range> map_shell_trinagles;
    map<int,int> shell_laminate_number;
    Range tris;
    {
      Tag th_surface_parametrisation;
      double def_val[] = {0,0,0};
      rval = moab.tag_get_handle(
        "SURFACE_PARAMETRISATION",3,MB_TYPE_DOUBLE,th_surface_parametrisation,MB_TAG_CREAT|MB_TAG_SPARSE,def_val
      ); CHKERRQ_MOAB(rval);
      for(_IT_CUBITMESHSETS_BY_SET_TYPE_FOR_LOOP_(m_field,BLOCKSET,it)) {
        const string &name = it->getName();
        if(name.compare(0,5,"SHELL")==0) {
          Range &it_tris = map_shell_trinagles[it->getMeshsetId()];
          ierr = it->getMeshsetIdEntitiesByType(moab,MBTRI,it_tris,true); CHKERRQ(ierr);
          tris.merge(it_tris);
          // cerr << name << endl;
          vector<double> attributes;
          it->getAttributes(attributes);
          if(attributes.size()==3) {
            // SETERRQ1(
            //   PETSC_COMM_SELF,
            //   MOFEM_DATA_INCONSISTENCY,
            //   "Shell block should have 3 attributes but has %d",attributes.size()
            // );
            for(Range::iterator tit = it_tris.begin();tit!=it_tris.end();tit++) {
              rval = moab.tag_set_data(
                th_surface_parametrisation,&*tit,1,&attributes[0]
              ); CHKERRQ_MOAB(rval);
            }
          }
          EntityHandle meshset = it->getMeshset();
          rval = moab.tag_get_data(
            th_layer_number,&meshset,1,&shell_laminate_number[it->getMeshsetId()]
          ); CHKERRQ_MOAB(rval);
        }
      }
    }

    Range prisms,quads_bc;
    EntityHandle meshset;
    rval = moab.create_meshset(MESHSET_SET,meshset); CHKERRQ_MOAB(rval);
    BitRefLevel bit_level0;
    bit_level0.set(0);

    if(is_partitioned) {

      rval = moab.get_entities_by_type(0,MBPRISM,prisms,false); CHKERRQ_MOAB(rval);
      rval = moab.add_entities(meshset,prisms); CHKERRQ_MOAB(rval);

      PrismsFromSurfaceInterface *prisms_from_surface_interface;
      ierr = m_field.query_interface(prisms_from_surface_interface); CHKERRQ(ierr);
      ierr = prisms_from_surface_interface->seedPrismsEntities(prisms,bit_level0); CHKERRQ(ierr);
      ierr = m_field.seed_ref_level_3D(meshset,bit_level0); CHKERRQ(ierr);

      // get boundary conditions
      ierr = get_bounadry_conditions(m_field,bit_level0,quads_bc); CHKERRQ(ierr);

      // assign gids to entities
      rval = pcomm->assign_global_ids(0,3,0); CHKERRQ_MOAB(rval);

    } else {

      PrismsFromSurfaceInterface *prisms_from_surface_interface;
      ierr = m_field.query_interface(prisms_from_surface_interface); CHKERRQ(ierr);

      //rval = moab.get_entities_by_type(0,MBTRI,tris,false); CHKERRQ_MOAB(rval);
      ierr = prisms_from_surface_interface->createPrisms(tris,prisms); CHKERRQ(ierr);
      ierr = prisms_from_surface_interface->seedPrismsEntities(prisms,bit_level0); CHKERRQ(ierr);
      rval = moab.add_entities(meshset,prisms); CHKERRQ_MOAB(rval);
      ierr = m_field.seed_ref_level_3D(meshset,bit_level0); CHKERRQ(ierr);

      // get bounadry conditions
      ierr = get_bounadry_conditions(m_field,bit_level0,quads_bc); CHKERRQ(ierr);

    }

    Range quads,all_tris_edges,quads_edges,all_tris;
    {
      rval = moab.get_entities_by_type(0,MBTRI,all_tris,false); CHKERRQ_MOAB(rval);
      rval = moab.get_entities_by_type(0,MBQUAD,quads,false); CHKERRQ_MOAB(rval);
      rval = moab.get_adjacencies(all_tris,1,false,all_tris_edges,moab::Interface::UNION); CHKERRQ_MOAB(rval);
      rval = moab.get_adjacencies(quads,1,false,quads_edges,moab::Interface::UNION); CHKERRQ_MOAB(rval);
    }

    //Fields
    ierr = m_field.add_field("ZETA",H1,AINSWORTH_LOBATTO_BASE,1,MB_TAG_SPARSE,MF_ZERO); CHKERRQ(ierr);
    ierr = m_field.add_field("KSIETA",H1,AINSWORTH_LOBATTO_BASE,2,MB_TAG_SPARSE,MF_ZERO); CHKERRQ(ierr);
    ierr = m_field.add_field("DISPLACEMENT",H1,AINSWORTH_LOBATTO_BASE,3,MB_TAG_SPARSE,MF_ZERO); CHKERRQ(ierr);
    if(!is_partitioned) {
      ierr = m_field.add_field("MESH_NODE_POSITIONS",H1,AINSWORTH_LEGENDRE_BASE,3,MB_TAG_SPARSE,MF_ZERO); CHKERRQ(ierr);
    }
    ierr = m_field.add_field("DIRECTOR",H1,AINSWORTH_LOBATTO_BASE,3,MB_TAG_SPARSE,MF_ZERO); CHKERRQ(ierr);
    ierr = m_field.add_field("COVECTORS",H1,AINSWORTH_LOBATTO_BASE,3,MB_TAG_SPARSE,MF_ZERO); CHKERRQ(ierr);
    ierr = m_field.add_field("SCALAR",H1,AINSWORTH_LOBATTO_BASE,1,MB_TAG_SPARSE,MF_ZERO); CHKERRQ(ierr);

    //add entities (by tets) to the field
    ierr = m_field.add_ents_to_field_by_type(meshset,MBPRISM,"ZETA"); CHKERRQ(ierr);
    ierr = m_field.add_ents_to_field_by_type(meshset,MBPRISM,"KSIETA"); CHKERRQ(ierr);
    ierr = m_field.add_ents_to_field_by_type(meshset,MBPRISM,"DISPLACEMENT"); CHKERRQ(ierr);
    if(!is_partitioned) {
      ierr = m_field.add_ents_to_field_by_type(meshset,MBPRISM,"MESH_NODE_POSITIONS"); CHKERRQ(ierr);
    }

    for(
      map<int,Range>::iterator mit = map_shell_trinagles.begin();
      mit!=map_shell_trinagles.end();
      mit++
    ) {
      ierr = m_field.add_ents_to_field_by_type(mit->second,MBTRI,"DIRECTOR"); CHKERRQ(ierr);
      ierr = m_field.add_ents_to_field_by_type(mit->second,MBTRI,"COVECTORS"); CHKERRQ(ierr);
      ierr = m_field.add_ents_to_field_by_type(mit->second,MBTRI,"SCALAR"); CHKERRQ(ierr);
    }

    // ierr = m_field.set_field_order(prisms,"DISPLACEMENT",order_thickness); CHKERRQ(ierr);
    // ierr = m_field.set_field_order(0,MBQUAD,"DISPLACEMENT",order_thickness); CHKERRQ(ierr);
    // ierr = m_field.set_field_order(quads_edges,"DISPLACEMENT",order_thickness); CHKERRQ(ierr);
    ierr = m_field.set_field_order(0,MBTRI,"DISPLACEMENT",order); CHKERRQ(ierr);
    ierr = m_field.set_field_order(all_tris_edges,"DISPLACEMENT",order); CHKERRQ(ierr);
    ierr = m_field.set_field_order(0,MBVERTEX,"DISPLACEMENT",1); CHKERRQ(ierr);

    if(order_thickness) {
      ierr = m_field.set_field_order(prisms,"ZETA",order_thickness); CHKERRQ(ierr);
      ierr = m_field.set_field_order(0,MBQUAD,"ZETA",order_thickness); CHKERRQ(ierr);
      ierr = m_field.set_field_order(
        subtract(quads_edges,all_tris_edges),"ZETA",order_thickness
      ); CHKERRQ(ierr);
    }

    if(order_3d) {
      ierr = m_field.set_field_order(prisms,"KSIETA",order_3d); CHKERRQ(ierr);
      ierr = m_field.set_field_order(0,MBQUAD,"KSIETA",order_3d); CHKERRQ(ierr);
      ierr = m_field.set_field_order(
        subtract(quads_edges,all_tris_edges),"KSIETA",order_3d
      ); CHKERRQ(ierr);
    }

    if(!is_partitioned) {
      // ierr = m_field.set_field_order(quads_edges,"MESH_NODE_POSITIONS",2); CHKERRQ(ierr);
      ierr = m_field.set_field_order(all_tris_edges,"MESH_NODE_POSITIONS",2); CHKERRQ(ierr);
      ierr = m_field.set_field_order(0,MBVERTEX,"MESH_NODE_POSITIONS",1); CHKERRQ(ierr);
    }

    ierr = m_field.set_field_order(all_tris_edges,"DIRECTOR",2); CHKERRQ(ierr);
    ierr = m_field.set_field_order(0,MBVERTEX,"DIRECTOR",1); CHKERRQ(ierr);
    ierr = m_field.set_field_order(all_tris_edges,"COVECTORS",2); CHKERRQ(ierr);
    ierr = m_field.set_field_order(0,MBVERTEX,"COVECTORS",1); CHKERRQ(ierr);
    ierr = m_field.set_field_order(all_tris_edges,"SCALAR",2); CHKERRQ(ierr);
    ierr = m_field.set_field_order(0,MBVERTEX,"SCALAR",1); CHKERRQ(ierr);

    //build field
    ierr = m_field.build_fields(); CHKERRQ(ierr);
    //get HO geometry for 10 node tets
    if(!is_partitioned) {
      Projection10NodeCoordsOnField ent_method(m_field,"MESH_NODE_POSITIONS");
      ierr = m_field.loop_dofs("MESH_NODE_POSITIONS",ent_method); CHKERRQ(ierr);
    }

    //Elements
    ierr = m_field.add_finite_element("SHELL"); CHKERRQ(ierr);
    ierr = m_field.modify_finite_element_add_field_row("SHELL","DISPLACEMENT"); CHKERRQ(ierr);
    ierr = m_field.modify_finite_element_add_field_col("SHELL","DISPLACEMENT"); CHKERRQ(ierr);
    ierr = m_field.modify_finite_element_add_field_data("SHELL","DISPLACEMENT"); CHKERRQ(ierr);
    ierr = m_field.modify_finite_element_add_field_row("SHELL","ZETA"); CHKERRQ(ierr);
    ierr = m_field.modify_finite_element_add_field_col("SHELL","ZETA"); CHKERRQ(ierr);
    ierr = m_field.modify_finite_element_add_field_data("SHELL","ZETA"); CHKERRQ(ierr);
    ierr = m_field.modify_finite_element_add_field_row("SHELL","KSIETA"); CHKERRQ(ierr);
    ierr = m_field.modify_finite_element_add_field_col("SHELL","KSIETA"); CHKERRQ(ierr);
    ierr = m_field.modify_finite_element_add_field_data("SHELL","KSIETA"); CHKERRQ(ierr);
    ierr = m_field.modify_finite_element_add_field_data("SHELL","DIRECTOR"); CHKERRQ(ierr);
    ierr = m_field.modify_finite_element_add_field_data("SHELL","COVECTORS"); CHKERRQ(ierr);
    ierr = m_field.modify_finite_element_add_field_data("SHELL","MESH_NODE_POSITIONS"); CHKERRQ(ierr);
    ierr = m_field.add_ents_to_finite_element_by_type(meshset,MBPRISM,"SHELL"); CHKERRQ(ierr);

    for(
      map<int,Range>::iterator mit = map_shell_trinagles.begin();
      mit!=map_shell_trinagles.end();
      mit++
    ) {
      int id = mit->first;
      string fe_name = "ELEMENT_DIRECTOR_"+SSTR(id);
      ierr = m_field.add_finite_element(fe_name); CHKERRQ(ierr);
      ierr = m_field.modify_finite_element_add_field_row(fe_name,"SCALAR"); CHKERRQ(ierr);
      ierr = m_field.modify_finite_element_add_field_col(fe_name,"SCALAR"); CHKERRQ(ierr);
      ierr = m_field.modify_finite_element_add_field_data(fe_name,"SCALAR"); CHKERRQ(ierr);
      ierr = m_field.modify_finite_element_add_field_data(fe_name,"MESH_NODE_POSITIONS"); CHKERRQ(ierr);
      ierr = m_field.add_ents_to_finite_element_by_type(mit->second,MBTRI,fe_name); CHKERRQ(ierr);
    }

    // Add Neumann forces
    ierr = MetaNeummanForces::addNeumannBCElements(m_field,"DISPLACEMENT"); CHKERRQ(ierr);
    ierr = MetaNodalForces::addElement(m_field,"DISPLACEMENT"); CHKERRQ(ierr);
    ierr = MetaEdgeForces::addElement(m_field,"DISPLACEMENT"); CHKERRQ(ierr);

    //build finite elements
    ierr = m_field.build_finite_elements(); CHKERRQ(ierr);
    //build adjacencies
    ierr = m_field.build_adjacencies(bit_level0); CHKERRQ(ierr);

    // End current profiling stage
    PetscLogStagePop();

    // Stage 1: Calculate directors
    PetscLogStagePush(stages[1]);

    SolidShellPrismElement::CommonData shell_common_data;

    // Calculate normal and co-vectors
    if(1) {
      string name = "DIRECTOR_PROBLEM";
      ierr = m_field.add_problem(name); CHKERRQ(ierr);
      ierr = m_field.modify_problem_ref_level_add_bit(name,bit_level0); CHKERRQ(ierr);
      FieldApproximationH1 normal_approx(m_field);
      SolidShellPrismElement::NormalApprox normal_eval(moab);
      vector<Vec> f(9);
      for(
        map<int,Range>::iterator mit = map_shell_trinagles.begin();
        mit!=map_shell_trinagles.end();
        mit++
      ) {
        int id = mit->first;
        if(plate_laminate_orientations.size()<=shell_laminate_number[id]) {
          SETERRQ(
            PETSC_COMM_WORLD,
            MOFEM_DATA_INCONSISTENCY,
            "More layers than given plate orientations, see command line option -my_plate_orientation"
          );
        }
        normal_eval.aNgle = plate_laminate_orientations[shell_laminate_number[id]];
        DMType dm_name = name.c_str();
        ierr = DMRegister_MoFEM(dm_name); CHKERRQ(ierr);
        DM dm;
        ierr = DMCreate(PETSC_COMM_WORLD,&dm);CHKERRQ(ierr);
        ierr = DMSetType(dm,dm_name);CHKERRQ(ierr);
        ierr = DMMoFEMCreateMoFEM(dm,&m_field,dm_name,bit_level0); CHKERRQ(ierr);
        ierr = DMSetFromOptions(dm); CHKERRQ(ierr);
        ierr = DMMoFEMSetIsPartitioned(dm,is_partitioned); CHKERRQ(ierr);
        //add elements to dm
        ierr = DMMoFEMAddElement(dm,("ELEMENT_DIRECTOR_"+SSTR(id)).c_str()); CHKERRQ(ierr);
        {
          EntityHandle meshset = m_field.get_finite_element_meshset(
            ("ELEMENT_DIRECTOR_"+SSTR(id)).c_str()
          );
          Range tris;
          rval = moab.get_entities_by_type(meshset,MBTRI,tris,true); CHKERRQ_MOAB(rval);
          for(Range::iterator tit = tris.begin();tit!=tris.end();tit++) {
            shell_common_data.shellElementsMap[*tit] = id;
          }
        }
        if(is_partitioned) {
          ierr = DMSetUp(dm); CHKERRQ(ierr);
        } else {
          // Simple partioning
          const MoFEM::Problem *problem_ptr;
          ierr = DMMoFEMGetProblemPtr(dm,&problem_ptr); CHKERRQ(ierr);
          ierr = m_field.build_problem(problem_ptr->getName(),true); CHKERRQ(ierr);
          ierr = m_field.partition_simple_problem(problem_ptr->getName()); CHKERRQ(ierr);
          ierr = m_field.partition_finite_elements(problem_ptr->getName());
          ierr = m_field.partition_ghost_dofs(problem_ptr->getName()); CHKERRQ(ierr);
        }

        Vec D;
        Mat Aij;
        {
          ierr = DMCreateGlobalVector(dm,&D); CHKERRQ(ierr);
          for(int vv = 0;vv<9;vv++) {
            ierr = VecDuplicate(D,&f[vv]); CHKERRQ(ierr);
          }
          ierr = DMCreateMatrix(dm,&Aij); CHKERRQ(ierr);
        }
        {
          normal_approx.feFace.getOpPtrVector().clear();
          ierr = normal_approx.setOperatorsFace("SCALAR",Aij,f,normal_eval); CHKERRQ(ierr);
          ierr = DMoFEMLoopFiniteElements(
            dm,("ELEMENT_DIRECTOR_"+SSTR(id)).c_str(),&normal_approx.feFace
          ); CHKERRQ(ierr);
          for(int vv = 0;vv<9;vv++) {
            ierr = VecAssemblyBegin(f[vv]); CHKERRQ(ierr);
            ierr = VecAssemblyEnd(f[vv]); CHKERRQ(ierr);
            ierr = VecGhostUpdateBegin(f[vv],ADD_VALUES,SCATTER_REVERSE); CHKERRQ(ierr);
            ierr = VecGhostUpdateEnd(f[vv],ADD_VALUES,SCATTER_REVERSE); CHKERRQ(ierr);
          }
          ierr = MatAssemblyBegin(Aij,MAT_FINAL_ASSEMBLY); CHKERRQ(ierr);
          ierr = MatAssemblyEnd(Aij,MAT_FINAL_ASSEMBLY); CHKERRQ(ierr);
          // ierr = MatView(Aij,PETSC_VIEWER_DRAW_SELF); CHKERRQ(ierr);
          // std::string wait;
          // std::cin >> wait;
        }
        {
          KSP solver;
          ierr = KSPCreate(PETSC_COMM_WORLD,&solver); CHKERRQ(ierr);
          ierr = KSPSetDM(solver,dm); CHKERRQ(ierr);
          ierr = KSPSetOperators(solver,Aij,Aij); CHKERRQ(ierr);
          ierr = KSPSetFromOptions(solver); CHKERRQ(ierr);
          PC pc;
          ierr = KSPGetPC(solver,&pc); CHKERRQ(ierr);
          ierr = PCSetType(pc,PCLU); CHKERRQ(ierr);
          ierr = PCFactorSetMatSolverPackage(pc,MATSOLVERMUMPS); CHKERRQ(ierr);
          // Operators are already set, do not use DM for doing that
          ierr = KSPSetDMActive(solver,PETSC_FALSE); CHKERRQ(ierr);
          ierr = KSPSetUp(solver); CHKERRQ(ierr);

          const Problem *problem_ptr;
          ierr = m_field.get_problem("DIRECTOR_PROBLEM",&problem_ptr); CHKERRQ(ierr);
          const DofEntity_multiIndex *dofs_ptr;
          ierr = m_field.get_dofs(&dofs_ptr); CHKERRQ(ierr);
          for(int vv = 0;vv<9;vv++) {
            ierr = KSPSolve(solver,f[vv],D); CHKERRQ(ierr);
            ierr = VecGhostUpdateBegin(D,INSERT_VALUES,SCATTER_FORWARD); CHKERRQ(ierr);
            ierr = VecGhostUpdateEnd(D,INSERT_VALUES,SCATTER_FORWARD); CHKERRQ(ierr);
            ierr = DMoFEMMeshToGlobalVector(dm,D,INSERT_VALUES,SCATTER_REVERSE); CHKERRQ(ierr);
            if(vv < 3) {
              for(
                _IT_NUMEREDDOF_ROW_FOR_LOOP_(problem_ptr,dof)
              ) {
                EntityHandle ent = dof->get()->getEnt();
                int idx = dof->get()->getEntDofIdx();
                double val = dof->get()->getFieldData();
                dofs_ptr->get<Composite_Name_And_Ent_And_EntDofIdx_mi_tag>().find(
                  boost::make_tuple("DIRECTOR",ent,3*idx+vv-0)
                )->get()->getFieldData() += val;
              }
            } else {
              for(
                _IT_NUMEREDDOF_ROW_FOR_LOOP_(problem_ptr,dof)
              ) {
                EntityHandle ent = dof->get()->getEnt();
                int idx = dof->get()->getEntDofIdx();
                double val = dof->get()->getFieldData();
                DofEntity_multiIndex::index<Composite_Name_And_Ent_And_EntDofIdx_mi_tag>::type::iterator cit;
                int m = (vv < 6) ? 3 : 6;
                cit = dofs_ptr->get<Composite_Name_And_Ent_And_EntDofIdx_mi_tag>().find(
                  boost::make_tuple("COVECTORS",ent,3*idx+vv-m)
                );
                if(cit == dofs_ptr->get<Composite_Name_And_Ent_And_EntDofIdx_mi_tag>().end()) {
                  SETERRQ1(PETSC_COMM_SELF,MOFEM_DATA_INCONSISTENCY,"data inconsistency %d",vv);
                }
                cit->get()->getFieldData() += val;
                UId uid = cit->get()->getGlobalUniqueId();
                if(vv < 6) {
                  shell_common_data.coVector0DofsMap[id][uid] = val;
                } else {
                  shell_common_data.coVector1DofsMap[id][uid] = val;
                }
              }
            }
          }
          ierr = KSPDestroy(&solver); CHKERRQ(ierr);
        }
        ierr = MatDestroy(&Aij); CHKERRQ(ierr);
        for(int vv = 0;vv<9;vv++) {
          ierr = VecDestroy(&f[vv]); CHKERRQ(ierr);
        }
        ierr = VecDestroy(&D); CHKERRQ(ierr);
        ierr = DMMoFEMUnSetElement(dm,("ELEMENT_DIRECTOR_"+SSTR(id)).c_str()); CHKERRQ(ierr);
        ierr = DMDestroy(&dm); CHKERRQ(ierr);
        ierr = m_field.clear_problem(name); CHKERRQ(ierr);
      }
      ierr = m_field.delete_problem(name); CHKERRQ(ierr);
    }

    // End current profiling stage
    PetscLogStagePop();

    // Stage 2: Building DM
    PetscLogStagePush(stages[2]);

    //define problems
    ierr = m_field.add_problem("SHELL_PROBLEM"); CHKERRQ(ierr);
    //set refinement level for problem
    ierr = m_field.modify_problem_ref_level_add_bit("SHELL_PROBLEM",bit_level0); CHKERRQ(ierr);
    DMType dm_name = "SHELL_PROBLEM";
    ierr = DMRegister_MGViaApproxOrders(dm_name); CHKERRQ(ierr);
    //craete dm instance
    DM dm;
    {
      ierr = DMCreate(PETSC_COMM_WORLD,&dm);CHKERRQ(ierr);
      ierr = DMSetType(dm,dm_name);CHKERRQ(ierr);
      //set dm data structure which created mofem data structures
      ierr = DMMoFEMCreateMoFEM(dm,&m_field,dm_name,bit_level0); CHKERRQ(ierr);
      ierr = DMSetFromOptions(dm); CHKERRQ(ierr);
      ierr = DMMoFEMSetIsPartitioned(dm,is_partitioned); CHKERRQ(ierr);
      //add elements to dm
      ierr = DMMoFEMAddElement(dm,"SHELL"); CHKERRQ(ierr);
      ierr = DMMoFEMAddElement(dm,"FORCE_FE"); CHKERRQ(ierr);
      ierr = DMMoFEMAddElement(dm,"PRESSURE_FE"); CHKERRQ(ierr);
      ierr = DMSetUp(dm); CHKERRQ(ierr);
      if(!is_partitioned) {
        ierr = DMMoFEMResolveSharedEntities(dm,"SHELL"); CHKERRQ(ierr);
      }
    }

    // End current profiling stage
    PetscLogStagePop();

    // Stage 3: Addemble system
    PetscLogStagePush(stages[3]);

    //create matrices
    Vec F,D;
    Mat Aij;
    {
      ierr = DMCreateGlobalVector(dm,&F); CHKERRQ(ierr);
      ierr = VecDuplicate(F,&D); CHKERRQ(ierr);
      ierr = DMCreateMatrix(dm,&Aij); CHKERRQ(ierr);
    }

    SolidShellPrismElement::SolidShellError elastic_shell_error(m_field,shell_common_data);
    SolidShellPrismElement::SolidShell elastic_shell_rhs(m_field,shell_common_data);
    SolidShellPrismElement::SolidShell elastic_shell_lhs(m_field,shell_common_data);
    boost::ptr_map<string,NeummanForcesSurface> neumann_forces;
    boost::ptr_map<string,NodalForce> nodal_forces;
    boost::ptr_map<string,EdgeForce> edge_forces;
    DirichletDisplacementBc dirichlet_bc(m_field,"DISPLACEMENT",Aij,D,F);
    dirichlet_bc.snes_ctx = FEMethod::CTX_SNESNONE;
    dirichlet_bc.ts_ctx = FEMethod::CTX_TSNONE;
    DirichletFixFieldAtEntitiesBc fix_ents(m_field,"KSIETA",Aij,D,F,quads_bc);
    fix_ents.snes_ctx = FEMethod::CTX_SNESNONE;
    fix_ents.ts_ctx = FEMethod::CTX_TSNONE;
    if(1) {

      ierr = VecZeroEntries(F); CHKERRQ(ierr);
      ierr = VecGhostUpdateBegin(F,INSERT_VALUES,SCATTER_FORWARD); CHKERRQ(ierr);
      ierr = VecGhostUpdateEnd(F,INSERT_VALUES,SCATTER_FORWARD); CHKERRQ(ierr);
      ierr = VecGhostUpdateBegin(F,INSERT_VALUES,SCATTER_FORWARD); CHKERRQ(ierr);
      ierr = VecGhostUpdateEnd(F,INSERT_VALUES,SCATTER_FORWARD); CHKERRQ(ierr);
      ierr = VecZeroEntries(D); CHKERRQ(ierr);
      ierr = VecGhostUpdateBegin(D,INSERT_VALUES,SCATTER_FORWARD); CHKERRQ(ierr);
      ierr = VecGhostUpdateEnd(D,INSERT_VALUES,SCATTER_FORWARD); CHKERRQ(ierr);
      ierr = DMoFEMMeshToGlobalVector(dm,D,INSERT_VALUES,SCATTER_REVERSE); CHKERRQ(ierr);
      ierr = MatZeroEntries(Aij); CHKERRQ(ierr);

      //assemble Aij and F
      {
        shell_common_data.meshNodalPosition = "MESH_NODE_POSITIONS";
        shell_common_data.displacementFieldName = "DISPLACEMENT";
        shell_common_data.zetaFieldName = "ZETA";
        shell_common_data.ksiEtaFieldName = "KSIETA";
        shell_common_data.directorFieldName = "DIRECTOR";
        shell_common_data.coVectorFieldName = "COVECTORS";
        shell_common_data.tHickness = shell_thickness;
        shell_common_data.sHift = shell_thickness;
        if(!flg_input_file_Dmat) {
          PetscPrintf(
            PETSC_COMM_WORLD,
            "young modulus = %6.4e poisson_ratio = %6.4e\n",
            young_modulus,
            poisson_ratio
          );
          MatrixDouble D_lambda,D_mu;
          double mu = MU(young_modulus,poisson_ratio);
          double lambda = LAMBDA(young_modulus,poisson_ratio);
          PetscPrintf(PETSC_COMM_WORLD,"mu = %6.4e lambda = %6.4e\n",mu,lambda);
          D_lambda.resize(6,6);
          D_lambda.clear();
          for(int rr = 0;rr<3;rr++) {
            for(int cc = 0;cc<3;cc++) {
              D_lambda(rr,cc) = 1;
            }
          }
          D_mu.resize(6,6);
          D_mu.clear();
          for(int rr = 0;rr<6;rr++) {
            D_mu(rr,rr) = rr<3 ? 2 : 1;
          }
          shell_common_data.materialStiffness = lambda*D_lambda + mu*D_mu;
        } else {
          shell_common_data.materialStiffness.resize(6,6,false);
          int fd;
          PetscViewer view_in;
          PetscViewerBinaryOpen(PETSC_COMM_SELF,input_file_Dmat,FILE_MODE_READ,&view_in);
          PetscViewerBinaryGetDescriptor(view_in,&fd);
          double *ptr = &shell_common_data.materialStiffness(0,0);
          PetscBinaryRead(fd,ptr,36,PETSC_DOUBLE);
          PetscViewerDestroy(&view_in);

          // int fd;
          // PetscViewer view_out;
          // ierr = PetscViewerBinaryOpen(
          //   PETSC_COMM_SELF,input_file_Dmat,FILE_MODE_READ,&view_out
          // ); CHKERRQ(ierr);
          // ierr = PetscViewerBinaryGetDescriptor(view_out,&fd); CHKERRQ(ierr);
          // double *ptr = &shell_common_data.materialStiffness(0,0);
          // ierr = PetscBinaryRead(fd,ptr,36,PETSC_DOUBLE); CHKERRQ(ierr);
          // ierr = PetscViewerDestroy(&view_out); CHKERRQ(ierr);
          if(m_field.get_comm_rank()==0) {
            cout << "Material stiffnes file" << endl;
            cout << shell_common_data.materialStiffness << endl;
          }
        }

      }

      // Error
      elastic_shell_error.getOpPtrVector().push_back(
        new SolidShellPrismElement::OpGetDirectorVector(shell_common_data)
      );
      elastic_shell_error.getOpPtrVector().push_back(
        new SolidShellPrismElement::OpGetCoVectors(shell_common_data)
      );
      elastic_shell_error.getOpPtrVector().push_back(
        new SolidShellPrismElement::OpGetReferenceBase(shell_common_data)
      );
      elastic_shell_error.getOpPtrVector().push_back(
        new SolidShellPrismElement::OpJacobioan(shell_common_data)
      );
      elastic_shell_error.getOpPtrVector().push_back(
        new SolidShellPrismElement::OpInvJacobian(shell_common_data)
      );
      elastic_shell_error.getOpPtrVector().push_back(
        new SolidShellPrismElement::OpGetStrainDisp(shell_common_data)
      );
      elastic_shell_error.getOpPtrVector().push_back(
        new SolidShellPrismElement::OpGetStrainLocal(shell_common_data,"ZETA")
      );
      elastic_shell_error.getOpPtrVector().push_back(
        new SolidShellPrismElement::OpGetStrainLocal(shell_common_data,"KSIETA")
      );
      elastic_shell_error.getOpPtrVector().push_back(
        new SolidShellPrismElement::OpGetStress(shell_common_data)
      );
      elastic_shell_error.getOpPtrVector().push_back(
        new SolidShellPrismElement::OpEnergyNormH1(shell_common_data)
      );

      // Assemble vector
      elastic_shell_rhs.getOpPtrVector().push_back(
        new SolidShellPrismElement::OpGetDirectorVector(shell_common_data)
      );
      elastic_shell_rhs.getOpPtrVector().push_back(
        new SolidShellPrismElement::OpGetCoVectors(shell_common_data)
      );
      elastic_shell_rhs.getOpPtrVector().push_back(
        new SolidShellPrismElement::OpGetReferenceBase(shell_common_data)
      );
      elastic_shell_rhs.getOpPtrVector().push_back(
        new SolidShellPrismElement::OpJacobioan(shell_common_data)
      );
      elastic_shell_rhs.getOpPtrVector().push_back(
        new SolidShellPrismElement::OpInvJacobian(shell_common_data)
      );
      // bool small_strain = false;
      elastic_shell_rhs.getOpPtrVector().push_back(
        new SolidShellPrismElement::OpGetStrainDisp(shell_common_data)
      );
      elastic_shell_rhs.getOpPtrVector().push_back(
        new SolidShellPrismElement::OpGetStrainLocal(shell_common_data,"ZETA")
      );
      elastic_shell_rhs.getOpPtrVector().push_back(
        new SolidShellPrismElement::OpGetStrainLocal(shell_common_data,"KSIETA")
      );
      elastic_shell_rhs.getOpPtrVector().push_back(
        new SolidShellPrismElement::OpGetStress(shell_common_data)
      );
      elastic_shell_rhs.getOpPtrVector().push_back(
        new SolidShellPrismElement::OpAssembleRhsDisp(shell_common_data)
      );
      elastic_shell_rhs.getOpPtrVector().push_back(
        new SolidShellPrismElement::OpAssembleRhsLocal(shell_common_data,"ZETA")
      );
      elastic_shell_rhs.getOpPtrVector().push_back(
        new SolidShellPrismElement::OpAssembleRhsLocal(shell_common_data,"KSIETA")
      );

      // Assemble matrix
      elastic_shell_lhs.getOpPtrVector().push_back(
        new SolidShellPrismElement::OpGetDirectorVector(shell_common_data)
      );
      elastic_shell_lhs.getOpPtrVector().push_back(
        new SolidShellPrismElement::OpGetCoVectors(shell_common_data)
      );
      elastic_shell_lhs.getOpPtrVector().push_back(
        new SolidShellPrismElement::OpGetReferenceBase(shell_common_data)
      );
      elastic_shell_lhs.getOpPtrVector().push_back(
        new SolidShellPrismElement::OpJacobioan(shell_common_data)
      );
      elastic_shell_lhs.getOpPtrVector().push_back(
        new SolidShellPrismElement::OpInvJacobian(shell_common_data)
      );
      elastic_shell_lhs.getOpPtrVector().push_back(
        new SolidShellPrismElement::OpAssembleLhsMix(shell_common_data,"DISPLACEMENT","DISPLACEMENT",true)
      );
      elastic_shell_lhs.getOpPtrVector().push_back(
        new SolidShellPrismElement::OpAssembleLhsMix(shell_common_data,"ZETA","ZETA",true)
      );
      elastic_shell_lhs.getOpPtrVector().push_back(
        new SolidShellPrismElement::OpAssembleLhsMix(shell_common_data,"KSIETA","KSIETA",true)
      );
      elastic_shell_lhs.getOpPtrVector().push_back(
        new SolidShellPrismElement::OpAssembleLhsMix(shell_common_data,"DISPLACEMENT","ZETA",false)
      );
      elastic_shell_lhs.getOpPtrVector().push_back(
        new SolidShellPrismElement::OpAssembleLhsMix(shell_common_data,"DISPLACEMENT","KSIETA",false)
      );
      elastic_shell_lhs.getOpPtrVector().push_back(
        new SolidShellPrismElement::OpAssembleLhsMix(shell_common_data,"ZETA","KSIETA",false)
      );

      ierr = MetaNeummanForces::setMomentumFluxOperators(m_field,neumann_forces,F,"DISPLACEMENT"); CHKERRQ(ierr);
      ierr = MetaNodalForces::setOperators(m_field,nodal_forces,F,"DISPLACEMENT"); CHKERRQ(ierr);
      ierr = MetaEdgeForces::setOperators(m_field,edge_forces,F,"DISPLACEMENT"); CHKERRQ(ierr);

      ierr = DMoFEMPreProcessFiniteElements(dm,&dirichlet_bc); CHKERRQ(ierr);
      ierr = DMoFEMPreProcessFiniteElements(dm,&fix_ents); CHKERRQ(ierr);
      //internal force vector (to take into account Dirichlet boundary conditions)
      // elastic_shell_rhs.snes_f = F;
      // ierr = DMoFEMLoopFiniteElements(dm,"SHELL",&elastic_shell_rhs); CHKERRQ(ierr);
      // elastic_shell element matrix
      elastic_shell_lhs.snes_B = Aij;
      ierr = DMoFEMLoopFiniteElements(dm,"SHELL",&elastic_shell_lhs); CHKERRQ(ierr);
      {
        boost::ptr_map<string,NeummanForcesSurface>::iterator mit;
        //forces and pressures on surface
        mit = neumann_forces.begin();
        for(;mit!=neumann_forces.end();mit++) {
          ierr = DMoFEMLoopFiniteElements(dm,mit->first.c_str(),&mit->second->getLoopFe()); CHKERRQ(ierr);
        }
        //noadl forces
        mit = nodal_forces.begin();
        for(;mit!=nodal_forces.end();mit++) {
          ierr = DMoFEMLoopFiniteElements(dm,mit->first.c_str(),&mit->second->getLoopFe()); CHKERRQ(ierr);
        }
        //edge forces
        mit = edge_forces.begin();
        for(;mit!=edge_forces.end();mit++) {
          ierr = DMoFEMLoopFiniteElements(dm,mit->first.c_str(),&mit->second->getLoopFe()); CHKERRQ(ierr);
        }
      }


      ierr = DMoFEMPostProcessFiniteElements(dm,&dirichlet_bc); CHKERRQ(ierr);
      ierr = DMoFEMPostProcessFiniteElements(dm,&fix_ents); CHKERRQ(ierr);
      ierr = VecGhostUpdateBegin(F,ADD_VALUES,SCATTER_REVERSE); CHKERRQ(ierr);
      ierr = VecGhostUpdateEnd(F,ADD_VALUES,SCATTER_REVERSE); CHKERRQ(ierr);
      ierr = VecScale(F,-1); CHKERRQ(ierr);
      double nrm2_F;
      ierr = VecNorm(F,NORM_2,&nrm2_F); CHKERRQ(ierr);
      PetscPrintf(PETSC_COMM_WORLD,"nrm2_F = %6.4e\n",nrm2_F);

      // ierr = VecView(F,PETSC_VIEWER_STDOUT_WORLD); CHKERRQ(ierr);
      // ierr = MatView(Aij,PETSC_VIEWER_DRAW_SELF); CHKERRQ(ierr);
      // std::string wait;
      // std::cin >> wait;

      // double nrm2_A;
      // ierr = MatNorm(Aij,NORM_FROBENIUS,&nrm2_A); CHKERRQ(ierr);
      // PetscPrintf(PETSC_COMM_WORLD,"nrm2_A = %6.4e\n",nrm2_A);


    }

    // End current profiling stage
    PetscLogStagePop();

    RunAdaptivity run_adaptivity(
      m_field,dm,shell_common_data,elastic_shell_lhs,elastic_shell_error,dirichlet_bc,fix_ents
    );
    ierr = run_adaptivity.setUpOperators(); CHKERRQ(ierr);

    // solve problem
    if(1) {

      // The exchanged cells will be used to keep approx. order of adjacent entities
      if(is_partitioned) {

        // Now exchange 1 layer of ghost elements, using vertices as bridge
        // (we could have done this as part of reading process, using the PARALLEL_GHOSTS read option)
        rval = pcomm->exchange_ghost_cells(
          3, // int ghost_dim,
          1, // int bridge_dim,
          1, //int num_layers,
          0, //int addl_ents,
          true // bool store_remote_handles);
        ); CHKERRQ_MOAB(rval);

        // if(1) {
        //   vector<Tag> tags;
        //   tags.push_back(pcomm->part_tag());
        //   Tag th_global_id;
        //   rval = m_field.get_moab().tag_get_handle(GLOBAL_ID_TAG_NAME,th_global_id); CHKERRQ_MOAB(rval);
        //   tags.push_back(th_global_id);
        //   for(int rr = 0;rr<m_field.get_comm_size();rr++) {
        //     if(m_field.get_comm_rank()==rr) {
        //       ostringstream o1;
        //       o1 << "exchange_" << rr << ".vtk";
        //       m_field.get_moab().write_file(o1.str().c_str(),"VTK");
        //     }
        //   }
        // }

      }

      // Stage 4: Solve system
      PetscLogStagePush(stages[4]);

      if(!run_adapt) {
        KSP solver;
        ierr = KSPCreate(PETSC_COMM_WORLD,&solver); CHKERRQ(ierr);
        ierr = KSPSetDM(solver,dm); CHKERRQ(ierr);
        ierr = KSPSetOperators(solver,Aij,Aij); CHKERRQ(ierr);
        ierr = KSPSetFromOptions(solver); CHKERRQ(ierr);
        {
          //from PETSc example ex42.c
          PetscBool same = PETSC_FALSE;
          PC pc;
          ierr = KSPGetPC(solver,&pc); CHKERRQ(ierr);
          PetscObjectTypeCompare((PetscObject)pc,PCMG,&same);
          if (same) {
            PCMGSetUpViaApproxOrdersCtx pc_ctx(dm,Aij,false);
            ierr = PCMGSetUpViaApproxOrders(pc,&pc_ctx); CHKERRQ(ierr);
          } else {
            // Operators are already set, do not use DM for doing that
            ierr = KSPSetDMActive(solver,PETSC_FALSE); CHKERRQ(ierr);
          }
        }
        ierr = KSPSetUp(solver); CHKERRQ(ierr);

        ierr = KSPSolve(solver,F,D); CHKERRQ(ierr);
        ierr = VecGhostUpdateBegin(D,INSERT_VALUES,SCATTER_FORWARD); CHKERRQ(ierr);
        ierr = VecGhostUpdateEnd(D,INSERT_VALUES,SCATTER_FORWARD); CHKERRQ(ierr);
        ierr = KSPDestroy(&solver); CHKERRQ(ierr);
        ierr = DMoFEMMeshToLocalVector(
          dm,D,INSERT_VALUES,SCATTER_REVERSE
        ); CHKERRQ(ierr);

        PetscLayout layout;
        ierr = DMMoFEMGetProblemFiniteElementLayout(dm,"SHELL",&layout); CHKERRQ(ierr);
        int size;
        ierr = PetscLayoutGetLocalSize(layout,&size); CHKERRQ(ierr);
        // {
        //   PetscSynchronizedPrintf(PETSC_COMM_WORLD,"Number of shell element on part %d on part %d\n",size,m_field.get_comm_rank());
        //   PetscSynchronizedFlush(PETSC_COMM_WORLD,PETSC_STDOUT);
        // }
        ierr = VecCreateMPI(PETSC_COMM_WORLD,size,PETSC_DETERMINE,&shell_common_data.vecErrorOnElements); CHKERRQ(ierr);
        ierr = PetscLayoutDestroy(&layout); CHKERRQ(ierr);

        ierr = DMoFEMLoopFiniteElements(dm,"SHELL",&elastic_shell_error); CHKERRQ(ierr);
        // ierr = VecView(shell_common_data.vecErrorOnElements,PETSC_VIEWER_STDOUT_WORLD); CHKERRQ(ierr);

        double energy;
        ierr = VecSum(shell_common_data.vecErrorOnElements,&energy); CHKERRQ(ierr);

        PetscPrintf(PETSC_COMM_WORLD,"Energy = %8.6e\n",energy);
        ierr = VecDestroy(&shell_common_data.vecErrorOnElements); CHKERRQ(ierr);

      } else {

        ierr = run_adaptivity.solveProblem(dm,Aij,F,D,adapt_nb_levels,order,adaptivity_percent,verbose_run_adaptivity); CHKERRQ(ierr);
        ierr = DMoFEMMeshToLocalVector(
          dm,D,INSERT_VALUES,SCATTER_REVERSE
        ); CHKERRQ(ierr);

      }

      // End current profiling stage
      PetscLogStagePop();

      // Stage 5: Post process
      PetscLogStagePush(stages[5]);

      ierr = run_adaptivity.printDisplacements(); CHKERRQ(ierr);
      ierr = run_adaptivity.postProcFatPrims("solid.h5m"); CHKERRQ(ierr);

      // Post proc shell
      if(1) {
        SolidShellPrismElement::PostProcFatPrismOnTriangleOnRefinedMesh post_proc(m_field);
        ierr = post_proc.generateReferenceElementMesh(); CHKERRQ(ierr);
        // ierr = post_proc.addFieldValuesPostProc("DISPLACEMENT"); CHKERRQ(ierr);
        // ierr = post_proc.addFieldValuesPostProc("MESH_NODE_POSITIONS"); CHKERRQ(ierr);
        post_proc.getOpPtrVector().push_back(
          new SolidShellPrismElement::OpGetDirectorVector(shell_common_data)
        );
        post_proc.getOpPtrVector().push_back(
          new SolidShellPrismElement::OpGetCoVectors(shell_common_data)
        );
        post_proc.getOpPtrVector().push_back(
          new SolidShellPrismElement::OpGetReferenceBase(shell_common_data)
        );
        post_proc.getOpPtrVector().push_back(
          new SolidShellPrismElement::OpJacobioan(shell_common_data)
        );
        post_proc.getOpPtrVector().push_back(
          new SolidShellPrismElement::OpInvJacobian(shell_common_data)
        );
        post_proc.getOpPtrVector().push_back(
          new SolidShellPrismElement::OpGetDisplacements(shell_common_data)
        );
        post_proc.getOpPtrVector().push_back(
          new SolidShellPrismElement::OpPostProcMeshNodeDispalcements(
            post_proc.postProcMesh,post_proc.mapGaussPts,shell_common_data
          )
        );
        post_proc.getOpPtrVector().push_back(
          new SolidShellPrismElement::OpPostProcMeshNodePositions(
            post_proc.postProcMesh,post_proc.mapGaussPts,shell_common_data
          )
        );
        post_proc.getOpPtrVector().push_back(
          new SolidShellPrismElement::OpGetStrainDisp(shell_common_data)
        );
        post_proc.getOpPtrVector().push_back(
          new SolidShellPrismElement::OpGetStrainLocal(shell_common_data,"ZETA")
        );
        post_proc.getOpPtrVector().push_back(
          new SolidShellPrismElement::OpGetStrainLocal(shell_common_data,"KSIETA")
        );
        post_proc.getOpPtrVector().push_back(
          new SolidShellPrismElement::OpGetStress(shell_common_data)
        );
        post_proc.getOpPtrVector().push_back(
          new SolidShellPrismElement::OpGetAxialForces(
            post_proc.postProcMesh,post_proc.mapGaussPts,shell_common_data
          )
        );
        post_proc.getOpPtrVector().push_back(
          new SolidShellPrismElement::OpGetMoment(
            post_proc.postProcMesh,post_proc.mapGaussPts,shell_common_data
          )
        );
        ierr = DMoFEMLoopFiniteElements(dm,"SHELL",&post_proc); CHKERRQ(ierr);
        //Save data on mesh
        rval = post_proc.postProcMesh.write_file(
          "shell.h5m","MOAB","PARALLEL=WRITE_PART"
        ); CHKERRQ_MOAB(rval);
      }

      // Calculate residual
      if(1) {
        ierr = VecZeroEntries(F); CHKERRQ(ierr);
        ierr = VecGhostUpdateBegin(F,INSERT_VALUES,SCATTER_FORWARD); CHKERRQ(ierr);
        ierr = VecGhostUpdateEnd(F,INSERT_VALUES,SCATTER_FORWARD); CHKERRQ(ierr);
        dirichlet_bc.snes_f = F;
        ierr = DMoFEMPreProcessFiniteElements(dm,&dirichlet_bc); CHKERRQ(ierr);
        fix_ents.snes_f = F;
        ierr = DMoFEMPreProcessFiniteElements(dm,&fix_ents); CHKERRQ(ierr);
        elastic_shell_rhs.snes_f = F;
        ierr = DMoFEMLoopFiniteElements(dm,"SHELL",&elastic_shell_rhs); CHKERRQ(ierr);
        {
          boost::ptr_map<string,NeummanForcesSurface>::iterator mit;
          //forces and pressures on surface
          mit = neumann_forces.begin();
          for(;mit!=neumann_forces.end();mit++) {
            ierr = DMoFEMLoopFiniteElements(dm,mit->first.c_str(),&mit->second->getLoopFe()); CHKERRQ(ierr);
          }
          //noadl forces
          mit = nodal_forces.begin();
          for(;mit!=nodal_forces.end();mit++) {
            ierr = DMoFEMLoopFiniteElements(dm,mit->first.c_str(),&mit->second->getLoopFe()); CHKERRQ(ierr);
          }
          //edge forces
          mit = edge_forces.begin();
          for(;mit!=edge_forces.end();mit++) {
            ierr = DMoFEMLoopFiniteElements(dm,mit->first.c_str(),&mit->second->getLoopFe()); CHKERRQ(ierr);
          }
        }
        ierr = DMoFEMPostProcessFiniteElements(dm,&dirichlet_bc); CHKERRQ(ierr);
        ierr = DMoFEMPostProcessFiniteElements(dm,&fix_ents); CHKERRQ(ierr);
        ierr = VecGhostUpdateBegin(F,ADD_VALUES,SCATTER_REVERSE); CHKERRQ(ierr);
        ierr = VecGhostUpdateEnd(F,ADD_VALUES,SCATTER_REVERSE); CHKERRQ(ierr);
        double nrm2_F;
        ierr = VecNorm(F,NORM_2,&nrm2_F); CHKERRQ(ierr);
        PetscPrintf(PETSC_COMM_WORLD,"nrm2_F = %6.4e\n",nrm2_F);
      }



    }

    {
      ierr = VecDestroy(&F); CHKERRQ(ierr);
      ierr = VecDestroy(&D); CHKERRQ(ierr);
      ierr = MatDestroy(&Aij); CHKERRQ(ierr);
      ierr = DMDestroy(&dm); CHKERRQ(ierr);
    }

    // End current profiling stage
    PetscLogStagePop();

  } catch (MoFEMException const &e) {
    SETERRQ(PETSC_COMM_SELF,e.errorCode,e.errorMessage);
  }

  PetscFinalize();
  return 0;

}
