/** \file solid_shell_partition_mesh.cpp
 * \ingroup solid_shell_prism_element
 * \brief Solid shell prism element example
 *
 */

/* This file is part of MoFEM.
 * MoFEM is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * MoFEM is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with MoFEM. If not, see <http://www.gnu.org/licenses/>. */

#include <MoFEM.hpp>
using namespace MoFEM;

#include <PrismsFromSurfaceInterface.hpp>
#include <Projection10NodeCoordsOnField.hpp>




static char help[] =
"Create prisms and partition mesh\n"
"\n";

int main(int argc, char *argv[]) {

  PetscInitialize(&argc,&argv,(char *)0,help);

  try {

    moab::Core mb_instance;
    moab::Interface& moab = mb_instance;
    int rank;
    MPI_Comm_rank(PETSC_COMM_WORLD,&rank);

    //Read parameters from line command
    PetscBool flg_file,flg_out_file;
    char mesh_file_name[255] = "mesh.h5m";
    char out_file_name[255] = "partitioned_mesh";

    int nb_parts = 1;
    int nb_layers = 1;

    //read options
    {
      ierr = PetscOptionsBegin(
        PETSC_COMM_WORLD,"","Shell prism configure","none"
      ); CHKERRQ(ierr);
      ierr = PetscOptionsString(
        "-my_file",
        "mesh file name","",mesh_file_name,mesh_file_name,255,&flg_file
      ); CHKERRQ(ierr);
      ierr = PetscOptionsString(
        "-my_out_file",
        "partitioned file name","",out_file_name,out_file_name,255,&flg_out_file
      ); CHKERRQ(ierr);
      ierr = PetscOptionsInt(
        "-my_nb_parts",
        "number of parts",
        "",nb_parts,&nb_parts,PETSC_NULL
      ); CHKERRQ(ierr);
      ierr = PetscOptionsInt(
        "-my_nb_layers",
        "number of layers",
        "",nb_layers,&nb_layers,PETSC_NULL
      ); CHKERRQ(ierr);
      ierr = PetscOptionsEnd(); CHKERRQ(ierr);
    }

    ParallelComm* pcomm = ParallelComm::get_pcomm(&moab,MYPCOMM_INDEX);
    if(pcomm == NULL) pcomm = new ParallelComm(&moab,PETSC_COMM_WORLD);

    const char *option;
    option = "";
    rval = moab.load_file(mesh_file_name, 0, option); CHKERRQ_MOAB(rval);

    //Create MoFEM (Joseph) database
    MoFEM::Core core(moab);
    MoFEM::Interface& m_field = core;

    // get shell surface elements
    Tag th_surface_parametrisation;
    map<int,Range> map_shell_trinagles;
    Range tris;
    {
      double def_val[] = {0,0,0};
      rval = moab.tag_get_handle(
        "SURFACE_PARAMETRISATION",3,MB_TYPE_DOUBLE,th_surface_parametrisation,MB_TAG_CREAT|MB_TAG_SPARSE,def_val
      ); CHKERRQ_MOAB(rval);
      for(_IT_CUBITMESHSETS_BY_SET_TYPE_FOR_LOOP_(m_field,BLOCKSET,it)) {
        const string &name = it->getName();
        if(name.compare(0,5,"SHELL")==0) {
          vector<double> attributes;
          it->getAttributes(attributes);
          if(attributes.size()!=3) {
            SETERRQ1(
              PETSC_COMM_SELF,
              MOFEM_DATA_INCONSISTENCY,
              "Shell block should have 3 attributes but has %d",attributes.size()
            );
          }
          Range &it_tris = map_shell_trinagles[it->getMeshsetId()];
          ierr = it->getMeshsetIdEntitiesByType(moab,MBTRI,it_tris,true); CHKERRQ(ierr);
          tris.merge(it_tris);
          for(Range::iterator tit = it_tris.begin();tit!=it_tris.end();tit++) {
            rval = moab.tag_set_data(
              th_surface_parametrisation,&*tit,1,&attributes[0]
            ); CHKERRQ_MOAB(rval);
          }
        }
      }
    }

    Tag th_layer_number;
    int def_layer_number = 0;
    rval = moab.tag_get_handle(
      "LAYER_NUMBER",1,MB_TYPE_INTEGER,th_layer_number,MB_TAG_CREAT|MB_TAG_SPARSE,&def_layer_number
    ); CHKERRQ_MOAB(rval);

    PrismsFromSurfaceInterface *prisms_from_surface_interface;
    ierr = m_field.query_interface(prisms_from_surface_interface); CHKERRQ(ierr);

    // create prisms
    Range prisms;
    ierr = prisms_from_surface_interface->createPrisms(tris,prisms); CHKERRQ(ierr);
    // const double zero[] = {0,0,0};
    // const double disp3[] = {0,2.0,0};
    // const double disp4[] = {0,1.0,0};
    // ierr = prisms_from_surface_interface->setThickness(prisms,zero,disp3); CHKERRQ(ierr);
    Range old_layer = prisms.subset_by_type(MBPRISM);
    for(int ll = 1;ll<nb_layers;ll++) {
      ierr = m_field.add_cubit_msId(BLOCKSET,1000+ll,"SHELL"+SSTR(1000+ll)); CHKERRQ(ierr);
      const CubitMeshSets *cubit_meshset_ptr;
      ierr = m_field.get_cubit_msId(1000+ll,BLOCKSET,&cubit_meshset_ptr); CHKERRQ(ierr);
      // copy surface parametrization from one face to another
      for(Range::iterator pit = old_layer.begin();pit!=old_layer.end();pit++) {
        EntityHandle face;
        rval = m_field.get_moab().side_element(*pit,2,3,face); CHKERRQ_MOAB(rval);
        vector<double> attributes(3);
        rval = moab.tag_get_data(
          th_surface_parametrisation,&face,1,&attributes[0]
        ); CHKERRQ_MOAB(rval);
        rval = m_field.get_moab().side_element(*pit,2,4,face); CHKERRQ_MOAB(rval);
        rval = moab.tag_set_data(
          th_surface_parametrisation,&face,1,&attributes[0]
        ); CHKERRQ_MOAB(rval);
        rval = moab.tag_set_data(
          th_layer_number,&face,1,&ll
        ); CHKERRQ_MOAB(rval);
        rval = moab.add_entities(cubit_meshset_ptr->meshset,&face,1); CHKERRQ_MOAB(rval);
        rval = moab.tag_set_data(th_layer_number,&cubit_meshset_ptr->meshset,1,&ll); CHKERRQ_MOAB(rval);
      }
      prisms_from_surface_interface->createdVertices.clear();
      Range add_layer;
      ierr = prisms_from_surface_interface->createPrismsFromPrisms(old_layer,false,add_layer); CHKERRQ(ierr);
      // ierr = prisms_from_surface_interface->setThickness(add_layer,zero,disp4); CHKERRQ(ierr);
      prisms.merge(add_layer.subset_by_type(MBPRISM));
      old_layer = add_layer.subset_by_type(MBPRISM);
      vector<int> layer_number(add_layer.size(),ll);
      rval = moab.tag_set_data(th_layer_number,add_layer,&layer_number[0]); CHKERRQ_MOAB(rval);
    }
    Range edges;
    rval = m_field.get_moab().get_adjacencies(prisms,1,true,edges,moab::Interface::UNION); CHKERRQ_MOAB(rval);
    Range faces;
    rval = m_field.get_moab().get_adjacencies(prisms,2,true,faces,moab::Interface::UNION); CHKERRQ_MOAB(rval);
    EntityHandle meshset;
    rval = moab.create_meshset(MESHSET_SET,meshset); CHKERRQ_MOAB(rval);

    BitRefLevel bit_level0;
    bit_level0.set(0);
    ierr = prisms_from_surface_interface->seedPrismsEntities(prisms,bit_level0); CHKERRQ(ierr);

    {
      Range tris,prims,edges;
      rval = moab.get_entities_by_type(0,MBPRISM,prisms); CHKERRQ_MOAB(rval);
      rval = moab.get_entities_by_type(0,MBTRI,tris); CHKERRQ_MOAB(rval);
      rval = moab.get_entities_by_type(0,MBEDGE,edges); CHKERRQ_MOAB(rval);
      Range tris_edges;
      rval = moab.get_adjacencies(tris,1,false,tris_edges,moab::Interface::UNION); CHKERRQ_MOAB(rval);

      ierr = m_field.add_field("MESH_NODE_POSITIONS",H1,AINSWORTH_LEGENDRE_BASE,3,MB_TAG_SPARSE,MF_ZERO); CHKERRQ(ierr);
      ierr = m_field.add_ents_to_field_by_type(prisms,MBPRISM,"MESH_NODE_POSITIONS"); CHKERRQ(ierr);
      ierr = m_field.set_field_order(tris_edges,"MESH_NODE_POSITIONS",2); CHKERRQ(ierr);
      ierr = m_field.set_field_order(0,MBVERTEX,"MESH_NODE_POSITIONS",1); CHKERRQ(ierr);

      ierr = m_field.build_fields(); CHKERRQ(ierr);
      {
        Projection10NodeCoordsOnField ent_method(m_field,"MESH_NODE_POSITIONS");
        ierr = m_field.loop_dofs("MESH_NODE_POSITIONS",ent_method); CHKERRQ(ierr);
      }
      {
        // remove ho-nodes
        EntityHandle meshset;
        rval = moab.create_meshset(MESHSET_SET,meshset); CHKERRQ_MOAB(rval);
        rval = moab.add_entities(meshset,tris); CHKERRQ_MOAB(rval);
        rval = moab.add_entities(meshset,prims); CHKERRQ_MOAB(rval);
        rval = moab.add_entities(meshset,edges); CHKERRQ_MOAB(rval);
        rval = moab.convert_entities(meshset,false,false,false,NULL); CHKERRQ_MOAB(rval);
        rval = moab.delete_entities(&meshset,1); CHKERRQ_MOAB(rval);

      }
    }

    // partition mesh
    ierr = m_field.partition_mesh(prisms,3,2,nb_parts); CHKERRQ(ierr);

    // Range all_edges;
    // rval = moab.get_entities_by_type(0,MBEDGE,all_edges); CHKERRQ_MOAB(rval);
    // Range all_faces;
    // rval = moab.get_entities_by_type(0,MBQUAD,all_faces); CHKERRQ_MOAB(rval);
    // all_edges = subtract(all_edges,edges);
    // all_faces = subtract(all_faces,faces);
    // cerr << all_edges << endl;
    // cerr << all_faces << endl;
    // rval = m_field.get_moab().delete_entities(all_edges); CHKERRQ_MOAB(rval);
    // rval = m_field.get_moab().delete_entities(all_faces); CHKERRQ_MOAB(rval);

    // write file
    // Tag th_gid;
    // moab.tag_get_handle(GLOBAL_ID_TAG_NAME,th_gid);
    // moab.tag_delete(th_gid);
    rval = moab.write_file(out_file_name); CHKERRQ_MOAB(rval);


    {
      EntityHandle meshset;
      rval = moab.create_meshset(MESHSET_SET,meshset); CHKERRQ_MOAB(rval);
      rval = moab.add_entities(meshset,prisms); CHKERRQ_MOAB(rval);
      rval = moab.write_file("out.vtk","VTK","",&meshset,1); CHKERRQ_MOAB(rval);

    }


  } catch (MoFEMException const &e) {
    SETERRQ(PETSC_COMM_SELF,e.errorCode,e.errorMessage);
  }

  PetscFinalize();
  return 0;

}
