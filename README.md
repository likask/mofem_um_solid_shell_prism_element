
# Module
This module contains the development of solid shell prism elements that are based on
hierarchical, heterogeneous, and anisotropic shape functions.

The codes in this module are developed based on [MoFEM](http://mofem.eng.gla.ac.uk/mofem/html/) library.

## Reference

```
@misc{kaczmarczyk2020solid,
      title={Solid shell prism elements based on hierarchical, heterogeneous, and anisotropic shape functions}, 
      author={Lukasz Kaczmarczyk and Hoang Nguyen and Zahur Ullah and Mebratu Wakeni and Chris Pearce},
      year={2020},
      eprint={2010.08799},
      archivePrefix={arXiv},
      primaryClass={math.NA}
}
```

# How to install

1. Install developer version of [MoFEM](http://mofem.eng.gla.ac.uk/mofem/html/)
   following instruction [here](http://mofem.eng.gla.ac.uk/mofem/html/installation.html)
2. Add this module of solid shell and run with MoFEM following instruction [here](http://mofem.eng.gla.ac.uk/mofem/html/how_to_add_new_module_and_program.html) 

# Benchmark examples

## Raasch challenge
- Input references

Original paper (Imperial unit): Knight (1997) Raasch Challenge for Shell Elements, AIAA Journal 35
https://arc.aiaa.org/doi/abs/10.2514/2.104

Code_Aster document (SI unit, used in this example): https://www.code-aster.org/V2/doc/v11/en/man_v/v3/v3.03.119.pdf 

- Command line

```
mpirun -np 2 \
mofem_um_solid_shell_prism_element/solid_shell \
-my_file raasch_challenge.cub  \
-ksp_type fgmres \
-pc_type lu \
-pc_factor_mat_solver_package mumps \
-ksp_monitor \
-ksp_atol 1e-12 \
-ksp_rtol 1e-12  \
-my_order 4 \
-my_order_thickness 2 \
-my_order_3d 2 \
-my_shell_thickness 0.0508 \
-my_poisson_ratio 0.35 \
-my_young_modulus 22752510
```

Note: Input mesh is created using [Coreform
Cubit](https://coreform.com/products/coreform-cubit/) and is available in
example folder

- Result

![Raasch Challenge Displacement](example/raasch_challenge/raasch_challenge_displacement.png)

| Mesh     | Order |   |   | DOFs   | Displacement |
|----------|-------|---|---|--------|--------------|
|          | u     | v | w |        |              |
| Coarse   | 1     | 2 | 2 | 4778   | 1.738E-02    |
|          | 2     | 2 | 2 | 5918   | 1.224E-01    |
|          | 3     | 2 | 2 | 7730   | 1.290E-01    |
|          | 4     | 2 | 2 | 10214  | 1.301E-01    |
|          | 5     | 2 | 2 | 13370  | 1.304E-01    |
| Moderate | 1     | 2 | 2 | 25614  | 6.224E-02    |
|          | 2     | 2 | 2 | 32010  | 1.295E-01    |
|          | 3     | 2 | 2 | 42474  | 1.305E-01    |
|          | 4     | 2 | 2 | 57006  | 1.306E-01    |
|          | 5     | 2 | 2 | 75606  | 1.306E-01    |
| Fine     | 1     | 2 | 2 | 81078  | 9.587E-02    |
|          | 2     | 2 | 2 | 101574 | 1.294E-01    |
|          | 3     | 2 | 2 | 135366 | 1.298E-01    |
|          | 4     | 2 | 2 | 182454 | 1.299E-01    |
|          | 5     | 2 | 2 | 242838 | 1.299E-01    |

![Raasch Challenge Convergence](example/raasch_challenge/raasch_challenge_convergence.png)


