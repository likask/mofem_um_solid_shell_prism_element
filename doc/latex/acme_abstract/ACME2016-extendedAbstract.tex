\documentclass[11pt]{ACMEarticle}
\usepackage{amsmath,amssymb,amsthm,mathrsfs,graphicx}
\usepackage{natbib,subfigure}
\usepackage{titlesec}
\usepackage{geometry}
\usepackage{fancyhdr}
\usepackage{newtxtext,newtxmath}

\setlength{\parindent}{0pt}
\setlength{\parskip}{5pt plus 2pt minus 1 pt}

%\topmargin  -25mm
%\evensidemargin 0mm
%\oddsidemargin  0mm
%\textwidth  160mm
%\textheight 267mm
\renewcommand{\baselinestretch}{1.0}
\frenchspacing
\sloppy
\titlespacing{\section}{0pt}{\parskip}{0.01\parskip}
\renewcommand{\headrulewidth}{0pt}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


\begin {document}
\vspace*{-1mm}
\thispagestyle{fancy}
%\fancyhead[R]{{}
\rhead{{\footnotesize
\it Proceedings of the 24$^\mathrm{th}$ UK Conference of the\\Association for Computational Mechanics in Engineering\\
31 March - 01 April 2016, Cardiff University, Cardiff\\} }
\cfoot{}

\pagestyle{empty}

\begin{center}

{\fontsize{14}{20}\bf Prism solid-shell with heterogonous and hierarchical approximation basis
%
%   The title goes here...
%
}\end{center}

%\restoregeometry

\begin{center}
\textbf{*Lukasz Kaczmarczyk$^1$, Zahur Ullah$^1$ and Chris Pearce$^1$}\\
\end{center}

\begin{center}
{\small
$^1$School of Engineering, University of Glasgow\\
}
\end{center}

\begin{center}
*Lukasz.Kaczmarczyk@glasgow.ac.uk\\
\end{center}
%
\begin{center}
\textbf{ABSTRACT}\\[1mm]
\end{center}
%

{\small

A solid-shell element which does not possess rotational degrees of freedom
(DOFs) and which is applicable to thin plate/shell problems is considered. The
element approximation is constructed in prisms, where displacements on the upper
and lower surfaces are approximated in the global coordinate system. In
addition, two other fields are defined in the shell natural (local) coordinate
system that represent the components of the displacement vector in both the
current shell normal direction and the current shell tangent plane. To each
field, an arbitrary order of approximation can be defined, and all fields
reproduce a complete and conforming polynomial approximation basis for the solid
prism element. It is not necessary to augment the formulation with an assumed
natural strain (ANS) field or enhanced assumed strain (EAS) field or to use
reduced integration, making the element ideally suited for geometrically and
physically nonlinear problems.

\textbf{\textit{Key Words:}} {\it solid shell, large deformations, hierarchical approximation}
}
%
\\ \section{Introduction}

In standard thin shell formulations, the approximation through the thickness is
assumed to be linear or higher order, i.e. the normals to the mid-surface in the
initial configuration remain straight but not normal during the deformation. In
the proposed formulation the thickness of the element is not constant and the
normal stress in through-thickness direction is considered. For this reason, the
kinematics of this solid-shell element is richer than the kinematic assumed in
Kirchhoff~-~Love plate theory. In formulating this element, we do not aim to
reproduce classical shell kinematic, thereby avoiding the problem of element
locking. This work builds on a substantial body of published work by a number of
different authors,  most notably \cite{hauptmann1998systematic}. However this
implementation proposes a new formulation exploiting hierarchical, heterogeneous
and anisotropic approximation spaces.

\section{Hierarchical approximation in prism}

In constructing the approximation space in a prism we apply a similar procedure to that shown in
\cite{NME:NME847} for tetrahedra. Nodal basis functions using barycentric
coordinates are given by
\begin{equation}
\phi^v = \lambda_v,\quad
{}_L\phi^v = \phi^v \zeta,\quad
{}_U\phi^v = \phi^v (1-\zeta)
\end{equation}
where right subscript ${}_L\phi^v$ and ${}_U\phi^v$ indicates shape functions on
lower and upper triangles respectively and $\lambda_v$ denotes the barycentric
coordinates. Convective coordinate $\zeta \in [0,1]$ is the coordinate through the prism
thickness. $\zeta = 0$ defines the lower prism triangle and $\zeta=1$ the upper prism triangle.

The edge hierarchical approximation basis is constructed as follows
\begin{equation}
\beta_{0i} = \lambda_0\lambda_i,\quad
\phi^{e_t}_l=\beta_{0i} L_l(\lambda_i-\lambda_j), \quad
{}_L\phi^{e_t}_l=\phi^e_l\zeta,
\quad
{}_U\phi^{e_t}_l=\phi^e_l(1-\zeta),
\end{equation}
where $i$ and $j$ are nodal indices on a triangle. $L_l$ is the
Legendre polynomial of order $l$. If $p$ is the order of the polynomial for the triangle, then
$0 \geq l \geq p-2$ and the number of DOFs on an edge is $p-1$.
The triangle approximation basis is constructed by
\begin{equation}
\beta_{0ij}=\lambda_0\lambda_i\lambda_j,\quad
\phi^t_{l,m}=\beta_{0ij}L_l(\lambda_0-\lambda_i)L_m(\lambda_0-\lambda_j)\zeta, \quad
{}_L\phi^t_{l,m}=\phi^t_{l,m}\zeta,
\quad
{}_U\phi^t_{l,m}=\phi^t_{l,m}(1-\zeta)
\end{equation}
If $p$ is the order of the polynomial on a triangle, then $0 \geq l,m,l+m \geq p-2$ and
number of DOFs on triangle is $(p-1)(p-2)/2$.

The edge through-thickness basis function is given by
\begin{equation}
\beta_{00} = \lambda_0\lambda_0,\quad
\phi^{e_q}_l = \beta_{00} \zeta (1-\zeta) L_l(2\zeta-1)
\end{equation}
where $\lambda_0$ is the barycentric coordinate for the node of the triangle to
which the edge through thickness is adjacent. If $p$ is the order of the polynomial in the prism,
then $0 \geq l \geq p-2$ and the number of DOFs on edge is $p-1$.
The quadrilateral through thickness basis function is
\begin{equation}
\beta_{0i} = \lambda_0\lambda_i,\quad
\phi^q_{l,m} = \beta_{0i} \zeta (1-\zeta) L_l(\lambda_0-\lambda_i)L_m(2\zeta-1)
\end{equation}
where $0$ and $i$ indicate nodes on opposite corner nodes of a
quadrilateral with its own canonical numbering. If $p$ is the order of the polynomial in the
prism, then $0 \geq l,m,l+m \geq p-4$ and the number of DOFs on the
quadrilateral is $(p-3)(p-2)/2$.
The bubble prism basis functions are given by
\begin{equation}
\beta_{0i} = \lambda_0\lambda_i\lambda_j,\quad
\phi^p_{l,m,k} = \beta_{0ij} \zeta(1-\zeta)
L_l(\lambda_0-\lambda_i)L_m(\lambda_0-\lambda_j)L_k(2\zeta-1)
\end{equation}
where $0,i,j$ are indices of nodes on the triangle. If $p$ is polynomials
order in prism, then $0 \geq l,m,k,l+m+k \geq p-5$ and number of DOFs
of the prism is $(P-5)(P-4)(P-3)/6$.

\section{Geometry approximation in reference configuration}

A pragmatic approach is to generate the input mesh surface using a
standard mesh generator. In case of non-planar surfaces, the geometry can be defined
by 6-noded or higher-order triangles. The geometry defined by the surface mesh is then
projected using a hierarchical basis onto the prism.
Thus all approximations for the shell geometry using a hierarchal basis shown below is
directly inferred from the input triangular mesh. The only additional information required is the shell thickness.

The reference position of points on the shell mid-surface, in the global Cartesian coordinate
system, is given on the triangular mesh by
\begin{equation}
^M\mathbf{Z}(\xi,\eta)
=
{}^3\boldsymbol{\phi}^v(\xi,\eta) \underline{\mathbf{Z}}^v+
{}^3\boldsymbol{\phi}^{e_t}(\xi,\eta) \underline{\mathbf{Z}}^{e_t}+
{}^3\boldsymbol{\phi}^{t}(\xi,\eta) \underline{\mathbf{Z}}^{t}=
\sum_{g=v,e_t,t} {}^3\boldsymbol{\phi}^g(\xi,\eta) \underline{\mathbf{Z}}^{g}
\end{equation}
where $\underline{\mathbf{Z}}^{v,e_t,t}$ are DOFs on vertices, edges and
triangles. Left upper-script ${}^3\boldsymbol{\phi}$ indicate that it is matrix of
base approximation functions for vector field, where size of vector is
three. In addition we define on the surface mesh a field of unit length director
vectors
\cite{Sussman20132}, as follows
\begin{equation}
^M\mathbf{V}(\xi,\eta)=
\sum_{g=v,e_t,t} {}^3\boldsymbol{\phi}^g(\xi,\eta) \underline{\mathbf{V}}^{g}
\end{equation}
Finally reference position vector in prism element is given by in global Cartesian coordinate system
\begin{equation}
\mathbf{Z}(\xi,\eta,\zeta) =
{}^M\mathbf{Z}(\xi,\eta)+
a{}^M\mathbf{V}(\xi,\eta)(\zeta-\frac{1}{2})
=
\sum_{s=L,U}\sum_{g=v,e_t,t} {}^3\boldsymbol{\phi}^g_s(\xi,\eta,\zeta) \underline{\mathbf{Z}}^{g}_{s}
\end{equation}
where $a$ is the shell thickness, which may not necessarily be constant.

\section{Curvilinear systems in reference and current configuration}

The vector coefficients in the Cartesian coordinate system are $Z^I$ and $z^i$
for the current and reference configuration, respectively. For simplicity, we use
both coordinate systems with the same base vectors and origin. In addition we
use local curvilinear coordinate base for reference configuration, where vectors
of the base are $\mathbf{E}_\alpha$ and vector of covariant coefficients in that
base are $X^A$. The field of $\mathbf{E}_\alpha$ is consequently approximated
using hierarchical approximation as follows
\begin{equation}
^M\mathbf{E}_{0,1}(\xi,\eta) =
\left\{
\sum_{g=v,e_t,t} {}^3\boldsymbol{\phi}^g(\xi,\eta) \underline{\mathbf{E}}_{0,1}^{g},
\right\}
\mathbf{I}^I
\quad
^M\mathbf{E}_2(\xi,\eta)
=
\left\{
\textrm{Spin}[^M\mathbf{E}_0(\xi,\eta)]^M\mathbf{E}_1(\xi,\eta)
\right\}
\mathbf{I}^I
\end{equation}
where $\textrm{Spin}[\cdot]$ is a spin operator acting as a vector product and
$\underline{\mathbf{E}}_{0,1}^{g}$ are coefficients of the approximation functions.

The local curvilinear coordinate base in the current configuration, convected by the motion of the
shell mid-surface is given by
\begin{equation}
\mathbf{e}_a = e^a_i \mathbf{i}_i = {}^MF^i_I {}^ME^{AI} \delta^a_A \mathbf{i}_i
\end{equation}
where ${}^M\mathbf{F}^i_I(\xi,\eta,\zeta)$ is component of the gradient of deformation,
i.e. for the discretized system, the gradient of deformation is additively decomposed into a
part resulting from through-thickness DOFs and a part resulting from DOFs on the triangles.
The latter part is used to push the base vectors.

\section{Displacements and physical equations}

In the following approach, for convenience, we use the local shell coordinate system to
evaluate the physical equations. The Cartesian global coordinate system is used to
express coefficients of DOFs on entities adjacent to the upper and lower triangles.
For DOFs on entities through the shell thickness, i.e. edges through the thickness,
quads and the prism itself, the current curvilinear (convected) shell coordinate
system is used to express coefficients of the displacement vector.

The position of a material point in the current configuration is given by
\begin{equation}
z^a\mathbf{i}_a = Z^J\mathbf{I}_J + u^J \mathbf{I}_J + v e_2^J \mathbf{I}_J
\end{equation}
where displacements are additively decomposed into a global and through
thickness component. The displacement $\mathbf{u}$ is approximated in the
global coordinate system as
\begin{equation}
u^J(\boldsymbol \xi)
=
\sum_{s=L,U}
\sum_{g=v,e_t,t} {}^3\boldsymbol{\phi}^g_s(\boldsymbol \xi) \underline{\mathbf{u}}^{g,J}_s
= \left[{}^3\boldsymbol{\overline{\phi}}(\boldsymbol \xi)\right] \underline{\mathbf{u}}^J.
\end{equation}
Note that displacement DOFs in vector $\underline{\mathbf{u}}$ are
in global Cartesian coordinate system, thus this solid shell element could be assembled with other
tetrahedral elements without the need for any linking/transfer elements. The displacement
$\mathbf{v}=v e_2^J \mathbf{I}_J$ is approximated in the local conservative system
following global mid-surface deformation. The vector of displacements through the
thickness is given by
\begin{equation}
v(\boldsymbol \xi)
=
\sum_{g=e_q,p}
{}^1\boldsymbol{\phi}^g(\boldsymbol \xi)
\underline{\mathbf{v}}^{g,a}
= \left[{}^1\boldsymbol{\overline{\phi}}(\boldsymbol \xi)\right]\underline{\mathbf{v}}.
\end{equation}
Note that the vector of values of DOFs $\underline{\mathbf{v}}$ is
in the local, curvilinear current (conservative) coordinate system. Those degrees of
freedom are associated with edges, quads and volume of prism
itself, thus are inside volume of shell structure and are not adjacent to any
other DOFs except those of the considered shell.

Consequently the vector of internal forces is
\begin{equation}
\mathbf{f}^\textrm{int}_\textrm{u}(\underline{\mathbf{u}},\underline{\mathbf{v}}) =
\int_\Omega
\left[\nabla_{Z^I}{}^3\boldsymbol{\overline{\phi}} \right]
P^I_i(\underline{\mathbf{u}},\underline{\mathbf{v}})
\textrm{d}\Omega
\quad \textrm{and} \;
\mathbf{f}^\textrm{int}_\textrm{v}(\underline{\mathbf{u}},\underline{\mathbf{v}}) =
\int_\Omega
\left[\nabla_{X^A}{}^1\boldsymbol{\overline{\phi}} \right]
P^A_2(\underline{\mathbf{u}},\underline{\mathbf{v}})
\textrm{d}\Omega,
\end{equation}
where $P^I_i$ and $P^A_a$ are Piola~-~Kirchhoff tensors in the Cartesian and
local convected curvilinear system. Consequently
$\mathbf{f}^\textrm{int}_\textrm{u}$ and $\mathbf{f}^\textrm{int}_\textrm{v}$
are internal force vectors associated with DOFs adjacent to triangles (upper and
lower) in a global cartesian system and DOFs adjacent to edges through the
thickness, quads and prism itself in a local curvilinear coordinate system.

In the formulation presented here, the current curvilinear system follows the
shell deformation given by DOFs on upper and lower triangle. This, in some
sense, leads to a co-rotational like formulation \cite{crisfield1990consistent}.
As result it is noted that the tangent stiffness is non-symmetric. This
observation is consistent with that made by Crisfield
\cite{crisfield1990consistent}, who adapted the co-rotational formulation
whereby the coordinate systems is rotated when shell is deformed. However,
numerical experiments have shown that for the problems addressed here, like for
the co-rotational formulation, the tangent stiffness matrix becomes symmetric as
the iterative procedure reaches equilibrium, thus the matrix can be symmetrized
without deteriorating the rate of convergence.

\section{Example}

In the following examples we present two classical tests of a pinched cylinder.
The reference solutions provided by \cite{hosseini2013isogeometric} are
reproduced. However, exploring the flexibility of the method, increasing the
approximation order in the shell plane or shell thickness, we are able to find a
softer, converged solution, see Figure \ref{fig:1}.

\begin{figure}[h]
  \includegraphics[width=\linewidth]{plots.png}

  \caption{\label{fig:1} Pinched cylinder without diaphragm tension on the left and with
  diaphragm compression on the right. See \cite{hosseini2013isogeometric} for
  more information about geometry, material parameters and reference solution. Analysed
  meshes are available from \cite{MoFEMWebPage}.
  }

\end{figure}

\section{Conclusions}

The solid-shell element presented here has a number of properties. First,
element DOFs do not posses rotations, such that element could be used in
conjunction with classical solid elements without the need for any additional
transfer elements. Second, the approximation basis is hierarchical. Such an
approximation allows for efficient construction of iterative solvers tailored
for hp-adative code. Third, the approximation basis is heterogenous, that is an
arbitrary approximation order can be set independently for each geometrical
entity, i.e. edge, triangle, quad or prism. Fourth, local approximation of
membrane displacements and normal displacements through the thickness are
independent from each other. Finally, the physical equation for 3d solid can be
used in the local shall coordinate system.


\bibliography{references}{}
\bibliographystyle{plain}

\end{document}
