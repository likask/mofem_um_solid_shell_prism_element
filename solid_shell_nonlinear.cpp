/** \file solid_shell.cpp
 * \ingroup solid_shell_prism_element
 * \brief Solid shell prism element example
 *
 */

/* This file is part of MoFEM.
 * MoFEM is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * MoFEM is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with MoFEM. If not, see <http://www.gnu.org/licenses/>. */

#define BOOST_UBLAS_NDEBUG

#include <MoFEM.hpp>
using namespace MoFEM;

#if PETSC_VERSION_LE(3,6,0)
  #error "PETSc version has to be newer or equal to 3.6.0"
#endif


#include <boost/program_options.hpp>
using namespace std;
namespace po = boost::program_options;

#include <boost/numeric/ublas/vector_proxy.hpp>
#include <boost/numeric/ublas/matrix.hpp>
#include <boost/numeric/ublas/matrix_proxy.hpp>
#include <boost/numeric/ublas/vector.hpp>
#include <boost/numeric/ublas/symmetric.hpp>

#include <Projection10NodeCoordsOnField.hpp>
#include <PrismsFromSurfaceInterface.hpp>

#include <MethodForForceScaling.hpp>
#include <TimeForceScale.hpp>
#include <SurfacePressure.hpp>
#include <NodalForce.hpp>
#include <EdgeForce.hpp>
#include <DirichletBC.hpp>

#include <PostProcOnRefMesh.hpp>
#include <PCMGSetUpViaApproxOrders.hpp>

#ifdef WITH_ADOL_C
// #define ADOLC_TAPELESS
// #include <adolc/adtl.h>
#include <adolc/adolc.h>
#endif

#include <ArcLengthTools.hpp>
#include <SolidShellPrismElement.hpp>
using namespace SolidShellModule;
#include <FieldApproximation.hpp>

#include <string>

// #include <petsc/private/tsimpl.h>

#include <boost/program_options.hpp>
using namespace std;
namespace po = boost::program_options;

// typedef PetscErrorCode (*TSAlphaAdaptFunction)(TS,PetscReal,Vec,Vec,PetscReal*,PetscBool*,void*);
//
// typedef struct {
//   Vec       X0,Xa,X1;
//   Vec       V0,Va,V1;
//   Vec       R,E;
//   PetscReal Alpha_m;
//   PetscReal Alpha_f;
//   PetscReal Gamma;
//   PetscReal stage_time;
//   PetscReal shift;
//
//   TSAlphaAdaptFunction adapt;
//   void                 *adaptctx;
//   PetscReal            rtol;
//   PetscReal            atol;
//   PetscReal            rho;
//   PetscReal            scale_min;
//   PetscReal            scale_max;
//   PetscReal            dt_min;
//   PetscReal            dt_max;
// } TS_Alpha;




static char help[] =
"...\n"
"\n";

struct AssembleRhsVectors: public FEMethod {
  ArcLengthCtx *arcPtrRaw;
  AssembleRhsVectors(
    ArcLengthCtx *arc_ptr
  ):
  FEMethod(),
  arcPtrRaw(arc_ptr) {
  }
  PetscErrorCode preProcess()  {
    PetscFunctionBegin;

    switch(ts_ctx) {
      case CTX_TSSETIFUNCTION: {
        ierr = VecZeroEntries(arcPtrRaw->F_lambda); CHKERRQ(ierr);
        ierr = VecGhostUpdateBegin(arcPtrRaw->F_lambda,INSERT_VALUES,SCATTER_FORWARD); CHKERRQ(ierr);
        ierr = VecGhostUpdateEnd(arcPtrRaw->F_lambda,INSERT_VALUES,SCATTER_FORWARD); CHKERRQ(ierr);
      }
      break;
      default:
      SETERRQ(PETSC_COMM_SELF,MOFEM_NOT_IMPLEMENTED,"not implemented");
    }
    PetscFunctionReturn(0);
  }
  PetscErrorCode postProcess()  {
    PetscFunctionBegin;

    switch(ts_ctx) {
      case CTX_TSSETIFUNCTION:
      ierr = VecAssemblyBegin(arcPtrRaw->F_lambda); CHKERRQ(ierr);
      ierr = VecAssemblyEnd(arcPtrRaw->F_lambda); CHKERRQ(ierr);
      ierr = VecGhostUpdateBegin(arcPtrRaw->F_lambda,ADD_VALUES,SCATTER_REVERSE); CHKERRQ(ierr);
      ierr = VecGhostUpdateEnd(arcPtrRaw->F_lambda,ADD_VALUES,SCATTER_REVERSE); CHKERRQ(ierr);
      break;
      default:
      SETERRQ(PETSC_COMM_SELF,MOFEM_NOT_IMPLEMENTED,"not implemented");
    }
    PetscFunctionReturn(0);
  }
};

struct AddLambdaVectorToFinternal: public FEMethod {
  ArcLengthCtx *arcPtrRaw;
  AddLambdaVectorToFinternal(
    ArcLengthCtx *arc_ptr
  ):
  arcPtrRaw(arc_ptr) {
  }
  PetscErrorCode postProcess() {
    PetscFunctionBegin;

    switch(ts_ctx) {
      case CTX_TSSETIFUNCTION: {
        //F_lambda
        ierr = VecDot(arcPtrRaw->F_lambda,arcPtrRaw->F_lambda,&arcPtrRaw->F_lambda2); CHKERRQ(ierr);
        PetscPrintf(PETSC_COMM_WORLD,"\tFlambda2 = %6.4e\n",arcPtrRaw->F_lambda2);
        //add F_lambda
        ierr = VecAXPY(ts_F,arcPtrRaw->getFieldData(),arcPtrRaw->F_lambda); CHKERRQ(ierr);
        PetscPrintf(PETSC_COMM_WORLD,"\tlambda = %6.4e\n",arcPtrRaw->getFieldData());
        double fnorm;
        ierr = VecNorm(ts_F,NORM_2,&fnorm); CHKERRQ(ierr);
        PetscPrintf(PETSC_COMM_WORLD,"\tfnorm = %6.4e\n",fnorm);
      }
      break;
      default:
      SETERRQ(PETSC_COMM_SELF,MOFEM_NOT_IMPLEMENTED,"not implemented state");
    }
    PetscFunctionReturn(0);
  }
};

struct ShellArcLength: public SphericalArcLengthControl {
  Vec bSign;
  ShellArcLength(
    ArcLengthCtx *arc_ptr,Vec b_sign
  ):
  SphericalArcLengthControl(arc_ptr),
  bSign(b_sign) {
  }
  PetscErrorCode preProcess() {
    PetscFunctionBegin;
    PetscFunctionReturn(0);
  }
  PetscErrorCode operator()() {
    PetscFunctionBegin;
    PetscFunctionReturn(0);
  }
  PetscErrorCode postProcess() {
    PetscFunctionBegin;
    switch(ts_ctx) {
      case CTX_TSSETIFUNCTION: {
        ierr = calculateDxAndDlambda(); CHKERRQ(ierr);
        double dt;
        ierr = TSGetTimeStep(ts,&dt); CHKERRQ(ierr);
        arcPtrRaw->res_lambda = calculateLambdaInt() - dt*arcPtrRaw->s;
        if(arcPtrRaw->getPart()==arcPtrRaw->mField.get_comm_rank()) {
          ierr = VecSetValue(
            ts_F,arcPtrRaw->getPetscGlobalDofIdx(),arcPtrRaw->res_lambda,ADD_VALUES
          ); CHKERRQ(ierr);
        }
        PetscPrintf(
          arcPtrRaw->mField.get_comm(),
          "\tres_lambda = %6.4e step_size = %3.2f lambda = %3.2f\n",
          arcPtrRaw->res_lambda,arcPtrRaw->s,arcPtrRaw->getFieldData()
        );
        arcPtrRaw->dIag = arcPtrRaw->beta*arcPtrRaw->dLambda*arcPtrRaw->F_lambda2;
        ierr = VecGhostUpdateBegin(arcPtrRaw->ghostDiag,INSERT_VALUES,SCATTER_FORWARD); CHKERRQ(ierr);
        ierr = VecGhostUpdateEnd(arcPtrRaw->ghostDiag,INSERT_VALUES,SCATTER_FORWARD); CHKERRQ(ierr);
        PetscPrintf(arcPtrRaw->mField.get_comm(),"\tdiag = %6.4e\n",arcPtrRaw->dIag);
      }
      break;
      case CTX_TSSETIJACOBIAN: {
        ierr = calculateDxAndDlambda(); CHKERRQ(ierr);
        ierr = calculateDb(); CHKERRQ(ierr);
        if(arcPtrRaw->getPart()==arcPtrRaw->mField.get_comm_rank()) {
          int idx = arcPtrRaw->getPetscGlobalDofIdx();
          ierr = MatSetValue(
            ts_B,idx,idx,1,ADD_VALUES
          ); CHKERRQ(ierr);
        }
        ierr = MatAssemblyBegin(ts_B,MAT_FLUSH_ASSEMBLY); CHKERRQ(ierr);
        ierr = MatAssemblyEnd(ts_B,MAT_FLUSH_ASSEMBLY); CHKERRQ(ierr);
      }
      break;
      default:
      break;
    }
    PetscFunctionReturn(0);
  }
  double calculateLambdaInt() {
    PetscFunctionBegin;
    return arcPtrRaw->alpha*arcPtrRaw->dx2 + arcPtrRaw->beta*pow(arcPtrRaw->dLambda,2)*arcPtrRaw->F_lambda2;
    PetscFunctionReturn(0);
  }
  PetscErrorCode calculateDb() {
    PetscFunctionBegin;
    ierr = VecCopy(bSign,arcPtrRaw->db); CHKERRQ(ierr);
    ierr = VecScale(arcPtrRaw->db,arcPtrRaw->alpha); CHKERRQ(ierr);
    PetscFunctionReturn(0);
  }
  PetscErrorCode calculateDxAndDlambda() {
    PetscFunctionBegin;
    // SNES snes;
    // ierr = TSGetSNES(ts,&snes); CHKERRQ(ierr);
    // int nfuncs,iter;
    // ierr = SNESGetNumberFunctionEvals(snes,&nfuncs); CHKERRQ(ierr);
    // ierr = SNESGetIterationNumber(snes,&iter); CHKERRQ(ierr);
    // cerr << "   " << nfuncs << " " << iter << endl;
    // dx
    ierr = VecWAXPY(arcPtrRaw->dx,-1,arcPtrRaw->x0,ts_u); CHKERRQ(ierr);
    ierr = VecGhostUpdateBegin(arcPtrRaw->dx,INSERT_VALUES,SCATTER_FORWARD); CHKERRQ(ierr);
    ierr = VecGhostUpdateEnd(arcPtrRaw->dx,INSERT_VALUES,SCATTER_FORWARD); CHKERRQ(ierr);
    // dlambda
    int idx = arcPtrRaw->getPetscLocalDofIdx();
    if(idx!=-1) {
      double *array;
      ierr = VecGetArray(ts_u,&array); CHKERRQ(ierr);
      arcPtrRaw->dLambda = array[idx];
      ierr = VecRestoreArray(ts_u,&array); CHKERRQ(ierr);
      ierr = VecGetArray(arcPtrRaw->x0,&array); CHKERRQ(ierr);
      arcPtrRaw->dLambda -= array[idx];
      ierr = VecRestoreArray(arcPtrRaw->x0,&array); CHKERRQ(ierr);
      ierr = VecGetArray(arcPtrRaw->dx,&array); CHKERRQ(ierr);
      array[idx] = 0;
      ierr = VecRestoreArray(arcPtrRaw->dx,&array); CHKERRQ(ierr);
    }
    ierr = VecGhostUpdateBegin(
      arcPtrRaw->ghosTdLambda,INSERT_VALUES,SCATTER_FORWARD
    ); CHKERRQ(ierr);
    ierr = VecGhostUpdateEnd(
      arcPtrRaw->ghosTdLambda,INSERT_VALUES,SCATTER_FORWARD
    ); CHKERRQ(ierr);
    // dx2
    ierr = VecDot(bSign,arcPtrRaw->dx,&arcPtrRaw->dx2); CHKERRQ(ierr);
    // nrm2_u_t
    double nrm2_u_t;
    ierr = VecNorm(ts_u_t,NORM_2,&nrm2_u_t); CHKERRQ(ierr);
    PetscPrintf(
      arcPtrRaw->mField.get_comm(),
      "\tdlambda = %6.4e dx2 = %6.4e nrm2_u_t = %6.4e\n",
      arcPtrRaw->dLambda,arcPtrRaw->dx2,nrm2_u_t
    );
    PetscFunctionReturn(0);
  }
};

struct AlphaAdaptCtx {
  ArcLengthCtx *arcPtrRaw;
  double *damping_ptr;
  double maxnorm;
};

PetscErrorCode alpha_adapt(
  TS ts,PetscReal t,Vec X,Vec Xdot, PetscReal *nextdt,PetscBool *ok,void *ctx
) {
  PetscFunctionBegin;

  SNES snes;
  SNESConvergedReason snesreason;
  int snesits;
  int its_d = 6;
  double gamma = 0.5;
  double min_dt = 0.01;
  double max_dt = 10.0;
  double atol,rtol,stol;
  int maxit,maxf;
  double reduction;

  ierr = TSGetSNES(ts,&snes); CHKERRQ(ierr);
  ierr = SNESGetTolerances(snes,&atol,&rtol,&stol,&maxit,&maxf); CHKERRQ(ierr);
  AlphaAdaptCtx *alpha_adapt_ctx = (AlphaAdaptCtx*)(ctx);
  ArcLengthCtx *arc_ptr = alpha_adapt_ctx->arcPtrRaw;
  double &maxnorm = alpha_adapt_ctx->maxnorm;

  try {

    po::options_description config_file_options;
    config_file_options.add_options()
    ("its_d",po::value<int>(&its_d)->default_value(its_d));
    config_file_options.add_options()
    ("gamma",po::value<double>(&gamma)->default_value(gamma));
    config_file_options.add_options()
    ("min_dt",po::value<double>(&min_dt)->default_value(min_dt));
    config_file_options.add_options()
    ("max_dt",po::value<double>(&max_dt)->default_value(max_dt));

    config_file_options.add_options()
    ("snes.atol",po::value<double>(&atol)->default_value(atol));
    config_file_options.add_options()
    ("snes.rtol",po::value<double>(&rtol)->default_value(rtol));
    config_file_options.add_options()
    ("snes.stol",po::value<double>(&stol)->default_value(stol));
    config_file_options.add_options()
    ("snes.maxit",po::value<int>(&maxit)->default_value(maxit));
    config_file_options.add_options()
    ("snes.maxnorm",po::value<double>(&maxnorm)->default_value(maxnorm));

    config_file_options.add_options()
    ("arc_length.alpha",po::value<double>(&arc_ptr->alpha)->default_value(arc_ptr->alpha));
    config_file_options.add_options()
    ("arc_length.beta",po::value<double>(&arc_ptr->beta)->default_value(arc_ptr->beta));
    config_file_options.add_options()
    ("arc_length.step_size",po::value<double>(&arc_ptr->s)->default_value(arc_ptr->s));

    config_file_options.add_options()
    (
      "numerical_damping",po::value<double>(alpha_adapt_ctx->damping_ptr)->
      default_value(*alpha_adapt_ctx->damping_ptr)
    );

    ifstream file("snes_config.in");
    po::parsed_options parsed = parse_config_file(file,config_file_options,false);
    po::variables_map vm;
    store(parsed,vm);
    po::notify(vm);


  } catch (exception& ex) {
    SETERRQ(PETSC_COMM_SELF,MOFEM_STD_EXCEPTION_THROW,"error parsing material elastic configuration file");
  }

  ierr = PetscPrintf(PETSC_COMM_WORLD,"### its_d %d\n",its_d); CHKERRQ(ierr);
  ierr = PetscPrintf(PETSC_COMM_WORLD,"### gamma %3.2f\n",gamma); CHKERRQ(ierr);
  ierr = PetscPrintf(PETSC_COMM_WORLD,"### min_dt %3.2f\n",min_dt); CHKERRQ(ierr);
  ierr = PetscPrintf(PETSC_COMM_WORLD,"### max_dt %3.2f\n",max_dt); CHKERRQ(ierr);

  ierr = PetscPrintf(PETSC_COMM_WORLD,"### snes.atol %4.2e\n",atol); CHKERRQ(ierr);
  ierr = PetscPrintf(PETSC_COMM_WORLD,"### snes.rtol %4.2e\n",rtol); CHKERRQ(ierr);
  ierr = PetscPrintf(PETSC_COMM_WORLD,"### snes.stol %4.2e\n",stol); CHKERRQ(ierr);
  ierr = PetscPrintf(PETSC_COMM_WORLD,"### snes.maxit %d\n",maxit); CHKERRQ(ierr);

  ierr = PetscPrintf(PETSC_COMM_WORLD,"### arc_length.alpha %4.2e\n",arc_ptr->alpha); CHKERRQ(ierr);
  ierr = PetscPrintf(PETSC_COMM_WORLD,"### arc_length.beta %4.2e\n",arc_ptr->beta); CHKERRQ(ierr);
  ierr = PetscPrintf(PETSC_COMM_WORLD,"### arc_length.step_size %4.2e\n",arc_ptr->s); CHKERRQ(ierr);

  ierr = PetscPrintf(PETSC_COMM_WORLD,"### numerical_damping %4.2e\n",*alpha_adapt_ctx->damping_ptr); CHKERRQ(ierr);

  ierr = SNESSetTolerances(snes,atol,rtol,stol,maxit,maxf); CHKERRQ(ierr);

  ierr = SNESGetConvergedReason(snes,&snesreason);CHKERRQ(ierr);
  if(snesreason < 0) {
    *ok = PETSC_FALSE;
    *nextdt *= 0.1;
    goto finally;
  }

  ierr = SNESGetIterationNumber(snes,&snesits); CHKERRQ(ierr);
  reduction = pow((double)its_d/((double)(snesits+1)),gamma);

  *ok = PETSC_TRUE;
  *nextdt *= reduction;

  finally:

  *nextdt = PetscMax(*nextdt,min_dt);
  *nextdt = PetscMin(*nextdt,max_dt);

  PetscFunctionReturn(0);
}

struct MonitorCtx {
  DM dm;
  Vec bSign;
  MoFEM::Interface *m_field_ptr;
  ArcLengthCtx *arc_ctx;
  SolidShellPrismElement::PostProcFatPrismOnTriangleOnRefinedMesh *shell_post_proc_ptr;
  PostProcFatPrismOnRefinedMesh *solid_post_proc_ptr;
};

PetscErrorCode monitor(TS ts,PetscInt step,PetscReal crtime,Vec u,void *ctx) {
  PetscFunctionBegin;
  MonitorCtx *monitor_ctx = (MonitorCtx*)(ctx);
  DM dm = monitor_ctx->dm;
  MoFEM::Interface *m_field_ptr = monitor_ctx->m_field_ptr;
  ArcLengthCtx *arc_ctx = monitor_ctx->arc_ctx;
  SolidShellPrismElement::PostProcFatPrismOnTriangleOnRefinedMesh *shell_post_proc_ptr = monitor_ctx->shell_post_proc_ptr;
  PostProcFatPrismOnRefinedMesh *solid_post_proc_ptr = monitor_ctx->solid_post_proc_ptr;


  ierr = DMoFEMMeshToGlobalVector(dm,u,INSERT_VALUES,SCATTER_REVERSE); CHKERRQ(ierr);
  PetscPrintf(PETSC_COMM_WORLD,"Post-processing data\n");
  ierr = DMoFEMLoopFiniteElements(dm,"SHELL",shell_post_proc_ptr); CHKERRQ(ierr);
  ierr = DMoFEMLoopFiniteElements(dm,"SHELL",solid_post_proc_ptr); CHKERRQ(ierr);
  //Save data on mesh
  PetscPrintf(PETSC_COMM_WORLD,"Writing file ...\n");
  {
    ostringstream o1;
    o1 << "shell_" << step << ".h5m";
    rval = shell_post_proc_ptr->postProcMesh.write_file(
      o1.str().c_str(),"MOAB","PARALLEL=WRITE_PART"
    ); CHKERRQ_MOAB(rval);
  }
  {
    ostringstream o1;
    o1 << "solid_" << step << ".h5m";
    rval = solid_post_proc_ptr->postProcMesh.write_file(
      o1.str().c_str(),"MOAB","PARALLEL=WRITE_PART"
    ); CHKERRQ_MOAB(rval);
  }


  // print displacement
  {
    const DofEntity_multiIndex *dofs_ptr;
    ierr = m_field_ptr->get_dofs(&dofs_ptr); CHKERRQ(ierr);
    for(_IT_CUBITMESHSETS_BY_NAME_FOR_LOOP_((*m_field_ptr),"RESULTS",it)) {
      EntityHandle meshset = it->getMeshset();
      Range nodes;
      rval = m_field_ptr->get_moab().get_entities_by_type(meshset,MBVERTEX,nodes,true); CHKERRQ_MOAB(rval);
      Range::iterator nit = nodes.begin();
      for(;nit!=nodes.end();nit++) {
        DofEntityByNameAndEnt::iterator dit,hi_dit;
        dit = dofs_ptr->get<Composite_Name_And_Ent_mi_tag>().lower_bound(boost::make_tuple("DISPLACEMENT",*nit));
        hi_dit = dofs_ptr->get<Composite_Name_And_Ent_mi_tag>().upper_bound(boost::make_tuple("DISPLACEMENT",*nit));
        for(;dit!=hi_dit;dit++) {
          EntityHandle ent = dit->get()->getEnt();
          double coords[3];
          rval = m_field_ptr->get_moab().get_coords(&*nit,1,coords); CHKERRQ_MOAB(rval);
          PetscPrintf(
            PETSC_COMM_WORLD,"STEP %d LAMBDA %6.4e DISPLACEMENT [ %d ] %6.4e coord [ %3.4f %3.4f %3.4f ] EntityHandle %ld\n",
            step,arc_ctx->getFieldData(),dit->get()->getDofCoeffIdx(),dit->get()->getFieldData(),coords[0],coords[1],coords[2],ent
          );
        }
      }
    }
  }

  if(step) {
    ierr = VecWAXPY(arc_ctx->dx,-1,arc_ctx->x0,u); CHKERRQ(ierr);
    double dot0;
    ierr = VecDot(monitor_ctx->bSign,arc_ctx->dx,&dot0); CHKERRQ(ierr);
    double dot1;
    ierr = VecDot(arc_ctx->dx,arc_ctx->dx,&dot1); CHKERRQ(ierr);
    const double eps = 1e-12;
    if(fabs(dot1/dot0) > eps) {
      ierr = VecCopy(arc_ctx->dx,monitor_ctx->bSign); CHKERRQ(ierr);
      {
        const MoFEM::Problem *problem_ptr;
        ierr = DMMoFEMGetProblemPtr(dm,&problem_ptr); CHKERRQ(ierr);
        double *array;
        ierr = VecGetArray(monitor_ctx->bSign,&array); CHKERRQ(ierr);
        const int max_order = 1; // max controlled dof order
        for(_IT_NUMEREDDOF_ROW_FOR_LOOP_(problem_ptr,dof)) {
          int idx = dof->get()->getPetscLocalDofIdx();
          if(idx!=-1 && dof->get()->getDofOrder()>max_order) {
            // zero higher-order dofs
            array[idx] = 0;
          }
        }
        ierr = VecRestoreArray(monitor_ctx->bSign,&array); CHKERRQ(ierr);
      }
      ierr = VecDot(monitor_ctx->bSign,arc_ctx->dx,&dot1); CHKERRQ(ierr);
      double nrm2 = sqrt(fabs(dot1));
      // \alpha_1  = \alpha_0 \| dx \| \frac{b \cdot dx}{dx \cdot dx}
      double alpha = fabs(arc_ctx->alpha*dot0/(dot1/nrm2));
      arc_ctx->setAlphaBeta(alpha,arc_ctx->beta); CHKERRQ(ierr);
      ierr = VecScale(monitor_ctx->bSign,1./nrm2); CHKERRQ(ierr);
    }
    ierr = VecCopy(u,arc_ctx->x0); CHKERRQ(ierr);
  }

  // TS_Alpha *th = (TS_Alpha*)ts->data;
  // ierr = VecView(th->V0,PETSC_VIEWER_STDOUT_WORLD); CHKERRQ(ierr);

  PetscFunctionReturn(0);
}

PetscErrorCode convergence_test(
  SNES snes,PetscInt it,PetscReal xnorm,PetscReal snorm,PetscReal fnorm,SNESConvergedReason *reason,void *ctx
) {


  PetscFunctionBegin;

  if(it == 0) {
     *reason = SNES_CONVERGED_ITERATING;
     PetscFunctionReturn(0);
  }

  AlphaAdaptCtx *alpha_adapt_ctx = (AlphaAdaptCtx*)(ctx);
  double &maxnorm = alpha_adapt_ctx->maxnorm;

  if(fnorm > maxnorm) {
    ierr    = PetscInfo(snes,"Failed to converged,  is dangerously large\n");CHKERRQ(ierr);
    *reason = SNES_DIVERGED_FNORM_NAN;
    PetscFunctionReturn(0);
  }

  // Run default convergence test
  ierr = SNESConvergedDefault(snes,it,xnorm,snorm,fnorm,reason,PETSC_NULL); CHKERRQ(ierr);

  PetscFunctionReturn(0);
}

int main(int argc, char *argv[]) {

  PetscInitialize(&argc,&argv,(char *)0,help);

  try {

    moab::Core mb_instance;
    moab::Interface& moab = mb_instance;
    int rank;
    MPI_Comm_rank(PETSC_COMM_WORLD,&rank);

    //Read parameters from line command
    PetscBool flg_file;
    char mesh_file_name[255];
    PetscInt order = 2,order_thickness = 2,order_3d = 0;
    PetscBool is_partitioned = PETSC_FALSE;
    PetscReal shell_thickness = 0.1;
    double young_modulus = 1;
    double poisson_ratio = 0.25;
    int nb_steps = 1;
    double step_size = 1;
    double arc_beta = 0;
    double damping_alpha0 = 0;
    int nb_sub_steps = 1;

    PetscBool restart = PETSC_FALSE;

    //read options
    {
      ierr = PetscOptionsBegin(
        PETSC_COMM_WORLD,"","Shell prism configure","none"
      ); CHKERRQ(ierr);
      ierr = PetscOptionsString(
        "-my_file",
        "mesh file name","", "mesh.h5m",mesh_file_name, 255, &flg_file
      ); CHKERRQ(ierr);
      ierr = PetscOptionsInt(
        "-my_order",
        "default approximation order",
        "",order,&order,PETSC_NULL
      ); CHKERRQ(ierr);
      ierr = PetscOptionsInt(
        "-my_order_thickness",
        "default approximation order for thickness",
        "",order_thickness,&order_thickness,PETSC_NULL
      ); CHKERRQ(ierr);
      ierr = PetscOptionsInt(
        "-my_order_3d",
        "default approximation order for non planar deformation of element "
        "(polish DEPLANACJA) (3d deformation effect)",
        "",order_3d,&order_3d,PETSC_NULL
      ); CHKERRQ(ierr);

      ierr = PetscOptionsReal(
        "-my_shell_thickness",
        "default shell thickness",
        "",shell_thickness,&shell_thickness,PETSC_NULL
      ); CHKERRQ(ierr);
      ierr = PetscOptionsBool(
        "-my_is_partitioned",
        "set if mesh is partitioned (this result that each process keeps only part of the mesh",
        "",PETSC_FALSE,&is_partitioned,PETSC_NULL
      ); CHKERRQ(ierr);
      // ierr = PetscOptionsString("-my_block_config",
      //   "elastic configure file name","",
      //   "block_conf.in",block_config_file,255,&flg_block_config); CHKERRQ(ierr);
      ierr = PetscOptionsReal(
        "-my_young_modulus","Young modulus","",young_modulus,&young_modulus,0
      ); CHKERRQ(ierr);
      ierr = PetscOptionsReal(
        "-my_poisson_ratio","Poisson ratio","",poisson_ratio,&poisson_ratio,0
      ); CHKERRQ(ierr);

      ierr = PetscPrintf(PETSC_COMM_WORLD,"### order %d\n",order); CHKERRQ(ierr);
      ierr = PetscPrintf(PETSC_COMM_WORLD,"### order thickness %d\n",order_thickness); CHKERRQ(ierr);
      ierr = PetscPrintf(PETSC_COMM_WORLD,"### order 3d %d\n",order_3d); CHKERRQ(ierr);
      ierr = PetscPrintf(PETSC_COMM_WORLD,"### shell thickness %4.3e\n",shell_thickness); CHKERRQ(ierr);
      ierr = PetscPrintf(PETSC_COMM_WORLD,"### shell Young modulus %4.3e\n",young_modulus); CHKERRQ(ierr);
      ierr = PetscPrintf(PETSC_COMM_WORLD,"### shell Poisson ratio %4.3e\n",poisson_ratio); CHKERRQ(ierr);

      // Control solution scheme
      ierr = PetscOptionsInt(
        "-my_nb_steps","number of load steps","",nb_steps,&nb_steps,0
      ); CHKERRQ(ierr);
      ierr = PetscOptionsReal(
        "-my_step_size","load step size","",step_size,&step_size,0
      ); CHKERRQ(ierr);
      ierr = PetscOptionsReal(
        "-my_arc_beta","set beta parameter in spherical arc-length","",arc_beta,&arc_beta,0
      ); CHKERRQ(ierr);
      ierr = PetscOptionsReal(
        "-my_numerical_damping","non-physical damping parameter","",damping_alpha0,&damping_alpha0,0
      ); CHKERRQ(ierr);

      ierr = PetscPrintf(PETSC_COMM_WORLD,"### number of load steps %d\n",nb_steps); CHKERRQ(ierr);
      ierr = PetscPrintf(PETSC_COMM_WORLD,"### step size %f\n",step_size); CHKERRQ(ierr);
      ierr = PetscPrintf(PETSC_COMM_WORLD,"### arc beta %f\n",arc_beta); CHKERRQ(ierr);
      ierr = PetscPrintf(PETSC_COMM_WORLD,"### dumping alpha0 %f\n",damping_alpha0); CHKERRQ(ierr);
      ierr = PetscPrintf(PETSC_COMM_WORLD,"### number of sub-steps %d\n",nb_sub_steps); CHKERRQ(ierr);

      ierr = PetscOptionsEnd(); CHKERRQ(ierr);
    }

    ParallelComm* pcomm = ParallelComm::get_pcomm(&moab,MYPCOMM_INDEX);
    if(pcomm == NULL) pcomm =  new ParallelComm(&moab,PETSC_COMM_WORLD);

    if(is_partitioned == PETSC_TRUE) {
      //Read mesh to MOAB
      const char *option;
      option = "PARALLEL=BCAST_DELETE;"
        "PARALLEL_RESOLVE_SHARED_ENTS;"
        "PARTITION=PARALLEL_PARTITION;";
      rval = moab.load_file(mesh_file_name, 0, option); CHKERRQ_MOAB(rval);
    } else {
      const char *option;
      option = "";
      rval = moab.load_file(mesh_file_name, 0, option); CHKERRQ_MOAB(rval);
    }

    Tag th_layer_number;
    int def_layer_number = 0;
    rval = moab.tag_get_handle(
      "LAYER_NUMBER",1,MB_TYPE_INTEGER,th_layer_number,MB_TAG_CREAT|MB_TAG_SPARSE,&def_layer_number
    ); CHKERRQ_MOAB(rval);

    //Create MoFEM (Joseph) database
    MoFEM::Core core(moab);
    MoFEM::Interface& m_field = core;

    PrismsFromSurfaceInterface *prisms_from_surface_interface;
    ierr = m_field.query_interface(prisms_from_surface_interface); CHKERRQ(ierr);

    map<int,Range> map_shell_trinagles;
    Range tris;
    {
      Tag th_surface_parametrisation;
      double def_val[] = {0,0,0};
      rval = moab.tag_get_handle(
        "SURFACE_PARAMETRISATION",3,MB_TYPE_DOUBLE,th_surface_parametrisation,MB_TAG_CREAT|MB_TAG_SPARSE,def_val
      ); CHKERRQ_MOAB(rval);
      for(_IT_CUBITMESHSETS_BY_SET_TYPE_FOR_LOOP_(m_field,BLOCKSET,it)) {
        const string &name = it->getName();
        if(name.compare(0,5,"SHELL")==0) {
          cerr << name << endl;
          vector<double> attributes;
          it->getAttributes(attributes);
          if(attributes.size()!=3) {
            SETERRQ1(
              PETSC_COMM_SELF,
              MOFEM_DATA_INCONSISTENCY,
              "Shell block should have 3 attributes but has %d",attributes.size()
            );
          }
          Range &it_tris = map_shell_trinagles[it->getMeshsetId()];
          ierr = it->getMeshsetIdEntitiesByType(moab,MBTRI,it_tris,true); CHKERRQ(ierr);
          tris.merge(it_tris);
          for(Range::iterator tit = it_tris.begin();tit!=it_tris.end();tit++) {
            rval = moab.tag_set_data(
              th_surface_parametrisation,&*tit,1,&attributes[0]
            ); CHKERRQ_MOAB(rval);
          }
        }
      }
    }

    Range prisms;
    if(!restart) {
      ierr = prisms_from_surface_interface->createPrisms(tris,prisms); CHKERRQ(ierr);
    } else {
      rval = m_field.get_moab().get_entities_by_type(0,MBPRISM,prisms,false); CHKERRQ_MOAB(rval);
    }
    EntityHandle meshset;
    rval = moab.create_meshset(MESHSET_SET,meshset); CHKERRQ_MOAB(rval);
    rval = moab.add_entities(meshset,prisms); CHKERRQ_MOAB(rval);
    BitRefLevel bit_level0;
    bit_level0.set(0);
    ierr = m_field.seed_ref_level_3D(meshset,bit_level0); CHKERRQ(ierr);
    ierr = prisms_from_surface_interface->seedPrismsEntities(prisms,bit_level0); CHKERRQ(ierr);

    Range quads_bc;
    for(_IT_CUBITMESHSETS_BY_BCDATA_TYPE_FOR_LOOP_(m_field,NODESET|DISPLACEMENTSET,it)) {
      DisplacementCubitBcData mydata;
      ierr = it->getBcDataStructure(mydata); CHKERRQ(ierr);
      Range ents;
      ierr = it->getMeshsetIdEntitiesByDimension(m_field.get_moab(),1,ents,true); CHKERRQ(ierr);
      if(mydata.data.flag4||mydata.data.flag5||mydata.data.flag6) {
        ierr = m_field.update_meshset_by_entities_children(
          it->getMeshset(),bit_level0,it->getMeshset(),MBVERTEX,true
        ); CHKERRQ(ierr);
        ierr = m_field.update_meshset_by_entities_children(
          it->getMeshset(),bit_level0,it->getMeshset(),MBEDGE,true
        ); CHKERRQ(ierr);
        ierr = m_field.update_meshset_by_entities_children(
          it->getMeshset(),bit_level0,it->getMeshset(),MBTRI,true
        ); CHKERRQ(ierr);
        Range edges;
        rval = m_field.get_moab().get_adjacencies(ents,1,false,edges,moab::Interface::UNION); CHKERRQ_MOAB(rval);
        Range faces;
        rval = m_field.get_moab().get_adjacencies(edges,2,false,faces,moab::Interface::UNION); CHKERRQ_MOAB(rval);
        Range quads;
        quads = faces.subset_by_type(MBQUAD);
        rval  = m_field.get_moab().add_entities(it->getMeshset(),quads);
        quads_bc.merge(quads);
        Range edges_quads_bc;
        rval = m_field.get_moab().get_adjacencies(
          quads,2,false,edges_quads_bc,moab::Interface::UNION
        ); CHKERRQ_MOAB(rval);
        quads_bc.merge(edges_quads_bc);
      }
      if((!mydata.data.flag1)&&(!mydata.data.flag2)&&(!mydata.data.flag3)) {
        rval = m_field.get_moab().remove_entities(it->getMeshset(),ents); CHKERRQ_MOAB(rval);
      }
    }

    Range quads,all_tris_edges,quads_edges,all_tris;
    {
      rval = moab.get_entities_by_type(0,MBTRI,all_tris,false); CHKERRQ_MOAB(rval);
      rval = moab.get_entities_by_type(0,MBQUAD,quads,false); CHKERRQ_MOAB(rval);
      rval = moab.get_adjacencies(all_tris,1,false,all_tris_edges,moab::Interface::UNION); CHKERRQ_MOAB(rval);
      rval = moab.get_adjacencies(quads,1,false,quads_edges,moab::Interface::UNION); CHKERRQ_MOAB(rval);
    }

    // Fields
    EntityHandle no_field_vertex;
    if(!restart) {

      //Fields
      ierr = m_field.add_field("ZETA",H1,AINSWORTH_LEGENDRE_BASE,1,MB_TAG_SPARSE,MF_ZERO); CHKERRQ(ierr);
      ierr = m_field.add_field("KSIETA",H1,AINSWORTH_LEGENDRE_BASE,2,MB_TAG_SPARSE,MF_ZERO); CHKERRQ(ierr);
      ierr = m_field.add_field("DISPLACEMENT",H1,AINSWORTH_LEGENDRE_BASE,3,MB_TAG_SPARSE,MF_ZERO); CHKERRQ(ierr);
      ierr = m_field.add_field("MESH_NODE_POSITIONS",H1,AINSWORTH_LEGENDRE_BASE,3,MB_TAG_SPARSE,MF_ZERO); CHKERRQ(ierr);
      ierr = m_field.add_field("DIRECTOR",H1,AINSWORTH_LEGENDRE_BASE,3,MB_TAG_SPARSE,MF_ZERO); CHKERRQ(ierr);
      ierr = m_field.add_field("COVECTORS",H1,AINSWORTH_LEGENDRE_BASE,3,MB_TAG_SPARSE,MF_ZERO); CHKERRQ(ierr);
      ierr = m_field.add_field("SCALAR",H1,AINSWORTH_LEGENDRE_BASE,1,MB_TAG_SPARSE,MF_ZERO); CHKERRQ(ierr);

      ierr = m_field.add_field("LAMBDA",NOFIELD,NOBASE,1); CHKERRQ(ierr);

      // Setting up LAMBDA field and ARC_LENGTH interface
      //Add dummy no-field vertex
      {
        const double coords[] = {0,0,0};
        rval = m_field.get_moab().create_vertex(coords,no_field_vertex); CHKERRQ_MOAB(rval);
        Range range_no_field_vertex;
        range_no_field_vertex.insert(no_field_vertex);
        ierr = m_field.seed_ref_level(range_no_field_vertex,BitRefLevel().set()); CHKERRQ(ierr);
        EntityHandle lambda_meshset = m_field.get_field_meshset("LAMBDA");
        rval = m_field.get_moab().add_entities(lambda_meshset,range_no_field_vertex); CHKERRQ_MOAB(rval);
      }

      //add entities (by tets) to the field
      ierr = m_field.add_ents_to_field_by_type(meshset,MBPRISM,"ZETA"); CHKERRQ(ierr);
      ierr = m_field.add_ents_to_field_by_type(meshset,MBPRISM,"KSIETA"); CHKERRQ(ierr);
      ierr = m_field.add_ents_to_field_by_type(meshset,MBPRISM,"DISPLACEMENT"); CHKERRQ(ierr);
      ierr = m_field.add_ents_to_field_by_type(meshset,MBPRISM,"MESH_NODE_POSITIONS"); CHKERRQ(ierr);

      for(
        map<int,Range>::iterator mit = map_shell_trinagles.begin();
        mit!=map_shell_trinagles.end();
        mit++
      ) {
        ierr = m_field.add_ents_to_field_by_type(mit->second,MBTRI,"DIRECTOR"); CHKERRQ(ierr);
        ierr = m_field.add_ents_to_field_by_type(mit->second,MBTRI,"COVECTORS"); CHKERRQ(ierr);
        ierr = m_field.add_ents_to_field_by_type(mit->second,MBTRI,"SCALAR"); CHKERRQ(ierr);
      }

      // ierr = m_field.set_field_order(prisms,"DISPLACEMENT",order_thickness); CHKERRQ(ierr);
      // ierr = m_field.set_field_order(0,MBQUAD,"DISPLACEMENT",order_thickness); CHKERRQ(ierr);
      // ierr = m_field.set_field_order(quads_edges,"DISPLACEMENT",order_thickness); CHKERRQ(ierr);
      ierr = m_field.set_field_order(0,MBTRI,"DISPLACEMENT",order); CHKERRQ(ierr);
      ierr = m_field.set_field_order(all_tris_edges,"DISPLACEMENT",order); CHKERRQ(ierr);
      ierr = m_field.set_field_order(0,MBVERTEX,"DISPLACEMENT",1); CHKERRQ(ierr);

      if(order_thickness) {
        ierr = m_field.set_field_order(prisms,"ZETA",order_thickness); CHKERRQ(ierr);
        ierr = m_field.set_field_order(0,MBQUAD,"ZETA",order_thickness); CHKERRQ(ierr);
        ierr = m_field.set_field_order(
          subtract(quads_edges,all_tris_edges),"ZETA",order_thickness
        ); CHKERRQ(ierr);
      }

      if(order_3d) {
        ierr = m_field.set_field_order(prisms,"KSIETA",order_3d); CHKERRQ(ierr);
        ierr = m_field.set_field_order(0,MBQUAD,"KSIETA",order_3d); CHKERRQ(ierr);
        ierr = m_field.set_field_order(
          subtract(quads_edges,all_tris_edges),"KSIETA",order_3d
        ); CHKERRQ(ierr);
      }

      // ierr = m_field.set_field_order(quads_edges,"MESH_NODE_POSITIONS",2); CHKERRQ(ierr);
      ierr = m_field.set_field_order(all_tris_edges,"MESH_NODE_POSITIONS",2); CHKERRQ(ierr);
      ierr = m_field.set_field_order(0,MBVERTEX,"MESH_NODE_POSITIONS",1); CHKERRQ(ierr);

      ierr = m_field.set_field_order(all_tris_edges,"DIRECTOR",2); CHKERRQ(ierr);
      ierr = m_field.set_field_order(0,MBVERTEX,"DIRECTOR",1); CHKERRQ(ierr);
      ierr = m_field.set_field_order(all_tris_edges,"COVECTORS",2); CHKERRQ(ierr);
      ierr = m_field.set_field_order(0,MBVERTEX,"COVECTORS",1); CHKERRQ(ierr);
      ierr = m_field.set_field_order(all_tris_edges,"SCALAR",2); CHKERRQ(ierr);
      ierr = m_field.set_field_order(0,MBVERTEX,"SCALAR",1); CHKERRQ(ierr);
    }
    //build field
    ierr = m_field.build_fields(); CHKERRQ(ierr);
    //get HO geometry for 10 node tets
    if(!restart) {
      Projection10NodeCoordsOnField ent_method(m_field,"MESH_NODE_POSITIONS");
      ierr = m_field.loop_dofs("MESH_NODE_POSITIONS",ent_method); CHKERRQ(ierr);
    }

    //Elements
    if(!restart) {
      ierr = m_field.add_finite_element("ARC_LENGTH"); CHKERRQ(ierr);
      ierr = m_field.modify_finite_element_add_field_row("ARC_LENGTH","LAMBDA"); CHKERRQ(ierr);
      ierr = m_field.modify_finite_element_add_field_col("ARC_LENGTH","LAMBDA"); CHKERRQ(ierr);
      //elem data
      ierr = m_field.modify_finite_element_add_field_data("ARC_LENGTH","LAMBDA"); CHKERRQ(ierr);

      //this entity will carray data for this finite element
      EntityHandle meshset_fe_arc_length;
      {
        rval = moab.create_meshset(MESHSET_SET,meshset_fe_arc_length); CHKERRQ_MOAB(rval);
        rval = moab.add_entities(meshset_fe_arc_length,&no_field_vertex,1); CHKERRQ_MOAB(rval);
        ierr = m_field.seed_ref_level_MESHSET(meshset_fe_arc_length,BitRefLevel().set()); CHKERRQ(ierr);
      }
      //finally add created meshset to the ARC_LENGTH finite element
      ierr = m_field.add_ents_to_finite_element_by_MESHSET(meshset_fe_arc_length,"ARC_LENGTH",false); CHKERRQ(ierr);

      ierr = m_field.add_finite_element("SHELL"); CHKERRQ(ierr);
      ierr = m_field.modify_finite_element_add_field_row("SHELL","DISPLACEMENT"); CHKERRQ(ierr);
      ierr = m_field.modify_finite_element_add_field_col("SHELL","DISPLACEMENT"); CHKERRQ(ierr);
      ierr = m_field.modify_finite_element_add_field_data("SHELL","DISPLACEMENT"); CHKERRQ(ierr);
      ierr = m_field.modify_finite_element_add_field_row("SHELL","ZETA"); CHKERRQ(ierr);
      ierr = m_field.modify_finite_element_add_field_col("SHELL","ZETA"); CHKERRQ(ierr);
      ierr = m_field.modify_finite_element_add_field_data("SHELL","ZETA"); CHKERRQ(ierr);
      ierr = m_field.modify_finite_element_add_field_row("SHELL","KSIETA"); CHKERRQ(ierr);
      ierr = m_field.modify_finite_element_add_field_col("SHELL","KSIETA"); CHKERRQ(ierr);
      ierr = m_field.modify_finite_element_add_field_data("SHELL","KSIETA"); CHKERRQ(ierr);
      ierr = m_field.modify_finite_element_add_field_data("SHELL","DIRECTOR"); CHKERRQ(ierr);
      ierr = m_field.modify_finite_element_add_field_data("SHELL","COVECTORS"); CHKERRQ(ierr);
      ierr = m_field.modify_finite_element_add_field_data("SHELL","MESH_NODE_POSITIONS"); CHKERRQ(ierr);
      ierr = m_field.modify_finite_element_add_field_row("SHELL","LAMBDA"); CHKERRQ(ierr);
      ierr = m_field.modify_finite_element_add_field_col("SHELL","LAMBDA"); CHKERRQ(ierr); //this is for parmetis
      ierr = m_field.modify_finite_element_add_field_data("SHELL","LAMBDA"); CHKERRQ(ierr);

      ierr = m_field.add_ents_to_finite_element_by_type(meshset,MBPRISM,"SHELL"); CHKERRQ(ierr);

      for(
        map<int,Range>::iterator mit = map_shell_trinagles.begin();
        mit!=map_shell_trinagles.end();
        mit++
      ) {
        int id = mit->first;
        string fe_name = "ELEMENT_DIRECTOR_"+SSTR(id);
        ierr = m_field.add_finite_element(fe_name); CHKERRQ(ierr);
        ierr = m_field.modify_finite_element_add_field_row(fe_name,"SCALAR"); CHKERRQ(ierr);
        ierr = m_field.modify_finite_element_add_field_col(fe_name,"SCALAR"); CHKERRQ(ierr);
        ierr = m_field.modify_finite_element_add_field_data(fe_name,"SCALAR"); CHKERRQ(ierr);
        ierr = m_field.modify_finite_element_add_field_data(fe_name,"MESH_NODE_POSITIONS"); CHKERRQ(ierr);
        ierr = m_field.add_ents_to_finite_element_by_type(mit->second,MBTRI,fe_name); CHKERRQ(ierr);
      }

      // Add Neumann forces
      ierr = MetaNeummanForces::addNeumannBCElements(m_field,"DISPLACEMENT"); CHKERRQ(ierr);
      ierr = MetaNodalForces::addElement(m_field,"DISPLACEMENT"); CHKERRQ(ierr);
      ierr = MetaEdgeForces::addElement(m_field,"DISPLACEMENT"); CHKERRQ(ierr);
    }

    //build finite elements
    ierr = m_field.build_finite_elements(); CHKERRQ(ierr);
    //build adjacencies
    ierr = m_field.build_adjacencies(bit_level0); CHKERRQ(ierr);

    SolidShellPrismElement::CommonData shell_common_data;

    // Calculate normal and co-vectors
    {
      string name = "DIRECTOR_PROBLEM";
      ierr = m_field.add_problem(name); CHKERRQ(ierr);
      ierr = m_field.modify_problem_ref_level_add_bit(name,bit_level0); CHKERRQ(ierr);
      FieldApproximationH1 normal_approx(m_field);
      SolidShellPrismElement::NormalApprox normal_eval(moab);
      vector<Vec> f(9);
      for(
        map<int,Range>::iterator mit = map_shell_trinagles.begin();
        mit!=map_shell_trinagles.end();
        mit++
      ) {
        int id = mit->first;
        DMType dm_name = name.c_str();
        ierr = DMRegister_MoFEM(dm_name); CHKERRQ(ierr);
        DM dm;
        ierr = DMCreate(PETSC_COMM_WORLD,&dm);CHKERRQ(ierr);
        ierr = DMSetType(dm,dm_name);CHKERRQ(ierr);
        ierr = DMMoFEMCreateMoFEM(dm,&m_field,dm_name,bit_level0); CHKERRQ(ierr);
        ierr = DMSetFromOptions(dm); CHKERRQ(ierr);
        ierr = DMMoFEMSetIsPartitioned(dm,is_partitioned); CHKERRQ(ierr);
        //add elements to dm
        ierr = DMMoFEMAddElement(dm,("ELEMENT_DIRECTOR_"+SSTR(id)).c_str()); CHKERRQ(ierr);
        {
          EntityHandle meshset = m_field.get_finite_element_meshset(
            ("ELEMENT_DIRECTOR_"+SSTR(id)).c_str()
          );
          Range tris;
          rval = moab.get_entities_by_type(meshset,MBTRI,tris,true); CHKERRQ_MOAB(rval);
          for(Range::iterator tit = tris.begin();tit!=tris.end();tit++) {
            shell_common_data.shellElementsMap[*tit] = id;
          }
        }
        {
          //ierr = DMSetUp(dm); CHKERRQ(ierr);
          // Do serial matrix
          const MoFEM::Problem *problem_ptr;
          ierr = DMMoFEMGetProblemPtr(dm,&problem_ptr); CHKERRQ(ierr);
          ierr = m_field.build_problem(problem_ptr->getName(),true); CHKERRQ(ierr);
          ierr = m_field.partition_simple_problem(problem_ptr->getName()); CHKERRQ(ierr);
          ierr = m_field.partition_finite_elements(problem_ptr->getName());
          ierr = m_field.partition_ghost_dofs(problem_ptr->getName()); CHKERRQ(ierr);
        }
        Vec D;
        Mat Aij;
        {
          ierr = DMCreateGlobalVector_MoFEM(dm,&D); CHKERRQ(ierr);
          for(int vv = 0;vv<9;vv++) {
            ierr = VecDuplicate(D,&f[vv]); CHKERRQ(ierr);
          }
          ierr = DMCreateMatrix_MoFEM(dm,&Aij); CHKERRQ(ierr);
        }
        {
          normal_approx.feFace.getOpPtrVector().clear();
          ierr = normal_approx.setOperatorsFace("SCALAR",Aij,f,normal_eval); CHKERRQ(ierr);
          ierr = DMoFEMLoopFiniteElements(
            dm,("ELEMENT_DIRECTOR_"+SSTR(id)).c_str(),&normal_approx.feFace
          ); CHKERRQ(ierr);
          for(int vv = 0;vv<9;vv++) {
            ierr = VecAssemblyBegin(f[vv]); CHKERRQ(ierr);
            ierr = VecAssemblyEnd(f[vv]); CHKERRQ(ierr);
            ierr = VecGhostUpdateBegin(f[vv],ADD_VALUES,SCATTER_REVERSE); CHKERRQ(ierr);
            ierr = VecGhostUpdateEnd(f[vv],ADD_VALUES,SCATTER_REVERSE); CHKERRQ(ierr);
          }
          ierr = MatAssemblyBegin(Aij,MAT_FINAL_ASSEMBLY); CHKERRQ(ierr);
          ierr = MatAssemblyEnd(Aij,MAT_FINAL_ASSEMBLY); CHKERRQ(ierr);
          // ierr = MatView(Aij,PETSC_VIEWER_DRAW_SELF); CHKERRQ(ierr);
          // std::string wait;
          // std::cin >> wait;
        }
        {
          KSP solver;
          ierr = KSPCreate(PETSC_COMM_WORLD,&solver); CHKERRQ(ierr);
          ierr = KSPSetOperators(solver,Aij,Aij); CHKERRQ(ierr);
          ierr = KSPSetFromOptions(solver); CHKERRQ(ierr);
          ierr = KSPSetUp(solver); CHKERRQ(ierr);

          const Problem *problem_ptr;
          ierr = m_field.get_problem("DIRECTOR_PROBLEM",&problem_ptr); CHKERRQ(ierr);
          const DofEntity_multiIndex *dofs_ptr;
          ierr = m_field.get_dofs(&dofs_ptr); CHKERRQ(ierr);
          for(int vv = 0;vv<9;vv++) {
            ierr = KSPSolve(solver,f[vv],D); CHKERRQ(ierr);
            ierr = VecGhostUpdateBegin(D,INSERT_VALUES,SCATTER_FORWARD); CHKERRQ(ierr);
            ierr = VecGhostUpdateEnd(D,INSERT_VALUES,SCATTER_FORWARD); CHKERRQ(ierr);
            ierr = DMoFEMMeshToGlobalVector(dm,D,INSERT_VALUES,SCATTER_REVERSE); CHKERRQ(ierr);
            if(vv < 3) {
              for(
                _IT_NUMEREDDOF_ROW_FOR_LOOP_(problem_ptr,dof)
              ) {
                EntityHandle ent = dof->get()->getEnt();
                int idx = dof->get()->getEntDofIdx();
                double val = dof->get()->getFieldData();
                dofs_ptr->get<Composite_Name_And_Ent_And_EntDofIdx_mi_tag>().find(
                  boost::make_tuple("DIRECTOR",ent,3*idx+vv-0)
                )->get()->getFieldData() += val;
              }
            } else {
              for(
                _IT_NUMEREDDOF_ROW_FOR_LOOP_(problem_ptr,dof)
              ) {
                EntityHandle ent = dof->get()->getEnt();
                int idx = dof->get()->getEntDofIdx();
                double val = dof->get()->getFieldData();
                DofEntity_multiIndex::index<Composite_Name_And_Ent_And_EntDofIdx_mi_tag>::type::iterator cit;
                int m = (vv < 6) ? 3 : 6;
                cit = dofs_ptr->get<Composite_Name_And_Ent_And_EntDofIdx_mi_tag>().find(
                  boost::make_tuple("COVECTORS",ent,3*idx+vv-m)
                );
                if(cit == dofs_ptr->get<Composite_Name_And_Ent_And_EntDofIdx_mi_tag>().end()) {
                  SETERRQ1(PETSC_COMM_SELF,MOFEM_DATA_INCONSISTENCY,"data inconsistency %d",vv);
                }
                cit->get()->getFieldData() += val;
                UId uid = cit->get()->getGlobalUniqueId();
                if(vv < 6) {
                  shell_common_data.coVector0DofsMap[id][uid] = val;
                } else {
                  shell_common_data.coVector1DofsMap[id][uid] = val;
                }
              }
            }
          }
          ierr = KSPDestroy(&solver); CHKERRQ(ierr);
        }
        ierr = MatDestroy(&Aij); CHKERRQ(ierr);
        for(int vv = 0;vv<6;vv++) {
          ierr = VecDestroy(&f[vv]); CHKERRQ(ierr);
        }
        ierr = VecDestroy(&D); CHKERRQ(ierr);
        ierr = DMMoFEMUnSetElement(dm,("ELEMENT_DIRECTOR_"+SSTR(id)).c_str()); CHKERRQ(ierr);
        ierr = DMDestroy(&dm); CHKERRQ(ierr);
        ierr = m_field.clear_problem(name); CHKERRQ(ierr);
      }
      ierr = m_field.delete_problem(name); CHKERRQ(ierr);
    }

    //define problems
    ierr = m_field.add_problem("SHELL_PROBLEM",MF_ZERO); CHKERRQ(ierr);
    //set refinement level for problem
    ierr = m_field.modify_problem_ref_level_add_bit("SHELL_PROBLEM",bit_level0); CHKERRQ(ierr);
    DMType dm_name = "SHELL_PROBLEM";
    ierr = DMRegister_MoFEM(dm_name); CHKERRQ(ierr);
    //craete dm instance
    DM dm;
    {
      ierr = DMCreate(PETSC_COMM_WORLD,&dm);CHKERRQ(ierr);
      ierr = DMSetType(dm,dm_name);CHKERRQ(ierr);
      //set dm data structure which created mofem data structures
      ierr = DMMoFEMCreateMoFEM(dm,&m_field,dm_name,bit_level0); CHKERRQ(ierr);
      ierr = DMSetFromOptions(dm); CHKERRQ(ierr);
      ierr = DMMoFEMSetIsPartitioned(dm,is_partitioned); CHKERRQ(ierr);
      //add elements to dm
      ierr = DMMoFEMAddElement(dm,"SHELL"); CHKERRQ(ierr);
      ierr = DMMoFEMAddElement(dm,"FORCE_FE"); CHKERRQ(ierr);
      ierr = DMMoFEMAddElement(dm,"PRESSURE_FE"); CHKERRQ(ierr);
      ierr = DMMoFEMAddElement(dm,"ARC_LENGTH"); CHKERRQ(ierr);
      ierr = DMSetUp(dm); CHKERRQ(ierr);
    }

    //create matrices
    Vec F,D;
    Mat Aij;
    {
      ierr = DMCreateGlobalVector_MoFEM(dm,&F); CHKERRQ(ierr);
      ierr = VecDuplicate(F,&D); CHKERRQ(ierr);
      ierr = DMCreateMatrix_MoFEM(dm,&Aij); CHKERRQ(ierr);
    }

    SolidShellPrismElement::SolidShell elastic_shell_rhs(m_field,shell_common_data);
    SolidShellPrismElement::SolidShell elastic_shell_lhs(m_field,shell_common_data);
    DirichletDisplacementBc dirichlet_bc(m_field,"DISPLACEMENT",Aij,D,F);
    dirichlet_bc.snes_ctx = FEMethod::CTX_SNESNONE;
    dirichlet_bc.ts_ctx = FEMethod::CTX_TSNONE;
    DirichletFixFieldAtEntitiesBc fix_ents(m_field,"KSIETA",Aij,D,F,quads_bc);
    fix_ents.snes_ctx = FEMethod::CTX_SNESNONE;
    fix_ents.ts_ctx = FEMethod::CTX_TSNONE;

    if(1) {

      ierr = VecZeroEntries(F); CHKERRQ(ierr);
      ierr = VecGhostUpdateBegin(F,INSERT_VALUES,SCATTER_FORWARD); CHKERRQ(ierr);
      ierr = VecGhostUpdateEnd(F,INSERT_VALUES,SCATTER_FORWARD); CHKERRQ(ierr);
      if(restart) {
        ierr = DMoFEMMeshToLocalVector(dm,D,INSERT_VALUES,SCATTER_FORWARD); CHKERRQ(ierr);
      } else {
        ierr = VecZeroEntries(D); CHKERRQ(ierr);
        ierr = VecGhostUpdateBegin(D,INSERT_VALUES,SCATTER_FORWARD); CHKERRQ(ierr);
        ierr = VecGhostUpdateEnd(D,INSERT_VALUES,SCATTER_FORWARD); CHKERRQ(ierr);
        ierr = DMoFEMMeshToLocalVector(dm,D,INSERT_VALUES,SCATTER_REVERSE); CHKERRQ(ierr);
      }
      ierr = MatZeroEntries(Aij); CHKERRQ(ierr);

      //assemble Aij and F
      {
        shell_common_data.meshNodalPosition = "MESH_NODE_POSITIONS";
        shell_common_data.displacementFieldName = "DISPLACEMENT";
        shell_common_data.zetaFieldName = "ZETA";
        shell_common_data.ksiEtaFieldName = "KSIETA";
        shell_common_data.directorFieldName = "DIRECTOR";
        shell_common_data.coVectorFieldName = "COVECTORS";
        shell_common_data.tHickness = shell_thickness;
        shell_common_data.sHift = 0;
      }

      // Assemble vector
      elastic_shell_rhs.getOpPtrVector().push_back(
        new SolidShellPrismElement::OpGetDirectorVector(shell_common_data)
      );
      elastic_shell_rhs.getOpPtrVector().push_back(
        new SolidShellPrismElement::OpGetCoVectors(shell_common_data)
      );
      elastic_shell_rhs.getOpPtrVector().push_back(
        new SolidShellPrismElement::OpGetReferenceBase(shell_common_data)
      );
      elastic_shell_rhs.getOpPtrVector().push_back(
        new SolidShellPrismElement::OpJacobioan(shell_common_data)
      );
      elastic_shell_rhs.getOpPtrVector().push_back(
        new SolidShellPrismElement::OpInvJacobian(shell_common_data)
      );
      {
        elastic_shell_rhs.getOpPtrVector().push_back(
          new SolidShellPrismElement::OpGetFCartesian(shell_common_data)
        );
        elastic_shell_rhs.getOpPtrVector().push_back(
          new SolidShellPrismElement::OpGetFLocal(shell_common_data,"ZETA",true)
        );
        elastic_shell_rhs.getOpPtrVector().push_back(
          new SolidShellPrismElement::OpGetFLocal(shell_common_data,"KSIETA",false)
        );
        double mu = MU(young_modulus,poisson_ratio);
        double lambda = LAMBDA(young_modulus,poisson_ratio);
        elastic_shell_rhs.getOpPtrVector().push_back(
          new SolidShellPrismElement::OpEvaluatePiolaStress(
            shell_common_data,lambda,mu,10,
            order_thickness>order_3d ? order_thickness : order_3d,false
          )
        );
        elastic_shell_rhs.getOpPtrVector().push_back(
          new SolidShellPrismElement::OpAssembleRhsPiolaStress(shell_common_data,"DISPLACEMENT")
        );
        elastic_shell_rhs.getOpPtrVector().push_back(
          new SolidShellPrismElement::OpAssembleRhsPiolaStress(shell_common_data,"ZETA")
        );
        elastic_shell_rhs.getOpPtrVector().push_back(
          new SolidShellPrismElement::OpAssembleRhsPiolaStress(shell_common_data,"KSIETA")
        );
      }

      // Assemble matrix
      elastic_shell_lhs.getOpPtrVector().push_back(
        new SolidShellPrismElement::OpGetDirectorVector(shell_common_data)
      );
      elastic_shell_lhs.getOpPtrVector().push_back(
        new SolidShellPrismElement::OpGetCoVectors(shell_common_data)
      );
      elastic_shell_lhs.getOpPtrVector().push_back(
        new SolidShellPrismElement::OpGetReferenceBase(shell_common_data)
      );
      elastic_shell_lhs.getOpPtrVector().push_back(
        new SolidShellPrismElement::OpJacobioan(shell_common_data)
      );
      elastic_shell_lhs.getOpPtrVector().push_back(
        new SolidShellPrismElement::OpInvJacobian(shell_common_data)
      );
      {
        elastic_shell_lhs.getOpPtrVector().push_back(
          new SolidShellPrismElement::OpGetFCartesian(shell_common_data)
        );
        elastic_shell_lhs.getOpPtrVector().push_back(
          new SolidShellPrismElement::OpGetFLocal(shell_common_data,"ZETA",true)
        );
        elastic_shell_lhs.getOpPtrVector().push_back(
          new SolidShellPrismElement::OpGetFLocal(shell_common_data,"KSIETA",false)
        );
        double mu = MU(young_modulus,poisson_ratio);
        double lambda = LAMBDA(young_modulus,poisson_ratio);
        elastic_shell_lhs.getOpPtrVector().push_back(
          new SolidShellPrismElement::OpEvaluatePiolaStress(
            shell_common_data,lambda,mu,10,
            order_thickness>order_3d ? order_thickness : order_3d,true
          )
        );
        bool symm = false;
        elastic_shell_lhs.getOpPtrVector().push_back(
          new SolidShellPrismElement::OpAssembleLhsPiolaStress(
            shell_common_data,"DISPLACEMENT","DISPLACEMENT",symm,symm
          )
        );
        elastic_shell_lhs.getOpPtrVector().push_back(
          new SolidShellPrismElement::OpAssembleLhsPiolaStress(
            shell_common_data,"ZETA","ZETA",symm,symm
          )
        );
        elastic_shell_lhs.getOpPtrVector().push_back(
          new SolidShellPrismElement::OpAssembleLhsPiolaStress(
            shell_common_data,"KSIETA","KSIETA",symm,symm
          )
        );
        bool assemble_transpose = false;
        elastic_shell_lhs.getOpPtrVector().push_back(
          new SolidShellPrismElement::OpAssembleLhsPiolaStress(
            shell_common_data,"DISPLACEMENT","ZETA",false,assemble_transpose
          )
        );
        elastic_shell_lhs.getOpPtrVector().push_back(
          new SolidShellPrismElement::OpAssembleLhsPiolaStress(
            shell_common_data,"DISPLACEMENT","KSIETA",false,assemble_transpose
          )
        );
        elastic_shell_lhs.getOpPtrVector().push_back(
          new SolidShellPrismElement::OpAssembleLhsPiolaStress(
            shell_common_data,"ZETA","KSIETA",false,assemble_transpose
          )
        );
        if(!assemble_transpose) {
          elastic_shell_lhs.getOpPtrVector().push_back(
            new SolidShellPrismElement::OpAssembleLhsPiolaStress(
              shell_common_data,"ZETA","DISPLACEMENT",false,assemble_transpose
            )
          );
          elastic_shell_lhs.getOpPtrVector().push_back(
            new SolidShellPrismElement::OpAssembleLhsPiolaStress(
              shell_common_data,"KSIETA","DISPLACEMENT",false,assemble_transpose
            )
          );
          elastic_shell_lhs.getOpPtrVector().push_back(
            new SolidShellPrismElement::OpAssembleLhsPiolaStress(
              shell_common_data,"KSIETA","ZETA",false,assemble_transpose
            )
          );
        }
      }
    }

    ArcLengthCtx arc_ctx(m_field,"SHELL_PROBLEM");
    ArcLengthMatShell mat_ctx(Aij,&arc_ctx,"SHELL_PROBLEM");

    Vec b_sign;
    ierr = VecDuplicate(D,&b_sign); CHKERRQ(ierr);
    {
      vector<int> controled_points;
      const MoFEM::Problem *problem_ptr;
      ierr = DMMoFEMGetProblemPtr(dm,&problem_ptr); CHKERRQ(ierr);

      for(_IT_CUBITMESHSETS_BY_NAME_FOR_LOOP_(m_field,"ARC_LENGTH_PLUS",it)) {
        EntityHandle meshset = it->getMeshset();
        Range nodes;
        rval = moab.get_entities_by_type(meshset,MBVERTEX,nodes,true); CHKERRQ_MOAB(rval);
        Range::iterator nit = nodes.begin();
        for(;nit!=nodes.end();nit++) {
          for(
            _IT_NUMEREDDOF_ROW_BY_NAME_ENT_PART_FOR_LOOP_(
              problem_ptr,"DISPLACEMENT",*nit,m_field.get_comm_rank(),dit
            )
          ) {
            int idx = dit->get()->getPetscGlobalDofIdx();
            controled_points.push_back(idx);
            ierr = VecSetValue(b_sign,idx,1,ADD_VALUES); CHKERRQ(ierr);
          }
        }
      }
      for(_IT_CUBITMESHSETS_BY_NAME_FOR_LOOP_(m_field,"ARC_LENGTH_MINUS",it)) {
        EntityHandle meshset = it->getMeshset();
        Range nodes;
        rval = moab.get_entities_by_type(meshset,MBVERTEX,nodes,true); CHKERRQ_MOAB(rval);
        Range::iterator nit = nodes.begin();
        for(;nit!=nodes.end();nit++) {
          for(
            _IT_NUMEREDDOF_ROW_BY_NAME_ENT_PART_FOR_LOOP_(
              problem_ptr,"DISPLACEMENT",*nit,m_field.get_comm_rank(),dit
            )
          ) {
            int idx = dit->get()->getPetscGlobalDofIdx();
            controled_points.push_back(idx);
            ierr = VecSetValue(b_sign,idx,-1,ADD_VALUES); CHKERRQ(ierr);
          }
        }
      }
      ierr = VecAssemblyBegin(b_sign); CHKERRQ(ierr);
      ierr = VecAssemblyEnd(b_sign); CHKERRQ(ierr);
    }

    ShellArcLength arc_method(&arc_ctx,b_sign);

    // Create shell matrix
    Mat ShellAij;
    {
      PetscInt M,N,m,n;
      ierr = MatGetSize(Aij,&M,&N); CHKERRQ(ierr);
      ierr = MatGetLocalSize(Aij,&m,&n); CHKERRQ(ierr);
      ierr = MatCreateShell(PETSC_COMM_WORLD,m,n,M,N,(void*)&mat_ctx,&ShellAij); CHKERRQ(ierr);
      ierr = MatShellSetOperation(ShellAij,MATOP_MULT,(void(*)(void))ArcLengthMatMultShellOp); CHKERRQ(ierr);
    }

    // Note: Non conservative forces
    // Setting finite element method for applying tractions
    boost::ptr_map<string,NeummanForcesSurface> neumann_forces;
    boost::ptr_map<string,NodalForce> nodal_forces;
    boost::ptr_map<string,EdgeForce> edge_forces;
    {
      //forces and pressures on surface
      ierr = MetaNeummanForces::setMomentumFluxOperators(
        m_field,neumann_forces,arc_ctx.F_lambda,"DISPLACEMENT"
      ); CHKERRQ(ierr);
      //noadl forces
      ierr = MetaNodalForces::setOperators(
        m_field,nodal_forces,arc_ctx.F_lambda,"DISPLACEMENT"
      ); CHKERRQ(ierr);
      //edge forces
      ierr = MetaEdgeForces::setOperators(
        m_field,edge_forces,arc_ctx.F_lambda,"DISPLACEMENT"
      ); CHKERRQ(ierr);
    }

    AssembleRhsVectors assemble_rhs_vectors(&arc_ctx);
    AddLambdaVectorToFinternal add_lambda_vector(&arc_ctx);

    // setting ts_ctx with additional structure for arc-length control
    boost::shared_ptr<MoFEM::TsCtx> ts_ctx;
    {
      ts_ctx = boost::shared_ptr<MoFEM::TsCtx>(new ArcLengthTsCtx(m_field,"SHELL_PROBLEM",&arc_ctx));
      ierr = DMMoFEMSetTsCtx(dm,ts_ctx); CHKERRQ(ierr);
    }

    //Adding elements to DMTs
    {
      //Rhs
      ierr = DMMoFEMTSSetIFunction(dm,DM_NO_ELEMENT,NULL,&assemble_rhs_vectors,NULL); CHKERRQ(ierr);
      ierr = DMMoFEMTSSetIFunction(dm,DM_NO_ELEMENT,NULL,&dirichlet_bc,NULL); CHKERRQ(ierr);
      ierr = DMMoFEMTSSetIFunction(dm,DM_NO_ELEMENT,NULL,&fix_ents,NULL); CHKERRQ(ierr);
      {
        boost::ptr_map<string,NeummanForcesSurface>::iterator fit;
        fit = neumann_forces.begin();
        for(;fit!=neumann_forces.end();fit++) {
          ierr = DMMoFEMTSSetIFunction(dm,fit->first.c_str(),&fit->second->getLoopFe(),NULL,NULL); CHKERRQ(ierr);
        }
        fit = nodal_forces.begin();
        for(;fit!=nodal_forces.end();fit++) {
          ierr = DMMoFEMTSSetIFunction(dm,fit->first.c_str(),&fit->second->getLoopFe(),NULL,NULL); CHKERRQ(ierr);
        }
        fit = edge_forces.begin();
        for(;fit!=edge_forces.end();fit++) {
          ierr = DMMoFEMTSSetIFunction(dm,fit->first.c_str(),&fit->second->getLoopFe(),NULL,NULL); CHKERRQ(ierr);
        }
      }

      ierr = DMMoFEMTSSetIFunction(dm,"SHELL",&elastic_shell_rhs,PETSC_NULL,PETSC_NULL); CHKERRQ(ierr);
      ierr = DMMoFEMTSSetIFunction(dm,DM_NO_ELEMENT,NULL,NULL,&assemble_rhs_vectors); CHKERRQ(ierr);
      ierr = DMMoFEMTSSetIFunction(dm,DM_NO_ELEMENT,NULL,NULL,&add_lambda_vector); CHKERRQ(ierr);
      ierr = DMMoFEMTSSetIFunction(dm,DM_NO_ELEMENT,NULL,NULL,&arc_method); CHKERRQ(ierr);
      ierr = DMMoFEMTSSetIFunction(dm,DM_NO_ELEMENT,NULL,NULL,&fix_ents); CHKERRQ(ierr);
      ierr = DMMoFEMTSSetIFunction(dm,DM_NO_ELEMENT,NULL,NULL,&dirichlet_bc); CHKERRQ(ierr);
      //Lhs
      ierr = DMMoFEMTSSetIJacobian(dm,DM_NO_ELEMENT,NULL,&dirichlet_bc,NULL); CHKERRQ(ierr);
      ierr = DMMoFEMTSSetIJacobian(dm,DM_NO_ELEMENT,NULL,&fix_ents,NULL); CHKERRQ(ierr);
      ierr = DMMoFEMTSSetIJacobian(dm,"SHELL",&elastic_shell_lhs,NULL,NULL); CHKERRQ(ierr);
      ierr = DMMoFEMTSSetIJacobian(dm,DM_NO_ELEMENT,NULL,NULL,&arc_method); CHKERRQ(ierr);
      ierr = DMMoFEMTSSetIJacobian(dm,DM_NO_ELEMENT,NULL,NULL,&fix_ents); CHKERRQ(ierr);
      ierr = DMMoFEMTSSetIJacobian(dm,DM_NO_ELEMENT,NULL,NULL,&dirichlet_bc); CHKERRQ(ierr);
    }

    TS ts;
    ierr = TSCreate(PETSC_COMM_WORLD,&ts); CHKERRQ(ierr);
    SNES snes;
    KSP ksp;
    PC pc;
    // TSAdapt adapt;

    PCArcLengthCtx pc_ctx(ShellAij,Aij,&arc_ctx);
    {
      ierr = TSSetIFunction(ts,F,f_TSSetIFunction,ts_ctx.get()); CHKERRQ(ierr);
      ierr = TSSetIJacobian(ts,ShellAij,Aij,f_TSSetIJacobian,ts_ctx.get()); CHKERRQ(ierr);
      // double ftime = 1;
      // ierr = TSSetDuration(ts,PETSC_DEFAULT,ftime); CHKERRQ(ierr);
      ierr = TSSetFromOptions(ts); CHKERRQ(ierr);
      ierr = TSSetType(ts,TSALPHA); CHKERRQ(ierr);
      ierr = TSGetSNES(ts,&snes); CHKERRQ(ierr);
      // ierr = SNESSetFromOptions(snes); CHKERRQ(ierr);
      ierr = SNESGetKSP(snes,&ksp); CHKERRQ(ierr);
      ierr = KSPGetPC(ksp,&pc); CHKERRQ(ierr);
      ierr = PCSetType(pc,PCSHELL); CHKERRQ(ierr);
      ierr = PCShellSetContext(pc,&pc_ctx); CHKERRQ(ierr);
      ierr = PCShellSetApply(pc,PCApplyArcLength); CHKERRQ(ierr);
      ierr = PCShellSetSetUp(pc,PCSetupArcLength); CHKERRQ(ierr);
      // ierr = TSGetAdapt(ts,&adapt); CHKERRQ(ierr);
      // ierr = TSAdaptSetMonitor(adapt,PETSC_TRUE); CHKERRQ(ierr);
      ierr = TSAlphaSetParams(ts,1,1,1); CHKERRQ(ierr);
    }

    // Post proc shell
    SolidShellPrismElement::PostProcFatPrismOnTriangleOnRefinedMesh shell_post_proc(m_field);
    {
      ierr = shell_post_proc.generateReferenceElementMesh(); CHKERRQ(ierr);
      shell_post_proc.getOpPtrVector().push_back(
        new SolidShellPrismElement::OpGetDirectorVector(shell_common_data)
      );
      shell_post_proc.getOpPtrVector().push_back(
        new SolidShellPrismElement::OpGetCoVectors(shell_common_data)
      );
      shell_post_proc.getOpPtrVector().push_back(
        new SolidShellPrismElement::OpGetReferenceBase(shell_common_data)
      );
      shell_post_proc.getOpPtrVector().push_back(
        new SolidShellPrismElement::OpJacobioan(shell_common_data)
      );
      shell_post_proc.getOpPtrVector().push_back(
        new SolidShellPrismElement::OpInvJacobian(shell_common_data)
      );
      shell_post_proc.getOpPtrVector().push_back(
        new SolidShellPrismElement::OpGetDisplacements(shell_common_data)
      );
      shell_post_proc.getOpPtrVector().push_back(
        new SolidShellPrismElement::OpPostProcMeshNodeDispalcements(
          shell_post_proc.postProcMesh,shell_post_proc.mapGaussPts,shell_common_data
        )
      );
      shell_post_proc.getOpPtrVector().push_back(
        new SolidShellPrismElement::OpPostProcMeshNodePositions(
          shell_post_proc.postProcMesh,shell_post_proc.mapGaussPts,shell_common_data
        )
      );
    }

    //Post proc solid
    PostProcFatPrismOnRefinedMesh solid_post_proc(m_field);
    {
      ierr = solid_post_proc.generateReferenceElementMesh(); CHKERRQ(ierr);
      solid_post_proc.getOpPtrVector().push_back(
        new SolidShellPrismElement::OpGetDirectorVector(shell_common_data)
      );
      solid_post_proc.getOpPtrVector().push_back(
        new SolidShellPrismElement::OpGetCoVectors(shell_common_data)
      );
      solid_post_proc.getOpPtrVector().push_back(
        new SolidShellPrismElement::OpGetReferenceBase(shell_common_data)
      );
      solid_post_proc.getOpPtrVector().push_back(
        new SolidShellPrismElement::OpJacobioan(shell_common_data)
      );
      solid_post_proc.getOpPtrVector().push_back(
        new SolidShellPrismElement::OpInvJacobian(shell_common_data)
      );
      solid_post_proc.getOpPtrVector().push_back(
        new SolidShellPrismElement::OpGetDisplacements(shell_common_data)
      );
      solid_post_proc.getOpPtrVector().push_back(
        new SolidShellPrismElement::OpPostProcMeshNodeDispalcements(
          solid_post_proc.postProcMesh,solid_post_proc.mapGaussPts,shell_common_data
        )
      );
      solid_post_proc.getOpPtrVector().push_back(
        new SolidShellPrismElement::OpPostProcMeshNodePositions(
          solid_post_proc.postProcMesh,solid_post_proc.mapGaussPts,shell_common_data
        )
      );
      ierr = solid_post_proc.addFieldValuesGradientPostProc("DISPLACEMENT"); CHKERRQ(ierr);
    }

    // Add dumping to the system
    double damping_alpha = damping_alpha0;
    {
      // Add non-physical numerical dumping
      elastic_shell_rhs.getOpPtrVector().push_back(
        new SolidShellPrismElement::OpGetIncrementOfDisplacements(shell_common_data)
      );
      elastic_shell_rhs.getOpPtrVector().push_back(
        new SolidShellPrismElement::OpNumericalViscousDampingRhs(
          shell_common_data,damping_alpha
        )
      );
      // elastic_shell_lhs.getOpPtrVector().push_back(
      //   new SolidShellPrismElement::OpGetIncrementOfDisplacements(shell_common_data)
      // );
      elastic_shell_lhs.getOpPtrVector().push_back(
        new SolidShellPrismElement::OpNumericalViscousDampingLhs(
          shell_common_data,damping_alpha
        )
      );
    }

    {

      ierr = arc_ctx.setAlphaBeta(1,arc_beta); CHKERRQ(ierr);
      ierr = arc_ctx.setS(step_size); CHKERRQ(ierr);

      #if PETSC_VERSION_GE(3,7,0)
      #warning "Step adaptation not working from petsc version 3.7.0 and beyond"
      #else
      AlphaAdaptCtx alpha_adapt_ctx;
      {
        alpha_adapt_ctx.arcPtrRaw = &arc_ctx;
        alpha_adapt_ctx.damping_ptr = &damping_alpha;
        alpha_adapt_ctx.maxnorm = 1e3;
      }
      ierr = TSAlphaSetAdapt(ts,alpha_adapt,&alpha_adapt_ctx); CHKERRQ(ierr);
      ierr = SNESSetConvergenceTest(snes,convergence_test,(void*)&alpha_adapt_ctx,PETSC_NULL); CHKERRQ(ierr);
      #endif

      MonitorCtx monitor_ctx;
      {
        monitor_ctx.dm = dm;
        monitor_ctx.bSign = b_sign;
        monitor_ctx.m_field_ptr = &m_field;
        monitor_ctx.arc_ctx = &arc_ctx;
        monitor_ctx.shell_post_proc_ptr = &shell_post_proc;
        monitor_ctx.solid_post_proc_ptr = &solid_post_proc;
      }
      ierr = TSMonitorSet(ts,monitor,&monitor_ctx,NULL);
      // ierr = PetscInfoAllow(PETSC_TRUE,NULL); CHKERRQ(ierr);
      // ierr = PetscInfoDeactivateClass(0); CHKERRQ(ierr);
      // ierr = PetscInfoActivateClass(TS_CLASSID); CHKERRQ(ierr);
      #if PETSC_VERSION_GE(3,7,0)
      ierr = TSSetExactFinalTime(ts,TS_EXACTFINALTIME_STEPOVER); CHKERRQ(ierr);
      #endif
      ierr = TSSolve(ts,D); CHKERRQ(ierr);

    }


    {
      ierr = TSDestroy(&ts); CHKERRQ(ierr);
      ierr = MatDestroy(&ShellAij); CHKERRQ(ierr);
      ierr = VecDestroy(&F); CHKERRQ(ierr);
      ierr = VecDestroy(&D); CHKERRQ(ierr);
      ierr = MatDestroy(&Aij); CHKERRQ(ierr);
      ierr = DMDestroy(&dm); CHKERRQ(ierr);
    }

  } catch (MoFEMException const &e) {
    SETERRQ(PETSC_COMM_SELF,e.errorCode,e.errorMessage);
  }

  PetscFinalize();
  return 0;

}
