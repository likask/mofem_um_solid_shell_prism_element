#!/bin/sh

NB_PROC=1

BIN_PATH="@CMAKE_CURRENT_BINARY_DIR@"
SRC_PATH="@CMAKE_CURRENT_SOURCE_DIR@"

MPIRUN="@MoFEM_MPI_RUN@ -np $NB_PROC"

set -x
cd $BIN_PATH

# Elastic analysis
# for ODISP in `seq 2 2`;
# do
#   FILE=~/Desktop/FE2_solid_shell/more_example/cantilever_shear.cub
#   ./solid_shell_partition_mesh -my_file $FILE -my_out_file tmp.h5m -my_nb_parts $NB_PROC
#   $MPIRUN $BIN_PATH/solid_shell \
#   -my_file tmp.h5m -my_is_partitioned 1 \
#   -ksp_type fgmres -pc_type lu -pc_factor_mat_solver_package mumps -ksp_monitor -ksp_atol 1e-12 -ksp_rtol 1e-12  \
#   -my_order $ODISP -my_order_thickness 1 -my_order_3d 1 \
#   -my_shell_thickness 0.1 -my_poisson_ratio 0.0 -my_young_modulus 1.2e6 \
#   2>&1 | tee tmp_log
#   echo LOG: `grep 'Problem SHELL_PROBLEM Nb' tmp_log | awk '{print $5}' | uniq` $ODISP 0 0 `grep "DISPLACEMENT \[ 1 \]" tmp_log | awk '{print $5}'`
# done

# nonlinear analysis
$MPIRUN $BIN_PATH/solid_shell_nonlinear \
-my_file $SRC_PATH/example/cantilever_shear.cub  \
-snes_monitor -snes_max_it 24 -snes_atol 1e-6 -snes_rtol 1e-5	-snes_stol 0 \
-snes_linesearch_type l2 -snes_linesearch_monitor -snes_linesearch_minlambda 0.01 \
-snes_converged_reason \
-ksp_type fgmres -pc_type lu -pc_factor_mat_solver_package mumps -ksp_monitor -ksp_atol 1e-12 -ksp_rtol 1e-12  \
-ksp_converged_reason \
-my_step_size 1 -my_arc_beta 0 \
-my_numerical_damping 0 \
-my_order 2 -my_order_thickness 1 -my_order_3d 0 \
-my_shell_thickness 0.1 -my_poisson_ratio 0.0 -my_young_modulus 1.2e6 \
-ts_monitor -ts_dt 0.05 -ts_final_time 1 \
-ts_max_snes_failures -1 \
2>&1 | tee tmp_log

# MESH 9

# echo LOG: Mesh 9 - Convergence in plane - locking
#
# for ODISP in `seq 1 8`;
# do
#   $MPIRUN $BIN_PATH/solid_shell \
#   -my_file $SRC_PATH/meshes/pinched_cylinder_9.h5m  \
#   -ksp_type fgmres -pc_type lu -pc_factor_mat_solver_package mumps -ksp_monitor -ksp_atol 1e-12 -ksp_rtol 1e-12  \
#   -my_order $ODISP -my_order_thickness 0 -my_order_3d 0 \
#   -my_shell_thickness 3 -my_poisson_ratio 0.3 -my_young_modulus 3 \
#   2>&1 | tee tmp_log
#   echo LOG: `grep 'Problem SHELL_PROBLEM Nb' tmp_log | awk '{print $5}' | uniq` $ODISP 0 0 `grep "DISPLACEMENT \[ 1 \]" tmp_log | awk '{print $5}'`
# done
#
# echo LOG: Mesh 9 - Convergence in plane - thinckes 2 - plane locking
#
# for ODISP in `seq 1 8`;
# do
#   $MPIRUN $BIN_PATH/solid_shell \
#   -my_file $SRC_PATH/meshes/pinched_cylinder_9.h5m  \
#   -ksp_type fgmres -pc_type lu -pc_factor_mat_solver_package mumps -ksp_monitor -ksp_atol 1e-12 -ksp_rtol 1e-12  \
#   -my_order $ODISP -my_order_thickness 2 -my_order_3d 0 \
#   -my_shell_thickness 3 -my_poisson_ratio 0.3 -my_young_modulus 3 \
#   2>&1 | tee tmp_log
#   echo LOG: `grep 'Problem SHELL_PROBLEM Nb' tmp_log | awk '{print $5}' | uniq` $ODISP 2 0 `grep "DISPLACEMENT \[ 1 \]" tmp_log | awk '{print $5}'`
# done
#
# echo LOG: Mesh 9 - Convergence in plane - thinckes 2 - plane 3
#
# for ODISP in `seq 1 8`;
# do
#   $MPIRUN $BIN_PATH/solid_shell \
#   -my_file $SRC_PATH/meshes/pinched_cylinder_9.h5m  \
#   -ksp_type fgmres -pc_type lu -pc_factor_mat_solver_package mumps -ksp_monitor -ksp_atol 1e-12 -ksp_rtol 1e-12  \
#   -my_order $ODISP -my_order_thickness 3 -my_order_3d 2 \
#   -my_shell_thickness 3 -my_poisson_ratio 0.3 -my_young_modulus 3 \
#   2>&1 | tee tmp_log
#   echo LOG: `grep 'Problem SHELL_PROBLEM Nb' tmp_log | awk '{print $5}' | uniq` $ODISP 2 2 `grep "DISPLACEMENT \[ 1 \]" tmp_log | awk '{print $5}'`
# done
#
# echo LOG: Mesh 6 - Convergence in plane - locking
#
#
# echo LOG: Mesh 6 - Convergence in plane - thinckes 2 - plane locking
#
# for ODISP in `seq 1 8`;
# do
#   $MPIRUN $BIN_PATH/solid_shell \
#   -my_file $SRC_PATH/meshes/pinched_cylinder_6.h5m  \
#   -ksp_type fgmres -pc_type lu -pc_factor_mat_solver_package mumps -ksp_monitor -ksp_atol 1e-12 -ksp_rtol 1e-12  \
#   -my_order $ODISP -my_order_thickness 2 -my_order_3d 0 \
#   -my_shell_thickness 3 -my_poisson_ratio 0.3 -my_young_modulus 3 \
#   2>&1 | tee tmp_log
#   echo LOG: `grep 'Problem SHELL_PROBLEM Nb' tmp_log | awk '{print $5}' | uniq` $ODISP 2 0 `grep "DISPLACEMENT \[ 1 \]" tmp_log | awk '{print $5}'`
# done
#
# echo LOG: Mesh 6 - Convergence in plane - thinckes 3 - plane 2
#
# for ODISP in `seq 1 8`;
# do
#   $MPIRUN $BIN_PATH/solid_shell \
#   -my_file $SRC_PATH/meshes/pinched_cylinder_6.h5m  \
#   -ksp_type fgmres -pc_type lu -pc_factor_mat_solver_package mumps -ksp_monitor -ksp_atol 1e-12 -ksp_rtol 1e-12  \
#   -my_order $ODISP -my_order_thickness 2 -my_order_3d 3 \
#   -my_shell_thickness 3 -my_poisson_ratio 0.3 -my_young_modulus 3 \
#   2>&1 | tee tmp_log
#   echo LOG: `grep 'Problem SHELL_PROBLEM Nb' tmp_log | awk '{print $5}' | uniq` $ODISP 3 2 `grep "DISPLACEMENT \[ 1 \]" tmp_log | awk '{print $5}'`
# done

# Mesh 4

# echo LOG: Mesh 4 - Convergence in plane - locking
#
# for ODISP in `seq 1 8`;
# do
#   $MPIRUN $BIN_PATH/solid_shell \
#   -my_file $SRC_PATH/meshes/pinched_cylinder_4.h5m  \
#   -ksp_type fgmres -pc_type lu -pc_factor_mat_solver_package mumps -ksp_monitor -ksp_atol 1e-12 -ksp_rtol 1e-12  \
#   -my_order $ODISP -my_order_thickness 0 -my_order_3d 0 \
#   -my_shell_thickness 3 -my_poisson_ratio 0.3 -my_young_modulus 3 \
#   2>&1 | tee tmp_log
#   echo LOG: `grep 'Problem SHELL_PROBLEM Nb' tmp_log | awk '{print $5}' | uniq` $ODISP 0 0 `grep "DISPLACEMENT \[ 1 \]" tmp_log | awk '{print $5}'`
# done
#
# echo LOG: Mesh 4 - Convergence in plane - thinckes 2 - plane locking
#
# for ODISP in `seq 1 8`;
# do
#   $MPIRUN $BIN_PATH/solid_shell \
#   -my_file $SRC_PATH/meshes/pinched_cylinder_4.h5m  \
#   -ksp_type fgmres -pc_type lu -pc_factor_mat_solver_package mumps -ksp_monitor -ksp_atol 1e-12 -ksp_rtol 1e-12  \
#   -my_order $ODISP -my_order_thickness 2 -my_order_3d 0 \
#   -my_shell_thickness 3 -my_poisson_ratio 0.3 -my_young_modulus 3 \
#   2>&1 | tee tmp_log
#   echo LOG: `grep 'Problem SHELL_PROBLEM Nb' tmp_log | awk '{print $5}' | uniq` $ODISP 2 0 `grep "DISPLACEMENT \[ 1 \]" tmp_log | awk '{print $5}'`
# done
#
# echo LOG: Mesh 4 - Convergence in plane - thinckes 3 - plane 2
#
# for ODISP in `seq 1 8`;
# do
#   $MPIRUN $BIN_PATH/solid_shell \
#   -my_file $SRC_PATH/meshes/pinched_cylinder_4.h5m  \
#   -ksp_type fgmres -pc_type lu -pc_factor_mat_solver_package mumps -ksp_monitor -ksp_atol 1e-12 -ksp_rtol 1e-12  \
#   -my_order $ODISP -my_order_thickness 3 -my_order_3d 2 \
#   -my_shell_thickness 3 -my_poisson_ratio 0.3 -my_young_modulus 3 \
#   2>&1 | tee tmp_log
#   echo LOG: `grep 'Problem SHELL_PROBLEM Nb' tmp_log | awk '{print $5}' | uniq` $ODISP 3 2 `grep "DISPLACEMENT \[ 1 \]" tmp_log | awk '{print $5}'`
# done
#
# # Mesh 2
#
# echo LOG: Mesh 2 - Convergence in plane - locking
#
# for ODISP in `seq 1 6`;
# do
#   $MPIRUN $BIN_PATH/solid_shell \
#   -my_file $SRC_PATH/meshes/pinched_cylinder_2.h5m  \
#   -ksp_type fgmres -pc_type lu -pc_factor_mat_solver_package mumps -ksp_monitor -ksp_atol 1e-12 -ksp_rtol 1e-12  \
#   -my_order $ODISP -my_order_thickness 0 -my_order_3d 0 \
#   -my_shell_thickness 3 -my_poisson_ratio 0.3 -my_young_modulus 3 \
#   2>&1 | tee tmp_log
#   echo LOG: `grep 'Problem SHELL_PROBLEM Nb' tmp_log | awk '{print $5}' | uniq` $ODISP 0 0 `grep "DISPLACEMENT \[ 1 \]" tmp_log | awk '{print $5}'`
# done
#
# echo LOG: Mesh 2 - Convergence in plane - thinckes 2 - plane locking
#
# for ODISP in `seq 1 6`;
# do
#   $MPIRUN $BIN_PATH/solid_shell \
#   -my_file $SRC_PATH/meshes/pinched_cylinder_2.h5m  \
#   -ksp_type fgmres -pc_type lu -pc_factor_mat_solver_package mumps -ksp_monitor -ksp_atol 1e-12 -ksp_rtol 1e-12  \
#   -my_order $ODISP -my_order_thickness 2 -my_order_3d 0 \
#   -my_shell_thickness 3 -my_poisson_ratio 0.3 -my_young_modulus 3 \
#   2>&1 | tee tmp_log
#   echo LOG: `grep 'Problem SHELL_PROBLEM Nb' tmp_log | awk '{print $5}' | uniq` $ODISP 2 0 `grep "DISPLACEMENT \[ 1 \]" tmp_log | awk '{print $5}'`
# done
#
# echo LOG: Mesh 2 - Convergence in plane - thinckes 3 - plane 2
#
# for ODISP in `seq 1 6`;
# do
#   $MPIRUN $BIN_PATH/solid_shell \
#   -my_file $SRC_PATH/meshes/pinched_cylinder_2.h5m  \
#   -ksp_type fgmres -pc_type lu -pc_factor_mat_solver_package mumps -ksp_monitor -ksp_atol 1e-12 -ksp_rtol 1e-12  \
#   -my_order $ODISP -my_order_thickness 3 -my_order_3d 2 \
#   -my_shell_thickness 3 -my_poisson_ratio 0.3 -my_young_modulus 3 \
#   2>&1 | tee tmp_log
#   echo LOG: `grep 'Problem SHELL_PROBLEM Nb' tmp_log | awk '{print $5}' | uniq` $ODISP 3 2 `grep "DISPLACEMENT \[ 1 \]" tmp_log | awk '{print $5}'`
# done

# rm -f tmp_log

#/opt/local/bin/valgrind --dsymutil=yes --track-origins=yes --trace-children=yes

# $MPIRUN $BIN_PATH/solid_shell \
# -my_file $SRC_PATH/meshes/pinched_cylinder_4.h5m  \
# -ksp_type fgmres -pc_type lu -pc_factor_mat_solver_package mumps -ksp_monitor -ksp_atol 1e-12 -ksp_rtol 1e-12  \
# -my_order 5 -my_order_thickness 2 -my_order_3d 0 \
# -my_shell_thickness 3 -my_poisson_ratio 0.3 -my_young_modulus 3e6 \
# 2>&1 | tee tmp_log

# $MPIRUN $BIN_PATH/solid_shell \
# -my_file $SRC_PATH/meshes/simple_plate.h5m  \
# -ksp_type fgmres -pc_type lu -pc_factor_mat_solver_package mumps -ksp_monitor -ksp_atol 1e-12 -ksp_rtol 1e-12  \
# -my_order 2 -my_order_thickness 0 -my_order_3d 0 \
# -my_shell_thickness 3 -my_poisson_ratio 0.3 -my_young_modulus 3e6 \
# 2>&1 | tee tmp_log

# nonlinear analysis

# Tension test
# $MPIRUN $BIN_PATH/solid_shell_nonlinear \
# -my_file $SRC_PATH/meshes/pinched_cylinder_4_long_tension.h5m  \
# -snes_monitor -snes_max_it 24 -snes_atol 1e-6 -snes_rtol 1e-5	-snes_stol 0 \
# -snes_linesearch_type l2 -snes_linesearch_monitor -snes_linesearch_minlambda 0.01 \
# -snes_converged_reason \
# -ksp_type fgmres -pc_type lu -pc_factor_mat_solver_package mumps -ksp_monitor -ksp_atol 1e-12 -ksp_rtol 1e-12  \
# -ksp_converged_reason \
# -my_step_size 1 -my_arc_beta 0 \
# -my_numerical_damping 0 \
# -my_order 6 -my_order_thickness 2 -my_order_3d 0 \
# -my_shell_thickness 0.094 -my_poisson_ratio 0.3125 -my_young_modulus 10.5 \
# -ts_monitor -ts_dt 0.001 -ts_final_time 100000 \
# -ts_max_snes_failures -1 \
# 2>&1 | tee tmp_log

# Compresion test
# $MPIRUN $BIN_PATH/solid_shell_nonlinear \
# -my_file $SRC_PATH/meshes/pinched_cylinder_6_long_compression.h5m  \
# -snes_monitor -snes_max_it 24 -snes_atol 1e-6 -snes_rtol 1e-5	-snes_stol 0 \
# -snes_linesearch_type l2 -snes_linesearch_monitor -snes_linesearch_minlambda 0.01 \
# -snes_converged_reason \
# -ksp_type fgmres -pc_type lu -pc_factor_mat_solver_package mumps -ksp_monitor -ksp_atol 1e-12 -ksp_rtol 1e-12  \
# -ksp_converged_reason \
# -my_step_size 1 -my_arc_beta 0 \
# -my_numerical_damping 0 \
# -my_order 5 -my_order_thickness 2 -my_order_3d 0 \
# -my_shell_thickness 1 -my_poisson_ratio 0.3 -my_young_modulus 30 \
# -ts_monitor -ts_dt 0.001 -ts_final_time 100000 \
# -ts_max_snes_failures -1 \
# 2>&1 | tee tmp_log
